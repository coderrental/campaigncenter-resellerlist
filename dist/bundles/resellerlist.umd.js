(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('rxjs'), require('@angular/core'), require('@angular/common'), require('@angular/animations'), require('@angular/platform-browser'), require('@angular/material'), require('@angular/forms'), require('@angular/http')) :
    typeof define === 'function' && define.amd ? define(['exports', 'rxjs', '@angular/core', '@angular/common', '@angular/animations', '@angular/platform-browser', '@angular/material', '@angular/forms', '@angular/http'], factory) :
    (factory((global.resellerlist = {}),global.ng.rxjs,global.ng.core,global.ng.common,global.ng.animations,global.ng.platformBrowser,global.ng.material,global.ng.forms,global.http));
}(this, (function (exports,rxjs,core,common,animations,platformBrowser,material,forms,http) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * Coerces a data-bound value (typically a string) to a boolean.
     * @param {?} value
     * @return {?}
     */
    function coerceBooleanProperty(value) {
        return value != null && "" + value !== 'false';
    }

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * @abstract
     * @template T
     */
    var  /**
     * @abstract
     * @template T
     */
    DataSource = /** @class */ (function () {
        function DataSource() {
        }
        return DataSource;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * DataSource wrapper for a native array.
     * @template T
     */
    var  /**
     * DataSource wrapper for a native array.
     * @template T
     */
    ArrayDataSource = /** @class */ (function (_super) {
        __extends(ArrayDataSource, _super);
        function ArrayDataSource(_data) {
            var _this = _super.call(this) || this;
            _this._data = _data;
            return _this;
        }
        /**
         * @return {?}
         */
        ArrayDataSource.prototype.connect = /**
         * @return {?}
         */
        function () {
            return this._data instanceof rxjs.Observable ? this._data : rxjs.of(this._data);
        };
        /**
         * @return {?}
         */
        ArrayDataSource.prototype.disconnect = /**
         * @return {?}
         */
        function () { };
        return ArrayDataSource;
    }(DataSource));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Class to be used to power selecting one or more options from a list.
     * @template T
     */
    var  /**
     * Class to be used to power selecting one or more options from a list.
     * @template T
     */
    SelectionModel = /** @class */ (function () {
        function SelectionModel(_multiple, initiallySelectedValues, _emitChanges) {
            if (_multiple === void 0) { _multiple = false; }
            if (_emitChanges === void 0) { _emitChanges = true; }
            var _this = this;
            this._multiple = _multiple;
            this._emitChanges = _emitChanges;
            /**
             * Currently-selected values.
             */
            this._selection = new Set();
            /**
             * Keeps track of the deselected options that haven't been emitted by the change event.
             */
            this._deselectedToEmit = [];
            /**
             * Keeps track of the selected options that haven't been emitted by the change event.
             */
            this._selectedToEmit = [];
            /**
             * Event emitted when the value has changed.
             */
            this.onChange = this._emitChanges ? new rxjs.Subject() : null;
            if (initiallySelectedValues && initiallySelectedValues.length) {
                if (_multiple) {
                    initiallySelectedValues.forEach(function (value) { return _this._markSelected(value); });
                }
                else {
                    this._markSelected(initiallySelectedValues[0]);
                }
                // Clear the array in order to avoid firing the change event for preselected values.
                this._selectedToEmit.length = 0;
            }
        }
        Object.defineProperty(SelectionModel.prototype, "selected", {
            /** Selected values. */
            get: /**
             * Selected values.
             * @return {?}
             */
            function () {
                if (!this._selected) {
                    this._selected = Array.from(this._selection.values());
                }
                return this._selected;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Selects a value or an array of values.
         */
        /**
         * Selects a value or an array of values.
         * @param {...?} values
         * @return {?}
         */
        SelectionModel.prototype.select = /**
         * Selects a value or an array of values.
         * @param {...?} values
         * @return {?}
         */
        function () {
            var _this = this;
            var values = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                values[_i] = arguments[_i];
            }
            this._verifyValueAssignment(values);
            values.forEach(function (value) { return _this._markSelected(value); });
            this._emitChangeEvent();
        };
        /**
         * Deselects a value or an array of values.
         */
        /**
         * Deselects a value or an array of values.
         * @param {...?} values
         * @return {?}
         */
        SelectionModel.prototype.deselect = /**
         * Deselects a value or an array of values.
         * @param {...?} values
         * @return {?}
         */
        function () {
            var _this = this;
            var values = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                values[_i] = arguments[_i];
            }
            this._verifyValueAssignment(values);
            values.forEach(function (value) { return _this._unmarkSelected(value); });
            this._emitChangeEvent();
        };
        /**
         * Toggles a value between selected and deselected.
         */
        /**
         * Toggles a value between selected and deselected.
         * @param {?} value
         * @return {?}
         */
        SelectionModel.prototype.toggle = /**
         * Toggles a value between selected and deselected.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.isSelected(value) ? this.deselect(value) : this.select(value);
        };
        /**
         * Clears all of the selected values.
         */
        /**
         * Clears all of the selected values.
         * @return {?}
         */
        SelectionModel.prototype.clear = /**
         * Clears all of the selected values.
         * @return {?}
         */
        function () {
            this._unmarkAll();
            this._emitChangeEvent();
        };
        /**
         * Determines whether a value is selected.
         */
        /**
         * Determines whether a value is selected.
         * @param {?} value
         * @return {?}
         */
        SelectionModel.prototype.isSelected = /**
         * Determines whether a value is selected.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            return this._selection.has(value);
        };
        /**
         * Determines whether the model does not have a value.
         */
        /**
         * Determines whether the model does not have a value.
         * @return {?}
         */
        SelectionModel.prototype.isEmpty = /**
         * Determines whether the model does not have a value.
         * @return {?}
         */
        function () {
            return this._selection.size === 0;
        };
        /**
         * Determines whether the model has a value.
         */
        /**
         * Determines whether the model has a value.
         * @return {?}
         */
        SelectionModel.prototype.hasValue = /**
         * Determines whether the model has a value.
         * @return {?}
         */
        function () {
            return !this.isEmpty();
        };
        /**
         * Sorts the selected values based on a predicate function.
         */
        /**
         * Sorts the selected values based on a predicate function.
         * @param {?=} predicate
         * @return {?}
         */
        SelectionModel.prototype.sort = /**
         * Sorts the selected values based on a predicate function.
         * @param {?=} predicate
         * @return {?}
         */
        function (predicate) {
            if (this._multiple && this.selected) {
                /** @type {?} */ ((this._selected)).sort(predicate);
            }
        };
        /**
         * Gets whether multiple values can be selected.
         */
        /**
         * Gets whether multiple values can be selected.
         * @return {?}
         */
        SelectionModel.prototype.isMultipleSelection = /**
         * Gets whether multiple values can be selected.
         * @return {?}
         */
        function () {
            return this._multiple;
        };
        /**
         * Emits a change event and clears the records of selected and deselected values.
         * @return {?}
         */
        SelectionModel.prototype._emitChangeEvent = /**
         * Emits a change event and clears the records of selected and deselected values.
         * @return {?}
         */
        function () {
            // Clear the selected values so they can be re-cached.
            this._selected = null;
            if (this._selectedToEmit.length || this._deselectedToEmit.length) {
                if (this.onChange) {
                    this.onChange.next({
                        source: this,
                        added: this._selectedToEmit,
                        removed: this._deselectedToEmit
                    });
                }
                this._deselectedToEmit = [];
                this._selectedToEmit = [];
            }
        };
        /**
         * Selects a value.
         * @param {?} value
         * @return {?}
         */
        SelectionModel.prototype._markSelected = /**
         * Selects a value.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (!this.isSelected(value)) {
                if (!this._multiple) {
                    this._unmarkAll();
                }
                this._selection.add(value);
                if (this._emitChanges) {
                    this._selectedToEmit.push(value);
                }
            }
        };
        /**
         * Deselects a value.
         * @param {?} value
         * @return {?}
         */
        SelectionModel.prototype._unmarkSelected = /**
         * Deselects a value.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (this.isSelected(value)) {
                this._selection.delete(value);
                if (this._emitChanges) {
                    this._deselectedToEmit.push(value);
                }
            }
        };
        /**
         * Clears out the selected values.
         * @return {?}
         */
        SelectionModel.prototype._unmarkAll = /**
         * Clears out the selected values.
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.isEmpty()) {
                this._selection.forEach(function (value) { return _this._unmarkSelected(value); });
            }
        };
        /**
         * Verifies the value assignment and throws an error if the specified value array is
         * including multiple values while the selection model is not supporting multiple values.
         * @param {?} values
         * @return {?}
         */
        SelectionModel.prototype._verifyValueAssignment = /**
         * Verifies the value assignment and throws an error if the specified value array is
         * including multiple values while the selection model is not supporting multiple values.
         * @param {?} values
         * @return {?}
         */
        function (values) {
            if (values.length > 1 && !this._multiple) {
                throw getMultipleValuesInSingleSelectionError();
            }
        };
        return SelectionModel;
    }());
    /**
     * Returns an error that reports that multiple values are passed into a selection model
     * with a single value.
     * @return {?}
     */
    function getMultipleValuesInSingleSelectionError() {
        return Error('Cannot pass multiple values into SelectionModel with single-value mode.');
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Class to coordinate unique selection based on name.
     * Intended to be consumed as an Angular service.
     * This service is needed because native radio change events are only fired on the item currently
     * being selected, and we still need to uncheck the previous selection.
     *
     * This service does not *store* any IDs and names because they may change at any time, so it is
     * less error-prone if they are simply passed through when the events occur.
     */
    var UniqueSelectionDispatcher = /** @class */ (function () {
        function UniqueSelectionDispatcher() {
            this._listeners = [];
        }
        /**
         * Notify other items that selection for the given name has been set.
         * @param id ID of the item.
         * @param name Name of the item.
         */
        /**
         * Notify other items that selection for the given name has been set.
         * @param {?} id ID of the item.
         * @param {?} name Name of the item.
         * @return {?}
         */
        UniqueSelectionDispatcher.prototype.notify = /**
         * Notify other items that selection for the given name has been set.
         * @param {?} id ID of the item.
         * @param {?} name Name of the item.
         * @return {?}
         */
        function (id, name) {
            for (var _i = 0, _a = this._listeners; _i < _a.length; _i++) {
                var listener = _a[_i];
                listener(id, name);
            }
        };
        /**
         * Listen for future changes to item selection.
         * @return Function used to deregister listener
         */
        /**
         * Listen for future changes to item selection.
         * @param {?} listener
         * @return {?} Function used to deregister listener
         */
        UniqueSelectionDispatcher.prototype.listen = /**
         * Listen for future changes to item selection.
         * @param {?} listener
         * @return {?} Function used to deregister listener
         */
        function (listener) {
            var _this = this;
            this._listeners.push(listener);
            return function () {
                _this._listeners = _this._listeners.filter(function (registered) {
                    return listener !== registered;
                });
            };
        };
        /**
         * @return {?}
         */
        UniqueSelectionDispatcher.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this._listeners = [];
        };
        UniqueSelectionDispatcher.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] },
        ];
        /** @nocollapse */ UniqueSelectionDispatcher.ngInjectableDef = core.defineInjectable({ factory: function UniqueSelectionDispatcher_Factory() { return new UniqueSelectionDispatcher(); }, token: UniqueSelectionDispatcher, providedIn: "root" });
        return UniqueSelectionDispatcher;
    }());

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var errorObject = { e: {} };

    /** PURE_IMPORTS_START _errorObject PURE_IMPORTS_END */
    var tryCatchTarget;
    function tryCatcher() {
        try {
            return tryCatchTarget.apply(this, arguments);
        }
        catch (e) {
            errorObject.e = e;
            return errorObject;
        }
    }
    function tryCatch(fn) {
        tryCatchTarget = fn;
        return tryCatcher;
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isFunction(x) {
        return typeof x === 'function';
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var _enable_super_gross_mode_that_will_cause_bad_things = false;
    var config = {
        Promise: undefined,
        set useDeprecatedSynchronousErrorHandling(value) {
            if (value) {
                var error = /*@__PURE__*/ new Error();
                /*@__PURE__*/ console.warn('DEPRECATED! RxJS was set to use deprecated synchronous error handling behavior by code at: \n' + error.stack);
            }
            else if (_enable_super_gross_mode_that_will_cause_bad_things) {
                /*@__PURE__*/ console.log('RxJS: Back to a better error behavior. Thank you. <3');
            }
            _enable_super_gross_mode_that_will_cause_bad_things = value;
        },
        get useDeprecatedSynchronousErrorHandling() {
            return _enable_super_gross_mode_that_will_cause_bad_things;
        },
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function hostReportError(err) {
        setTimeout(function () { throw err; });
    }

    /** PURE_IMPORTS_START _config,_util_hostReportError PURE_IMPORTS_END */
    var empty = {
        closed: true,
        next: function (value) { },
        error: function (err) {
            if (config.useDeprecatedSynchronousErrorHandling) {
                throw err;
            }
            else {
                hostReportError(err);
            }
        },
        complete: function () { }
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var isArray = Array.isArray || (function (x) { return x && typeof x.length === 'number'; });

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isObject(x) {
        return x != null && typeof x === 'object';
    }

    /** PURE_IMPORTS_START tslib PURE_IMPORTS_END */
    var UnsubscriptionError = /*@__PURE__*/ (function (_super) {
        __extends(UnsubscriptionError, _super);
        function UnsubscriptionError(errors) {
            var _this = _super.call(this, errors ?
                errors.length + " errors occurred during unsubscription:\n  " + errors.map(function (err, i) { return i + 1 + ") " + err.toString(); }).join('\n  ') : '') || this;
            _this.errors = errors;
            _this.name = 'UnsubscriptionError';
            Object.setPrototypeOf(_this, UnsubscriptionError.prototype);
            return _this;
        }
        return UnsubscriptionError;
    }(Error));

    /** PURE_IMPORTS_START _util_isArray,_util_isObject,_util_isFunction,_util_tryCatch,_util_errorObject,_util_UnsubscriptionError PURE_IMPORTS_END */
    var Subscription = /*@__PURE__*/ (function () {
        function Subscription(unsubscribe) {
            this.closed = false;
            this._parent = null;
            this._parents = null;
            this._subscriptions = null;
            if (unsubscribe) {
                this._unsubscribe = unsubscribe;
            }
        }
        Subscription.prototype.unsubscribe = function () {
            var hasErrors = false;
            var errors;
            if (this.closed) {
                return;
            }
            var _a = this, _parent = _a._parent, _parents = _a._parents, _unsubscribe = _a._unsubscribe, _subscriptions = _a._subscriptions;
            this.closed = true;
            this._parent = null;
            this._parents = null;
            this._subscriptions = null;
            var index = -1;
            var len = _parents ? _parents.length : 0;
            while (_parent) {
                _parent.remove(this);
                _parent = ++index < len && _parents[index] || null;
            }
            if (isFunction(_unsubscribe)) {
                var trial = tryCatch(_unsubscribe).call(this);
                if (trial === errorObject) {
                    hasErrors = true;
                    errors = errors || (errorObject.e instanceof UnsubscriptionError ?
                        flattenUnsubscriptionErrors(errorObject.e.errors) : [errorObject.e]);
                }
            }
            if (isArray(_subscriptions)) {
                index = -1;
                len = _subscriptions.length;
                while (++index < len) {
                    var sub = _subscriptions[index];
                    if (isObject(sub)) {
                        var trial = tryCatch(sub.unsubscribe).call(sub);
                        if (trial === errorObject) {
                            hasErrors = true;
                            errors = errors || [];
                            var err = errorObject.e;
                            if (err instanceof UnsubscriptionError) {
                                errors = errors.concat(flattenUnsubscriptionErrors(err.errors));
                            }
                            else {
                                errors.push(err);
                            }
                        }
                    }
                }
            }
            if (hasErrors) {
                throw new UnsubscriptionError(errors);
            }
        };
        Subscription.prototype.add = function (teardown) {
            if (!teardown || (teardown === Subscription.EMPTY)) {
                return Subscription.EMPTY;
            }
            if (teardown === this) {
                return this;
            }
            var subscription = teardown;
            switch (typeof teardown) {
                case 'function':
                    subscription = new Subscription(teardown);
                case 'object':
                    if (subscription.closed || typeof subscription.unsubscribe !== 'function') {
                        return subscription;
                    }
                    else if (this.closed) {
                        subscription.unsubscribe();
                        return subscription;
                    }
                    else if (typeof subscription._addParent !== 'function') {
                        var tmp = subscription;
                        subscription = new Subscription();
                        subscription._subscriptions = [tmp];
                    }
                    break;
                default:
                    throw new Error('unrecognized teardown ' + teardown + ' added to Subscription.');
            }
            var subscriptions = this._subscriptions || (this._subscriptions = []);
            subscriptions.push(subscription);
            subscription._addParent(this);
            return subscription;
        };
        Subscription.prototype.remove = function (subscription) {
            var subscriptions = this._subscriptions;
            if (subscriptions) {
                var subscriptionIndex = subscriptions.indexOf(subscription);
                if (subscriptionIndex !== -1) {
                    subscriptions.splice(subscriptionIndex, 1);
                }
            }
        };
        Subscription.prototype._addParent = function (parent) {
            var _a = this, _parent = _a._parent, _parents = _a._parents;
            if (!_parent || _parent === parent) {
                this._parent = parent;
            }
            else if (!_parents) {
                this._parents = [parent];
            }
            else if (_parents.indexOf(parent) === -1) {
                _parents.push(parent);
            }
        };
        Subscription.EMPTY = (function (empty) {
            empty.closed = true;
            return empty;
        }(new Subscription()));
        return Subscription;
    }());
    function flattenUnsubscriptionErrors(errors) {
        return errors.reduce(function (errs, err) { return errs.concat((err instanceof UnsubscriptionError) ? err.errors : err); }, []);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var rxSubscriber = (typeof Symbol === 'function' && typeof Symbol.for === 'function')
        ? /*@__PURE__*/ Symbol.for('rxSubscriber')
        : '@@rxSubscriber';

    /** PURE_IMPORTS_START tslib,_util_isFunction,_Observer,_Subscription,_internal_symbol_rxSubscriber,_config,_util_hostReportError PURE_IMPORTS_END */
    var Subscriber = /*@__PURE__*/ (function (_super) {
        __extends(Subscriber, _super);
        function Subscriber(destinationOrNext, error, complete) {
            var _this = _super.call(this) || this;
            _this.syncErrorValue = null;
            _this.syncErrorThrown = false;
            _this.syncErrorThrowable = false;
            _this.isStopped = false;
            switch (arguments.length) {
                case 0:
                    _this.destination = empty;
                    break;
                case 1:
                    if (!destinationOrNext) {
                        _this.destination = empty;
                        break;
                    }
                    if (typeof destinationOrNext === 'object') {
                        if (isTrustedSubscriber(destinationOrNext)) {
                            var trustedSubscriber = destinationOrNext[rxSubscriber]();
                            _this.syncErrorThrowable = trustedSubscriber.syncErrorThrowable;
                            _this.destination = trustedSubscriber;
                            trustedSubscriber.add(_this);
                        }
                        else {
                            _this.syncErrorThrowable = true;
                            _this.destination = new SafeSubscriber(_this, destinationOrNext);
                        }
                        break;
                    }
                default:
                    _this.syncErrorThrowable = true;
                    _this.destination = new SafeSubscriber(_this, destinationOrNext, error, complete);
                    break;
            }
            return _this;
        }
        Subscriber.prototype[rxSubscriber] = function () { return this; };
        Subscriber.create = function (next, error, complete) {
            var subscriber = new Subscriber(next, error, complete);
            subscriber.syncErrorThrowable = false;
            return subscriber;
        };
        Subscriber.prototype.next = function (value) {
            if (!this.isStopped) {
                this._next(value);
            }
        };
        Subscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                this.isStopped = true;
                this._error(err);
            }
        };
        Subscriber.prototype.complete = function () {
            if (!this.isStopped) {
                this.isStopped = true;
                this._complete();
            }
        };
        Subscriber.prototype.unsubscribe = function () {
            if (this.closed) {
                return;
            }
            this.isStopped = true;
            _super.prototype.unsubscribe.call(this);
        };
        Subscriber.prototype._next = function (value) {
            this.destination.next(value);
        };
        Subscriber.prototype._error = function (err) {
            this.destination.error(err);
            this.unsubscribe();
        };
        Subscriber.prototype._complete = function () {
            this.destination.complete();
            this.unsubscribe();
        };
        Subscriber.prototype._unsubscribeAndRecycle = function () {
            var _a = this, _parent = _a._parent, _parents = _a._parents;
            this._parent = null;
            this._parents = null;
            this.unsubscribe();
            this.closed = false;
            this.isStopped = false;
            this._parent = _parent;
            this._parents = _parents;
            return this;
        };
        return Subscriber;
    }(Subscription));
    var SafeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SafeSubscriber, _super);
        function SafeSubscriber(_parentSubscriber, observerOrNext, error, complete) {
            var _this = _super.call(this) || this;
            _this._parentSubscriber = _parentSubscriber;
            var next;
            var context = _this;
            if (isFunction(observerOrNext)) {
                next = observerOrNext;
            }
            else if (observerOrNext) {
                next = observerOrNext.next;
                error = observerOrNext.error;
                complete = observerOrNext.complete;
                if (observerOrNext !== empty) {
                    context = Object.create(observerOrNext);
                    if (isFunction(context.unsubscribe)) {
                        _this.add(context.unsubscribe.bind(context));
                    }
                    context.unsubscribe = _this.unsubscribe.bind(_this);
                }
            }
            _this._context = context;
            _this._next = next;
            _this._error = error;
            _this._complete = complete;
            return _this;
        }
        SafeSubscriber.prototype.next = function (value) {
            if (!this.isStopped && this._next) {
                var _parentSubscriber = this._parentSubscriber;
                if (!config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                    this.__tryOrUnsub(this._next, value);
                }
                else if (this.__tryOrSetError(_parentSubscriber, this._next, value)) {
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var _parentSubscriber = this._parentSubscriber;
                var useDeprecatedSynchronousErrorHandling = config.useDeprecatedSynchronousErrorHandling;
                if (this._error) {
                    if (!useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                        this.__tryOrUnsub(this._error, err);
                        this.unsubscribe();
                    }
                    else {
                        this.__tryOrSetError(_parentSubscriber, this._error, err);
                        this.unsubscribe();
                    }
                }
                else if (!_parentSubscriber.syncErrorThrowable) {
                    this.unsubscribe();
                    if (useDeprecatedSynchronousErrorHandling) {
                        throw err;
                    }
                    hostReportError(err);
                }
                else {
                    if (useDeprecatedSynchronousErrorHandling) {
                        _parentSubscriber.syncErrorValue = err;
                        _parentSubscriber.syncErrorThrown = true;
                    }
                    else {
                        hostReportError(err);
                    }
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.complete = function () {
            var _this = this;
            if (!this.isStopped) {
                var _parentSubscriber = this._parentSubscriber;
                if (this._complete) {
                    var wrappedComplete = function () { return _this._complete.call(_this._context); };
                    if (!config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                        this.__tryOrUnsub(wrappedComplete);
                        this.unsubscribe();
                    }
                    else {
                        this.__tryOrSetError(_parentSubscriber, wrappedComplete);
                        this.unsubscribe();
                    }
                }
                else {
                    this.unsubscribe();
                }
            }
        };
        SafeSubscriber.prototype.__tryOrUnsub = function (fn, value) {
            try {
                fn.call(this._context, value);
            }
            catch (err) {
                this.unsubscribe();
                if (config.useDeprecatedSynchronousErrorHandling) {
                    throw err;
                }
                else {
                    hostReportError(err);
                }
            }
        };
        SafeSubscriber.prototype.__tryOrSetError = function (parent, fn, value) {
            if (!config.useDeprecatedSynchronousErrorHandling) {
                throw new Error('bad call');
            }
            try {
                fn.call(this._context, value);
            }
            catch (err) {
                if (config.useDeprecatedSynchronousErrorHandling) {
                    parent.syncErrorValue = err;
                    parent.syncErrorThrown = true;
                    return true;
                }
                else {
                    hostReportError(err);
                    return true;
                }
            }
            return false;
        };
        SafeSubscriber.prototype._unsubscribe = function () {
            var _parentSubscriber = this._parentSubscriber;
            this._context = null;
            this._parentSubscriber = null;
            _parentSubscriber.unsubscribe();
        };
        return SafeSubscriber;
    }(Subscriber));
    function isTrustedSubscriber(obj) {
        return obj instanceof Subscriber || ('syncErrorThrowable' in obj && obj[rxSubscriber]);
    }

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var OuterSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(OuterSubscriber, _super);
        function OuterSubscriber() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        OuterSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        OuterSubscriber.prototype.notifyError = function (error, innerSub) {
            this.destination.error(error);
        };
        OuterSubscriber.prototype.notifyComplete = function (innerSub) {
            this.destination.complete();
        };
        return OuterSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var InnerSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(InnerSubscriber, _super);
        function InnerSubscriber(parent, outerValue, outerIndex) {
            var _this = _super.call(this) || this;
            _this.parent = parent;
            _this.outerValue = outerValue;
            _this.outerIndex = outerIndex;
            _this.index = 0;
            return _this;
        }
        InnerSubscriber.prototype._next = function (value) {
            this.parent.notifyNext(this.outerValue, value, this.outerIndex, this.index++, this);
        };
        InnerSubscriber.prototype._error = function (error) {
            this.parent.notifyError(error, this);
            this.unsubscribe();
        };
        InnerSubscriber.prototype._complete = function () {
            this.parent.notifyComplete(this);
            this.unsubscribe();
        };
        return InnerSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _Subscriber,_symbol_rxSubscriber,_Observer PURE_IMPORTS_END */
    function toSubscriber(nextOrObserver, error, complete) {
        if (nextOrObserver) {
            if (nextOrObserver instanceof Subscriber) {
                return nextOrObserver;
            }
            if (nextOrObserver[rxSubscriber]) {
                return nextOrObserver[rxSubscriber]();
            }
        }
        if (!nextOrObserver && !error && !complete) {
            return new Subscriber(empty);
        }
        return new Subscriber(nextOrObserver, error, complete);
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var observable = typeof Symbol === 'function' && Symbol.observable || '@@observable';

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function noop() { }

    /** PURE_IMPORTS_START _noop PURE_IMPORTS_END */
    function pipeFromArray(fns) {
        if (!fns) {
            return noop;
        }
        if (fns.length === 1) {
            return fns[0];
        }
        return function piped(input) {
            return fns.reduce(function (prev, fn) { return fn(prev); }, input);
        };
    }

    /** PURE_IMPORTS_START _util_toSubscriber,_internal_symbol_observable,_util_pipe,_config PURE_IMPORTS_END */
    var Observable = /*@__PURE__*/ (function () {
        function Observable(subscribe) {
            this._isScalar = false;
            if (subscribe) {
                this._subscribe = subscribe;
            }
        }
        Observable.prototype.lift = function (operator) {
            var observable$$1 = new Observable();
            observable$$1.source = this;
            observable$$1.operator = operator;
            return observable$$1;
        };
        Observable.prototype.subscribe = function (observerOrNext, error, complete) {
            var operator = this.operator;
            var sink = toSubscriber(observerOrNext, error, complete);
            if (operator) {
                operator.call(sink, this.source);
            }
            else {
                sink.add(this.source || (config.useDeprecatedSynchronousErrorHandling && !sink.syncErrorThrowable) ?
                    this._subscribe(sink) :
                    this._trySubscribe(sink));
            }
            if (config.useDeprecatedSynchronousErrorHandling) {
                if (sink.syncErrorThrowable) {
                    sink.syncErrorThrowable = false;
                    if (sink.syncErrorThrown) {
                        throw sink.syncErrorValue;
                    }
                }
            }
            return sink;
        };
        Observable.prototype._trySubscribe = function (sink) {
            try {
                return this._subscribe(sink);
            }
            catch (err) {
                if (config.useDeprecatedSynchronousErrorHandling) {
                    sink.syncErrorThrown = true;
                    sink.syncErrorValue = err;
                }
                sink.error(err);
            }
        };
        Observable.prototype.forEach = function (next, promiseCtor) {
            var _this = this;
            promiseCtor = getPromiseCtor(promiseCtor);
            return new promiseCtor(function (resolve, reject) {
                var subscription;
                subscription = _this.subscribe(function (value) {
                    try {
                        next(value);
                    }
                    catch (err) {
                        reject(err);
                        if (subscription) {
                            subscription.unsubscribe();
                        }
                    }
                }, reject, resolve);
            });
        };
        Observable.prototype._subscribe = function (subscriber) {
            var source = this.source;
            return source && source.subscribe(subscriber);
        };
        Observable.prototype[observable] = function () {
            return this;
        };
        Observable.prototype.pipe = function () {
            var operations = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                operations[_i] = arguments[_i];
            }
            if (operations.length === 0) {
                return this;
            }
            return pipeFromArray(operations)(this);
        };
        Observable.prototype.toPromise = function (promiseCtor) {
            var _this = this;
            promiseCtor = getPromiseCtor(promiseCtor);
            return new promiseCtor(function (resolve, reject) {
                var value;
                _this.subscribe(function (x) { return value = x; }, function (err) { return reject(err); }, function () { return resolve(value); });
            });
        };
        Observable.create = function (subscribe) {
            return new Observable(subscribe);
        };
        return Observable;
    }());
    function getPromiseCtor(promiseCtor) {
        if (!promiseCtor) {
            promiseCtor = config.Promise || Promise;
        }
        if (!promiseCtor) {
            throw new Error('no Promise impl found');
        }
        return promiseCtor;
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var subscribeToArray = function (array) {
        return function (subscriber) {
            for (var i = 0, len = array.length; i < len && !subscriber.closed; i++) {
                subscriber.next(array[i]);
            }
            if (!subscriber.closed) {
                subscriber.complete();
            }
        };
    };

    /** PURE_IMPORTS_START _hostReportError PURE_IMPORTS_END */
    var subscribeToPromise = function (promise) {
        return function (subscriber) {
            promise.then(function (value) {
                if (!subscriber.closed) {
                    subscriber.next(value);
                    subscriber.complete();
                }
            }, function (err) { return subscriber.error(err); })
                .then(null, hostReportError);
            return subscriber;
        };
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function getSymbolIterator() {
        if (typeof Symbol !== 'function' || !Symbol.iterator) {
            return '@@iterator';
        }
        return Symbol.iterator;
    }
    var iterator = /*@__PURE__*/ getSymbolIterator();

    /** PURE_IMPORTS_START _symbol_iterator PURE_IMPORTS_END */
    var subscribeToIterable = function (iterable) {
        return function (subscriber) {
            var iterator$$1 = iterable[iterator]();
            do {
                var item = iterator$$1.next();
                if (item.done) {
                    subscriber.complete();
                    break;
                }
                subscriber.next(item.value);
                if (subscriber.closed) {
                    break;
                }
            } while (true);
            if (typeof iterator$$1.return === 'function') {
                subscriber.add(function () {
                    if (iterator$$1.return) {
                        iterator$$1.return();
                    }
                });
            }
            return subscriber;
        };
    };

    /** PURE_IMPORTS_START _symbol_observable PURE_IMPORTS_END */
    var subscribeToObservable = function (obj) {
        return function (subscriber) {
            var obs = obj[observable]();
            if (typeof obs.subscribe !== 'function') {
                throw new TypeError('Provided object does not correctly implement Symbol.observable');
            }
            else {
                return obs.subscribe(subscriber);
            }
        };
    };

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var isArrayLike = (function (x) { return x && typeof x.length === 'number' && typeof x !== 'function'; });

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isPromise(value) {
        return value && typeof value.subscribe !== 'function' && typeof value.then === 'function';
    }

    /** PURE_IMPORTS_START _Observable,_subscribeToArray,_subscribeToPromise,_subscribeToIterable,_subscribeToObservable,_isArrayLike,_isPromise,_isObject,_symbol_iterator,_symbol_observable PURE_IMPORTS_END */
    var subscribeTo = function (result) {
        if (result instanceof Observable) {
            return function (subscriber) {
                if (result._isScalar) {
                    subscriber.next(result.value);
                    subscriber.complete();
                    return undefined;
                }
                else {
                    return result.subscribe(subscriber);
                }
            };
        }
        else if (result && typeof result[observable] === 'function') {
            return subscribeToObservable(result);
        }
        else if (isArrayLike(result)) {
            return subscribeToArray(result);
        }
        else if (isPromise(result)) {
            return subscribeToPromise(result);
        }
        else if (result && typeof result[iterator] === 'function') {
            return subscribeToIterable(result);
        }
        else {
            var value = isObject(result) ? 'an invalid object' : "'" + result + "'";
            var msg = "You provided " + value + " where a stream was expected."
                + ' You can provide an Observable, Promise, Array, or Iterable.';
            throw new TypeError(msg);
        }
    };

    /** PURE_IMPORTS_START _InnerSubscriber,_subscribeTo PURE_IMPORTS_END */
    function subscribeToResult(outerSubscriber, result, outerValue, outerIndex) {
        var destination = new InnerSubscriber(outerSubscriber, outerValue, outerIndex);
        return subscribeTo(result)(destination);
    }

    /** PURE_IMPORTS_START tslib,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var AuditSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(AuditSubscriber, _super);
        function AuditSubscriber(destination, durationSelector) {
            var _this = _super.call(this, destination) || this;
            _this.durationSelector = durationSelector;
            _this.hasValue = false;
            return _this;
        }
        AuditSubscriber.prototype._next = function (value) {
            this.value = value;
            this.hasValue = true;
            if (!this.throttled) {
                var duration = tryCatch(this.durationSelector)(value);
                if (duration === errorObject) {
                    this.destination.error(errorObject.e);
                }
                else {
                    var innerSubscription = subscribeToResult(this, duration);
                    if (!innerSubscription || innerSubscription.closed) {
                        this.clearThrottle();
                    }
                    else {
                        this.add(this.throttled = innerSubscription);
                    }
                }
            }
        };
        AuditSubscriber.prototype.clearThrottle = function () {
            var _a = this, value = _a.value, hasValue = _a.hasValue, throttled = _a.throttled;
            if (throttled) {
                this.remove(throttled);
                this.throttled = null;
                throttled.unsubscribe();
            }
            if (hasValue) {
                this.value = null;
                this.hasValue = false;
                this.destination.next(value);
            }
        };
        AuditSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex) {
            this.clearThrottle();
        };
        AuditSubscriber.prototype.notifyComplete = function () {
            this.clearThrottle();
        };
        return AuditSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscription PURE_IMPORTS_END */
    var Action = /*@__PURE__*/ (function (_super) {
        __extends(Action, _super);
        function Action(scheduler, work) {
            return _super.call(this) || this;
        }
        Action.prototype.schedule = function (state, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            return this;
        };
        return Action;
    }(Subscription));

    /** PURE_IMPORTS_START tslib,_Action PURE_IMPORTS_END */
    var AsyncAction = /*@__PURE__*/ (function (_super) {
        __extends(AsyncAction, _super);
        function AsyncAction(scheduler, work) {
            var _this = _super.call(this, scheduler, work) || this;
            _this.scheduler = scheduler;
            _this.work = work;
            _this.pending = false;
            return _this;
        }
        AsyncAction.prototype.schedule = function (state, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if (this.closed) {
                return this;
            }
            this.state = state;
            var id = this.id;
            var scheduler = this.scheduler;
            if (id != null) {
                this.id = this.recycleAsyncId(scheduler, id, delay);
            }
            this.pending = true;
            this.delay = delay;
            this.id = this.id || this.requestAsyncId(scheduler, this.id, delay);
            return this;
        };
        AsyncAction.prototype.requestAsyncId = function (scheduler, id, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            return setInterval(scheduler.flush.bind(scheduler, this), delay);
        };
        AsyncAction.prototype.recycleAsyncId = function (scheduler, id, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if (delay !== null && this.delay === delay && this.pending === false) {
                return id;
            }
            return clearInterval(id) && undefined || undefined;
        };
        AsyncAction.prototype.execute = function (state, delay) {
            if (this.closed) {
                return new Error('executing a cancelled action');
            }
            this.pending = false;
            var error = this._execute(state, delay);
            if (error) {
                return error;
            }
            else if (this.pending === false && this.id != null) {
                this.id = this.recycleAsyncId(this.scheduler, this.id, null);
            }
        };
        AsyncAction.prototype._execute = function (state, delay) {
            var errored = false;
            var errorValue = undefined;
            try {
                this.work(state);
            }
            catch (e) {
                errored = true;
                errorValue = !!e && e || new Error(e);
            }
            if (errored) {
                this.unsubscribe();
                return errorValue;
            }
        };
        AsyncAction.prototype._unsubscribe = function () {
            var id = this.id;
            var scheduler = this.scheduler;
            var actions = scheduler.actions;
            var index = actions.indexOf(this);
            this.work = null;
            this.state = null;
            this.pending = false;
            this.scheduler = null;
            if (index !== -1) {
                actions.splice(index, 1);
            }
            if (id != null) {
                this.id = this.recycleAsyncId(scheduler, id, null);
            }
            this.delay = null;
        };
        return AsyncAction;
    }(Action));

    var Scheduler = /*@__PURE__*/ (function () {
        function Scheduler(SchedulerAction, now) {
            if (now === void 0) {
                now = Scheduler.now;
            }
            this.SchedulerAction = SchedulerAction;
            this.now = now;
        }
        Scheduler.prototype.schedule = function (work, delay, state) {
            if (delay === void 0) {
                delay = 0;
            }
            return new this.SchedulerAction(this, work).schedule(state, delay);
        };
        Scheduler.now = function () { return Date.now(); };
        return Scheduler;
    }());

    /** PURE_IMPORTS_START tslib,_Scheduler PURE_IMPORTS_END */
    var AsyncScheduler = /*@__PURE__*/ (function (_super) {
        __extends(AsyncScheduler, _super);
        function AsyncScheduler(SchedulerAction, now) {
            if (now === void 0) {
                now = Scheduler.now;
            }
            var _this = _super.call(this, SchedulerAction, function () {
                if (AsyncScheduler.delegate && AsyncScheduler.delegate !== _this) {
                    return AsyncScheduler.delegate.now();
                }
                else {
                    return now();
                }
            }) || this;
            _this.actions = [];
            _this.active = false;
            _this.scheduled = undefined;
            return _this;
        }
        AsyncScheduler.prototype.schedule = function (work, delay, state) {
            if (delay === void 0) {
                delay = 0;
            }
            if (AsyncScheduler.delegate && AsyncScheduler.delegate !== this) {
                return AsyncScheduler.delegate.schedule(work, delay, state);
            }
            else {
                return _super.prototype.schedule.call(this, work, delay, state);
            }
        };
        AsyncScheduler.prototype.flush = function (action) {
            var actions = this.actions;
            if (this.active) {
                actions.push(action);
                return;
            }
            var error;
            this.active = true;
            do {
                if (error = action.execute(action.state, action.delay)) {
                    break;
                }
            } while (action = actions.shift());
            this.active = false;
            if (error) {
                while (action = actions.shift()) {
                    action.unsubscribe();
                }
                throw error;
            }
        };
        return AsyncScheduler;
    }(Scheduler));

    /** PURE_IMPORTS_START _AsyncAction,_AsyncScheduler PURE_IMPORTS_END */
    var async = /*@__PURE__*/ new AsyncScheduler(AsyncAction);

    /** PURE_IMPORTS_START _isArray PURE_IMPORTS_END */
    function isNumeric(val) {
        return !isArray(val) && (val - parseFloat(val) + 1) >= 0;
    }

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    function isScheduler(value) {
        return value && typeof value.schedule === 'function';
    }

    /** PURE_IMPORTS_START _Observable,_scheduler_async,_util_isNumeric,_util_isScheduler PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _scheduler_async,_audit,_observable_timer PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var BufferSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferSubscriber, _super);
        function BufferSubscriber(destination, closingNotifier) {
            var _this = _super.call(this, destination) || this;
            _this.buffer = [];
            _this.add(subscribeToResult(_this, closingNotifier));
            return _this;
        }
        BufferSubscriber.prototype._next = function (value) {
            this.buffer.push(value);
        };
        BufferSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            var buffer = this.buffer;
            this.buffer = [];
            this.destination.next(buffer);
        };
        return BufferSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var BufferCountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferCountSubscriber, _super);
        function BufferCountSubscriber(destination, bufferSize) {
            var _this = _super.call(this, destination) || this;
            _this.bufferSize = bufferSize;
            _this.buffer = [];
            return _this;
        }
        BufferCountSubscriber.prototype._next = function (value) {
            var buffer = this.buffer;
            buffer.push(value);
            if (buffer.length == this.bufferSize) {
                this.destination.next(buffer);
                this.buffer = [];
            }
        };
        BufferCountSubscriber.prototype._complete = function () {
            var buffer = this.buffer;
            if (buffer.length > 0) {
                this.destination.next(buffer);
            }
            _super.prototype._complete.call(this);
        };
        return BufferCountSubscriber;
    }(Subscriber));
    var BufferSkipCountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferSkipCountSubscriber, _super);
        function BufferSkipCountSubscriber(destination, bufferSize, startBufferEvery) {
            var _this = _super.call(this, destination) || this;
            _this.bufferSize = bufferSize;
            _this.startBufferEvery = startBufferEvery;
            _this.buffers = [];
            _this.count = 0;
            return _this;
        }
        BufferSkipCountSubscriber.prototype._next = function (value) {
            var _a = this, bufferSize = _a.bufferSize, startBufferEvery = _a.startBufferEvery, buffers = _a.buffers, count = _a.count;
            this.count++;
            if (count % startBufferEvery === 0) {
                buffers.push([]);
            }
            for (var i = buffers.length; i--;) {
                var buffer = buffers[i];
                buffer.push(value);
                if (buffer.length === bufferSize) {
                    buffers.splice(i, 1);
                    this.destination.next(buffer);
                }
            }
        };
        BufferSkipCountSubscriber.prototype._complete = function () {
            var _a = this, buffers = _a.buffers, destination = _a.destination;
            while (buffers.length > 0) {
                var buffer = buffers.shift();
                if (buffer.length > 0) {
                    destination.next(buffer);
                }
            }
            _super.prototype._complete.call(this);
        };
        return BufferSkipCountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_scheduler_async,_Subscriber,_util_isScheduler PURE_IMPORTS_END */
    var Context = /*@__PURE__*/ (function () {
        function Context() {
            this.buffer = [];
        }
        return Context;
    }());
    var BufferTimeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferTimeSubscriber, _super);
        function BufferTimeSubscriber(destination, bufferTimeSpan, bufferCreationInterval, maxBufferSize, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.bufferTimeSpan = bufferTimeSpan;
            _this.bufferCreationInterval = bufferCreationInterval;
            _this.maxBufferSize = maxBufferSize;
            _this.scheduler = scheduler;
            _this.contexts = [];
            var context = _this.openContext();
            _this.timespanOnly = bufferCreationInterval == null || bufferCreationInterval < 0;
            if (_this.timespanOnly) {
                var timeSpanOnlyState = { subscriber: _this, context: context, bufferTimeSpan: bufferTimeSpan };
                _this.add(context.closeAction = scheduler.schedule(dispatchBufferTimeSpanOnly, bufferTimeSpan, timeSpanOnlyState));
            }
            else {
                var closeState = { subscriber: _this, context: context };
                var creationState = { bufferTimeSpan: bufferTimeSpan, bufferCreationInterval: bufferCreationInterval, subscriber: _this, scheduler: scheduler };
                _this.add(context.closeAction = scheduler.schedule(dispatchBufferClose, bufferTimeSpan, closeState));
                _this.add(scheduler.schedule(dispatchBufferCreation, bufferCreationInterval, creationState));
            }
            return _this;
        }
        BufferTimeSubscriber.prototype._next = function (value) {
            var contexts = this.contexts;
            var len = contexts.length;
            var filledBufferContext;
            for (var i = 0; i < len; i++) {
                var context_1 = contexts[i];
                var buffer = context_1.buffer;
                buffer.push(value);
                if (buffer.length == this.maxBufferSize) {
                    filledBufferContext = context_1;
                }
            }
            if (filledBufferContext) {
                this.onBufferFull(filledBufferContext);
            }
        };
        BufferTimeSubscriber.prototype._error = function (err) {
            this.contexts.length = 0;
            _super.prototype._error.call(this, err);
        };
        BufferTimeSubscriber.prototype._complete = function () {
            var _a = this, contexts = _a.contexts, destination = _a.destination;
            while (contexts.length > 0) {
                var context_2 = contexts.shift();
                destination.next(context_2.buffer);
            }
            _super.prototype._complete.call(this);
        };
        BufferTimeSubscriber.prototype._unsubscribe = function () {
            this.contexts = null;
        };
        BufferTimeSubscriber.prototype.onBufferFull = function (context) {
            this.closeContext(context);
            var closeAction = context.closeAction;
            closeAction.unsubscribe();
            this.remove(closeAction);
            if (!this.closed && this.timespanOnly) {
                context = this.openContext();
                var bufferTimeSpan = this.bufferTimeSpan;
                var timeSpanOnlyState = { subscriber: this, context: context, bufferTimeSpan: bufferTimeSpan };
                this.add(context.closeAction = this.scheduler.schedule(dispatchBufferTimeSpanOnly, bufferTimeSpan, timeSpanOnlyState));
            }
        };
        BufferTimeSubscriber.prototype.openContext = function () {
            var context = new Context();
            this.contexts.push(context);
            return context;
        };
        BufferTimeSubscriber.prototype.closeContext = function (context) {
            this.destination.next(context.buffer);
            var contexts = this.contexts;
            var spliceIndex = contexts ? contexts.indexOf(context) : -1;
            if (spliceIndex >= 0) {
                contexts.splice(contexts.indexOf(context), 1);
            }
        };
        return BufferTimeSubscriber;
    }(Subscriber));
    function dispatchBufferTimeSpanOnly(state) {
        var subscriber = state.subscriber;
        var prevContext = state.context;
        if (prevContext) {
            subscriber.closeContext(prevContext);
        }
        if (!subscriber.closed) {
            state.context = subscriber.openContext();
            state.context.closeAction = this.schedule(state, state.bufferTimeSpan);
        }
    }
    function dispatchBufferCreation(state) {
        var bufferCreationInterval = state.bufferCreationInterval, bufferTimeSpan = state.bufferTimeSpan, subscriber = state.subscriber, scheduler = state.scheduler;
        var context = subscriber.openContext();
        var action = this;
        if (!subscriber.closed) {
            subscriber.add(context.closeAction = scheduler.schedule(dispatchBufferClose, bufferTimeSpan, { subscriber: subscriber, context: context }));
            action.schedule(state, bufferCreationInterval);
        }
    }
    function dispatchBufferClose(arg) {
        var subscriber = arg.subscriber, context = arg.context;
        subscriber.closeContext(context);
    }

    /** PURE_IMPORTS_START tslib,_Subscription,_util_subscribeToResult,_OuterSubscriber PURE_IMPORTS_END */
    var BufferToggleSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferToggleSubscriber, _super);
        function BufferToggleSubscriber(destination, openings, closingSelector) {
            var _this = _super.call(this, destination) || this;
            _this.openings = openings;
            _this.closingSelector = closingSelector;
            _this.contexts = [];
            _this.add(subscribeToResult(_this, openings));
            return _this;
        }
        BufferToggleSubscriber.prototype._next = function (value) {
            var contexts = this.contexts;
            var len = contexts.length;
            for (var i = 0; i < len; i++) {
                contexts[i].buffer.push(value);
            }
        };
        BufferToggleSubscriber.prototype._error = function (err) {
            var contexts = this.contexts;
            while (contexts.length > 0) {
                var context_1 = contexts.shift();
                context_1.subscription.unsubscribe();
                context_1.buffer = null;
                context_1.subscription = null;
            }
            this.contexts = null;
            _super.prototype._error.call(this, err);
        };
        BufferToggleSubscriber.prototype._complete = function () {
            var contexts = this.contexts;
            while (contexts.length > 0) {
                var context_2 = contexts.shift();
                this.destination.next(context_2.buffer);
                context_2.subscription.unsubscribe();
                context_2.buffer = null;
                context_2.subscription = null;
            }
            this.contexts = null;
            _super.prototype._complete.call(this);
        };
        BufferToggleSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            outerValue ? this.closeBuffer(outerValue) : this.openBuffer(innerValue);
        };
        BufferToggleSubscriber.prototype.notifyComplete = function (innerSub) {
            this.closeBuffer(innerSub.context);
        };
        BufferToggleSubscriber.prototype.openBuffer = function (value) {
            try {
                var closingSelector = this.closingSelector;
                var closingNotifier = closingSelector.call(this, value);
                if (closingNotifier) {
                    this.trySubscribe(closingNotifier);
                }
            }
            catch (err) {
                this._error(err);
            }
        };
        BufferToggleSubscriber.prototype.closeBuffer = function (context) {
            var contexts = this.contexts;
            if (contexts && context) {
                var buffer = context.buffer, subscription = context.subscription;
                this.destination.next(buffer);
                contexts.splice(contexts.indexOf(context), 1);
                this.remove(subscription);
                subscription.unsubscribe();
            }
        };
        BufferToggleSubscriber.prototype.trySubscribe = function (closingNotifier) {
            var contexts = this.contexts;
            var buffer = [];
            var subscription = new Subscription();
            var context = { buffer: buffer, subscription: subscription };
            contexts.push(context);
            var innerSubscription = subscribeToResult(this, closingNotifier, context);
            if (!innerSubscription || innerSubscription.closed) {
                this.closeBuffer(context);
            }
            else {
                innerSubscription.context = context;
                this.add(innerSubscription);
                subscription.add(innerSubscription);
            }
        };
        return BufferToggleSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscription,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var BufferWhenSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(BufferWhenSubscriber, _super);
        function BufferWhenSubscriber(destination, closingSelector) {
            var _this = _super.call(this, destination) || this;
            _this.closingSelector = closingSelector;
            _this.subscribing = false;
            _this.openBuffer();
            return _this;
        }
        BufferWhenSubscriber.prototype._next = function (value) {
            this.buffer.push(value);
        };
        BufferWhenSubscriber.prototype._complete = function () {
            var buffer = this.buffer;
            if (buffer) {
                this.destination.next(buffer);
            }
            _super.prototype._complete.call(this);
        };
        BufferWhenSubscriber.prototype._unsubscribe = function () {
            this.buffer = null;
            this.subscribing = false;
        };
        BufferWhenSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.openBuffer();
        };
        BufferWhenSubscriber.prototype.notifyComplete = function () {
            if (this.subscribing) {
                this.complete();
            }
            else {
                this.openBuffer();
            }
        };
        BufferWhenSubscriber.prototype.openBuffer = function () {
            var closingSubscription = this.closingSubscription;
            if (closingSubscription) {
                this.remove(closingSubscription);
                closingSubscription.unsubscribe();
            }
            var buffer = this.buffer;
            if (this.buffer) {
                this.destination.next(buffer);
            }
            this.buffer = [];
            var closingNotifier = tryCatch(this.closingSelector)();
            if (closingNotifier === errorObject) {
                this.error(errorObject.e);
            }
            else {
                closingSubscription = new Subscription();
                this.closingSubscription = closingSubscription;
                this.add(closingSubscription);
                this.subscribing = true;
                closingSubscription.add(subscribeToResult(this, closingNotifier));
                this.subscribing = false;
            }
        };
        return BufferWhenSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var CatchSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(CatchSubscriber, _super);
        function CatchSubscriber(destination, selector, caught) {
            var _this = _super.call(this, destination) || this;
            _this.selector = selector;
            _this.caught = caught;
            return _this;
        }
        CatchSubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var result = void 0;
                try {
                    result = this.selector(err, this.caught);
                }
                catch (err2) {
                    _super.prototype.error.call(this, err2);
                    return;
                }
                this._unsubscribeAndRecycle();
                this.add(subscribeToResult(this, result));
            }
        };
        return CatchSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _Observable,_Subscription,_util_subscribeToArray PURE_IMPORTS_END */
    function fromArray(input, scheduler) {
        if (!scheduler) {
            return new Observable(subscribeToArray(input));
        }
        else {
            return new Observable(function (subscriber) {
                var sub = new Subscription();
                var i = 0;
                sub.add(scheduler.schedule(function () {
                    if (i === input.length) {
                        subscriber.complete();
                        return;
                    }
                    subscriber.next(input[i++]);
                    if (!subscriber.closed) {
                        sub.add(this.schedule());
                    }
                }));
                return sub;
            });
        }
    }

    /** PURE_IMPORTS_START tslib,_util_isScheduler,_util_isArray,_OuterSubscriber,_util_subscribeToResult,_fromArray PURE_IMPORTS_END */
    var NONE = {};
    var CombineLatestSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(CombineLatestSubscriber, _super);
        function CombineLatestSubscriber(destination, resultSelector) {
            var _this = _super.call(this, destination) || this;
            _this.resultSelector = resultSelector;
            _this.active = 0;
            _this.values = [];
            _this.observables = [];
            return _this;
        }
        CombineLatestSubscriber.prototype._next = function (observable) {
            this.values.push(NONE);
            this.observables.push(observable);
        };
        CombineLatestSubscriber.prototype._complete = function () {
            var observables = this.observables;
            var len = observables.length;
            if (len === 0) {
                this.destination.complete();
            }
            else {
                this.active = len;
                this.toRespond = len;
                for (var i = 0; i < len; i++) {
                    var observable = observables[i];
                    this.add(subscribeToResult(this, observable, observable, i));
                }
            }
        };
        CombineLatestSubscriber.prototype.notifyComplete = function (unused) {
            if ((this.active -= 1) === 0) {
                this.destination.complete();
            }
        };
        CombineLatestSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            var values = this.values;
            var oldVal = values[outerIndex];
            var toRespond = !this.toRespond
                ? 0
                : oldVal === NONE ? --this.toRespond : this.toRespond;
            values[outerIndex] = innerValue;
            if (toRespond === 0) {
                if (this.resultSelector) {
                    this._tryResultSelector(values);
                }
                else {
                    this.destination.next(values.slice());
                }
            }
        };
        CombineLatestSubscriber.prototype._tryResultSelector = function (values) {
            var result;
            try {
                result = this.resultSelector.apply(this, values);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(result);
        };
        return CombineLatestSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _observable_combineLatest PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _symbol_observable PURE_IMPORTS_END */
    function isInteropObservable(input) {
        return input && typeof input[observable] === 'function';
    }

    /** PURE_IMPORTS_START _symbol_iterator PURE_IMPORTS_END */
    function isIterable(input) {
        return input && typeof input[iterator] === 'function';
    }

    /** PURE_IMPORTS_START _Observable,_Subscription,_util_subscribeToPromise PURE_IMPORTS_END */
    function fromPromise(input, scheduler) {
        if (!scheduler) {
            return new Observable(subscribeToPromise(input));
        }
        else {
            return new Observable(function (subscriber) {
                var sub = new Subscription();
                sub.add(scheduler.schedule(function () {
                    return input.then(function (value) {
                        sub.add(scheduler.schedule(function () {
                            subscriber.next(value);
                            sub.add(scheduler.schedule(function () { return subscriber.complete(); }));
                        }));
                    }, function (err) {
                        sub.add(scheduler.schedule(function () { return subscriber.error(err); }));
                    });
                }));
                return sub;
            });
        }
    }

    /** PURE_IMPORTS_START _Observable,_Subscription,_symbol_iterator,_util_subscribeToIterable PURE_IMPORTS_END */
    function fromIterable(input, scheduler) {
        if (!input) {
            throw new Error('Iterable cannot be null');
        }
        if (!scheduler) {
            return new Observable(subscribeToIterable(input));
        }
        else {
            return new Observable(function (subscriber) {
                var sub = new Subscription();
                var iterator$$1;
                sub.add(function () {
                    if (iterator$$1 && typeof iterator$$1.return === 'function') {
                        iterator$$1.return();
                    }
                });
                sub.add(scheduler.schedule(function () {
                    iterator$$1 = input[iterator]();
                    sub.add(scheduler.schedule(function () {
                        if (subscriber.closed) {
                            return;
                        }
                        var value;
                        var done;
                        try {
                            var result = iterator$$1.next();
                            value = result.value;
                            done = result.done;
                        }
                        catch (err) {
                            subscriber.error(err);
                            return;
                        }
                        if (done) {
                            subscriber.complete();
                        }
                        else {
                            subscriber.next(value);
                            this.schedule();
                        }
                    }));
                }));
                return sub;
            });
        }
    }

    /** PURE_IMPORTS_START _Observable,_Subscription,_symbol_observable,_util_subscribeToObservable PURE_IMPORTS_END */
    function fromObservable(input, scheduler) {
        if (!scheduler) {
            return new Observable(subscribeToObservable(input));
        }
        else {
            return new Observable(function (subscriber) {
                var sub = new Subscription();
                sub.add(scheduler.schedule(function () {
                    var observable$$1 = input[observable]();
                    sub.add(observable$$1.subscribe({
                        next: function (value) { sub.add(scheduler.schedule(function () { return subscriber.next(value); })); },
                        error: function (err) { sub.add(scheduler.schedule(function () { return subscriber.error(err); })); },
                        complete: function () { sub.add(scheduler.schedule(function () { return subscriber.complete(); })); },
                    }));
                }));
                return sub;
            });
        }
    }

    /** PURE_IMPORTS_START _Observable,_util_isPromise,_util_isArrayLike,_util_isInteropObservable,_util_isIterable,_fromArray,_fromPromise,_fromIterable,_fromObservable,_util_subscribeTo PURE_IMPORTS_END */
    function from(input, scheduler) {
        if (!scheduler) {
            if (input instanceof Observable) {
                return input;
            }
            return new Observable(subscribeTo(input));
        }
        if (input != null) {
            if (isInteropObservable(input)) {
                return fromObservable(input, scheduler);
            }
            else if (isPromise(input)) {
                return fromPromise(input, scheduler);
            }
            else if (isArrayLike(input)) {
                return fromArray(input, scheduler);
            }
            else if (isIterable(input) || typeof input === 'string') {
                return fromIterable(input, scheduler);
            }
        }
        throw new TypeError((input !== null && typeof input || input) + ' is not observable');
    }

    /** PURE_IMPORTS_START _util_isArray,_observable_combineLatest,_observable_from PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _Observable PURE_IMPORTS_END */
    var EMPTY = /*@__PURE__*/ new Observable(function (subscriber) { return subscriber.complete(); });
    function empty$1(scheduler) {
        return scheduler ? emptyScheduled(scheduler) : EMPTY;
    }
    function emptyScheduled(scheduler) {
        return new Observable(function (subscriber) { return scheduler.schedule(function () { return subscriber.complete(); }); });
    }

    /** PURE_IMPORTS_START _Observable PURE_IMPORTS_END */
    function scalar(value) {
        var result = new Observable(function (subscriber) {
            subscriber.next(value);
            subscriber.complete();
        });
        result._isScalar = true;
        result.value = value;
        return result;
    }

    /** PURE_IMPORTS_START _util_isScheduler,_fromArray,_empty,_scalar PURE_IMPORTS_END */
    function of() {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var scheduler = args[args.length - 1];
        if (isScheduler(scheduler)) {
            args.pop();
        }
        else {
            scheduler = undefined;
        }
        switch (args.length) {
            case 0:
                return empty$1(scheduler);
            case 1:
                return scheduler ? fromArray(args, scheduler) : scalar(args[0]);
            default:
                return fromArray(args, scheduler);
        }
    }

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function map(project, thisArg) {
        return function mapOperation(source) {
            if (typeof project !== 'function') {
                throw new TypeError('argument is not a function. Are you looking for `mapTo()`?');
            }
            return source.lift(new MapOperator(project, thisArg));
        };
    }
    var MapOperator = /*@__PURE__*/ (function () {
        function MapOperator(project, thisArg) {
            this.project = project;
            this.thisArg = thisArg;
        }
        MapOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new MapSubscriber(subscriber, this.project, this.thisArg));
        };
        return MapOperator;
    }());
    var MapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MapSubscriber, _super);
        function MapSubscriber(destination, project, thisArg) {
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.count = 0;
            _this.thisArg = thisArg || _this;
            return _this;
        }
        MapSubscriber.prototype._next = function (value) {
            var result;
            try {
                result = this.project.call(this.thisArg, value, this.count++);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(result);
        };
        return MapSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_util_subscribeToResult,_OuterSubscriber,_map,_observable_from PURE_IMPORTS_END */
    function mergeMap(project, resultSelector, concurrent) {
        if (concurrent === void 0) {
            concurrent = Number.POSITIVE_INFINITY;
        }
        if (typeof resultSelector === 'function') {
            return function (source) { return source.pipe(mergeMap(function (a, i) { return from(project(a, i)).pipe(map(function (b, ii) { return resultSelector(a, b, i, ii); })); }, concurrent)); };
        }
        else if (typeof resultSelector === 'number') {
            concurrent = resultSelector;
        }
        return function (source) { return source.lift(new MergeMapOperator(project, concurrent)); };
    }
    var MergeMapOperator = /*@__PURE__*/ (function () {
        function MergeMapOperator(project, concurrent) {
            if (concurrent === void 0) {
                concurrent = Number.POSITIVE_INFINITY;
            }
            this.project = project;
            this.concurrent = concurrent;
        }
        MergeMapOperator.prototype.call = function (observer, source) {
            return source.subscribe(new MergeMapSubscriber(observer, this.project, this.concurrent));
        };
        return MergeMapOperator;
    }());
    var MergeMapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MergeMapSubscriber, _super);
        function MergeMapSubscriber(destination, project, concurrent) {
            if (concurrent === void 0) {
                concurrent = Number.POSITIVE_INFINITY;
            }
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.concurrent = concurrent;
            _this.hasCompleted = false;
            _this.buffer = [];
            _this.active = 0;
            _this.index = 0;
            return _this;
        }
        MergeMapSubscriber.prototype._next = function (value) {
            if (this.active < this.concurrent) {
                this._tryNext(value);
            }
            else {
                this.buffer.push(value);
            }
        };
        MergeMapSubscriber.prototype._tryNext = function (value) {
            var result;
            var index = this.index++;
            try {
                result = this.project(value, index);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.active++;
            this._innerSub(result, value, index);
        };
        MergeMapSubscriber.prototype._innerSub = function (ish, value, index) {
            this.add(subscribeToResult(this, ish, value, index));
        };
        MergeMapSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (this.active === 0 && this.buffer.length === 0) {
                this.destination.complete();
            }
        };
        MergeMapSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        MergeMapSubscriber.prototype.notifyComplete = function (innerSub) {
            var buffer = this.buffer;
            this.remove(innerSub);
            this.active--;
            if (buffer.length > 0) {
                this._next(buffer.shift());
            }
            else if (this.active === 0 && this.hasCompleted) {
                this.destination.complete();
            }
        };
        return MergeMapSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _mergeMap,_util_identity PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _mergeAll PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _util_isScheduler,_of,_from,_operators_concatAll PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _observable_concat PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _mergeMap PURE_IMPORTS_END */
    function concatMap(project, resultSelector) {
        return mergeMap(project, resultSelector, 1);
    }

    /** PURE_IMPORTS_START _concatMap PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var CountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(CountSubscriber, _super);
        function CountSubscriber(destination, predicate, source) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.source = source;
            _this.count = 0;
            _this.index = 0;
            return _this;
        }
        CountSubscriber.prototype._next = function (value) {
            if (this.predicate) {
                this._tryPredicate(value);
            }
            else {
                this.count++;
            }
        };
        CountSubscriber.prototype._tryPredicate = function (value) {
            var result;
            try {
                result = this.predicate(value, this.index++, this.source);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            if (result) {
                this.count++;
            }
        };
        CountSubscriber.prototype._complete = function () {
            this.destination.next(this.count);
            this.destination.complete();
        };
        return CountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var DebounceSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DebounceSubscriber, _super);
        function DebounceSubscriber(destination, durationSelector) {
            var _this = _super.call(this, destination) || this;
            _this.durationSelector = durationSelector;
            _this.hasValue = false;
            _this.durationSubscription = null;
            return _this;
        }
        DebounceSubscriber.prototype._next = function (value) {
            try {
                var result = this.durationSelector.call(this, value);
                if (result) {
                    this._tryNext(value, result);
                }
            }
            catch (err) {
                this.destination.error(err);
            }
        };
        DebounceSubscriber.prototype._complete = function () {
            this.emitValue();
            this.destination.complete();
        };
        DebounceSubscriber.prototype._tryNext = function (value, duration) {
            var subscription = this.durationSubscription;
            this.value = value;
            this.hasValue = true;
            if (subscription) {
                subscription.unsubscribe();
                this.remove(subscription);
            }
            subscription = subscribeToResult(this, duration);
            if (subscription && !subscription.closed) {
                this.add(this.durationSubscription = subscription);
            }
        };
        DebounceSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.emitValue();
        };
        DebounceSubscriber.prototype.notifyComplete = function () {
            this.emitValue();
        };
        DebounceSubscriber.prototype.emitValue = function () {
            if (this.hasValue) {
                var value = this.value;
                var subscription = this.durationSubscription;
                if (subscription) {
                    this.durationSubscription = null;
                    subscription.unsubscribe();
                    this.remove(subscription);
                }
                this.value = null;
                this.hasValue = false;
                _super.prototype._next.call(this, value);
            }
        };
        return DebounceSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_scheduler_async PURE_IMPORTS_END */
    var DebounceTimeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DebounceTimeSubscriber, _super);
        function DebounceTimeSubscriber(destination, dueTime, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.dueTime = dueTime;
            _this.scheduler = scheduler;
            _this.debouncedSubscription = null;
            _this.lastValue = null;
            _this.hasValue = false;
            return _this;
        }
        DebounceTimeSubscriber.prototype._next = function (value) {
            this.clearDebounce();
            this.lastValue = value;
            this.hasValue = true;
            this.add(this.debouncedSubscription = this.scheduler.schedule(dispatchNext, this.dueTime, this));
        };
        DebounceTimeSubscriber.prototype._complete = function () {
            this.debouncedNext();
            this.destination.complete();
        };
        DebounceTimeSubscriber.prototype.debouncedNext = function () {
            this.clearDebounce();
            if (this.hasValue) {
                var lastValue = this.lastValue;
                this.lastValue = null;
                this.hasValue = false;
                this.destination.next(lastValue);
            }
        };
        DebounceTimeSubscriber.prototype.clearDebounce = function () {
            var debouncedSubscription = this.debouncedSubscription;
            if (debouncedSubscription !== null) {
                this.remove(debouncedSubscription);
                debouncedSubscription.unsubscribe();
                this.debouncedSubscription = null;
            }
        };
        return DebounceTimeSubscriber;
    }(Subscriber));
    function dispatchNext(subscriber) {
        subscriber.debouncedNext();
    }

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var DefaultIfEmptySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DefaultIfEmptySubscriber, _super);
        function DefaultIfEmptySubscriber(destination, defaultValue) {
            var _this = _super.call(this, destination) || this;
            _this.defaultValue = defaultValue;
            _this.isEmpty = true;
            return _this;
        }
        DefaultIfEmptySubscriber.prototype._next = function (value) {
            this.isEmpty = false;
            this.destination.next(value);
        };
        DefaultIfEmptySubscriber.prototype._complete = function () {
            if (this.isEmpty) {
                this.destination.next(this.defaultValue);
            }
            this.destination.complete();
        };
        return DefaultIfEmptySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _Observable PURE_IMPORTS_END */
    function throwError(error, scheduler) {
        if (!scheduler) {
            return new Observable(function (subscriber) { return subscriber.error(error); });
        }
        else {
            return new Observable(function (subscriber) { return scheduler.schedule(dispatch$1, 0, { error: error, subscriber: subscriber }); });
        }
    }
    function dispatch$1(_a) {
        var error = _a.error, subscriber = _a.subscriber;
        subscriber.error(error);
    }

    /** PURE_IMPORTS_START _observable_empty,_observable_of,_observable_throwError PURE_IMPORTS_END */
    var Notification = /*@__PURE__*/ (function () {
        function Notification(kind, value, error) {
            this.kind = kind;
            this.value = value;
            this.error = error;
            this.hasValue = kind === 'N';
        }
        Notification.prototype.observe = function (observer) {
            switch (this.kind) {
                case 'N':
                    return observer.next && observer.next(this.value);
                case 'E':
                    return observer.error && observer.error(this.error);
                case 'C':
                    return observer.complete && observer.complete();
            }
        };
        Notification.prototype.do = function (next, error, complete) {
            var kind = this.kind;
            switch (kind) {
                case 'N':
                    return next && next(this.value);
                case 'E':
                    return error && error(this.error);
                case 'C':
                    return complete && complete();
            }
        };
        Notification.prototype.accept = function (nextOrObserver, error, complete) {
            if (nextOrObserver && typeof nextOrObserver.next === 'function') {
                return this.observe(nextOrObserver);
            }
            else {
                return this.do(nextOrObserver, error, complete);
            }
        };
        Notification.prototype.toObservable = function () {
            var kind = this.kind;
            switch (kind) {
                case 'N':
                    return of(this.value);
                case 'E':
                    return throwError(this.error);
                case 'C':
                    return empty$1();
            }
            throw new Error('unexpected notification kind value');
        };
        Notification.createNext = function (value) {
            if (typeof value !== 'undefined') {
                return new Notification('N', value);
            }
            return Notification.undefinedValueNotification;
        };
        Notification.createError = function (err) {
            return new Notification('E', undefined, err);
        };
        Notification.createComplete = function () {
            return Notification.completeNotification;
        };
        Notification.completeNotification = new Notification('C');
        Notification.undefinedValueNotification = new Notification('N', undefined);
        return Notification;
    }());

    /** PURE_IMPORTS_START tslib,_scheduler_async,_util_isDate,_Subscriber,_Notification PURE_IMPORTS_END */
    var DelaySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DelaySubscriber, _super);
        function DelaySubscriber(destination, delay, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.delay = delay;
            _this.scheduler = scheduler;
            _this.queue = [];
            _this.active = false;
            _this.errored = false;
            return _this;
        }
        DelaySubscriber.dispatch = function (state) {
            var source = state.source;
            var queue = source.queue;
            var scheduler = state.scheduler;
            var destination = state.destination;
            while (queue.length > 0 && (queue[0].time - scheduler.now()) <= 0) {
                queue.shift().notification.observe(destination);
            }
            if (queue.length > 0) {
                var delay_1 = Math.max(0, queue[0].time - scheduler.now());
                this.schedule(state, delay_1);
            }
            else {
                this.unsubscribe();
                source.active = false;
            }
        };
        DelaySubscriber.prototype._schedule = function (scheduler) {
            this.active = true;
            this.add(scheduler.schedule(DelaySubscriber.dispatch, this.delay, {
                source: this, destination: this.destination, scheduler: scheduler
            }));
        };
        DelaySubscriber.prototype.scheduleNotification = function (notification) {
            if (this.errored === true) {
                return;
            }
            var scheduler = this.scheduler;
            var message = new DelayMessage(scheduler.now() + this.delay, notification);
            this.queue.push(message);
            if (this.active === false) {
                this._schedule(scheduler);
            }
        };
        DelaySubscriber.prototype._next = function (value) {
            this.scheduleNotification(Notification.createNext(value));
        };
        DelaySubscriber.prototype._error = function (err) {
            this.errored = true;
            this.queue = [];
            this.destination.error(err);
        };
        DelaySubscriber.prototype._complete = function () {
            this.scheduleNotification(Notification.createComplete());
        };
        return DelaySubscriber;
    }(Subscriber));
    var DelayMessage = /*@__PURE__*/ (function () {
        function DelayMessage(time, notification) {
            this.time = time;
            this.notification = notification;
        }
        return DelayMessage;
    }());

    /** PURE_IMPORTS_START tslib,_Subscriber,_Observable,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var DelayWhenSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DelayWhenSubscriber, _super);
        function DelayWhenSubscriber(destination, delayDurationSelector) {
            var _this = _super.call(this, destination) || this;
            _this.delayDurationSelector = delayDurationSelector;
            _this.completed = false;
            _this.delayNotifierSubscriptions = [];
            return _this;
        }
        DelayWhenSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(outerValue);
            this.removeSubscription(innerSub);
            this.tryComplete();
        };
        DelayWhenSubscriber.prototype.notifyError = function (error, innerSub) {
            this._error(error);
        };
        DelayWhenSubscriber.prototype.notifyComplete = function (innerSub) {
            var value = this.removeSubscription(innerSub);
            if (value) {
                this.destination.next(value);
            }
            this.tryComplete();
        };
        DelayWhenSubscriber.prototype._next = function (value) {
            try {
                var delayNotifier = this.delayDurationSelector(value);
                if (delayNotifier) {
                    this.tryDelay(delayNotifier, value);
                }
            }
            catch (err) {
                this.destination.error(err);
            }
        };
        DelayWhenSubscriber.prototype._complete = function () {
            this.completed = true;
            this.tryComplete();
        };
        DelayWhenSubscriber.prototype.removeSubscription = function (subscription) {
            subscription.unsubscribe();
            var subscriptionIdx = this.delayNotifierSubscriptions.indexOf(subscription);
            if (subscriptionIdx !== -1) {
                this.delayNotifierSubscriptions.splice(subscriptionIdx, 1);
            }
            return subscription.outerValue;
        };
        DelayWhenSubscriber.prototype.tryDelay = function (delayNotifier, value) {
            var notifierSubscription = subscribeToResult(this, delayNotifier, value);
            if (notifierSubscription && !notifierSubscription.closed) {
                this.add(notifierSubscription);
                this.delayNotifierSubscriptions.push(notifierSubscription);
            }
        };
        DelayWhenSubscriber.prototype.tryComplete = function () {
            if (this.completed && this.delayNotifierSubscriptions.length === 0) {
                this.destination.complete();
            }
        };
        return DelayWhenSubscriber;
    }(OuterSubscriber));
    var SubscriptionDelayObservable = /*@__PURE__*/ (function (_super) {
        __extends(SubscriptionDelayObservable, _super);
        function SubscriptionDelayObservable(source, subscriptionDelay) {
            var _this = _super.call(this) || this;
            _this.source = source;
            _this.subscriptionDelay = subscriptionDelay;
            return _this;
        }
        SubscriptionDelayObservable.prototype._subscribe = function (subscriber) {
            this.subscriptionDelay.subscribe(new SubscriptionDelaySubscriber(subscriber, this.source));
        };
        return SubscriptionDelayObservable;
    }(Observable));
    var SubscriptionDelaySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SubscriptionDelaySubscriber, _super);
        function SubscriptionDelaySubscriber(parent, source) {
            var _this = _super.call(this) || this;
            _this.parent = parent;
            _this.source = source;
            _this.sourceSubscribed = false;
            return _this;
        }
        SubscriptionDelaySubscriber.prototype._next = function (unused) {
            this.subscribeToSource();
        };
        SubscriptionDelaySubscriber.prototype._error = function (err) {
            this.unsubscribe();
            this.parent.error(err);
        };
        SubscriptionDelaySubscriber.prototype._complete = function () {
            this.subscribeToSource();
        };
        SubscriptionDelaySubscriber.prototype.subscribeToSource = function () {
            if (!this.sourceSubscribed) {
                this.sourceSubscribed = true;
                this.unsubscribe();
                this.source.subscribe(this.parent);
            }
        };
        return SubscriptionDelaySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var DeMaterializeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DeMaterializeSubscriber, _super);
        function DeMaterializeSubscriber(destination) {
            return _super.call(this, destination) || this;
        }
        DeMaterializeSubscriber.prototype._next = function (value) {
            value.observe(this.destination);
        };
        return DeMaterializeSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var DistinctSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DistinctSubscriber, _super);
        function DistinctSubscriber(destination, keySelector, flushes) {
            var _this = _super.call(this, destination) || this;
            _this.keySelector = keySelector;
            _this.values = new Set();
            if (flushes) {
                _this.add(subscribeToResult(_this, flushes));
            }
            return _this;
        }
        DistinctSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.values.clear();
        };
        DistinctSubscriber.prototype.notifyError = function (error, innerSub) {
            this._error(error);
        };
        DistinctSubscriber.prototype._next = function (value) {
            if (this.keySelector) {
                this._useKeySelector(value);
            }
            else {
                this._finalizeNext(value, value);
            }
        };
        DistinctSubscriber.prototype._useKeySelector = function (value) {
            var key;
            var destination = this.destination;
            try {
                key = this.keySelector(value);
            }
            catch (err) {
                destination.error(err);
                return;
            }
            this._finalizeNext(key, value);
        };
        DistinctSubscriber.prototype._finalizeNext = function (key, value) {
            var values = this.values;
            if (!values.has(key)) {
                values.add(key);
                this.destination.next(value);
            }
        };
        return DistinctSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_tryCatch,_util_errorObject PURE_IMPORTS_END */
    var DistinctUntilChangedSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(DistinctUntilChangedSubscriber, _super);
        function DistinctUntilChangedSubscriber(destination, compare, keySelector) {
            var _this = _super.call(this, destination) || this;
            _this.keySelector = keySelector;
            _this.hasKey = false;
            if (typeof compare === 'function') {
                _this.compare = compare;
            }
            return _this;
        }
        DistinctUntilChangedSubscriber.prototype.compare = function (x, y) {
            return x === y;
        };
        DistinctUntilChangedSubscriber.prototype._next = function (value) {
            var keySelector = this.keySelector;
            var key = value;
            if (keySelector) {
                key = tryCatch(this.keySelector)(value);
                if (key === errorObject) {
                    return this.destination.error(errorObject.e);
                }
            }
            var result = false;
            if (this.hasKey) {
                result = tryCatch(this.compare)(this.key, key);
                if (result === errorObject) {
                    return this.destination.error(errorObject.e);
                }
            }
            else {
                this.hasKey = true;
            }
            if (Boolean(result) === false) {
                this.key = key;
                this.destination.next(value);
            }
        };
        return DistinctUntilChangedSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _distinctUntilChanged PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib PURE_IMPORTS_END */
    var ArgumentOutOfRangeError = /*@__PURE__*/ (function (_super) {
        __extends(ArgumentOutOfRangeError, _super);
        function ArgumentOutOfRangeError() {
            var _this = _super.call(this, 'argument out of range') || this;
            _this.name = 'ArgumentOutOfRangeError';
            Object.setPrototypeOf(_this, ArgumentOutOfRangeError.prototype);
            return _this;
        }
        return ArgumentOutOfRangeError;
    }(Error));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function filter(predicate, thisArg) {
        return function filterOperatorFunction(source) {
            return source.lift(new FilterOperator(predicate, thisArg));
        };
    }
    var FilterOperator = /*@__PURE__*/ (function () {
        function FilterOperator(predicate, thisArg) {
            this.predicate = predicate;
            this.thisArg = thisArg;
        }
        FilterOperator.prototype.call = function (subscriber, source) {
            return source.subscribe(new FilterSubscriber(subscriber, this.predicate, this.thisArg));
        };
        return FilterOperator;
    }());
    var FilterSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(FilterSubscriber, _super);
        function FilterSubscriber(destination, predicate, thisArg) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.thisArg = thisArg;
            _this.count = 0;
            return _this;
        }
        FilterSubscriber.prototype._next = function (value) {
            var result;
            try {
                result = this.predicate.call(this.thisArg, value, this.count++);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            if (result) {
                this.destination.next(value);
            }
        };
        return FilterSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_noop,_util_isFunction PURE_IMPORTS_END */
    var TapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TapSubscriber, _super);
        function TapSubscriber(destination, observerOrNext, error, complete) {
            var _this = _super.call(this, destination) || this;
            _this._tapNext = noop;
            _this._tapError = noop;
            _this._tapComplete = noop;
            _this._tapError = error || noop;
            _this._tapComplete = complete || noop;
            if (isFunction(observerOrNext)) {
                _this._context = _this;
                _this._tapNext = observerOrNext;
            }
            else if (observerOrNext) {
                _this._context = observerOrNext;
                _this._tapNext = observerOrNext.next || noop;
                _this._tapError = observerOrNext.error || noop;
                _this._tapComplete = observerOrNext.complete || noop;
            }
            return _this;
        }
        TapSubscriber.prototype._next = function (value) {
            try {
                this._tapNext.call(this._context, value);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(value);
        };
        TapSubscriber.prototype._error = function (err) {
            try {
                this._tapError.call(this._context, err);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.error(err);
        };
        TapSubscriber.prototype._complete = function () {
            try {
                this._tapComplete.call(this._context);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            return this.destination.complete();
        };
        return TapSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib PURE_IMPORTS_END */
    var EmptyError = /*@__PURE__*/ (function (_super) {
        __extends(EmptyError, _super);
        function EmptyError() {
            var _this = _super.call(this, 'no elements in sequence') || this;
            _this.name = 'EmptyError';
            Object.setPrototypeOf(_this, EmptyError.prototype);
            return _this;
        }
        return EmptyError;
    }(Error));

    /** PURE_IMPORTS_START _tap,_util_EmptyError PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_ArgumentOutOfRangeError,_observable_empty PURE_IMPORTS_END */
    var TakeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TakeSubscriber, _super);
        function TakeSubscriber(destination, total) {
            var _this = _super.call(this, destination) || this;
            _this.total = total;
            _this.count = 0;
            return _this;
        }
        TakeSubscriber.prototype._next = function (value) {
            var total = this.total;
            var count = ++this.count;
            if (count <= total) {
                this.destination.next(value);
                if (count === total) {
                    this.destination.complete();
                    this.unsubscribe();
                }
            }
        };
        return TakeSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _util_ArgumentOutOfRangeError,_filter,_throwIfEmpty,_defaultIfEmpty,_take PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _observable_fromArray,_observable_scalar,_observable_empty,_observable_concat,_util_isScheduler PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var EverySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(EverySubscriber, _super);
        function EverySubscriber(destination, predicate, thisArg, source) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.thisArg = thisArg;
            _this.source = source;
            _this.index = 0;
            _this.thisArg = thisArg || _this;
            return _this;
        }
        EverySubscriber.prototype.notifyComplete = function (everyValueMatch) {
            this.destination.next(everyValueMatch);
            this.destination.complete();
        };
        EverySubscriber.prototype._next = function (value) {
            var result = false;
            try {
                result = this.predicate.call(this.thisArg, value, this.index++, this.source);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            if (!result) {
                this.notifyComplete(false);
            }
        };
        EverySubscriber.prototype._complete = function () {
            this.notifyComplete(true);
        };
        return EverySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var SwitchFirstSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SwitchFirstSubscriber, _super);
        function SwitchFirstSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.hasCompleted = false;
            _this.hasSubscription = false;
            return _this;
        }
        SwitchFirstSubscriber.prototype._next = function (value) {
            if (!this.hasSubscription) {
                this.hasSubscription = true;
                this.add(subscribeToResult(this, value));
            }
        };
        SwitchFirstSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (!this.hasSubscription) {
                this.destination.complete();
            }
        };
        SwitchFirstSubscriber.prototype.notifyComplete = function (innerSub) {
            this.remove(innerSub);
            this.hasSubscription = false;
            if (this.hasCompleted) {
                this.destination.complete();
            }
        };
        return SwitchFirstSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult,_map,_observable_from PURE_IMPORTS_END */
    var ExhaustMapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ExhaustMapSubscriber, _super);
        function ExhaustMapSubscriber(destination, project) {
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.hasSubscription = false;
            _this.hasCompleted = false;
            _this.index = 0;
            return _this;
        }
        ExhaustMapSubscriber.prototype._next = function (value) {
            if (!this.hasSubscription) {
                this.tryNext(value);
            }
        };
        ExhaustMapSubscriber.prototype.tryNext = function (value) {
            var index = this.index++;
            var destination = this.destination;
            try {
                var result = this.project(value, index);
                this.hasSubscription = true;
                this.add(subscribeToResult(this, result, value, index));
            }
            catch (err) {
                destination.error(err);
            }
        };
        ExhaustMapSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (!this.hasSubscription) {
                this.destination.complete();
            }
        };
        ExhaustMapSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        ExhaustMapSubscriber.prototype.notifyError = function (err) {
            this.destination.error(err);
        };
        ExhaustMapSubscriber.prototype.notifyComplete = function (innerSub) {
            this.remove(innerSub);
            this.hasSubscription = false;
            if (this.hasCompleted) {
                this.destination.complete();
            }
        };
        return ExhaustMapSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var ExpandSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ExpandSubscriber, _super);
        function ExpandSubscriber(destination, project, concurrent, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.concurrent = concurrent;
            _this.scheduler = scheduler;
            _this.index = 0;
            _this.active = 0;
            _this.hasCompleted = false;
            if (concurrent < Number.POSITIVE_INFINITY) {
                _this.buffer = [];
            }
            return _this;
        }
        ExpandSubscriber.dispatch = function (arg) {
            var subscriber = arg.subscriber, result = arg.result, value = arg.value, index = arg.index;
            subscriber.subscribeToProjection(result, value, index);
        };
        ExpandSubscriber.prototype._next = function (value) {
            var destination = this.destination;
            if (destination.closed) {
                this._complete();
                return;
            }
            var index = this.index++;
            if (this.active < this.concurrent) {
                destination.next(value);
                var result = tryCatch(this.project)(value, index);
                if (result === errorObject) {
                    destination.error(errorObject.e);
                }
                else if (!this.scheduler) {
                    this.subscribeToProjection(result, value, index);
                }
                else {
                    var state = { subscriber: this, result: result, value: value, index: index };
                    this.add(this.scheduler.schedule(ExpandSubscriber.dispatch, 0, state));
                }
            }
            else {
                this.buffer.push(value);
            }
        };
        ExpandSubscriber.prototype.subscribeToProjection = function (result, value, index) {
            this.active++;
            this.add(subscribeToResult(this, result, value, index));
        };
        ExpandSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (this.hasCompleted && this.active === 0) {
                this.destination.complete();
            }
        };
        ExpandSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this._next(innerValue);
        };
        ExpandSubscriber.prototype.notifyComplete = function (innerSub) {
            var buffer = this.buffer;
            this.remove(innerSub);
            this.active--;
            if (buffer && buffer.length > 0) {
                this._next(buffer.shift());
            }
            if (this.hasCompleted && this.active === 0) {
                this.destination.complete();
            }
        };
        return ExpandSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_Subscription PURE_IMPORTS_END */
    var FinallySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(FinallySubscriber, _super);
        function FinallySubscriber(destination, callback) {
            var _this = _super.call(this, destination) || this;
            _this.add(new Subscription(callback));
            return _this;
        }
        return FinallySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var FindValueSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(FindValueSubscriber, _super);
        function FindValueSubscriber(destination, predicate, source, yieldIndex, thisArg) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.source = source;
            _this.yieldIndex = yieldIndex;
            _this.thisArg = thisArg;
            _this.index = 0;
            return _this;
        }
        FindValueSubscriber.prototype.notifyComplete = function (value) {
            var destination = this.destination;
            destination.next(value);
            destination.complete();
        };
        FindValueSubscriber.prototype._next = function (value) {
            var _a = this, predicate = _a.predicate, thisArg = _a.thisArg;
            var index = this.index++;
            try {
                var result = predicate.call(thisArg || this, value, index, this.source);
                if (result) {
                    this.notifyComplete(this.yieldIndex ? index : value);
                }
            }
            catch (err) {
                this.destination.error(err);
            }
        };
        FindValueSubscriber.prototype._complete = function () {
            this.notifyComplete(this.yieldIndex ? -1 : undefined);
        };
        return FindValueSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _operators_find PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _util_EmptyError,_filter,_take,_defaultIfEmpty,_throwIfEmpty,_util_identity PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib PURE_IMPORTS_END */
    var ObjectUnsubscribedError = /*@__PURE__*/ (function (_super) {
        __extends(ObjectUnsubscribedError, _super);
        function ObjectUnsubscribedError() {
            var _this = _super.call(this, 'object unsubscribed') || this;
            _this.name = 'ObjectUnsubscribedError';
            Object.setPrototypeOf(_this, ObjectUnsubscribedError.prototype);
            return _this;
        }
        return ObjectUnsubscribedError;
    }(Error));

    /** PURE_IMPORTS_START tslib,_Subscription PURE_IMPORTS_END */
    var SubjectSubscription = /*@__PURE__*/ (function (_super) {
        __extends(SubjectSubscription, _super);
        function SubjectSubscription(subject, subscriber) {
            var _this = _super.call(this) || this;
            _this.subject = subject;
            _this.subscriber = subscriber;
            _this.closed = false;
            return _this;
        }
        SubjectSubscription.prototype.unsubscribe = function () {
            if (this.closed) {
                return;
            }
            this.closed = true;
            var subject = this.subject;
            var observers = subject.observers;
            this.subject = null;
            if (!observers || observers.length === 0 || subject.isStopped || subject.closed) {
                return;
            }
            var subscriberIndex = observers.indexOf(this.subscriber);
            if (subscriberIndex !== -1) {
                observers.splice(subscriberIndex, 1);
            }
        };
        return SubjectSubscription;
    }(Subscription));

    /** PURE_IMPORTS_START tslib,_Observable,_Subscriber,_Subscription,_util_ObjectUnsubscribedError,_SubjectSubscription,_internal_symbol_rxSubscriber PURE_IMPORTS_END */
    var SubjectSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SubjectSubscriber, _super);
        function SubjectSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            return _this;
        }
        return SubjectSubscriber;
    }(Subscriber));
    var Subject = /*@__PURE__*/ (function (_super) {
        __extends(Subject, _super);
        function Subject() {
            var _this = _super.call(this) || this;
            _this.observers = [];
            _this.closed = false;
            _this.isStopped = false;
            _this.hasError = false;
            _this.thrownError = null;
            return _this;
        }
        Subject.prototype[rxSubscriber] = function () {
            return new SubjectSubscriber(this);
        };
        Subject.prototype.lift = function (operator) {
            var subject = new AnonymousSubject(this, this);
            subject.operator = operator;
            return subject;
        };
        Subject.prototype.next = function (value) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            if (!this.isStopped) {
                var observers = this.observers;
                var len = observers.length;
                var copy = observers.slice();
                for (var i = 0; i < len; i++) {
                    copy[i].next(value);
                }
            }
        };
        Subject.prototype.error = function (err) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            this.hasError = true;
            this.thrownError = err;
            this.isStopped = true;
            var observers = this.observers;
            var len = observers.length;
            var copy = observers.slice();
            for (var i = 0; i < len; i++) {
                copy[i].error(err);
            }
            this.observers.length = 0;
        };
        Subject.prototype.complete = function () {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            this.isStopped = true;
            var observers = this.observers;
            var len = observers.length;
            var copy = observers.slice();
            for (var i = 0; i < len; i++) {
                copy[i].complete();
            }
            this.observers.length = 0;
        };
        Subject.prototype.unsubscribe = function () {
            this.isStopped = true;
            this.closed = true;
            this.observers = null;
        };
        Subject.prototype._trySubscribe = function (subscriber) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else {
                return _super.prototype._trySubscribe.call(this, subscriber);
            }
        };
        Subject.prototype._subscribe = function (subscriber) {
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else if (this.hasError) {
                subscriber.error(this.thrownError);
                return Subscription.EMPTY;
            }
            else if (this.isStopped) {
                subscriber.complete();
                return Subscription.EMPTY;
            }
            else {
                this.observers.push(subscriber);
                return new SubjectSubscription(this, subscriber);
            }
        };
        Subject.prototype.asObservable = function () {
            var observable = new Observable();
            observable.source = this;
            return observable;
        };
        Subject.create = function (destination, source) {
            return new AnonymousSubject(destination, source);
        };
        return Subject;
    }(Observable));
    var AnonymousSubject = /*@__PURE__*/ (function (_super) {
        __extends(AnonymousSubject, _super);
        function AnonymousSubject(destination, source) {
            var _this = _super.call(this) || this;
            _this.destination = destination;
            _this.source = source;
            return _this;
        }
        AnonymousSubject.prototype.next = function (value) {
            var destination = this.destination;
            if (destination && destination.next) {
                destination.next(value);
            }
        };
        AnonymousSubject.prototype.error = function (err) {
            var destination = this.destination;
            if (destination && destination.error) {
                this.destination.error(err);
            }
        };
        AnonymousSubject.prototype.complete = function () {
            var destination = this.destination;
            if (destination && destination.complete) {
                this.destination.complete();
            }
        };
        AnonymousSubject.prototype._subscribe = function (subscriber) {
            var source = this.source;
            if (source) {
                return this.source.subscribe(subscriber);
            }
            else {
                return Subscription.EMPTY;
            }
        };
        return AnonymousSubject;
    }(Subject));

    /** PURE_IMPORTS_START tslib,_Subscriber,_Subscription,_Observable,_Subject PURE_IMPORTS_END */
    var GroupBySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(GroupBySubscriber, _super);
        function GroupBySubscriber(destination, keySelector, elementSelector, durationSelector, subjectSelector) {
            var _this = _super.call(this, destination) || this;
            _this.keySelector = keySelector;
            _this.elementSelector = elementSelector;
            _this.durationSelector = durationSelector;
            _this.subjectSelector = subjectSelector;
            _this.groups = null;
            _this.attemptedToUnsubscribe = false;
            _this.count = 0;
            return _this;
        }
        GroupBySubscriber.prototype._next = function (value) {
            var key;
            try {
                key = this.keySelector(value);
            }
            catch (err) {
                this.error(err);
                return;
            }
            this._group(value, key);
        };
        GroupBySubscriber.prototype._group = function (value, key) {
            var groups = this.groups;
            if (!groups) {
                groups = this.groups = new Map();
            }
            var group = groups.get(key);
            var element;
            if (this.elementSelector) {
                try {
                    element = this.elementSelector(value);
                }
                catch (err) {
                    this.error(err);
                }
            }
            else {
                element = value;
            }
            if (!group) {
                group = (this.subjectSelector ? this.subjectSelector() : new Subject());
                groups.set(key, group);
                var groupedObservable = new GroupedObservable(key, group, this);
                this.destination.next(groupedObservable);
                if (this.durationSelector) {
                    var duration = void 0;
                    try {
                        duration = this.durationSelector(new GroupedObservable(key, group));
                    }
                    catch (err) {
                        this.error(err);
                        return;
                    }
                    this.add(duration.subscribe(new GroupDurationSubscriber(key, group, this)));
                }
            }
            if (!group.closed) {
                group.next(element);
            }
        };
        GroupBySubscriber.prototype._error = function (err) {
            var groups = this.groups;
            if (groups) {
                groups.forEach(function (group, key) {
                    group.error(err);
                });
                groups.clear();
            }
            this.destination.error(err);
        };
        GroupBySubscriber.prototype._complete = function () {
            var groups = this.groups;
            if (groups) {
                groups.forEach(function (group, key) {
                    group.complete();
                });
                groups.clear();
            }
            this.destination.complete();
        };
        GroupBySubscriber.prototype.removeGroup = function (key) {
            this.groups.delete(key);
        };
        GroupBySubscriber.prototype.unsubscribe = function () {
            if (!this.closed) {
                this.attemptedToUnsubscribe = true;
                if (this.count === 0) {
                    _super.prototype.unsubscribe.call(this);
                }
            }
        };
        return GroupBySubscriber;
    }(Subscriber));
    var GroupDurationSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(GroupDurationSubscriber, _super);
        function GroupDurationSubscriber(key, group, parent) {
            var _this = _super.call(this, group) || this;
            _this.key = key;
            _this.group = group;
            _this.parent = parent;
            return _this;
        }
        GroupDurationSubscriber.prototype._next = function (value) {
            this.complete();
        };
        GroupDurationSubscriber.prototype._unsubscribe = function () {
            var _a = this, parent = _a.parent, key = _a.key;
            this.key = this.parent = null;
            if (parent) {
                parent.removeGroup(key);
            }
        };
        return GroupDurationSubscriber;
    }(Subscriber));
    var GroupedObservable = /*@__PURE__*/ (function (_super) {
        __extends(GroupedObservable, _super);
        function GroupedObservable(key, groupSubject, refCountSubscription) {
            var _this = _super.call(this) || this;
            _this.key = key;
            _this.groupSubject = groupSubject;
            _this.refCountSubscription = refCountSubscription;
            return _this;
        }
        GroupedObservable.prototype._subscribe = function (subscriber) {
            var subscription = new Subscription();
            var _a = this, refCountSubscription = _a.refCountSubscription, groupSubject = _a.groupSubject;
            if (refCountSubscription && !refCountSubscription.closed) {
                subscription.add(new InnerRefCountSubscription(refCountSubscription));
            }
            subscription.add(groupSubject.subscribe(subscriber));
            return subscription;
        };
        return GroupedObservable;
    }(Observable));
    var InnerRefCountSubscription = /*@__PURE__*/ (function (_super) {
        __extends(InnerRefCountSubscription, _super);
        function InnerRefCountSubscription(parent) {
            var _this = _super.call(this) || this;
            _this.parent = parent;
            parent.count++;
            return _this;
        }
        InnerRefCountSubscription.prototype.unsubscribe = function () {
            var parent = this.parent;
            if (!parent.closed && !this.closed) {
                _super.prototype.unsubscribe.call(this);
                parent.count -= 1;
                if (parent.count === 0 && parent.attemptedToUnsubscribe) {
                    parent.unsubscribe();
                }
            }
        };
        return InnerRefCountSubscription;
    }(Subscription));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var IgnoreElementsSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(IgnoreElementsSubscriber, _super);
        function IgnoreElementsSubscriber() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        IgnoreElementsSubscriber.prototype._next = function (unused) {
        };
        return IgnoreElementsSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var IsEmptySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(IsEmptySubscriber, _super);
        function IsEmptySubscriber(destination) {
            return _super.call(this, destination) || this;
        }
        IsEmptySubscriber.prototype.notifyComplete = function (isEmpty) {
            var destination = this.destination;
            destination.next(isEmpty);
            destination.complete();
        };
        IsEmptySubscriber.prototype._next = function (value) {
            this.notifyComplete(false);
        };
        IsEmptySubscriber.prototype._complete = function () {
            this.notifyComplete(true);
        };
        return IsEmptySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_ArgumentOutOfRangeError,_observable_empty PURE_IMPORTS_END */
    var TakeLastSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TakeLastSubscriber, _super);
        function TakeLastSubscriber(destination, total) {
            var _this = _super.call(this, destination) || this;
            _this.total = total;
            _this.ring = new Array();
            _this.count = 0;
            return _this;
        }
        TakeLastSubscriber.prototype._next = function (value) {
            var ring = this.ring;
            var total = this.total;
            var count = this.count++;
            if (ring.length < total) {
                ring.push(value);
            }
            else {
                var index = count % total;
                ring[index] = value;
            }
        };
        TakeLastSubscriber.prototype._complete = function () {
            var destination = this.destination;
            var count = this.count;
            if (count > 0) {
                var total = this.count >= this.total ? this.total : this.count;
                var ring = this.ring;
                for (var i = 0; i < total; i++) {
                    var idx = (count++) % total;
                    destination.next(ring[idx]);
                }
            }
            destination.complete();
        };
        return TakeLastSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _util_EmptyError,_filter,_takeLast,_throwIfEmpty,_defaultIfEmpty,_util_identity PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var MapToSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MapToSubscriber, _super);
        function MapToSubscriber(destination, value) {
            var _this = _super.call(this, destination) || this;
            _this.value = value;
            return _this;
        }
        MapToSubscriber.prototype._next = function (x) {
            this.destination.next(this.value);
        };
        return MapToSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_Notification PURE_IMPORTS_END */
    var MaterializeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MaterializeSubscriber, _super);
        function MaterializeSubscriber(destination) {
            return _super.call(this, destination) || this;
        }
        MaterializeSubscriber.prototype._next = function (value) {
            this.destination.next(Notification.createNext(value));
        };
        MaterializeSubscriber.prototype._error = function (err) {
            var destination = this.destination;
            destination.next(Notification.createError(err));
            destination.complete();
        };
        MaterializeSubscriber.prototype._complete = function () {
            var destination = this.destination;
            destination.next(Notification.createComplete());
            destination.complete();
        };
        return MaterializeSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var ScanSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ScanSubscriber, _super);
        function ScanSubscriber(destination, accumulator, _seed, hasSeed) {
            var _this = _super.call(this, destination) || this;
            _this.accumulator = accumulator;
            _this._seed = _seed;
            _this.hasSeed = hasSeed;
            _this.index = 0;
            return _this;
        }
        Object.defineProperty(ScanSubscriber.prototype, "seed", {
            get: function () {
                return this._seed;
            },
            set: function (value) {
                this.hasSeed = true;
                this._seed = value;
            },
            enumerable: true,
            configurable: true
        });
        ScanSubscriber.prototype._next = function (value) {
            if (!this.hasSeed) {
                this.seed = value;
                this.destination.next(value);
            }
            else {
                return this._tryNext(value);
            }
        };
        ScanSubscriber.prototype._tryNext = function (value) {
            var index = this.index++;
            var result;
            try {
                result = this.accumulator(this.seed, value, index);
            }
            catch (err) {
                this.destination.error(err);
            }
            this.seed = result;
            this.destination.next(result);
        };
        return ScanSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _scan,_takeLast,_defaultIfEmpty,_util_pipe PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _reduce PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _Observable,_util_isScheduler,_operators_mergeAll,_fromArray PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _observable_merge PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _mergeMap PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_util_tryCatch,_util_errorObject,_util_subscribeToResult,_OuterSubscriber PURE_IMPORTS_END */
    var MergeScanSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(MergeScanSubscriber, _super);
        function MergeScanSubscriber(destination, accumulator, acc, concurrent) {
            var _this = _super.call(this, destination) || this;
            _this.accumulator = accumulator;
            _this.acc = acc;
            _this.concurrent = concurrent;
            _this.hasValue = false;
            _this.hasCompleted = false;
            _this.buffer = [];
            _this.active = 0;
            _this.index = 0;
            return _this;
        }
        MergeScanSubscriber.prototype._next = function (value) {
            if (this.active < this.concurrent) {
                var index = this.index++;
                var ish = tryCatch(this.accumulator)(this.acc, value);
                var destination = this.destination;
                if (ish === errorObject) {
                    destination.error(errorObject.e);
                }
                else {
                    this.active++;
                    this._innerSub(ish, value, index);
                }
            }
            else {
                this.buffer.push(value);
            }
        };
        MergeScanSubscriber.prototype._innerSub = function (ish, value, index) {
            this.add(subscribeToResult(this, ish, value, index));
        };
        MergeScanSubscriber.prototype._complete = function () {
            this.hasCompleted = true;
            if (this.active === 0 && this.buffer.length === 0) {
                if (this.hasValue === false) {
                    this.destination.next(this.acc);
                }
                this.destination.complete();
            }
        };
        MergeScanSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            var destination = this.destination;
            this.acc = innerValue;
            this.hasValue = true;
            destination.next(innerValue);
        };
        MergeScanSubscriber.prototype.notifyComplete = function (innerSub) {
            var buffer = this.buffer;
            this.remove(innerSub);
            this.active--;
            if (buffer.length > 0) {
                this._next(buffer.shift());
            }
            else if (this.active === 0 && this.hasCompleted) {
                if (this.hasValue === false) {
                    this.destination.next(this.acc);
                }
                this.destination.complete();
            }
        };
        return MergeScanSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _reduce PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    function refCount() {
        return function refCountOperatorFunction(source) {
            return source.lift(new RefCountOperator(source));
        };
    }
    var RefCountOperator = /*@__PURE__*/ (function () {
        function RefCountOperator(connectable) {
            this.connectable = connectable;
        }
        RefCountOperator.prototype.call = function (subscriber, source) {
            var connectable = this.connectable;
            connectable._refCount++;
            var refCounter = new RefCountSubscriber(subscriber, connectable);
            var subscription = source.subscribe(refCounter);
            if (!refCounter.closed) {
                refCounter.connection = connectable.connect();
            }
            return subscription;
        };
        return RefCountOperator;
    }());
    var RefCountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RefCountSubscriber, _super);
        function RefCountSubscriber(destination, connectable) {
            var _this = _super.call(this, destination) || this;
            _this.connectable = connectable;
            return _this;
        }
        RefCountSubscriber.prototype._unsubscribe = function () {
            var connectable = this.connectable;
            if (!connectable) {
                this.connection = null;
                return;
            }
            this.connectable = null;
            var refCount = connectable._refCount;
            if (refCount <= 0) {
                this.connection = null;
                return;
            }
            connectable._refCount = refCount - 1;
            if (refCount > 1) {
                this.connection = null;
                return;
            }
            var connection = this.connection;
            var sharedConnection = connectable._connection;
            this.connection = null;
            if (sharedConnection && (!connection || sharedConnection === connection)) {
                sharedConnection.unsubscribe();
            }
        };
        return RefCountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_Observable,_Subscriber,_Subscription,_operators_refCount PURE_IMPORTS_END */
    var ConnectableObservable = /*@__PURE__*/ (function (_super) {
        __extends(ConnectableObservable, _super);
        function ConnectableObservable(source, subjectFactory) {
            var _this = _super.call(this) || this;
            _this.source = source;
            _this.subjectFactory = subjectFactory;
            _this._refCount = 0;
            _this._isComplete = false;
            return _this;
        }
        ConnectableObservable.prototype._subscribe = function (subscriber) {
            return this.getSubject().subscribe(subscriber);
        };
        ConnectableObservable.prototype.getSubject = function () {
            var subject = this._subject;
            if (!subject || subject.isStopped) {
                this._subject = this.subjectFactory();
            }
            return this._subject;
        };
        ConnectableObservable.prototype.connect = function () {
            var connection = this._connection;
            if (!connection) {
                this._isComplete = false;
                connection = this._connection = new Subscription();
                connection.add(this.source
                    .subscribe(new ConnectableSubscriber(this.getSubject(), this)));
                if (connection.closed) {
                    this._connection = null;
                    connection = Subscription.EMPTY;
                }
                else {
                    this._connection = connection;
                }
            }
            return connection;
        };
        ConnectableObservable.prototype.refCount = function () {
            return refCount()(this);
        };
        return ConnectableObservable;
    }(Observable));
    var connectableProto = ConnectableObservable.prototype;
    var connectableObservableDescriptor = {
        operator: { value: null },
        _refCount: { value: 0, writable: true },
        _subject: { value: null, writable: true },
        _connection: { value: null, writable: true },
        _subscribe: { value: connectableProto._subscribe },
        _isComplete: { value: connectableProto._isComplete, writable: true },
        getSubject: { value: connectableProto.getSubject },
        connect: { value: connectableProto.connect },
        refCount: { value: connectableProto.refCount }
    };
    var ConnectableSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ConnectableSubscriber, _super);
        function ConnectableSubscriber(destination, connectable) {
            var _this = _super.call(this, destination) || this;
            _this.connectable = connectable;
            return _this;
        }
        ConnectableSubscriber.prototype._error = function (err) {
            this._unsubscribe();
            _super.prototype._error.call(this, err);
        };
        ConnectableSubscriber.prototype._complete = function () {
            this.connectable._isComplete = true;
            this._unsubscribe();
            _super.prototype._complete.call(this);
        };
        ConnectableSubscriber.prototype._unsubscribe = function () {
            var connectable = this.connectable;
            if (connectable) {
                this.connectable = null;
                var connection = connectable._connection;
                connectable._refCount = 0;
                connectable._subject = null;
                connectable._connection = null;
                if (connection) {
                    connection.unsubscribe();
                }
            }
        };
        return ConnectableSubscriber;
    }(SubjectSubscriber));
    var RefCountSubscriber$1 = /*@__PURE__*/ (function (_super) {
        __extends(RefCountSubscriber, _super);
        function RefCountSubscriber(destination, connectable) {
            var _this = _super.call(this, destination) || this;
            _this.connectable = connectable;
            return _this;
        }
        RefCountSubscriber.prototype._unsubscribe = function () {
            var connectable = this.connectable;
            if (!connectable) {
                this.connection = null;
                return;
            }
            this.connectable = null;
            var refCount$$1 = connectable._refCount;
            if (refCount$$1 <= 0) {
                this.connection = null;
                return;
            }
            connectable._refCount = refCount$$1 - 1;
            if (refCount$$1 > 1) {
                this.connection = null;
                return;
            }
            var connection = this.connection;
            var sharedConnection = connectable._connection;
            this.connection = null;
            if (sharedConnection && (!connection || sharedConnection === connection)) {
                sharedConnection.unsubscribe();
            }
        };
        return RefCountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _observable_ConnectableObservable PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber,_Notification PURE_IMPORTS_END */
    var ObserveOnSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ObserveOnSubscriber, _super);
        function ObserveOnSubscriber(destination, scheduler, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            var _this = _super.call(this, destination) || this;
            _this.scheduler = scheduler;
            _this.delay = delay;
            return _this;
        }
        ObserveOnSubscriber.dispatch = function (arg) {
            var notification = arg.notification, destination = arg.destination;
            notification.observe(destination);
            this.unsubscribe();
        };
        ObserveOnSubscriber.prototype.scheduleMessage = function (notification) {
            this.add(this.scheduler.schedule(ObserveOnSubscriber.dispatch, this.delay, new ObserveOnMessage(notification, this.destination)));
        };
        ObserveOnSubscriber.prototype._next = function (value) {
            this.scheduleMessage(Notification.createNext(value));
        };
        ObserveOnSubscriber.prototype._error = function (err) {
            this.scheduleMessage(Notification.createError(err));
        };
        ObserveOnSubscriber.prototype._complete = function () {
            this.scheduleMessage(Notification.createComplete());
        };
        return ObserveOnSubscriber;
    }(Subscriber));
    var ObserveOnMessage = /*@__PURE__*/ (function () {
        function ObserveOnMessage(notification, destination) {
            this.notification = notification;
            this.destination = destination;
        }
        return ObserveOnMessage;
    }());

    /** PURE_IMPORTS_START tslib,_observable_from,_util_isArray,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var OnErrorResumeNextSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(OnErrorResumeNextSubscriber, _super);
        function OnErrorResumeNextSubscriber(destination, nextSources) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            _this.nextSources = nextSources;
            return _this;
        }
        OnErrorResumeNextSubscriber.prototype.notifyError = function (error, innerSub) {
            this.subscribeToNextSource();
        };
        OnErrorResumeNextSubscriber.prototype.notifyComplete = function (innerSub) {
            this.subscribeToNextSource();
        };
        OnErrorResumeNextSubscriber.prototype._error = function (err) {
            this.subscribeToNextSource();
        };
        OnErrorResumeNextSubscriber.prototype._complete = function () {
            this.subscribeToNextSource();
        };
        OnErrorResumeNextSubscriber.prototype.subscribeToNextSource = function () {
            var next = this.nextSources.shift();
            if (next) {
                this.add(subscribeToResult(this, next));
            }
            else {
                this.destination.complete();
            }
        };
        return OnErrorResumeNextSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var PairwiseSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(PairwiseSubscriber, _super);
        function PairwiseSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.hasPrev = false;
            return _this;
        }
        PairwiseSubscriber.prototype._next = function (value) {
            if (this.hasPrev) {
                this.destination.next([this.prev, value]);
            }
            else {
                this.hasPrev = true;
            }
            this.prev = value;
        };
        return PairwiseSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _util_not,_filter PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _map PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _Subject,_multicast PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subject,_util_ObjectUnsubscribedError PURE_IMPORTS_END */
    var BehaviorSubject = /*@__PURE__*/ (function (_super) {
        __extends(BehaviorSubject, _super);
        function BehaviorSubject(_value) {
            var _this = _super.call(this) || this;
            _this._value = _value;
            return _this;
        }
        Object.defineProperty(BehaviorSubject.prototype, "value", {
            get: function () {
                return this.getValue();
            },
            enumerable: true,
            configurable: true
        });
        BehaviorSubject.prototype._subscribe = function (subscriber) {
            var subscription = _super.prototype._subscribe.call(this, subscriber);
            if (subscription && !subscription.closed) {
                subscriber.next(this._value);
            }
            return subscription;
        };
        BehaviorSubject.prototype.getValue = function () {
            if (this.hasError) {
                throw this.thrownError;
            }
            else if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else {
                return this._value;
            }
        };
        BehaviorSubject.prototype.next = function (value) {
            _super.prototype.next.call(this, this._value = value);
        };
        return BehaviorSubject;
    }(Subject));

    /** PURE_IMPORTS_START _BehaviorSubject,_multicast PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subject,_Subscription PURE_IMPORTS_END */
    var AsyncSubject = /*@__PURE__*/ (function (_super) {
        __extends(AsyncSubject, _super);
        function AsyncSubject() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.value = null;
            _this.hasNext = false;
            _this.hasCompleted = false;
            return _this;
        }
        AsyncSubject.prototype._subscribe = function (subscriber) {
            if (this.hasError) {
                subscriber.error(this.thrownError);
                return Subscription.EMPTY;
            }
            else if (this.hasCompleted && this.hasNext) {
                subscriber.next(this.value);
                subscriber.complete();
                return Subscription.EMPTY;
            }
            return _super.prototype._subscribe.call(this, subscriber);
        };
        AsyncSubject.prototype.next = function (value) {
            if (!this.hasCompleted) {
                this.value = value;
                this.hasNext = true;
            }
        };
        AsyncSubject.prototype.error = function (error) {
            if (!this.hasCompleted) {
                _super.prototype.error.call(this, error);
            }
        };
        AsyncSubject.prototype.complete = function () {
            this.hasCompleted = true;
            if (this.hasNext) {
                _super.prototype.next.call(this, this.value);
            }
            _super.prototype.complete.call(this);
        };
        return AsyncSubject;
    }(Subject));

    /** PURE_IMPORTS_START _AsyncSubject,_multicast PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_AsyncAction PURE_IMPORTS_END */
    var QueueAction = /*@__PURE__*/ (function (_super) {
        __extends(QueueAction, _super);
        function QueueAction(scheduler, work) {
            var _this = _super.call(this, scheduler, work) || this;
            _this.scheduler = scheduler;
            _this.work = work;
            return _this;
        }
        QueueAction.prototype.schedule = function (state, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if (delay > 0) {
                return _super.prototype.schedule.call(this, state, delay);
            }
            this.delay = delay;
            this.state = state;
            this.scheduler.flush(this);
            return this;
        };
        QueueAction.prototype.execute = function (state, delay) {
            return (delay > 0 || this.closed) ?
                _super.prototype.execute.call(this, state, delay) :
                this._execute(state, delay);
        };
        QueueAction.prototype.requestAsyncId = function (scheduler, id, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if ((delay !== null && delay > 0) || (delay === null && this.delay > 0)) {
                return _super.prototype.requestAsyncId.call(this, scheduler, id, delay);
            }
            return scheduler.flush(this);
        };
        return QueueAction;
    }(AsyncAction));

    /** PURE_IMPORTS_START tslib,_AsyncScheduler PURE_IMPORTS_END */
    var QueueScheduler = /*@__PURE__*/ (function (_super) {
        __extends(QueueScheduler, _super);
        function QueueScheduler() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return QueueScheduler;
    }(AsyncScheduler));

    /** PURE_IMPORTS_START _QueueAction,_QueueScheduler PURE_IMPORTS_END */
    var queue = /*@__PURE__*/ new QueueScheduler(QueueAction);

    /** PURE_IMPORTS_START tslib,_Subject,_scheduler_queue,_Subscription,_operators_observeOn,_util_ObjectUnsubscribedError,_SubjectSubscription PURE_IMPORTS_END */
    var ReplaySubject = /*@__PURE__*/ (function (_super) {
        __extends(ReplaySubject, _super);
        function ReplaySubject(bufferSize, windowTime, scheduler) {
            if (bufferSize === void 0) {
                bufferSize = Number.POSITIVE_INFINITY;
            }
            if (windowTime === void 0) {
                windowTime = Number.POSITIVE_INFINITY;
            }
            var _this = _super.call(this) || this;
            _this.scheduler = scheduler;
            _this._events = [];
            _this._infiniteTimeWindow = false;
            _this._bufferSize = bufferSize < 1 ? 1 : bufferSize;
            _this._windowTime = windowTime < 1 ? 1 : windowTime;
            if (windowTime === Number.POSITIVE_INFINITY) {
                _this._infiniteTimeWindow = true;
                _this.next = _this.nextInfiniteTimeWindow;
            }
            else {
                _this.next = _this.nextTimeWindow;
            }
            return _this;
        }
        ReplaySubject.prototype.nextInfiniteTimeWindow = function (value) {
            var _events = this._events;
            _events.push(value);
            if (_events.length > this._bufferSize) {
                _events.shift();
            }
            _super.prototype.next.call(this, value);
        };
        ReplaySubject.prototype.nextTimeWindow = function (value) {
            this._events.push(new ReplayEvent(this._getNow(), value));
            this._trimBufferThenGetEvents();
            _super.prototype.next.call(this, value);
        };
        ReplaySubject.prototype._subscribe = function (subscriber) {
            var _infiniteTimeWindow = this._infiniteTimeWindow;
            var _events = _infiniteTimeWindow ? this._events : this._trimBufferThenGetEvents();
            var scheduler = this.scheduler;
            var len = _events.length;
            var subscription;
            if (this.closed) {
                throw new ObjectUnsubscribedError();
            }
            else if (this.isStopped || this.hasError) {
                subscription = Subscription.EMPTY;
            }
            else {
                this.observers.push(subscriber);
                subscription = new SubjectSubscription(this, subscriber);
            }
            if (scheduler) {
                subscriber.add(subscriber = new ObserveOnSubscriber(subscriber, scheduler));
            }
            if (_infiniteTimeWindow) {
                for (var i = 0; i < len && !subscriber.closed; i++) {
                    subscriber.next(_events[i]);
                }
            }
            else {
                for (var i = 0; i < len && !subscriber.closed; i++) {
                    subscriber.next(_events[i].value);
                }
            }
            if (this.hasError) {
                subscriber.error(this.thrownError);
            }
            else if (this.isStopped) {
                subscriber.complete();
            }
            return subscription;
        };
        ReplaySubject.prototype._getNow = function () {
            return (this.scheduler || queue).now();
        };
        ReplaySubject.prototype._trimBufferThenGetEvents = function () {
            var now = this._getNow();
            var _bufferSize = this._bufferSize;
            var _windowTime = this._windowTime;
            var _events = this._events;
            var eventsCount = _events.length;
            var spliceCount = 0;
            while (spliceCount < eventsCount) {
                if ((now - _events[spliceCount].time) < _windowTime) {
                    break;
                }
                spliceCount++;
            }
            if (eventsCount > _bufferSize) {
                spliceCount = Math.max(spliceCount, eventsCount - _bufferSize);
            }
            if (spliceCount > 0) {
                _events.splice(0, spliceCount);
            }
            return _events;
        };
        return ReplaySubject;
    }(Subject));
    var ReplayEvent = /*@__PURE__*/ (function () {
        function ReplayEvent(time, value) {
            this.time = time;
            this.value = value;
        }
        return ReplayEvent;
    }());

    /** PURE_IMPORTS_START _ReplaySubject,_multicast PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_util_isArray,_fromArray,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var RaceSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RaceSubscriber, _super);
        function RaceSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.hasFirst = false;
            _this.observables = [];
            _this.subscriptions = [];
            return _this;
        }
        RaceSubscriber.prototype._next = function (observable) {
            this.observables.push(observable);
        };
        RaceSubscriber.prototype._complete = function () {
            var observables = this.observables;
            var len = observables.length;
            if (len === 0) {
                this.destination.complete();
            }
            else {
                for (var i = 0; i < len && !this.hasFirst; i++) {
                    var observable = observables[i];
                    var subscription = subscribeToResult(this, observable, observable, i);
                    if (this.subscriptions) {
                        this.subscriptions.push(subscription);
                    }
                    this.add(subscription);
                }
                this.observables = null;
            }
        };
        RaceSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            if (!this.hasFirst) {
                this.hasFirst = true;
                for (var i = 0; i < this.subscriptions.length; i++) {
                    if (i !== outerIndex) {
                        var subscription = this.subscriptions[i];
                        subscription.unsubscribe();
                        this.remove(subscription);
                    }
                }
                this.subscriptions = null;
            }
            this.destination.next(innerValue);
        };
        return RaceSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _util_isArray,_observable_race PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber,_observable_empty PURE_IMPORTS_END */
    var RepeatSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RepeatSubscriber, _super);
        function RepeatSubscriber(destination, count, source) {
            var _this = _super.call(this, destination) || this;
            _this.count = count;
            _this.source = source;
            return _this;
        }
        RepeatSubscriber.prototype.complete = function () {
            if (!this.isStopped) {
                var _a = this, source = _a.source, count = _a.count;
                if (count === 0) {
                    return _super.prototype.complete.call(this);
                }
                else if (count > -1) {
                    this.count = count - 1;
                }
                source.subscribe(this._unsubscribeAndRecycle());
            }
        };
        return RepeatSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var RepeatWhenSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RepeatWhenSubscriber, _super);
        function RepeatWhenSubscriber(destination, notifier, source) {
            var _this = _super.call(this, destination) || this;
            _this.notifier = notifier;
            _this.source = source;
            _this.sourceIsBeingSubscribedTo = true;
            return _this;
        }
        RepeatWhenSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.sourceIsBeingSubscribedTo = true;
            this.source.subscribe(this);
        };
        RepeatWhenSubscriber.prototype.notifyComplete = function (innerSub) {
            if (this.sourceIsBeingSubscribedTo === false) {
                return _super.prototype.complete.call(this);
            }
        };
        RepeatWhenSubscriber.prototype.complete = function () {
            this.sourceIsBeingSubscribedTo = false;
            if (!this.isStopped) {
                if (!this.retries) {
                    this.subscribeToRetries();
                }
                if (!this.retriesSubscription || this.retriesSubscription.closed) {
                    return _super.prototype.complete.call(this);
                }
                this._unsubscribeAndRecycle();
                this.notifications.next();
            }
        };
        RepeatWhenSubscriber.prototype._unsubscribe = function () {
            var _a = this, notifications = _a.notifications, retriesSubscription = _a.retriesSubscription;
            if (notifications) {
                notifications.unsubscribe();
                this.notifications = null;
            }
            if (retriesSubscription) {
                retriesSubscription.unsubscribe();
                this.retriesSubscription = null;
            }
            this.retries = null;
        };
        RepeatWhenSubscriber.prototype._unsubscribeAndRecycle = function () {
            var _unsubscribe = this._unsubscribe;
            this._unsubscribe = null;
            _super.prototype._unsubscribeAndRecycle.call(this);
            this._unsubscribe = _unsubscribe;
            return this;
        };
        RepeatWhenSubscriber.prototype.subscribeToRetries = function () {
            this.notifications = new Subject();
            var retries = tryCatch(this.notifier)(this.notifications);
            if (retries === errorObject) {
                return _super.prototype.complete.call(this);
            }
            this.retries = retries;
            this.retriesSubscription = subscribeToResult(this, retries);
        };
        return RepeatWhenSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var RetrySubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RetrySubscriber, _super);
        function RetrySubscriber(destination, count, source) {
            var _this = _super.call(this, destination) || this;
            _this.count = count;
            _this.source = source;
            return _this;
        }
        RetrySubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var _a = this, source = _a.source, count = _a.count;
                if (count === 0) {
                    return _super.prototype.error.call(this, err);
                }
                else if (count > -1) {
                    this.count = count - 1;
                }
                source.subscribe(this._unsubscribeAndRecycle());
            }
        };
        return RetrySubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var RetryWhenSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(RetryWhenSubscriber, _super);
        function RetryWhenSubscriber(destination, notifier, source) {
            var _this = _super.call(this, destination) || this;
            _this.notifier = notifier;
            _this.source = source;
            return _this;
        }
        RetryWhenSubscriber.prototype.error = function (err) {
            if (!this.isStopped) {
                var errors = this.errors;
                var retries = this.retries;
                var retriesSubscription = this.retriesSubscription;
                if (!retries) {
                    errors = new Subject();
                    retries = tryCatch(this.notifier)(errors);
                    if (retries === errorObject) {
                        return _super.prototype.error.call(this, errorObject.e);
                    }
                    retriesSubscription = subscribeToResult(this, retries);
                }
                else {
                    this.errors = null;
                    this.retriesSubscription = null;
                }
                this._unsubscribeAndRecycle();
                this.errors = errors;
                this.retries = retries;
                this.retriesSubscription = retriesSubscription;
                errors.next(err);
            }
        };
        RetryWhenSubscriber.prototype._unsubscribe = function () {
            var _a = this, errors = _a.errors, retriesSubscription = _a.retriesSubscription;
            if (errors) {
                errors.unsubscribe();
                this.errors = null;
            }
            if (retriesSubscription) {
                retriesSubscription.unsubscribe();
                this.retriesSubscription = null;
            }
            this.retries = null;
        };
        RetryWhenSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            var _unsubscribe = this._unsubscribe;
            this._unsubscribe = null;
            this._unsubscribeAndRecycle();
            this._unsubscribe = _unsubscribe;
            this.source.subscribe(this);
        };
        return RetryWhenSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var SampleSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SampleSubscriber, _super);
        function SampleSubscriber() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.hasValue = false;
            return _this;
        }
        SampleSubscriber.prototype._next = function (value) {
            this.value = value;
            this.hasValue = true;
        };
        SampleSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.emitValue();
        };
        SampleSubscriber.prototype.notifyComplete = function () {
            this.emitValue();
        };
        SampleSubscriber.prototype.emitValue = function () {
            if (this.hasValue) {
                this.hasValue = false;
                this.destination.next(this.value);
            }
        };
        return SampleSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_scheduler_async PURE_IMPORTS_END */
    var SampleTimeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SampleTimeSubscriber, _super);
        function SampleTimeSubscriber(destination, period, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.period = period;
            _this.scheduler = scheduler;
            _this.hasValue = false;
            _this.add(scheduler.schedule(dispatchNotification, period, { subscriber: _this, period: period }));
            return _this;
        }
        SampleTimeSubscriber.prototype._next = function (value) {
            this.lastValue = value;
            this.hasValue = true;
        };
        SampleTimeSubscriber.prototype.notifyNext = function () {
            if (this.hasValue) {
                this.hasValue = false;
                this.destination.next(this.lastValue);
            }
        };
        return SampleTimeSubscriber;
    }(Subscriber));
    function dispatchNotification(state) {
        var subscriber = state.subscriber, period = state.period;
        subscriber.notifyNext();
        this.schedule(state, period);
    }

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_tryCatch,_util_errorObject PURE_IMPORTS_END */
    var SequenceEqualSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SequenceEqualSubscriber, _super);
        function SequenceEqualSubscriber(destination, compareTo, comparor) {
            var _this = _super.call(this, destination) || this;
            _this.compareTo = compareTo;
            _this.comparor = comparor;
            _this._a = [];
            _this._b = [];
            _this._oneComplete = false;
            _this.add(compareTo.subscribe(new SequenceEqualCompareToSubscriber(destination, _this)));
            return _this;
        }
        SequenceEqualSubscriber.prototype._next = function (value) {
            if (this._oneComplete && this._b.length === 0) {
                this.emit(false);
            }
            else {
                this._a.push(value);
                this.checkValues();
            }
        };
        SequenceEqualSubscriber.prototype._complete = function () {
            if (this._oneComplete) {
                this.emit(this._a.length === 0 && this._b.length === 0);
            }
            else {
                this._oneComplete = true;
            }
        };
        SequenceEqualSubscriber.prototype.checkValues = function () {
            var _c = this, _a = _c._a, _b = _c._b, comparor = _c.comparor;
            while (_a.length > 0 && _b.length > 0) {
                var a = _a.shift();
                var b = _b.shift();
                var areEqual = false;
                if (comparor) {
                    areEqual = tryCatch(comparor)(a, b);
                    if (areEqual === errorObject) {
                        this.destination.error(errorObject.e);
                    }
                }
                else {
                    areEqual = a === b;
                }
                if (!areEqual) {
                    this.emit(false);
                }
            }
        };
        SequenceEqualSubscriber.prototype.emit = function (value) {
            var destination = this.destination;
            destination.next(value);
            destination.complete();
        };
        SequenceEqualSubscriber.prototype.nextB = function (value) {
            if (this._oneComplete && this._a.length === 0) {
                this.emit(false);
            }
            else {
                this._b.push(value);
                this.checkValues();
            }
        };
        return SequenceEqualSubscriber;
    }(Subscriber));
    var SequenceEqualCompareToSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SequenceEqualCompareToSubscriber, _super);
        function SequenceEqualCompareToSubscriber(destination, parent) {
            var _this = _super.call(this, destination) || this;
            _this.parent = parent;
            return _this;
        }
        SequenceEqualCompareToSubscriber.prototype._next = function (value) {
            this.parent.nextB(value);
        };
        SequenceEqualCompareToSubscriber.prototype._error = function (err) {
            this.parent.error(err);
        };
        SequenceEqualCompareToSubscriber.prototype._complete = function () {
            this.parent._complete();
        };
        return SequenceEqualCompareToSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _multicast,_refCount,_Subject PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _ReplaySubject PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_EmptyError PURE_IMPORTS_END */
    var SingleSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SingleSubscriber, _super);
        function SingleSubscriber(destination, predicate, source) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.source = source;
            _this.seenValue = false;
            _this.index = 0;
            return _this;
        }
        SingleSubscriber.prototype.applySingleValue = function (value) {
            if (this.seenValue) {
                this.destination.error('Sequence contains more than one element');
            }
            else {
                this.seenValue = true;
                this.singleValue = value;
            }
        };
        SingleSubscriber.prototype._next = function (value) {
            var index = this.index++;
            if (this.predicate) {
                this.tryNext(value, index);
            }
            else {
                this.applySingleValue(value);
            }
        };
        SingleSubscriber.prototype.tryNext = function (value, index) {
            try {
                if (this.predicate(value, index, this.source)) {
                    this.applySingleValue(value);
                }
            }
            catch (err) {
                this.destination.error(err);
            }
        };
        SingleSubscriber.prototype._complete = function () {
            var destination = this.destination;
            if (this.index > 0) {
                destination.next(this.seenValue ? this.singleValue : undefined);
                destination.complete();
            }
            else {
                destination.error(new EmptyError);
            }
        };
        return SingleSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var SkipSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SkipSubscriber, _super);
        function SkipSubscriber(destination, total) {
            var _this = _super.call(this, destination) || this;
            _this.total = total;
            _this.count = 0;
            return _this;
        }
        SkipSubscriber.prototype._next = function (x) {
            if (++this.count > this.total) {
                this.destination.next(x);
            }
        };
        return SkipSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_util_ArgumentOutOfRangeError PURE_IMPORTS_END */
    var SkipLastSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SkipLastSubscriber, _super);
        function SkipLastSubscriber(destination, _skipCount) {
            var _this = _super.call(this, destination) || this;
            _this._skipCount = _skipCount;
            _this._count = 0;
            _this._ring = new Array(_skipCount);
            return _this;
        }
        SkipLastSubscriber.prototype._next = function (value) {
            var skipCount = this._skipCount;
            var count = this._count++;
            if (count < skipCount) {
                this._ring[count] = value;
            }
            else {
                var currentIndex = count % skipCount;
                var ring = this._ring;
                var oldValue = ring[currentIndex];
                ring[currentIndex] = value;
                this.destination.next(oldValue);
            }
        };
        return SkipLastSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var SkipUntilSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SkipUntilSubscriber, _super);
        function SkipUntilSubscriber(destination, notifier) {
            var _this = _super.call(this, destination) || this;
            _this.hasValue = false;
            _this.add(_this.innerSubscription = subscribeToResult(_this, notifier));
            return _this;
        }
        SkipUntilSubscriber.prototype._next = function (value) {
            if (this.hasValue) {
                _super.prototype._next.call(this, value);
            }
        };
        SkipUntilSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.hasValue = true;
            if (this.innerSubscription) {
                this.innerSubscription.unsubscribe();
            }
        };
        SkipUntilSubscriber.prototype.notifyComplete = function () {
        };
        return SkipUntilSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var SkipWhileSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SkipWhileSubscriber, _super);
        function SkipWhileSubscriber(destination, predicate) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.skipping = true;
            _this.index = 0;
            return _this;
        }
        SkipWhileSubscriber.prototype._next = function (value) {
            var destination = this.destination;
            if (this.skipping) {
                this.tryCallPredicate(value);
            }
            if (!this.skipping) {
                destination.next(value);
            }
        };
        SkipWhileSubscriber.prototype.tryCallPredicate = function (value) {
            try {
                var result = this.predicate(value, this.index++);
                this.skipping = Boolean(result);
            }
            catch (err) {
                this.destination.error(err);
            }
        };
        return SkipWhileSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START _observable_fromArray,_observable_scalar,_observable_empty,_observable_concat,_util_isScheduler PURE_IMPORTS_END */

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */
    var nextHandle = 1;
    var tasksByHandle = {};
    function runIfPresent(handle) {
        var cb = tasksByHandle[handle];
        if (cb) {
            cb();
        }
    }
    var Immediate = {
        setImmediate: function (cb) {
            var handle = nextHandle++;
            tasksByHandle[handle] = cb;
            Promise.resolve().then(function () { return runIfPresent(handle); });
            return handle;
        },
        clearImmediate: function (handle) {
            delete tasksByHandle[handle];
        },
    };

    /** PURE_IMPORTS_START tslib,_util_Immediate,_AsyncAction PURE_IMPORTS_END */
    var AsapAction = /*@__PURE__*/ (function (_super) {
        __extends(AsapAction, _super);
        function AsapAction(scheduler, work) {
            var _this = _super.call(this, scheduler, work) || this;
            _this.scheduler = scheduler;
            _this.work = work;
            return _this;
        }
        AsapAction.prototype.requestAsyncId = function (scheduler, id, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if (delay !== null && delay > 0) {
                return _super.prototype.requestAsyncId.call(this, scheduler, id, delay);
            }
            scheduler.actions.push(this);
            return scheduler.scheduled || (scheduler.scheduled = Immediate.setImmediate(scheduler.flush.bind(scheduler, null)));
        };
        AsapAction.prototype.recycleAsyncId = function (scheduler, id, delay) {
            if (delay === void 0) {
                delay = 0;
            }
            if ((delay !== null && delay > 0) || (delay === null && this.delay > 0)) {
                return _super.prototype.recycleAsyncId.call(this, scheduler, id, delay);
            }
            if (scheduler.actions.length === 0) {
                Immediate.clearImmediate(id);
                scheduler.scheduled = undefined;
            }
            return undefined;
        };
        return AsapAction;
    }(AsyncAction));

    /** PURE_IMPORTS_START tslib,_AsyncScheduler PURE_IMPORTS_END */
    var AsapScheduler = /*@__PURE__*/ (function (_super) {
        __extends(AsapScheduler, _super);
        function AsapScheduler() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        AsapScheduler.prototype.flush = function (action) {
            this.active = true;
            this.scheduled = undefined;
            var actions = this.actions;
            var error;
            var index = -1;
            var count = actions.length;
            action = action || actions.shift();
            do {
                if (error = action.execute(action.state, action.delay)) {
                    break;
                }
            } while (++index < count && (action = actions.shift()));
            this.active = false;
            if (error) {
                while (++index < count && (action = actions.shift())) {
                    action.unsubscribe();
                }
                throw error;
            }
        };
        return AsapScheduler;
    }(AsyncScheduler));

    /** PURE_IMPORTS_START _AsapAction,_AsapScheduler PURE_IMPORTS_END */
    var asap = /*@__PURE__*/ new AsapScheduler(AsapAction);

    /** PURE_IMPORTS_START tslib,_Observable,_scheduler_asap,_util_isNumeric PURE_IMPORTS_END */
    var SubscribeOnObservable = /*@__PURE__*/ (function (_super) {
        __extends(SubscribeOnObservable, _super);
        function SubscribeOnObservable(source, delayTime, scheduler) {
            if (delayTime === void 0) {
                delayTime = 0;
            }
            if (scheduler === void 0) {
                scheduler = asap;
            }
            var _this = _super.call(this) || this;
            _this.source = source;
            _this.delayTime = delayTime;
            _this.scheduler = scheduler;
            if (!isNumeric(delayTime) || delayTime < 0) {
                _this.delayTime = 0;
            }
            if (!scheduler || typeof scheduler.schedule !== 'function') {
                _this.scheduler = asap;
            }
            return _this;
        }
        SubscribeOnObservable.create = function (source, delay, scheduler) {
            if (delay === void 0) {
                delay = 0;
            }
            if (scheduler === void 0) {
                scheduler = asap;
            }
            return new SubscribeOnObservable(source, delay, scheduler);
        };
        SubscribeOnObservable.dispatch = function (arg) {
            var source = arg.source, subscriber = arg.subscriber;
            return this.add(source.subscribe(subscriber));
        };
        SubscribeOnObservable.prototype._subscribe = function (subscriber) {
            var delay = this.delayTime;
            var source = this.source;
            var scheduler = this.scheduler;
            return scheduler.schedule(SubscribeOnObservable.dispatch, delay, {
                source: source, subscriber: subscriber
            });
        };
        return SubscribeOnObservable;
    }(Observable));

    /** PURE_IMPORTS_START _observable_SubscribeOnObservable PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult,_map,_observable_from PURE_IMPORTS_END */
    var SwitchMapSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(SwitchMapSubscriber, _super);
        function SwitchMapSubscriber(destination, project) {
            var _this = _super.call(this, destination) || this;
            _this.project = project;
            _this.index = 0;
            return _this;
        }
        SwitchMapSubscriber.prototype._next = function (value) {
            var result;
            var index = this.index++;
            try {
                result = this.project(value, index);
            }
            catch (error) {
                this.destination.error(error);
                return;
            }
            this._innerSub(result, value, index);
        };
        SwitchMapSubscriber.prototype._innerSub = function (result, value, index) {
            var innerSubscription = this.innerSubscription;
            if (innerSubscription) {
                innerSubscription.unsubscribe();
            }
            this.add(this.innerSubscription = subscribeToResult(this, result, value, index));
        };
        SwitchMapSubscriber.prototype._complete = function () {
            var innerSubscription = this.innerSubscription;
            if (!innerSubscription || innerSubscription.closed) {
                _super.prototype._complete.call(this);
            }
        };
        SwitchMapSubscriber.prototype._unsubscribe = function () {
            this.innerSubscription = null;
        };
        SwitchMapSubscriber.prototype.notifyComplete = function (innerSub) {
            this.remove(innerSub);
            this.innerSubscription = null;
            if (this.isStopped) {
                _super.prototype._complete.call(this);
            }
        };
        SwitchMapSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.destination.next(innerValue);
        };
        return SwitchMapSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _switchMap,_util_identity PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _switchMap PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    function takeUntil(notifier) {
        return function (source) { return source.lift(new TakeUntilOperator(notifier)); };
    }
    var TakeUntilOperator = /*@__PURE__*/ (function () {
        function TakeUntilOperator(notifier) {
            this.notifier = notifier;
        }
        TakeUntilOperator.prototype.call = function (subscriber, source) {
            var takeUntilSubscriber = new TakeUntilSubscriber(subscriber);
            var notifierSubscription = subscribeToResult(takeUntilSubscriber, this.notifier);
            if (notifierSubscription && !notifierSubscription.closed) {
                takeUntilSubscriber.add(notifierSubscription);
                return source.subscribe(takeUntilSubscriber);
            }
            return takeUntilSubscriber;
        };
        return TakeUntilOperator;
    }());
    var TakeUntilSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TakeUntilSubscriber, _super);
        function TakeUntilSubscriber(destination) {
            return _super.call(this, destination) || this;
        }
        TakeUntilSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.complete();
        };
        TakeUntilSubscriber.prototype.notifyComplete = function () {
        };
        return TakeUntilSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber PURE_IMPORTS_END */
    var TakeWhileSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TakeWhileSubscriber, _super);
        function TakeWhileSubscriber(destination, predicate) {
            var _this = _super.call(this, destination) || this;
            _this.predicate = predicate;
            _this.index = 0;
            return _this;
        }
        TakeWhileSubscriber.prototype._next = function (value) {
            var destination = this.destination;
            var result;
            try {
                result = this.predicate(value, this.index++);
            }
            catch (err) {
                destination.error(err);
                return;
            }
            this.nextOrComplete(value, result);
        };
        TakeWhileSubscriber.prototype.nextOrComplete = function (value, predicateResult) {
            var destination = this.destination;
            if (Boolean(predicateResult)) {
                destination.next(value);
            }
            else {
                destination.complete();
            }
        };
        return TakeWhileSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var ThrottleSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ThrottleSubscriber, _super);
        function ThrottleSubscriber(destination, durationSelector, _leading, _trailing) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            _this.durationSelector = durationSelector;
            _this._leading = _leading;
            _this._trailing = _trailing;
            _this._hasValue = false;
            return _this;
        }
        ThrottleSubscriber.prototype._next = function (value) {
            this._hasValue = true;
            this._sendValue = value;
            if (!this._throttled) {
                if (this._leading) {
                    this.send();
                }
                else {
                    this.throttle(value);
                }
            }
        };
        ThrottleSubscriber.prototype.send = function () {
            var _a = this, _hasValue = _a._hasValue, _sendValue = _a._sendValue;
            if (_hasValue) {
                this.destination.next(_sendValue);
                this.throttle(_sendValue);
            }
            this._hasValue = false;
            this._sendValue = null;
        };
        ThrottleSubscriber.prototype.throttle = function (value) {
            var duration = this.tryDurationSelector(value);
            if (duration) {
                this.add(this._throttled = subscribeToResult(this, duration));
            }
        };
        ThrottleSubscriber.prototype.tryDurationSelector = function (value) {
            try {
                return this.durationSelector(value);
            }
            catch (err) {
                this.destination.error(err);
                return null;
            }
        };
        ThrottleSubscriber.prototype.throttlingDone = function () {
            var _a = this, _throttled = _a._throttled, _trailing = _a._trailing;
            if (_throttled) {
                _throttled.unsubscribe();
            }
            this._throttled = null;
            if (_trailing) {
                this.send();
            }
        };
        ThrottleSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.throttlingDone();
        };
        ThrottleSubscriber.prototype.notifyComplete = function () {
            this.throttlingDone();
        };
        return ThrottleSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_scheduler_async,_throttle PURE_IMPORTS_END */
    var ThrottleTimeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ThrottleTimeSubscriber, _super);
        function ThrottleTimeSubscriber(destination, duration, scheduler, leading, trailing) {
            var _this = _super.call(this, destination) || this;
            _this.duration = duration;
            _this.scheduler = scheduler;
            _this.leading = leading;
            _this.trailing = trailing;
            _this._hasTrailingValue = false;
            _this._trailingValue = null;
            return _this;
        }
        ThrottleTimeSubscriber.prototype._next = function (value) {
            if (this.throttled) {
                if (this.trailing) {
                    this._trailingValue = value;
                    this._hasTrailingValue = true;
                }
            }
            else {
                this.add(this.throttled = this.scheduler.schedule(dispatchNext$1, this.duration, { subscriber: this }));
                if (this.leading) {
                    this.destination.next(value);
                }
            }
        };
        ThrottleTimeSubscriber.prototype._complete = function () {
            if (this._hasTrailingValue) {
                this.destination.next(this._trailingValue);
                this.destination.complete();
            }
            else {
                this.destination.complete();
            }
        };
        ThrottleTimeSubscriber.prototype.clearThrottle = function () {
            var throttled = this.throttled;
            if (throttled) {
                if (this.trailing && this._hasTrailingValue) {
                    this.destination.next(this._trailingValue);
                    this._trailingValue = null;
                    this._hasTrailingValue = false;
                }
                throttled.unsubscribe();
                this.remove(throttled);
                this.throttled = null;
            }
        };
        return ThrottleTimeSubscriber;
    }(Subscriber));
    function dispatchNext$1(arg) {
        var subscriber = arg.subscriber;
        subscriber.clearThrottle();
    }

    /** PURE_IMPORTS_START _Observable,_from,_empty PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _scheduler_async,_scan,_observable_defer,_map PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib PURE_IMPORTS_END */
    var TimeoutError = /*@__PURE__*/ (function (_super) {
        __extends(TimeoutError, _super);
        function TimeoutError() {
            var _this = _super.call(this, 'Timeout has occurred') || this;
            _this.name = 'TimeoutError';
            Object.setPrototypeOf(_this, TimeoutError.prototype);
            return _this;
        }
        return TimeoutError;
    }(Error));

    /** PURE_IMPORTS_START tslib,_scheduler_async,_util_isDate,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var TimeoutWithSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(TimeoutWithSubscriber, _super);
        function TimeoutWithSubscriber(destination, absoluteTimeout, waitFor, withObservable, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.absoluteTimeout = absoluteTimeout;
            _this.waitFor = waitFor;
            _this.withObservable = withObservable;
            _this.scheduler = scheduler;
            _this.action = null;
            _this.scheduleTimeout();
            return _this;
        }
        TimeoutWithSubscriber.dispatchTimeout = function (subscriber) {
            var withObservable = subscriber.withObservable;
            subscriber._unsubscribeAndRecycle();
            subscriber.add(subscribeToResult(subscriber, withObservable));
        };
        TimeoutWithSubscriber.prototype.scheduleTimeout = function () {
            var action = this.action;
            if (action) {
                this.action = action.schedule(this, this.waitFor);
            }
            else {
                this.add(this.action = this.scheduler.schedule(TimeoutWithSubscriber.dispatchTimeout, this.waitFor, this));
            }
        };
        TimeoutWithSubscriber.prototype._next = function (value) {
            if (!this.absoluteTimeout) {
                this.scheduleTimeout();
            }
            _super.prototype._next.call(this, value);
        };
        TimeoutWithSubscriber.prototype._unsubscribe = function () {
            this.action = null;
            this.scheduler = null;
            this.withObservable = null;
        };
        return TimeoutWithSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _scheduler_async,_util_TimeoutError,_timeoutWith,_observable_throwError PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _scheduler_async,_map PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _reduce PURE_IMPORTS_END */

    /** PURE_IMPORTS_START tslib,_Subject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var WindowSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(WindowSubscriber, _super);
        function WindowSubscriber(destination) {
            var _this = _super.call(this, destination) || this;
            _this.window = new Subject();
            destination.next(_this.window);
            return _this;
        }
        WindowSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.openWindow();
        };
        WindowSubscriber.prototype.notifyError = function (error, innerSub) {
            this._error(error);
        };
        WindowSubscriber.prototype.notifyComplete = function (innerSub) {
            this._complete();
        };
        WindowSubscriber.prototype._next = function (value) {
            this.window.next(value);
        };
        WindowSubscriber.prototype._error = function (err) {
            this.window.error(err);
            this.destination.error(err);
        };
        WindowSubscriber.prototype._complete = function () {
            this.window.complete();
            this.destination.complete();
        };
        WindowSubscriber.prototype._unsubscribe = function () {
            this.window = null;
        };
        WindowSubscriber.prototype.openWindow = function () {
            var prevWindow = this.window;
            if (prevWindow) {
                prevWindow.complete();
            }
            var destination = this.destination;
            var newWindow = this.window = new Subject();
            destination.next(newWindow);
        };
        return WindowSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subscriber,_Subject PURE_IMPORTS_END */
    var WindowCountSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(WindowCountSubscriber, _super);
        function WindowCountSubscriber(destination, windowSize, startWindowEvery) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            _this.windowSize = windowSize;
            _this.startWindowEvery = startWindowEvery;
            _this.windows = [new Subject()];
            _this.count = 0;
            destination.next(_this.windows[0]);
            return _this;
        }
        WindowCountSubscriber.prototype._next = function (value) {
            var startWindowEvery = (this.startWindowEvery > 0) ? this.startWindowEvery : this.windowSize;
            var destination = this.destination;
            var windowSize = this.windowSize;
            var windows = this.windows;
            var len = windows.length;
            for (var i = 0; i < len && !this.closed; i++) {
                windows[i].next(value);
            }
            var c = this.count - windowSize + 1;
            if (c >= 0 && c % startWindowEvery === 0 && !this.closed) {
                windows.shift().complete();
            }
            if (++this.count % startWindowEvery === 0 && !this.closed) {
                var window_1 = new Subject();
                windows.push(window_1);
                destination.next(window_1);
            }
        };
        WindowCountSubscriber.prototype._error = function (err) {
            var windows = this.windows;
            if (windows) {
                while (windows.length > 0 && !this.closed) {
                    windows.shift().error(err);
                }
            }
            this.destination.error(err);
        };
        WindowCountSubscriber.prototype._complete = function () {
            var windows = this.windows;
            if (windows) {
                while (windows.length > 0 && !this.closed) {
                    windows.shift().complete();
                }
            }
            this.destination.complete();
        };
        WindowCountSubscriber.prototype._unsubscribe = function () {
            this.count = 0;
            this.windows = null;
        };
        return WindowCountSubscriber;
    }(Subscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_scheduler_async,_Subscriber,_util_isNumeric,_util_isScheduler PURE_IMPORTS_END */
    var CountedSubject = /*@__PURE__*/ (function (_super) {
        __extends(CountedSubject, _super);
        function CountedSubject() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._numberOfNextedValues = 0;
            return _this;
        }
        CountedSubject.prototype.next = function (value) {
            this._numberOfNextedValues++;
            _super.prototype.next.call(this, value);
        };
        Object.defineProperty(CountedSubject.prototype, "numberOfNextedValues", {
            get: function () {
                return this._numberOfNextedValues;
            },
            enumerable: true,
            configurable: true
        });
        return CountedSubject;
    }(Subject));
    var WindowTimeSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(WindowTimeSubscriber, _super);
        function WindowTimeSubscriber(destination, windowTimeSpan, windowCreationInterval, maxWindowSize, scheduler) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            _this.windowTimeSpan = windowTimeSpan;
            _this.windowCreationInterval = windowCreationInterval;
            _this.maxWindowSize = maxWindowSize;
            _this.scheduler = scheduler;
            _this.windows = [];
            var window = _this.openWindow();
            if (windowCreationInterval !== null && windowCreationInterval >= 0) {
                var closeState = { subscriber: _this, window: window, context: null };
                var creationState = { windowTimeSpan: windowTimeSpan, windowCreationInterval: windowCreationInterval, subscriber: _this, scheduler: scheduler };
                _this.add(scheduler.schedule(dispatchWindowClose, windowTimeSpan, closeState));
                _this.add(scheduler.schedule(dispatchWindowCreation, windowCreationInterval, creationState));
            }
            else {
                var timeSpanOnlyState = { subscriber: _this, window: window, windowTimeSpan: windowTimeSpan };
                _this.add(scheduler.schedule(dispatchWindowTimeSpanOnly, windowTimeSpan, timeSpanOnlyState));
            }
            return _this;
        }
        WindowTimeSubscriber.prototype._next = function (value) {
            var windows = this.windows;
            var len = windows.length;
            for (var i = 0; i < len; i++) {
                var window_1 = windows[i];
                if (!window_1.closed) {
                    window_1.next(value);
                    if (window_1.numberOfNextedValues >= this.maxWindowSize) {
                        this.closeWindow(window_1);
                    }
                }
            }
        };
        WindowTimeSubscriber.prototype._error = function (err) {
            var windows = this.windows;
            while (windows.length > 0) {
                windows.shift().error(err);
            }
            this.destination.error(err);
        };
        WindowTimeSubscriber.prototype._complete = function () {
            var windows = this.windows;
            while (windows.length > 0) {
                var window_2 = windows.shift();
                if (!window_2.closed) {
                    window_2.complete();
                }
            }
            this.destination.complete();
        };
        WindowTimeSubscriber.prototype.openWindow = function () {
            var window = new CountedSubject();
            this.windows.push(window);
            var destination = this.destination;
            destination.next(window);
            return window;
        };
        WindowTimeSubscriber.prototype.closeWindow = function (window) {
            window.complete();
            var windows = this.windows;
            windows.splice(windows.indexOf(window), 1);
        };
        return WindowTimeSubscriber;
    }(Subscriber));
    function dispatchWindowTimeSpanOnly(state) {
        var subscriber = state.subscriber, windowTimeSpan = state.windowTimeSpan, window = state.window;
        if (window) {
            subscriber.closeWindow(window);
        }
        state.window = subscriber.openWindow();
        this.schedule(state, windowTimeSpan);
    }
    function dispatchWindowCreation(state) {
        var windowTimeSpan = state.windowTimeSpan, subscriber = state.subscriber, scheduler = state.scheduler, windowCreationInterval = state.windowCreationInterval;
        var window = subscriber.openWindow();
        var action = this;
        var context = { action: action, subscription: null };
        var timeSpanState = { subscriber: subscriber, window: window, context: context };
        context.subscription = scheduler.schedule(dispatchWindowClose, windowTimeSpan, timeSpanState);
        action.add(context.subscription);
        action.schedule(state, windowCreationInterval);
    }
    function dispatchWindowClose(state) {
        var subscriber = state.subscriber, window = state.window, context = state.context;
        if (context && context.action && context.subscription) {
            context.action.remove(context.subscription);
        }
        subscriber.closeWindow(window);
    }

    /** PURE_IMPORTS_START tslib,_Subject,_Subscription,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var WindowToggleSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(WindowToggleSubscriber, _super);
        function WindowToggleSubscriber(destination, openings, closingSelector) {
            var _this = _super.call(this, destination) || this;
            _this.openings = openings;
            _this.closingSelector = closingSelector;
            _this.contexts = [];
            _this.add(_this.openSubscription = subscribeToResult(_this, openings, openings));
            return _this;
        }
        WindowToggleSubscriber.prototype._next = function (value) {
            var contexts = this.contexts;
            if (contexts) {
                var len = contexts.length;
                for (var i = 0; i < len; i++) {
                    contexts[i].window.next(value);
                }
            }
        };
        WindowToggleSubscriber.prototype._error = function (err) {
            var contexts = this.contexts;
            this.contexts = null;
            if (contexts) {
                var len = contexts.length;
                var index = -1;
                while (++index < len) {
                    var context_1 = contexts[index];
                    context_1.window.error(err);
                    context_1.subscription.unsubscribe();
                }
            }
            _super.prototype._error.call(this, err);
        };
        WindowToggleSubscriber.prototype._complete = function () {
            var contexts = this.contexts;
            this.contexts = null;
            if (contexts) {
                var len = contexts.length;
                var index = -1;
                while (++index < len) {
                    var context_2 = contexts[index];
                    context_2.window.complete();
                    context_2.subscription.unsubscribe();
                }
            }
            _super.prototype._complete.call(this);
        };
        WindowToggleSubscriber.prototype._unsubscribe = function () {
            var contexts = this.contexts;
            this.contexts = null;
            if (contexts) {
                var len = contexts.length;
                var index = -1;
                while (++index < len) {
                    var context_3 = contexts[index];
                    context_3.window.unsubscribe();
                    context_3.subscription.unsubscribe();
                }
            }
        };
        WindowToggleSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            if (outerValue === this.openings) {
                var closingSelector = this.closingSelector;
                var closingNotifier = tryCatch(closingSelector)(innerValue);
                if (closingNotifier === errorObject) {
                    return this.error(errorObject.e);
                }
                else {
                    var window_1 = new Subject();
                    var subscription = new Subscription();
                    var context_4 = { window: window_1, subscription: subscription };
                    this.contexts.push(context_4);
                    var innerSubscription = subscribeToResult(this, closingNotifier, context_4);
                    if (innerSubscription.closed) {
                        this.closeWindow(this.contexts.length - 1);
                    }
                    else {
                        innerSubscription.context = context_4;
                        subscription.add(innerSubscription);
                    }
                    this.destination.next(window_1);
                }
            }
            else {
                this.closeWindow(this.contexts.indexOf(outerValue));
            }
        };
        WindowToggleSubscriber.prototype.notifyError = function (err) {
            this.error(err);
        };
        WindowToggleSubscriber.prototype.notifyComplete = function (inner) {
            if (inner !== this.openSubscription) {
                this.closeWindow(this.contexts.indexOf(inner.context));
            }
        };
        WindowToggleSubscriber.prototype.closeWindow = function (index) {
            if (index === -1) {
                return;
            }
            var contexts = this.contexts;
            var context = contexts[index];
            var window = context.window, subscription = context.subscription;
            contexts.splice(index, 1);
            window.complete();
            subscription.unsubscribe();
        };
        return WindowToggleSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_Subject,_util_tryCatch,_util_errorObject,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var WindowSubscriber$1 = /*@__PURE__*/ (function (_super) {
        __extends(WindowSubscriber, _super);
        function WindowSubscriber(destination, closingSelector) {
            var _this = _super.call(this, destination) || this;
            _this.destination = destination;
            _this.closingSelector = closingSelector;
            _this.openWindow();
            return _this;
        }
        WindowSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.openWindow(innerSub);
        };
        WindowSubscriber.prototype.notifyError = function (error, innerSub) {
            this._error(error);
        };
        WindowSubscriber.prototype.notifyComplete = function (innerSub) {
            this.openWindow(innerSub);
        };
        WindowSubscriber.prototype._next = function (value) {
            this.window.next(value);
        };
        WindowSubscriber.prototype._error = function (err) {
            this.window.error(err);
            this.destination.error(err);
            this.unsubscribeClosingNotification();
        };
        WindowSubscriber.prototype._complete = function () {
            this.window.complete();
            this.destination.complete();
            this.unsubscribeClosingNotification();
        };
        WindowSubscriber.prototype.unsubscribeClosingNotification = function () {
            if (this.closingNotification) {
                this.closingNotification.unsubscribe();
            }
        };
        WindowSubscriber.prototype.openWindow = function (innerSub) {
            if (innerSub === void 0) {
                innerSub = null;
            }
            if (innerSub) {
                this.remove(innerSub);
                innerSub.unsubscribe();
            }
            var prevWindow = this.window;
            if (prevWindow) {
                prevWindow.complete();
            }
            var window = this.window = new Subject();
            this.destination.next(window);
            var closingNotifier = tryCatch(this.closingSelector)();
            if (closingNotifier === errorObject) {
                var err = errorObject.e;
                this.destination.error(err);
                this.window.error(err);
            }
            else {
                this.add(this.closingNotification = subscribeToResult(this, closingNotifier));
            }
        };
        return WindowSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_OuterSubscriber,_util_subscribeToResult PURE_IMPORTS_END */
    var WithLatestFromSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(WithLatestFromSubscriber, _super);
        function WithLatestFromSubscriber(destination, observables, project) {
            var _this = _super.call(this, destination) || this;
            _this.observables = observables;
            _this.project = project;
            _this.toRespond = [];
            var len = observables.length;
            _this.values = new Array(len);
            for (var i = 0; i < len; i++) {
                _this.toRespond.push(i);
            }
            for (var i = 0; i < len; i++) {
                var observable = observables[i];
                _this.add(subscribeToResult(_this, observable, observable, i));
            }
            return _this;
        }
        WithLatestFromSubscriber.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.values[outerIndex] = innerValue;
            var toRespond = this.toRespond;
            if (toRespond.length > 0) {
                var found = toRespond.indexOf(outerIndex);
                if (found !== -1) {
                    toRespond.splice(found, 1);
                }
            }
        };
        WithLatestFromSubscriber.prototype.notifyComplete = function () {
        };
        WithLatestFromSubscriber.prototype._next = function (value) {
            if (this.toRespond.length === 0) {
                var args = [value].concat(this.values);
                if (this.project) {
                    this._tryProject(args);
                }
                else {
                    this.destination.next(args);
                }
            }
        };
        WithLatestFromSubscriber.prototype._tryProject = function (args) {
            var result;
            try {
                result = this.project.apply(this, args);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(result);
        };
        return WithLatestFromSubscriber;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START tslib,_fromArray,_util_isArray,_Subscriber,_OuterSubscriber,_util_subscribeToResult,_.._internal_symbol_iterator PURE_IMPORTS_END */
    var ZipSubscriber = /*@__PURE__*/ (function (_super) {
        __extends(ZipSubscriber, _super);
        function ZipSubscriber(destination, resultSelector, values) {
            if (values === void 0) {
                values = Object.create(null);
            }
            var _this = _super.call(this, destination) || this;
            _this.iterators = [];
            _this.active = 0;
            _this.resultSelector = (typeof resultSelector === 'function') ? resultSelector : null;
            _this.values = values;
            return _this;
        }
        ZipSubscriber.prototype._next = function (value) {
            var iterators = this.iterators;
            if (isArray(value)) {
                iterators.push(new StaticArrayIterator(value));
            }
            else if (typeof value[iterator] === 'function') {
                iterators.push(new StaticIterator(value[iterator]()));
            }
            else {
                iterators.push(new ZipBufferIterator(this.destination, this, value));
            }
        };
        ZipSubscriber.prototype._complete = function () {
            var iterators = this.iterators;
            var len = iterators.length;
            if (len === 0) {
                this.destination.complete();
                return;
            }
            this.active = len;
            for (var i = 0; i < len; i++) {
                var iterator$$1 = iterators[i];
                if (iterator$$1.stillUnsubscribed) {
                    this.add(iterator$$1.subscribe(iterator$$1, i));
                }
                else {
                    this.active--;
                }
            }
        };
        ZipSubscriber.prototype.notifyInactive = function () {
            this.active--;
            if (this.active === 0) {
                this.destination.complete();
            }
        };
        ZipSubscriber.prototype.checkIterators = function () {
            var iterators = this.iterators;
            var len = iterators.length;
            var destination = this.destination;
            for (var i = 0; i < len; i++) {
                var iterator$$1 = iterators[i];
                if (typeof iterator$$1.hasValue === 'function' && !iterator$$1.hasValue()) {
                    return;
                }
            }
            var shouldComplete = false;
            var args = [];
            for (var i = 0; i < len; i++) {
                var iterator$$1 = iterators[i];
                var result = iterator$$1.next();
                if (iterator$$1.hasCompleted()) {
                    shouldComplete = true;
                }
                if (result.done) {
                    destination.complete();
                    return;
                }
                args.push(result.value);
            }
            if (this.resultSelector) {
                this._tryresultSelector(args);
            }
            else {
                destination.next(args);
            }
            if (shouldComplete) {
                destination.complete();
            }
        };
        ZipSubscriber.prototype._tryresultSelector = function (args) {
            var result;
            try {
                result = this.resultSelector.apply(this, args);
            }
            catch (err) {
                this.destination.error(err);
                return;
            }
            this.destination.next(result);
        };
        return ZipSubscriber;
    }(Subscriber));
    var StaticIterator = /*@__PURE__*/ (function () {
        function StaticIterator(iterator$$1) {
            this.iterator = iterator$$1;
            this.nextResult = iterator$$1.next();
        }
        StaticIterator.prototype.hasValue = function () {
            return true;
        };
        StaticIterator.prototype.next = function () {
            var result = this.nextResult;
            this.nextResult = this.iterator.next();
            return result;
        };
        StaticIterator.prototype.hasCompleted = function () {
            var nextResult = this.nextResult;
            return nextResult && nextResult.done;
        };
        return StaticIterator;
    }());
    var StaticArrayIterator = /*@__PURE__*/ (function () {
        function StaticArrayIterator(array) {
            this.array = array;
            this.index = 0;
            this.length = 0;
            this.length = array.length;
        }
        StaticArrayIterator.prototype[iterator] = function () {
            return this;
        };
        StaticArrayIterator.prototype.next = function (value) {
            var i = this.index++;
            var array = this.array;
            return i < this.length ? { value: array[i], done: false } : { value: null, done: true };
        };
        StaticArrayIterator.prototype.hasValue = function () {
            return this.array.length > this.index;
        };
        StaticArrayIterator.prototype.hasCompleted = function () {
            return this.array.length === this.index;
        };
        return StaticArrayIterator;
    }());
    var ZipBufferIterator = /*@__PURE__*/ (function (_super) {
        __extends(ZipBufferIterator, _super);
        function ZipBufferIterator(destination, parent, observable) {
            var _this = _super.call(this, destination) || this;
            _this.parent = parent;
            _this.observable = observable;
            _this.stillUnsubscribed = true;
            _this.buffer = [];
            _this.isComplete = false;
            return _this;
        }
        ZipBufferIterator.prototype[iterator] = function () {
            return this;
        };
        ZipBufferIterator.prototype.next = function () {
            var buffer = this.buffer;
            if (buffer.length === 0 && this.isComplete) {
                return { value: null, done: true };
            }
            else {
                return { value: buffer.shift(), done: false };
            }
        };
        ZipBufferIterator.prototype.hasValue = function () {
            return this.buffer.length > 0;
        };
        ZipBufferIterator.prototype.hasCompleted = function () {
            return this.buffer.length === 0 && this.isComplete;
        };
        ZipBufferIterator.prototype.notifyComplete = function () {
            if (this.buffer.length > 0) {
                this.isComplete = true;
                this.parent.notifyInactive();
            }
            else {
                this.destination.complete();
            }
        };
        ZipBufferIterator.prototype.notifyNext = function (outerValue, innerValue, outerIndex, innerIndex, innerSub) {
            this.buffer.push(innerValue);
            this.parent.checkIterators();
        };
        ZipBufferIterator.prototype.subscribe = function (value, index) {
            return subscribeToResult(this, this.observable, this, index);
        };
        return ZipBufferIterator;
    }(OuterSubscriber));

    /** PURE_IMPORTS_START _observable_zip PURE_IMPORTS_END */

    /** PURE_IMPORTS_START _observable_zip PURE_IMPORTS_END */

    /** PURE_IMPORTS_START  PURE_IMPORTS_END */

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Injection token used to inject the document into Directionality.
     * This is used so that the value can be faked in tests.
     *
     * We can't use the real document in tests because changing the real `dir` causes geometry-based
     * tests in Safari to fail.
     *
     * We also can't re-provide the DOCUMENT token from platform-brower because the unit tests
     * themselves use things like `querySelector` in test code.
     *
     * This token is defined in a separate file from Directionality as a workaround for
     * https://github.com/angular/angular/issues/22559
     *
     * \@docs-private
     */
    var /** @type {?} */ DIR_DOCUMENT = new core.InjectionToken('cdk-dir-doc', {
        providedIn: 'root',
        factory: DIR_DOCUMENT_FACTORY,
    });
    /**
     * \@docs-private
     * @return {?}
     */
    function DIR_DOCUMENT_FACTORY() {
        return core.inject(common.DOCUMENT);
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * The directionality (LTR / RTL) context for the application (or a subtree of it).
     * Exposes the current direction and a stream of direction changes.
     */
    var Directionality = /** @class */ (function () {
        function Directionality(_document) {
            /**
             * The current 'ltr' or 'rtl' value.
             */
            this.value = 'ltr';
            /**
             * Stream that emits whenever the 'ltr' / 'rtl' state changes.
             */
            this.change = new core.EventEmitter();
            if (_document) {
                // TODO: handle 'auto' value -
                // We still need to account for dir="auto".
                // It looks like HTMLElemenet.dir is also "auto" when that's set to the attribute,
                // but getComputedStyle return either "ltr" or "rtl". avoiding getComputedStyle for now
                var /** @type {?} */ bodyDir = _document.body ? _document.body.dir : null;
                var /** @type {?} */ htmlDir = _document.documentElement ? _document.documentElement.dir : null;
                this.value = /** @type {?} */ ((bodyDir || htmlDir || 'ltr'));
            }
        }
        /**
         * @return {?}
         */
        Directionality.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.change.complete();
        };
        Directionality.decorators = [
            { type: core.Injectable, args: [{ providedIn: 'root' },] },
        ];
        /** @nocollapse */
        Directionality.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Optional }, { type: core.Inject, args: [DIR_DOCUMENT,] },] },
        ]; };
        /** @nocollapse */ Directionality.ngInjectableDef = core.defineInjectable({ factory: function Directionality_Factory() { return new Directionality(core.inject(DIR_DOCUMENT, 8)); }, token: Directionality, providedIn: "root" });
        return Directionality;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Directive to listen for changes of direction of part of the DOM.
     *
     * Provides itself as Directionality such that descendant directives only need to ever inject
     * Directionality to get the closest direction.
     */
    var Dir = /** @class */ (function () {
        function Dir() {
            this._dir = 'ltr';
            /**
             * Whether the `value` has been set to its initial value.
             */
            this._isInitialized = false;
            /**
             * Event emitted when the direction changes.
             */
            this.change = new core.EventEmitter();
        }
        Object.defineProperty(Dir.prototype, "dir", {
            get: /**
             * \@docs-private
             * @return {?}
             */
            function () { return this._dir; },
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                var /** @type {?} */ old = this._dir;
                this._dir = v;
                if (old !== this._dir && this._isInitialized) {
                    this.change.emit(this._dir);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Dir.prototype, "value", {
            /** Current layout direction of the element. */
            get: /**
             * Current layout direction of the element.
             * @return {?}
             */
            function () { return this.dir; },
            enumerable: true,
            configurable: true
        });
        /** Initialize once default value has been set. */
        /**
         * Initialize once default value has been set.
         * @return {?}
         */
        Dir.prototype.ngAfterContentInit = /**
         * Initialize once default value has been set.
         * @return {?}
         */
        function () {
            this._isInitialized = true;
        };
        /**
         * @return {?}
         */
        Dir.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.change.complete();
        };
        Dir.decorators = [
            { type: core.Directive, args: [{
                        selector: '[dir]',
                        providers: [{ provide: Directionality, useExisting: Dir }],
                        host: { '[dir]': 'dir' },
                        exportAs: 'dir',
                    },] },
        ];
        /** @nocollapse */
        Dir.propDecorators = {
            "change": [{ type: core.Output, args: ['dirChange',] },],
            "dir": [{ type: core.Input },],
        };
        return Dir;
    }());

    /**
     * @license
     * Copyright Google LLC All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Mixin to provide a directive with a function that checks if the sticky input has been
     * changed since the last time the function was called. Essentially adds a dirty-check to the
     * sticky value.
     * @template T
     * @param {?} base
     * @return {?}
     */
    function mixinHasStickyInput(base) {
        return /** @class */ (function (_super) {
            __extends(class_1, _super);
            function class_1() {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                var _this = _super.apply(this, args) || this;
                _this._sticky = false;
                /**
                 * Whether the sticky input has changed since it was last checked.
                 */
                _this._hasStickyChanged = false;
                return _this;
            }
            Object.defineProperty(class_1.prototype, "sticky", {
                /** Whether sticky positioning should be applied. */
                get: /**
                 * Whether sticky positioning should be applied.
                 * @return {?}
                 */
                function () { return this._sticky; },
                set: /**
                 * @param {?} v
                 * @return {?}
                 */
                function (v) {
                    var /** @type {?} */ prevValue = this._sticky;
                    this._sticky = coerceBooleanProperty(v);
                    this._hasStickyChanged = prevValue !== this._sticky;
                },
                enumerable: true,
                configurable: true
            });
            /** Whether the sticky value has changed since this was last called. */
            /**
             * Whether the sticky value has changed since this was last called.
             * @return {?}
             */
            class_1.prototype.hasStickyChanged = /**
             * Whether the sticky value has changed since this was last called.
             * @return {?}
             */
            function () {
                var /** @type {?} */ hasStickyChanged = this._hasStickyChanged;
                this._hasStickyChanged = false;
                return hasStickyChanged;
            };
            /** Resets the dirty check for cases where the sticky state has been used without checking. */
            /**
             * Resets the dirty check for cases where the sticky state has been used without checking.
             * @return {?}
             */
            class_1.prototype.resetStickyChanged = /**
             * Resets the dirty check for cases where the sticky state has been used without checking.
             * @return {?}
             */
            function () {
                this._hasStickyChanged = false;
            };
            return class_1;
        }(base));
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Cell definition for a CDK table.
     * Captures the template of a column's data row cell as well as cell-specific properties.
     */
    var CdkCellDef = /** @class */ (function () {
        function CdkCellDef(template) {
            this.template = template;
        }
        CdkCellDef.decorators = [
            { type: core.Directive, args: [{ selector: '[cdkCellDef]' },] },
        ];
        /** @nocollapse */
        CdkCellDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
        ]; };
        return CdkCellDef;
    }());
    /**
     * Header cell definition for a CDK table.
     * Captures the template of a column's header cell and as well as cell-specific properties.
     */
    var CdkHeaderCellDef = /** @class */ (function () {
        function CdkHeaderCellDef(template) {
            this.template = template;
        }
        CdkHeaderCellDef.decorators = [
            { type: core.Directive, args: [{ selector: '[cdkHeaderCellDef]' },] },
        ];
        /** @nocollapse */
        CdkHeaderCellDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
        ]; };
        return CdkHeaderCellDef;
    }());
    /**
     * Footer cell definition for a CDK table.
     * Captures the template of a column's footer cell and as well as cell-specific properties.
     */
    var CdkFooterCellDef = /** @class */ (function () {
        function CdkFooterCellDef(template) {
            this.template = template;
        }
        CdkFooterCellDef.decorators = [
            { type: core.Directive, args: [{ selector: '[cdkFooterCellDef]' },] },
        ];
        /** @nocollapse */
        CdkFooterCellDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
        ]; };
        return CdkFooterCellDef;
    }());
    /**
     * \@docs-private
     */
    var  /**
     * \@docs-private
     */
    CdkColumnDefBase = /** @class */ (function () {
        function CdkColumnDefBase() {
        }
        return CdkColumnDefBase;
    }());
    var /** @type {?} */ _CdkColumnDefBase = mixinHasStickyInput(CdkColumnDefBase);
    /**
     * Column definition for the CDK table.
     * Defines a set of cells available for a table column.
     */
    var CdkColumnDef = /** @class */ (function (_super) {
        __extends(CdkColumnDef, _super);
        function CdkColumnDef() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._stickyEnd = false;
            return _this;
        }
        Object.defineProperty(CdkColumnDef.prototype, "name", {
            get: /**
             * Unique name for this column.
             * @return {?}
             */
            function () { return this._name; },
            set: /**
             * @param {?} name
             * @return {?}
             */
            function (name) {
                // If the directive is set without a name (updated programatically), then this setter will
                // trigger with an empty string and should not overwrite the programatically set value.
                if (!name) {
                    return;
                }
                this._name = name;
                this.cssClassFriendlyName = name.replace(/[^a-z0-9_-]/ig, '-');
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CdkColumnDef.prototype, "stickyEnd", {
            get: /**
             * Whether this column should be sticky positioned on the end of the row. Should make sure
             * that it mimics the `CanStick` mixin such that `_hasStickyChanged` is set to true if the value
             * has been changed.
             * @return {?}
             */
            function () { return this._stickyEnd; },
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                var /** @type {?} */ prevValue = this._stickyEnd;
                this._stickyEnd = coerceBooleanProperty(v);
                this._hasStickyChanged = prevValue !== this._stickyEnd;
            },
            enumerable: true,
            configurable: true
        });
        CdkColumnDef.decorators = [
            { type: core.Directive, args: [{
                        selector: '[cdkColumnDef]',
                        inputs: ['sticky']
                    },] },
        ];
        /** @nocollapse */
        CdkColumnDef.propDecorators = {
            "name": [{ type: core.Input, args: ['cdkColumnDef',] },],
            "stickyEnd": [{ type: core.Input, args: ['stickyEnd',] },],
            "cell": [{ type: core.ContentChild, args: [CdkCellDef,] },],
            "headerCell": [{ type: core.ContentChild, args: [CdkHeaderCellDef,] },],
            "footerCell": [{ type: core.ContentChild, args: [CdkFooterCellDef,] },],
        };
        return CdkColumnDef;
    }(_CdkColumnDefBase));
    /**
     * Base class for the cells. Adds a CSS classname that identifies the column it renders in.
     */
    var  /**
     * Base class for the cells. Adds a CSS classname that identifies the column it renders in.
     */
    BaseCdkCell = /** @class */ (function () {
        function BaseCdkCell(columnDef, elementRef) {
            var /** @type {?} */ columnClassName = "cdk-column-" + columnDef.cssClassFriendlyName;
            elementRef.nativeElement.classList.add(columnClassName);
        }
        return BaseCdkCell;
    }());
    /**
     * Header cell template container that adds the right classes and role.
     */
    var CdkHeaderCell = /** @class */ (function (_super) {
        __extends(CdkHeaderCell, _super);
        function CdkHeaderCell(columnDef, elementRef) {
            return _super.call(this, columnDef, elementRef) || this;
        }
        CdkHeaderCell.decorators = [
            { type: core.Directive, args: [{
                        selector: 'cdk-header-cell, th[cdk-header-cell]',
                        host: {
                            'class': 'cdk-header-cell',
                            'role': 'columnheader',
                        },
                    },] },
        ];
        /** @nocollapse */
        CdkHeaderCell.ctorParameters = function () { return [
            { type: CdkColumnDef, },
            { type: core.ElementRef, },
        ]; };
        return CdkHeaderCell;
    }(BaseCdkCell));
    /**
     * Footer cell template container that adds the right classes and role.
     */
    var CdkFooterCell = /** @class */ (function (_super) {
        __extends(CdkFooterCell, _super);
        function CdkFooterCell(columnDef, elementRef) {
            return _super.call(this, columnDef, elementRef) || this;
        }
        CdkFooterCell.decorators = [
            { type: core.Directive, args: [{
                        selector: 'cdk-footer-cell, td[cdk-footer-cell]',
                        host: {
                            'class': 'cdk-footer-cell',
                            'role': 'gridcell',
                        },
                    },] },
        ];
        /** @nocollapse */
        CdkFooterCell.ctorParameters = function () { return [
            { type: CdkColumnDef, },
            { type: core.ElementRef, },
        ]; };
        return CdkFooterCell;
    }(BaseCdkCell));
    /**
     * Cell template container that adds the right classes and role.
     */
    var CdkCell = /** @class */ (function (_super) {
        __extends(CdkCell, _super);
        function CdkCell(columnDef, elementRef) {
            return _super.call(this, columnDef, elementRef) || this;
        }
        CdkCell.decorators = [
            { type: core.Directive, args: [{
                        selector: 'cdk-cell, td[cdk-cell]',
                        host: {
                            'class': 'cdk-cell',
                            'role': 'gridcell',
                        },
                    },] },
        ];
        /** @nocollapse */
        CdkCell.ctorParameters = function () { return [
            { type: CdkColumnDef, },
            { type: core.ElementRef, },
        ]; };
        return CdkCell;
    }(BaseCdkCell));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * The row template that can be used by the mat-table. Should not be used outside of the
     * material library.
     */
    var /** @type {?} */ CDK_ROW_TEMPLATE = "<ng-container cdkCellOutlet></ng-container>";
    /**
     * Base class for the CdkHeaderRowDef and CdkRowDef that handles checking their columns inputs
     * for changes and notifying the table.
     * @abstract
     */
    var  /**
     * Base class for the CdkHeaderRowDef and CdkRowDef that handles checking their columns inputs
     * for changes and notifying the table.
     * @abstract
     */
    BaseRowDef = /** @class */ (function () {
        function BaseRowDef(template, _differs) {
            this.template = template;
            this._differs = _differs;
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        BaseRowDef.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            // Create a new columns differ if one does not yet exist. Initialize it based on initial value
            // of the columns property or an empty array if none is provided.
            if (!this._columnsDiffer) {
                var /** @type {?} */ columns = (changes['columns'] && changes['columns'].currentValue) || [];
                this._columnsDiffer = this._differs.find(columns).create();
                this._columnsDiffer.diff(columns);
            }
        };
        /**
         * Returns the difference between the current columns and the columns from the last diff, or null
         * if there is no difference.
         */
        /**
         * Returns the difference between the current columns and the columns from the last diff, or null
         * if there is no difference.
         * @return {?}
         */
        BaseRowDef.prototype.getColumnsDiff = /**
         * Returns the difference between the current columns and the columns from the last diff, or null
         * if there is no difference.
         * @return {?}
         */
        function () {
            return this._columnsDiffer.diff(this.columns);
        };
        /** Gets this row def's relevant cell template from the provided column def. */
        /**
         * Gets this row def's relevant cell template from the provided column def.
         * @param {?} column
         * @return {?}
         */
        BaseRowDef.prototype.extractCellTemplate = /**
         * Gets this row def's relevant cell template from the provided column def.
         * @param {?} column
         * @return {?}
         */
        function (column) {
            if (this instanceof CdkHeaderRowDef) {
                return column.headerCell.template;
            }
            if (this instanceof CdkFooterRowDef) {
                return column.footerCell.template;
            }
            else {
                return column.cell.template;
            }
        };
        return BaseRowDef;
    }());
    /**
     * \@docs-private
     */
    var  /**
     * \@docs-private
     */
    CdkHeaderRowDefBase = /** @class */ (function (_super) {
        __extends(CdkHeaderRowDefBase, _super);
        function CdkHeaderRowDefBase() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return CdkHeaderRowDefBase;
    }(BaseRowDef));
    var /** @type {?} */ _CdkHeaderRowDefBase = mixinHasStickyInput(CdkHeaderRowDefBase);
    /**
     * Header row definition for the CDK table.
     * Captures the header row's template and other header properties such as the columns to display.
     */
    var CdkHeaderRowDef = /** @class */ (function (_super) {
        __extends(CdkHeaderRowDef, _super);
        function CdkHeaderRowDef(template, _differs) {
            return _super.call(this, template, _differs) || this;
        }
        // Prerender fails to recognize that ngOnChanges in a part of this class through inheritance.
        // Explicitly define it so that the method is called as part of the Angular lifecycle.
        /**
         * @param {?} changes
         * @return {?}
         */
        CdkHeaderRowDef.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            _super.prototype.ngOnChanges.call(this, changes);
        };
        CdkHeaderRowDef.decorators = [
            { type: core.Directive, args: [{
                        selector: '[cdkHeaderRowDef]',
                        inputs: ['columns: cdkHeaderRowDef', 'sticky: cdkHeaderRowDefSticky'],
                    },] },
        ];
        /** @nocollapse */
        CdkHeaderRowDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
            { type: core.IterableDiffers, },
        ]; };
        return CdkHeaderRowDef;
    }(_CdkHeaderRowDefBase));
    /**
     * \@docs-private
     */
    var  /**
     * \@docs-private
     */
    CdkFooterRowDefBase = /** @class */ (function (_super) {
        __extends(CdkFooterRowDefBase, _super);
        function CdkFooterRowDefBase() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return CdkFooterRowDefBase;
    }(BaseRowDef));
    var /** @type {?} */ _CdkFooterRowDefBase = mixinHasStickyInput(CdkFooterRowDefBase);
    /**
     * Footer row definition for the CDK table.
     * Captures the footer row's template and other footer properties such as the columns to display.
     */
    var CdkFooterRowDef = /** @class */ (function (_super) {
        __extends(CdkFooterRowDef, _super);
        function CdkFooterRowDef(template, _differs) {
            return _super.call(this, template, _differs) || this;
        }
        // Prerender fails to recognize that ngOnChanges in a part of this class through inheritance.
        // Explicitly define it so that the method is called as part of the Angular lifecycle.
        /**
         * @param {?} changes
         * @return {?}
         */
        CdkFooterRowDef.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            _super.prototype.ngOnChanges.call(this, changes);
        };
        CdkFooterRowDef.decorators = [
            { type: core.Directive, args: [{
                        selector: '[cdkFooterRowDef]',
                        inputs: ['columns: cdkFooterRowDef', 'sticky: cdkFooterRowDefSticky'],
                    },] },
        ];
        /** @nocollapse */
        CdkFooterRowDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
            { type: core.IterableDiffers, },
        ]; };
        return CdkFooterRowDef;
    }(_CdkFooterRowDefBase));
    /**
     * Data row definition for the CDK table.
     * Captures the header row's template and other row properties such as the columns to display and
     * a when predicate that describes when this row should be used.
     * @template T
     */
    var CdkRowDef = /** @class */ (function (_super) {
        __extends(CdkRowDef, _super);
        // TODO(andrewseguin): Add an input for providing a switch function to determine
        //   if this template should be used.
        function CdkRowDef(template, _differs) {
            return _super.call(this, template, _differs) || this;
        }
        CdkRowDef.decorators = [
            { type: core.Directive, args: [{
                        selector: '[cdkRowDef]',
                        inputs: ['columns: cdkRowDefColumns', 'when: cdkRowDefWhen'],
                    },] },
        ];
        /** @nocollapse */
        CdkRowDef.ctorParameters = function () { return [
            { type: core.TemplateRef, },
            { type: core.IterableDiffers, },
        ]; };
        return CdkRowDef;
    }(BaseRowDef));
    /**
     * Outlet for rendering cells inside of a row or header row.
     * \@docs-private
     */
    var CdkCellOutlet = /** @class */ (function () {
        function CdkCellOutlet(_viewContainer) {
            this._viewContainer = _viewContainer;
            CdkCellOutlet.mostRecentCellOutlet = this;
        }
        /**
         * Static property containing the latest constructed instance of this class.
         * Used by the CDK table when each CdkHeaderRow and CdkRow component is created using
         * createEmbeddedView. After one of these components are created, this property will provide
         * a handle to provide that component's cells and context. After init, the CdkCellOutlet will
         * construct the cells with the provided context.
         */
        CdkCellOutlet.mostRecentCellOutlet = null;
        CdkCellOutlet.decorators = [
            { type: core.Directive, args: [{ selector: '[cdkCellOutlet]' },] },
        ];
        /** @nocollapse */
        CdkCellOutlet.ctorParameters = function () { return [
            { type: core.ViewContainerRef, },
        ]; };
        return CdkCellOutlet;
    }());
    /**
     * Header template container that contains the cell outlet. Adds the right class and role.
     */
    var CdkHeaderRow = /** @class */ (function () {
        function CdkHeaderRow() {
        }
        CdkHeaderRow.decorators = [
            { type: core.Component, args: [{selector: 'cdk-header-row, tr[cdk-header-row]',
                        template: CDK_ROW_TEMPLATE,
                        host: {
                            'class': 'cdk-header-row',
                            'role': 'row',
                        },
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                    },] },
        ];
        return CdkHeaderRow;
    }());
    /**
     * Footer template container that contains the cell outlet. Adds the right class and role.
     */
    var CdkFooterRow = /** @class */ (function () {
        function CdkFooterRow() {
        }
        CdkFooterRow.decorators = [
            { type: core.Component, args: [{selector: 'cdk-footer-row, tr[cdk-footer-row]',
                        template: CDK_ROW_TEMPLATE,
                        host: {
                            'class': 'cdk-footer-row',
                            'role': 'row',
                        },
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                    },] },
        ];
        return CdkFooterRow;
    }());
    /**
     * Data row template container that contains the cell outlet. Adds the right class and role.
     */
    var CdkRow = /** @class */ (function () {
        function CdkRow() {
        }
        CdkRow.decorators = [
            { type: core.Component, args: [{selector: 'cdk-row, tr[cdk-row]',
                        template: CDK_ROW_TEMPLATE,
                        host: {
                            'class': 'cdk-row',
                            'role': 'row',
                        },
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                        encapsulation: core.ViewEncapsulation.None,
                    },] },
        ];
        return CdkRow;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * Returns an error to be thrown when attempting to find an unexisting column.
     * \@docs-private
     * @param {?} id Id whose lookup failed.
     * @return {?}
     */
    function getTableUnknownColumnError(id) {
        return Error("Could not find column with id \"" + id + "\".");
    }
    /**
     * Returns an error to be thrown when two column definitions have the same name.
     * \@docs-private
     * @param {?} name
     * @return {?}
     */
    function getTableDuplicateColumnNameError(name) {
        return Error("Duplicate column definition name provided: \"" + name + "\".");
    }
    /**
     * Returns an error to be thrown when there are multiple rows that are missing a when function.
     * \@docs-private
     * @return {?}
     */
    function getTableMultipleDefaultRowDefsError() {
        return Error("There can only be one default row without a when predicate function.");
    }
    /**
     * Returns an error to be thrown when there are no matching row defs for a particular set of data.
     * \@docs-private
     * @param {?} data
     * @return {?}
     */
    function getTableMissingMatchingRowDefError(data) {
        return Error("Could not find a matching row definition for the" +
            ("provided row data: " + JSON.stringify(data)));
    }
    /**
     * Returns an error to be thrown when there is no row definitions present in the content.
     * \@docs-private
     * @return {?}
     */
    function getTableMissingRowDefsError() {
        return Error('Missing definitions for header, footer, and row; ' +
            'cannot determine which columns should be rendered.');
    }
    /**
     * Returns an error to be thrown when the data source does not match the compatible types.
     * \@docs-private
     * @return {?}
     */
    function getTableUnknownDataSourceError() {
        return Error("Provided data source did not match an array, Observable, or DataSource");
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */

    /**
     * List of all possible directions that can be used for sticky positioning.
     * \@docs-private
     */
    var /** @type {?} */ STICKY_DIRECTIONS = ['top', 'bottom', 'left', 'right'];
    /**
     * Applies and removes sticky positioning styles to the `CdkTable` rows and columns cells.
     * \@docs-private
     */
    var  /**
     * Applies and removes sticky positioning styles to the `CdkTable` rows and columns cells.
     * \@docs-private
     */
    StickyStyler = /** @class */ (function () {
        /**
         * @param isNativeHtmlTable Whether the sticky logic should be based on a table
         *     that uses the native `<table>` element.
         * @param stickCellCss The CSS class that will be applied to every row/cell that has
         *     sticky positioning applied.
         * @param direction The directionality context of the table (ltr/rtl); affects column positioning
         *     by reversing left/right positions.
         */
        function StickyStyler(isNativeHtmlTable, stickCellCss, direction) {
            this.isNativeHtmlTable = isNativeHtmlTable;
            this.stickCellCss = stickCellCss;
            this.direction = direction;
        }
        /**
         * Clears the sticky positioning styles from the row and its cells by resetting the `position`
         * style, setting the zIndex to 0, and unsetting each provided sticky direction.
         * @param rows The list of rows that should be cleared from sticking in the provided directions
         * @param stickyDirections The directions that should no longer be set as sticky on the rows.
         */
        /**
         * Clears the sticky positioning styles from the row and its cells by resetting the `position`
         * style, setting the zIndex to 0, and unsetting each provided sticky direction.
         * @param {?} rows The list of rows that should be cleared from sticking in the provided directions
         * @param {?} stickyDirections The directions that should no longer be set as sticky on the rows.
         * @return {?}
         */
        StickyStyler.prototype.clearStickyPositioning = /**
         * Clears the sticky positioning styles from the row and its cells by resetting the `position`
         * style, setting the zIndex to 0, and unsetting each provided sticky direction.
         * @param {?} rows The list of rows that should be cleared from sticking in the provided directions
         * @param {?} stickyDirections The directions that should no longer be set as sticky on the rows.
         * @return {?}
         */
        function (rows, stickyDirections) {
            for (var _i = 0, rows_1 = rows; _i < rows_1.length; _i++) {
                var row = rows_1[_i];
                this._removeStickyStyle(row, stickyDirections);
                for (var /** @type {?} */ i = 0; i < row.children.length; i++) {
                    var /** @type {?} */ cell = /** @type {?} */ (row.children[i]);
                    this._removeStickyStyle(cell, stickyDirections);
                }
            }
        };
        /**
         * Applies sticky left and right positions to the cells of each row according to the sticky
         * states of the rendered column definitions.
         * @param rows The rows that should have its set of cells stuck according to the sticky states.
         * @param stickyStartStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the start of the row.
         * @param stickyEndStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the end of the row.
         */
        /**
         * Applies sticky left and right positions to the cells of each row according to the sticky
         * states of the rendered column definitions.
         * @param {?} rows The rows that should have its set of cells stuck according to the sticky states.
         * @param {?} stickyStartStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the start of the row.
         * @param {?} stickyEndStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the end of the row.
         * @return {?}
         */
        StickyStyler.prototype.updateStickyColumns = /**
         * Applies sticky left and right positions to the cells of each row according to the sticky
         * states of the rendered column definitions.
         * @param {?} rows The rows that should have its set of cells stuck according to the sticky states.
         * @param {?} stickyStartStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the start of the row.
         * @param {?} stickyEndStates A list of boolean states where each state represents whether the cell
         *     in this index position should be stuck to the end of the row.
         * @return {?}
         */
        function (rows, stickyStartStates, stickyEndStates) {
            var /** @type {?} */ hasStickyColumns = stickyStartStates.some(function (state) { return state; }) || stickyEndStates.some(function (state) { return state; });
            if (!rows.length || !hasStickyColumns) {
                return;
            }
            var /** @type {?} */ firstRow = rows[0];
            var /** @type {?} */ numCells = firstRow.children.length;
            var /** @type {?} */ cellWidths = this._getCellWidths(firstRow);
            var /** @type {?} */ startPositions = this._getStickyStartColumnPositions(cellWidths, stickyStartStates);
            var /** @type {?} */ endPositions = this._getStickyEndColumnPositions(cellWidths, stickyEndStates);
            var /** @type {?} */ isRtl = this.direction === 'rtl';
            for (var _i = 0, rows_2 = rows; _i < rows_2.length; _i++) {
                var row = rows_2[_i];
                for (var /** @type {?} */ i = 0; i < numCells; i++) {
                    var /** @type {?} */ cell = /** @type {?} */ (row.children[i]);
                    if (stickyStartStates[i]) {
                        this._addStickyStyle(cell, isRtl ? 'right' : 'left', startPositions[i]);
                    }
                    if (stickyEndStates[i]) {
                        this._addStickyStyle(cell, isRtl ? 'left' : 'right', endPositions[i]);
                    }
                }
            }
        };
        /**
         * Applies sticky positioning to the row's cells if using the native table layout, and to the
         * row itself otherwise.
         * @param rowsToStick The list of rows that should be stuck according to their corresponding
         *     sticky state and to the provided top or bottom position.
         * @param stickyStates A list of boolean states where each state represents whether the row
         *     should be stuck in the particular top or bottom position.
         * @param position The position direction in which the row should be stuck if that row should be
         *     sticky.
         *
         */
        /**
         * Applies sticky positioning to the row's cells if using the native table layout, and to the
         * row itself otherwise.
         * @param {?} rowsToStick The list of rows that should be stuck according to their corresponding
         *     sticky state and to the provided top or bottom position.
         * @param {?} stickyStates A list of boolean states where each state represents whether the row
         *     should be stuck in the particular top or bottom position.
         * @param {?} position The position direction in which the row should be stuck if that row should be
         *     sticky.
         *
         * @return {?}
         */
        StickyStyler.prototype.stickRows = /**
         * Applies sticky positioning to the row's cells if using the native table layout, and to the
         * row itself otherwise.
         * @param {?} rowsToStick The list of rows that should be stuck according to their corresponding
         *     sticky state and to the provided top or bottom position.
         * @param {?} stickyStates A list of boolean states where each state represents whether the row
         *     should be stuck in the particular top or bottom position.
         * @param {?} position The position direction in which the row should be stuck if that row should be
         *     sticky.
         *
         * @return {?}
         */
        function (rowsToStick, stickyStates, position) {
            // If positioning the rows to the bottom, reverse their order when evaluating the sticky
            // position such that the last row stuck will be "bottom: 0px" and so on.
            var /** @type {?} */ rows = position === 'bottom' ? rowsToStick.reverse() : rowsToStick;
            var /** @type {?} */ stickyHeight = 0;
            for (var /** @type {?} */ rowIndex = 0; rowIndex < rows.length; rowIndex++) {
                if (!stickyStates[rowIndex]) {
                    continue;
                }
                var /** @type {?} */ row = rows[rowIndex];
                if (this.isNativeHtmlTable) {
                    for (var /** @type {?} */ j = 0; j < row.children.length; j++) {
                        var /** @type {?} */ cell = /** @type {?} */ (row.children[j]);
                        this._addStickyStyle(cell, position, stickyHeight);
                    }
                }
                else {
                    // Flex does not respect the stick positioning on the cells, needs to be applied to the row.
                    // If this is applied on a native table, Safari causes the header to fly in wrong direction.
                    this._addStickyStyle(row, position, stickyHeight);
                }
                stickyHeight += row.getBoundingClientRect().height;
            }
        };
        /**
         * When using the native table in Safari, sticky footer cells do not stick. The only way to stick
         * footer rows is to apply sticky styling to the tfoot container. This should only be done if
         * all footer rows are sticky. If not all footer rows are sticky, remove sticky positioning from
         * the tfoot element.
         */
        /**
         * When using the native table in Safari, sticky footer cells do not stick. The only way to stick
         * footer rows is to apply sticky styling to the tfoot container. This should only be done if
         * all footer rows are sticky. If not all footer rows are sticky, remove sticky positioning from
         * the tfoot element.
         * @param {?} tableElement
         * @param {?} stickyStates
         * @return {?}
         */
        StickyStyler.prototype.updateStickyFooterContainer = /**
         * When using the native table in Safari, sticky footer cells do not stick. The only way to stick
         * footer rows is to apply sticky styling to the tfoot container. This should only be done if
         * all footer rows are sticky. If not all footer rows are sticky, remove sticky positioning from
         * the tfoot element.
         * @param {?} tableElement
         * @param {?} stickyStates
         * @return {?}
         */
        function (tableElement, stickyStates) {
            if (!this.isNativeHtmlTable) {
                return;
            }
            var /** @type {?} */ tfoot = /** @type {?} */ ((tableElement.querySelector('tfoot')));
            if (stickyStates.some(function (state) { return !state; })) {
                this._removeStickyStyle(tfoot, ['bottom']);
            }
            else {
                this._addStickyStyle(tfoot, 'bottom', 0);
            }
        };
        /**
         * Removes the sticky style on the element by removing the sticky cell CSS class, re-evaluating
         * the zIndex, removing each of the provided sticky directions, and removing the
         * sticky position if there are no more directions.
         */
        /**
         * Removes the sticky style on the element by removing the sticky cell CSS class, re-evaluating
         * the zIndex, removing each of the provided sticky directions, and removing the
         * sticky position if there are no more directions.
         * @param {?} element
         * @param {?} stickyDirections
         * @return {?}
         */
        StickyStyler.prototype._removeStickyStyle = /**
         * Removes the sticky style on the element by removing the sticky cell CSS class, re-evaluating
         * the zIndex, removing each of the provided sticky directions, and removing the
         * sticky position if there are no more directions.
         * @param {?} element
         * @param {?} stickyDirections
         * @return {?}
         */
        function (element, stickyDirections) {
            for (var _i = 0, stickyDirections_1 = stickyDirections; _i < stickyDirections_1.length; _i++) {
                var dir = stickyDirections_1[_i];
                element.style[dir] = '';
            }
            element.style.zIndex = this._getCalculatedZIndex(element);
            // If the element no longer has any more sticky directions, remove sticky positioning and
            // the sticky CSS class.
            var /** @type {?} */ hasDirection = STICKY_DIRECTIONS.some(function (dir) { return !!element.style[dir]; });
            if (!hasDirection) {
                element.style.position = '';
                element.classList.remove(this.stickCellCss);
            }
        };
        /**
         * Adds the sticky styling to the element by adding the sticky style class, changing position
         * to be sticky (and -webkit-sticky), setting the appropriate zIndex, and adding a sticky
         * direction and value.
         */
        /**
         * Adds the sticky styling to the element by adding the sticky style class, changing position
         * to be sticky (and -webkit-sticky), setting the appropriate zIndex, and adding a sticky
         * direction and value.
         * @param {?} element
         * @param {?} dir
         * @param {?} dirValue
         * @return {?}
         */
        StickyStyler.prototype._addStickyStyle = /**
         * Adds the sticky styling to the element by adding the sticky style class, changing position
         * to be sticky (and -webkit-sticky), setting the appropriate zIndex, and adding a sticky
         * direction and value.
         * @param {?} element
         * @param {?} dir
         * @param {?} dirValue
         * @return {?}
         */
        function (element, dir, dirValue) {
            element.classList.add(this.stickCellCss);
            element.style[dir] = dirValue + "px";
            element.style.cssText += 'position: -webkit-sticky; position: sticky; ';
            element.style.zIndex = this._getCalculatedZIndex(element);
        };
        /**
         * Calculate what the z-index should be for the element, depending on what directions (top,
         * bottom, left, right) have been set. It should be true that elements with a top direction
         * should have the highest index since these are elements like a table header. If any of those
         * elements are also sticky in another direction, then they should appear above other elements
         * that are only sticky top (e.g. a sticky column on a sticky header). Bottom-sticky elements
         * (e.g. footer rows) should then be next in the ordering such that they are below the header
         * but above any non-sticky elements. Finally, left/right sticky elements (e.g. sticky columns)
         * should minimally increment so that they are above non-sticky elements but below top and bottom
         * elements.
         */
        /**
         * Calculate what the z-index should be for the element, depending on what directions (top,
         * bottom, left, right) have been set. It should be true that elements with a top direction
         * should have the highest index since these are elements like a table header. If any of those
         * elements are also sticky in another direction, then they should appear above other elements
         * that are only sticky top (e.g. a sticky column on a sticky header). Bottom-sticky elements
         * (e.g. footer rows) should then be next in the ordering such that they are below the header
         * but above any non-sticky elements. Finally, left/right sticky elements (e.g. sticky columns)
         * should minimally increment so that they are above non-sticky elements but below top and bottom
         * elements.
         * @param {?} element
         * @return {?}
         */
        StickyStyler.prototype._getCalculatedZIndex = /**
         * Calculate what the z-index should be for the element, depending on what directions (top,
         * bottom, left, right) have been set. It should be true that elements with a top direction
         * should have the highest index since these are elements like a table header. If any of those
         * elements are also sticky in another direction, then they should appear above other elements
         * that are only sticky top (e.g. a sticky column on a sticky header). Bottom-sticky elements
         * (e.g. footer rows) should then be next in the ordering such that they are below the header
         * but above any non-sticky elements. Finally, left/right sticky elements (e.g. sticky columns)
         * should minimally increment so that they are above non-sticky elements but below top and bottom
         * elements.
         * @param {?} element
         * @return {?}
         */
        function (element) {
            var /** @type {?} */ zIndexIncrements = {
                top: 100,
                bottom: 10,
                left: 1,
                right: 1,
            };
            var /** @type {?} */ zIndex = 0;
            for (var _i = 0, STICKY_DIRECTIONS_1 = STICKY_DIRECTIONS; _i < STICKY_DIRECTIONS_1.length; _i++) {
                var dir = STICKY_DIRECTIONS_1[_i];
                if (element.style[dir]) {
                    zIndex += zIndexIncrements[dir];
                }
            }
            return zIndex ? "" + zIndex : '';
        };
        /** Gets the widths for each cell in the provided row. */
        /**
         * Gets the widths for each cell in the provided row.
         * @param {?} row
         * @return {?}
         */
        StickyStyler.prototype._getCellWidths = /**
         * Gets the widths for each cell in the provided row.
         * @param {?} row
         * @return {?}
         */
        function (row) {
            var /** @type {?} */ cellWidths = [];
            var /** @type {?} */ firstRowCells = row.children;
            for (var /** @type {?} */ i = 0; i < firstRowCells.length; i++) {
                var /** @type {?} */ cell = /** @type {?} */ (firstRowCells[i]);
                cellWidths.push(cell.getBoundingClientRect().width);
            }
            return cellWidths;
        };
        /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         */
        /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         * @param {?} widths
         * @param {?} stickyStates
         * @return {?}
         */
        StickyStyler.prototype._getStickyStartColumnPositions = /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         * @param {?} widths
         * @param {?} stickyStates
         * @return {?}
         */
        function (widths, stickyStates) {
            var /** @type {?} */ positions = [];
            var /** @type {?} */ nextPosition = 0;
            for (var /** @type {?} */ i = 0; i < widths.length; i++) {
                if (stickyStates[i]) {
                    positions[i] = nextPosition;
                    nextPosition += widths[i];
                }
            }
            return positions;
        };
        /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         */
        /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         * @param {?} widths
         * @param {?} stickyStates
         * @return {?}
         */
        StickyStyler.prototype._getStickyEndColumnPositions = /**
         * Determines the left and right positions of each sticky column cell, which will be the
         * accumulation of all sticky column cell widths to the left and right, respectively.
         * Non-sticky cells do not need to have a value set since their positions will not be applied.
         * @param {?} widths
         * @param {?} stickyStates
         * @return {?}
         */
        function (widths, stickyStates) {
            var /** @type {?} */ positions = [];
            var /** @type {?} */ nextPosition = 0;
            for (var /** @type {?} */ i = widths.length; i > 0; i--) {
                if (stickyStates[i]) {
                    positions[i] = nextPosition;
                    nextPosition += widths[i];
                }
            }
            return positions;
        };
        return StickyStyler;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    /**
     * Provides a handle for the table to grab the view container's ng-container to insert data rows.
     * \@docs-private
     */
    var DataRowOutlet = /** @class */ (function () {
        function DataRowOutlet(viewContainer, elementRef) {
            this.viewContainer = viewContainer;
            this.elementRef = elementRef;
        }
        DataRowOutlet.decorators = [
            { type: core.Directive, args: [{ selector: '[rowOutlet]' },] },
        ];
        /** @nocollapse */
        DataRowOutlet.ctorParameters = function () { return [
            { type: core.ViewContainerRef, },
            { type: core.ElementRef, },
        ]; };
        return DataRowOutlet;
    }());
    /**
     * Provides a handle for the table to grab the view container's ng-container to insert the header.
     * \@docs-private
     */
    var HeaderRowOutlet = /** @class */ (function () {
        function HeaderRowOutlet(viewContainer, elementRef) {
            this.viewContainer = viewContainer;
            this.elementRef = elementRef;
        }
        HeaderRowOutlet.decorators = [
            { type: core.Directive, args: [{ selector: '[headerRowOutlet]' },] },
        ];
        /** @nocollapse */
        HeaderRowOutlet.ctorParameters = function () { return [
            { type: core.ViewContainerRef, },
            { type: core.ElementRef, },
        ]; };
        return HeaderRowOutlet;
    }());
    /**
     * Provides a handle for the table to grab the view container's ng-container to insert the footer.
     * \@docs-private
     */
    var FooterRowOutlet = /** @class */ (function () {
        function FooterRowOutlet(viewContainer, elementRef) {
            this.viewContainer = viewContainer;
            this.elementRef = elementRef;
        }
        FooterRowOutlet.decorators = [
            { type: core.Directive, args: [{ selector: '[footerRowOutlet]' },] },
        ];
        /** @nocollapse */
        FooterRowOutlet.ctorParameters = function () { return [
            { type: core.ViewContainerRef, },
            { type: core.ElementRef, },
        ]; };
        return FooterRowOutlet;
    }());
    /**
     * The table template that can be used by the mat-table. Should not be used outside of the
     * material library.
     * \@docs-private
     */
    var /** @type {?} */ CDK_TABLE_TEMPLATE = "\n  <ng-container headerRowOutlet></ng-container>\n  <ng-container rowOutlet></ng-container>\n  <ng-container footerRowOutlet></ng-container>";
    /**
     * Class used to conveniently type the embedded view ref for rows with a context.
     * \@docs-private
     * @abstract
     * @template T
     */
    var /**
     * Class used to conveniently type the embedded view ref for rows with a context.
     * \@docs-private
     * @abstract
     * @template T
     */
    RowViewRef = /** @class */ (function (_super) {
        __extends(RowViewRef, _super);
        function RowViewRef() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return RowViewRef;
    }(core.EmbeddedViewRef));
    /**
     * A data table that can render a header row, data rows, and a footer row.
     * Uses the dataSource input to determine the data to be rendered. The data can be provided either
     * as a data array, an Observable stream that emits the data array to render, or a DataSource with a
     * connect function that will return an Observable stream that emits the data array to render.
     * @template T
     */
    var CdkTable = /** @class */ (function () {
        function CdkTable(_differs, _changeDetectorRef, _elementRef, role, _dir) {
            this._differs = _differs;
            this._changeDetectorRef = _changeDetectorRef;
            this._elementRef = _elementRef;
            this._dir = _dir;
            /**
             * Subject that emits when the component has been destroyed.
             */
            this._onDestroy = new rxjs.Subject();
            /**
             * Map of all the user's defined columns (header, data, and footer cell template) identified by
             * name. Collection populated by the column definitions gathered by `ContentChildren` as well as
             * any custom column definitions added to `_customColumnDefs`.
             */
            this._columnDefsByName = new Map();
            /**
             * Column definitions that were defined outside of the direct content children of the table.
             * These will be defined when, e.g., creating a wrapper around the cdkTable that has
             * column definitions as *it's* content child.
             */
            this._customColumnDefs = new Set();
            /**
             * Data row definitions that were defined outside of the direct content children of the table.
             * These will be defined when, e.g., creating a wrapper around the cdkTable that has
             * built-in data rows as *it's* content child.
             */
            this._customRowDefs = new Set();
            /**
             * Header row definitions that were defined outside of the direct content children of the table.
             * These will be defined when, e.g., creating a wrapper around the cdkTable that has
             * built-in header rows as *it's* content child.
             */
            this._customHeaderRowDefs = new Set();
            /**
             * Footer row definitions that were defined outside of the direct content children of the table.
             * These will be defined when, e.g., creating a wrapper around the cdkTable that has a
             * built-in footer row as *it's* content child.
             */
            this._customFooterRowDefs = new Set();
            /**
             * Whether the header row definition has been changed. Triggers an update to the header row after
             * content is checked. Initialized as true so that the table renders the initial set of rows.
             */
            this._headerRowDefChanged = true;
            /**
             * Whether the footer row definition has been changed. Triggers an update to the footer row after
             * content is checked. Initialized as true so that the table renders the initial set of rows.
             */
            this._footerRowDefChanged = true;
            /**
             * Cache of the latest rendered `RenderRow` objects as a map for easy retrieval when constructing
             * a new list of `RenderRow` objects for rendering rows. Since the new list is constructed with
             * the cached `RenderRow` objects when possible, the row identity is preserved when the data
             * and row template matches, which allows the `IterableDiffer` to check rows by reference
             * and understand which rows are added/moved/removed.
             *
             * Implemented as a map of maps where the first key is the `data: T` object and the second is the
             * `CdkRowDef<T>` object. With the two keys, the cache points to a `RenderRow<T>` object that
             * contains an array of created pairs. The array is necessary to handle cases where the data
             * array contains multiple duplicate data objects and each instantiated `RenderRow` must be
             * stored.
             */
            this._cachedRenderRowsMap = new Map();
            /**
             * CSS class added to any row or cell that has sticky positioning applied. May be overriden by
             * table subclasses.
             */
            this.stickyCssClass = 'cdk-table-sticky';
            this._multiTemplateDataRows = false;
            /**
             * Stream containing the latest information on what rows are being displayed on screen.
             * Can be used by the data source to as a heuristic of what data should be provided.
             */
            this.viewChange = new rxjs.BehaviorSubject({ start: 0, end: Number.MAX_VALUE });
            if (!role) {
                this._elementRef.nativeElement.setAttribute('role', 'grid');
            }
            this._isNativeHtmlTable = this._elementRef.nativeElement.nodeName === 'TABLE';
        }
        Object.defineProperty(CdkTable.prototype, "trackBy", {
            get: /**
             * Tracking function that will be used to check the differences in data changes. Used similarly
             * to `ngFor` `trackBy` function. Optimize row operations by identifying a row based on its data
             * relative to the function to know if a row should be added/removed/moved.
             * Accepts a function that takes two parameters, `index` and `item`.
             * @return {?}
             */
            function () { return this._trackByFn; },
            set: /**
             * @param {?} fn
             * @return {?}
             */
            function (fn) {
                if (core.isDevMode() &&
                    fn != null && typeof fn !== 'function' && /** @type {?} */ (console) && /** @type {?} */ (console.warn)) {
                    console.warn("trackBy must be a function, but received " + JSON.stringify(fn) + ".");
                }
                this._trackByFn = fn;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CdkTable.prototype, "dataSource", {
            get: /**
             * The table's source of data, which can be provided in three ways (in order of complexity):
             *   - Simple data array (each object represents one table row)
             *   - Stream that emits a data array each time the array changes
             *   - `DataSource` object that implements the connect/disconnect interface.
             *
             * If a data array is provided, the table must be notified when the array's objects are
             * added, removed, or moved. This can be done by calling the `renderRows()` function which will
             * render the diff since the last table render. If the data array reference is changed, the table
             * will automatically trigger an update to the rows.
             *
             * When providing an Observable stream, the table will trigger an update automatically when the
             * stream emits a new array of data.
             *
             * Finally, when providing a `DataSource` object, the table will use the Observable stream
             * provided by the connect function and trigger updates when that stream emits new data array
             * values. During the table's ngOnDestroy or when the data source is removed from the table, the
             * table will call the DataSource's `disconnect` function (may be useful for cleaning up any
             * subscriptions registered during the connect process).
             * @return {?}
             */
            function () { return this._dataSource; },
            set: /**
             * @param {?} dataSource
             * @return {?}
             */
            function (dataSource) {
                if (this._dataSource !== dataSource) {
                    this._switchDataSource(dataSource);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CdkTable.prototype, "multiTemplateDataRows", {
            get: /**
             * Whether to allow multiple rows per data object by evaluating which rows evaluate their 'when'
             * predicate to true. If `multiTemplateDataRows` is false, which is the default value, then each
             * dataobject will render the first row that evaluates its when predicate to true, in the order
             * defined in the table, or otherwise the default row which does not have a when predicate.
             * @return {?}
             */
            function () { return this._multiTemplateDataRows; },
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                this._multiTemplateDataRows = coerceBooleanProperty(v);
                if (this._rowOutlet.viewContainer.length) {
                    this._forceRenderDataRows();
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        CdkTable.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this._setupStickyStyler();
            if (this._isNativeHtmlTable) {
                this._applyNativeTableSections();
            }
            // Set up the trackBy function so that it uses the `RenderRow` as its identity by default. If
            // the user has provided a custom trackBy, return the result of that function as evaluated
            // with the values of the `RenderRow`'s data and index.
            this._dataDiffer = this._differs.find([]).create(function (_i, dataRow) {
                return _this.trackBy ? _this.trackBy(dataRow.dataIndex, dataRow.data) : dataRow;
            });
        };
        /**
         * @return {?}
         */
        CdkTable.prototype.ngAfterContentChecked = /**
         * @return {?}
         */
        function () {
            // Cache the row and column definitions gathered by ContentChildren and programmatic injection.
            this._cacheRowDefs();
            this._cacheColumnDefs();
            // Make sure that the user has at least added header, footer, or data row def.
            if (!this._headerRowDefs.length && !this._footerRowDefs.length && !this._rowDefs.length) {
                throw getTableMissingRowDefsError();
            }
            // Render updates if the list of columns have been changed for the header, row, or footer defs.
            this._renderUpdatedColumns();
            // If the header row definition has been changed, trigger a render to the header row.
            if (this._headerRowDefChanged) {
                this._forceRenderHeaderRows();
                this._headerRowDefChanged = false;
            }
            // If the footer row definition has been changed, trigger a render to the footer row.
            if (this._footerRowDefChanged) {
                this._forceRenderFooterRows();
                this._footerRowDefChanged = false;
            }
            // If there is a data source and row definitions, connect to the data source unless a
            // connection has already been made.
            if (this.dataSource && this._rowDefs.length > 0 && !this._renderChangeSubscription) {
                this._observeRenderChanges();
            }
            this._checkStickyStates();
        };
        /**
         * @return {?}
         */
        CdkTable.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this._rowOutlet.viewContainer.clear();
            this._headerRowOutlet.viewContainer.clear();
            this._footerRowOutlet.viewContainer.clear();
            this._cachedRenderRowsMap.clear();
            this._onDestroy.next();
            this._onDestroy.complete();
            if (this.dataSource instanceof DataSource) {
                this.dataSource.disconnect(this);
            }
        };
        /**
         * Renders rows based on the table's latest set of data, which was either provided directly as an
         * input or retrieved through an Observable stream (directly or from a DataSource).
         * Checks for differences in the data since the last diff to perform only the necessary
         * changes (add/remove/move rows).
         *
         * If the table's data source is a DataSource or Observable, this will be invoked automatically
         * each time the provided Observable stream emits a new data array. Otherwise if your data is
         * an array, this function will need to be called to render any changes.
         */
        /**
         * Renders rows based on the table's latest set of data, which was either provided directly as an
         * input or retrieved through an Observable stream (directly or from a DataSource).
         * Checks for differences in the data since the last diff to perform only the necessary
         * changes (add/remove/move rows).
         *
         * If the table's data source is a DataSource or Observable, this will be invoked automatically
         * each time the provided Observable stream emits a new data array. Otherwise if your data is
         * an array, this function will need to be called to render any changes.
         * @return {?}
         */
        CdkTable.prototype.renderRows = /**
         * Renders rows based on the table's latest set of data, which was either provided directly as an
         * input or retrieved through an Observable stream (directly or from a DataSource).
         * Checks for differences in the data since the last diff to perform only the necessary
         * changes (add/remove/move rows).
         *
         * If the table's data source is a DataSource or Observable, this will be invoked automatically
         * each time the provided Observable stream emits a new data array. Otherwise if your data is
         * an array, this function will need to be called to render any changes.
         * @return {?}
         */
        function () {
            var _this = this;
            this._renderRows = this._getAllRenderRows();
            var /** @type {?} */ changes = this._dataDiffer.diff(this._renderRows);
            if (!changes) {
                return;
            }
            var /** @type {?} */ viewContainer = this._rowOutlet.viewContainer;
            changes.forEachOperation(function (record, prevIndex, currentIndex) {
                if (record.previousIndex == null) {
                    _this._insertRow(record.item, currentIndex);
                }
                else if (currentIndex == null) {
                    viewContainer.remove(prevIndex);
                }
                else {
                    var /** @type {?} */ view = /** @type {?} */ (viewContainer.get(prevIndex));
                    viewContainer.move(/** @type {?} */ ((view)), currentIndex);
                }
            });
            // Update the meta context of a row's context data (index, count, first, last, ...)
            this._updateRowIndexContext();
            // Update rows that did not get added/removed/moved but may have had their identity changed,
            // e.g. if trackBy matched data on some property but the actual data reference changed.
            changes.forEachIdentityChange(function (record) {
                var /** @type {?} */ rowView = /** @type {?} */ (viewContainer.get(/** @type {?} */ ((record.currentIndex))));
                rowView.context.$implicit = record.item.data;
            });
            this.updateStickyColumnStyles();
        };
        /**
         * Sets the header row definition to be used. Overrides the header row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the header row after the
         * table's content is checked.
         * @docs-private
         * @deprecated Use `addHeaderRowDef` and `removeHeaderRowDef` instead
         * @deletion-target 8.0.0
         */
        /**
         * Sets the header row definition to be used. Overrides the header row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the header row after the
         * table's content is checked.
         * \@docs-private
         * @deprecated Use `addHeaderRowDef` and `removeHeaderRowDef` instead
         * \@deletion-target 8.0.0
         * @param {?} headerRowDef
         * @return {?}
         */
        CdkTable.prototype.setHeaderRowDef = /**
         * Sets the header row definition to be used. Overrides the header row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the header row after the
         * table's content is checked.
         * \@docs-private
         * @deprecated Use `addHeaderRowDef` and `removeHeaderRowDef` instead
         * \@deletion-target 8.0.0
         * @param {?} headerRowDef
         * @return {?}
         */
        function (headerRowDef) {
            this._customHeaderRowDefs = new Set([headerRowDef]);
            this._headerRowDefChanged = true;
        };
        /**
         * Sets the footer row definition to be used. Overrides the footer row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the footer row after the
         * table's content is checked.
         * @docs-private
         * @deprecated Use `addFooterRowDef` and `removeFooterRowDef` instead
         * @deletion-target 8.0.0
         */
        /**
         * Sets the footer row definition to be used. Overrides the footer row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the footer row after the
         * table's content is checked.
         * \@docs-private
         * @deprecated Use `addFooterRowDef` and `removeFooterRowDef` instead
         * \@deletion-target 8.0.0
         * @param {?} footerRowDef
         * @return {?}
         */
        CdkTable.prototype.setFooterRowDef = /**
         * Sets the footer row definition to be used. Overrides the footer row definition gathered by
         * using `ContentChild`, if one exists. Sets a flag that will re-render the footer row after the
         * table's content is checked.
         * \@docs-private
         * @deprecated Use `addFooterRowDef` and `removeFooterRowDef` instead
         * \@deletion-target 8.0.0
         * @param {?} footerRowDef
         * @return {?}
         */
        function (footerRowDef) {
            this._customFooterRowDefs = new Set([footerRowDef]);
            this._footerRowDefChanged = true;
        };
        /** Adds a column definition that was not included as part of the content children. */
        /**
         * Adds a column definition that was not included as part of the content children.
         * @param {?} columnDef
         * @return {?}
         */
        CdkTable.prototype.addColumnDef = /**
         * Adds a column definition that was not included as part of the content children.
         * @param {?} columnDef
         * @return {?}
         */
        function (columnDef) {
            this._customColumnDefs.add(columnDef);
        };
        /** Removes a column definition that was not included as part of the content children. */
        /**
         * Removes a column definition that was not included as part of the content children.
         * @param {?} columnDef
         * @return {?}
         */
        CdkTable.prototype.removeColumnDef = /**
         * Removes a column definition that was not included as part of the content children.
         * @param {?} columnDef
         * @return {?}
         */
        function (columnDef) {
            this._customColumnDefs.delete(columnDef);
        };
        /** Adds a row definition that was not included as part of the content children. */
        /**
         * Adds a row definition that was not included as part of the content children.
         * @param {?} rowDef
         * @return {?}
         */
        CdkTable.prototype.addRowDef = /**
         * Adds a row definition that was not included as part of the content children.
         * @param {?} rowDef
         * @return {?}
         */
        function (rowDef) {
            this._customRowDefs.add(rowDef);
        };
        /** Removes a row definition that was not included as part of the content children. */
        /**
         * Removes a row definition that was not included as part of the content children.
         * @param {?} rowDef
         * @return {?}
         */
        CdkTable.prototype.removeRowDef = /**
         * Removes a row definition that was not included as part of the content children.
         * @param {?} rowDef
         * @return {?}
         */
        function (rowDef) {
            this._customRowDefs.delete(rowDef);
        };
        /** Adds a header row definition that was not included as part of the content children. */
        /**
         * Adds a header row definition that was not included as part of the content children.
         * @param {?} headerRowDef
         * @return {?}
         */
        CdkTable.prototype.addHeaderRowDef = /**
         * Adds a header row definition that was not included as part of the content children.
         * @param {?} headerRowDef
         * @return {?}
         */
        function (headerRowDef) {
            this._customHeaderRowDefs.add(headerRowDef);
            this._headerRowDefChanged = true;
        };
        /** Removes a header row definition that was not included as part of the content children. */
        /**
         * Removes a header row definition that was not included as part of the content children.
         * @param {?} headerRowDef
         * @return {?}
         */
        CdkTable.prototype.removeHeaderRowDef = /**
         * Removes a header row definition that was not included as part of the content children.
         * @param {?} headerRowDef
         * @return {?}
         */
        function (headerRowDef) {
            this._customHeaderRowDefs.delete(headerRowDef);
            this._headerRowDefChanged = true;
        };
        /** Adds a footer row definition that was not included as part of the content children. */
        /**
         * Adds a footer row definition that was not included as part of the content children.
         * @param {?} footerRowDef
         * @return {?}
         */
        CdkTable.prototype.addFooterRowDef = /**
         * Adds a footer row definition that was not included as part of the content children.
         * @param {?} footerRowDef
         * @return {?}
         */
        function (footerRowDef) {
            this._customFooterRowDefs.add(footerRowDef);
            this._footerRowDefChanged = true;
        };
        /** Removes a footer row definition that was not included as part of the content children. */
        /**
         * Removes a footer row definition that was not included as part of the content children.
         * @param {?} footerRowDef
         * @return {?}
         */
        CdkTable.prototype.removeFooterRowDef = /**
         * Removes a footer row definition that was not included as part of the content children.
         * @param {?} footerRowDef
         * @return {?}
         */
        function (footerRowDef) {
            this._customFooterRowDefs.delete(footerRowDef);
            this._footerRowDefChanged = true;
        };
        /**
         * Updates the header sticky styles. First resets all applied styles with respect to the cells
         * sticking to the top. Then, evaluating which cells need to be stuck to the top. This is
         * automatically called when the header row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         */
        /**
         * Updates the header sticky styles. First resets all applied styles with respect to the cells
         * sticking to the top. Then, evaluating which cells need to be stuck to the top. This is
         * automatically called when the header row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         * @return {?}
         */
        CdkTable.prototype.updateStickyHeaderRowStyles = /**
         * Updates the header sticky styles. First resets all applied styles with respect to the cells
         * sticking to the top. Then, evaluating which cells need to be stuck to the top. This is
         * automatically called when the header row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         * @return {?}
         */
        function () {
            var /** @type {?} */ headerRows = this._getRenderedRows(this._headerRowOutlet);
            this._stickyStyler.clearStickyPositioning(headerRows, ['top']);
            var /** @type {?} */ stickyStates = this._headerRowDefs.map(function (def) { return def.sticky; });
            this._stickyStyler.stickRows(headerRows, stickyStates, 'top');
            // Reset the dirty state of the sticky input change since it has been used.
            this._headerRowDefs.forEach(function (def) { return def.resetStickyChanged(); });
        };
        /**
         * Updates the footer sticky styles. First resets all applied styles with respect to the cells
         * sticking to the bottom. Then, evaluating which cells need to be stuck to the bottom. This is
         * automatically called when the footer row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         */
        /**
         * Updates the footer sticky styles. First resets all applied styles with respect to the cells
         * sticking to the bottom. Then, evaluating which cells need to be stuck to the bottom. This is
         * automatically called when the footer row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         * @return {?}
         */
        CdkTable.prototype.updateStickyFooterRowStyles = /**
         * Updates the footer sticky styles. First resets all applied styles with respect to the cells
         * sticking to the bottom. Then, evaluating which cells need to be stuck to the bottom. This is
         * automatically called when the footer row changes its displayed set of columns, or if its
         * sticky input changes. May be called manually for cases where the cell content changes outside
         * of these events.
         * @return {?}
         */
        function () {
            var /** @type {?} */ footerRows = this._getRenderedRows(this._footerRowOutlet);
            this._stickyStyler.clearStickyPositioning(footerRows, ['bottom']);
            var /** @type {?} */ stickyStates = this._footerRowDefs.map(function (def) { return def.sticky; });
            this._stickyStyler.stickRows(footerRows, stickyStates, 'bottom');
            this._stickyStyler.updateStickyFooterContainer(this._elementRef.nativeElement, stickyStates);
            // Reset the dirty state of the sticky input change since it has been used.
            this._footerRowDefs.forEach(function (def) { return def.resetStickyChanged(); });
        };
        /**
         * Updates the column sticky styles. First resets all applied styles with respect to the cells
         * sticking to the left and right. Then sticky styles are added for the left and right according
         * to the column definitions for each cell in each row. This is automatically called when
         * the data source provides a new set of data or when a column definition changes its sticky
         * input. May be called manually for cases where the cell content changes outside of these events.
         */
        /**
         * Updates the column sticky styles. First resets all applied styles with respect to the cells
         * sticking to the left and right. Then sticky styles are added for the left and right according
         * to the column definitions for each cell in each row. This is automatically called when
         * the data source provides a new set of data or when a column definition changes its sticky
         * input. May be called manually for cases where the cell content changes outside of these events.
         * @return {?}
         */
        CdkTable.prototype.updateStickyColumnStyles = /**
         * Updates the column sticky styles. First resets all applied styles with respect to the cells
         * sticking to the left and right. Then sticky styles are added for the left and right according
         * to the column definitions for each cell in each row. This is automatically called when
         * the data source provides a new set of data or when a column definition changes its sticky
         * input. May be called manually for cases where the cell content changes outside of these events.
         * @return {?}
         */
        function () {
            var _this = this;
            var /** @type {?} */ headerRows = this._getRenderedRows(this._headerRowOutlet);
            var /** @type {?} */ dataRows = this._getRenderedRows(this._rowOutlet);
            var /** @type {?} */ footerRows = this._getRenderedRows(this._footerRowOutlet);
            // Clear the left and right positioning from all columns in the table across all rows since
            // sticky columns span across all table sections (header, data, footer)
            this._stickyStyler.clearStickyPositioning(headerRows.concat(dataRows, footerRows), ['left', 'right']);
            // Update the sticky styles for each header row depending on the def's sticky state
            headerRows.forEach(function (headerRow, i) {
                _this._addStickyColumnStyles([headerRow], _this._headerRowDefs[i]);
            });
            // Update the sticky styles for each data row depending on its def's sticky state
            this._rowDefs.forEach(function (rowDef) {
                // Collect all the rows rendered with this row definition.
                var /** @type {?} */ rows = [];
                for (var /** @type {?} */ i = 0; i < dataRows.length; i++) {
                    if (_this._renderRows[i].rowDef === rowDef) {
                        rows.push(dataRows[i]);
                    }
                }
                _this._addStickyColumnStyles(rows, rowDef);
            });
            // Update the sticky styles for each footer row depending on the def's sticky state
            footerRows.forEach(function (footerRow, i) {
                _this._addStickyColumnStyles([footerRow], _this._footerRowDefs[i]);
            });
            // Reset the dirty state of the sticky input change since it has been used.
            Array.from(this._columnDefsByName.values()).forEach(function (def) { return def.resetStickyChanged(); });
        };
        /**
         * Get the list of RenderRow objects to render according to the current list of data and defined
         * row definitions. If the previous list already contained a particular pair, it should be reused
         * so that the differ equates their references.
         * @return {?}
         */
        CdkTable.prototype._getAllRenderRows = /**
         * Get the list of RenderRow objects to render according to the current list of data and defined
         * row definitions. If the previous list already contained a particular pair, it should be reused
         * so that the differ equates their references.
         * @return {?}
         */
        function () {
            var /** @type {?} */ renderRows = [];
            // Store the cache and create a new one. Any re-used RenderRow objects will be moved into the
            // new cache while unused ones can be picked up by garbage collection.
            var /** @type {?} */ prevCachedRenderRows = this._cachedRenderRowsMap;
            this._cachedRenderRowsMap = new Map();
            // For each data object, get the list of rows that should be rendered, represented by the
            // respective `RenderRow` object which is the pair of `data` and `CdkRowDef`.
            for (var /** @type {?} */ i = 0; i < this._data.length; i++) {
                var /** @type {?} */ data = this._data[i];
                var /** @type {?} */ renderRowsForData = this._getRenderRowsForData(data, i, prevCachedRenderRows.get(data));
                if (!this._cachedRenderRowsMap.has(data)) {
                    this._cachedRenderRowsMap.set(data, new WeakMap());
                }
                for (var /** @type {?} */ j = 0; j < renderRowsForData.length; j++) {
                    var /** @type {?} */ renderRow = renderRowsForData[j];
                    var /** @type {?} */ cache = /** @type {?} */ ((this._cachedRenderRowsMap.get(renderRow.data)));
                    if (cache.has(renderRow.rowDef)) {
                        /** @type {?} */ ((cache.get(renderRow.rowDef))).push(renderRow);
                    }
                    else {
                        cache.set(renderRow.rowDef, [renderRow]);
                    }
                    renderRows.push(renderRow);
                }
            }
            return renderRows;
        };
        /**
         * Gets a list of `RenderRow<T>` for the provided data object and any `CdkRowDef` objects that
         * should be rendered for this data. Reuses the cached RenderRow objects if they match the same
         * `(T, CdkRowDef)` pair.
         * @param {?} data
         * @param {?} dataIndex
         * @param {?=} cache
         * @return {?}
         */
        CdkTable.prototype._getRenderRowsForData = /**
         * Gets a list of `RenderRow<T>` for the provided data object and any `CdkRowDef` objects that
         * should be rendered for this data. Reuses the cached RenderRow objects if they match the same
         * `(T, CdkRowDef)` pair.
         * @param {?} data
         * @param {?} dataIndex
         * @param {?=} cache
         * @return {?}
         */
        function (data, dataIndex, cache) {
            var /** @type {?} */ rowDefs = this._getRowDefs(data, dataIndex);
            return rowDefs.map(function (rowDef) {
                var /** @type {?} */ cachedRenderRows = (cache && cache.has(rowDef)) ? /** @type {?} */ ((cache.get(rowDef))) : [];
                if (cachedRenderRows.length) {
                    var /** @type {?} */ dataRow = /** @type {?} */ ((cachedRenderRows.shift()));
                    dataRow.dataIndex = dataIndex;
                    return dataRow;
                }
                else {
                    return { data: data, rowDef: rowDef, dataIndex: dataIndex };
                }
            });
        };
        /**
         * Update the map containing the content's column definitions.
         * @return {?}
         */
        CdkTable.prototype._cacheColumnDefs = /**
         * Update the map containing the content's column definitions.
         * @return {?}
         */
        function () {
            var _this = this;
            this._columnDefsByName.clear();
            var /** @type {?} */ columnDefs = mergeQueryListAndSet(this._contentColumnDefs, this._customColumnDefs);
            columnDefs.forEach(function (columnDef) {
                if (_this._columnDefsByName.has(columnDef.name)) {
                    throw getTableDuplicateColumnNameError(columnDef.name);
                }
                _this._columnDefsByName.set(columnDef.name, columnDef);
            });
        };
        /**
         * Update the list of all available row definitions that can be used.
         * @return {?}
         */
        CdkTable.prototype._cacheRowDefs = /**
         * Update the list of all available row definitions that can be used.
         * @return {?}
         */
        function () {
            this._headerRowDefs =
                mergeQueryListAndSet(this._contentHeaderRowDefs, this._customHeaderRowDefs);
            this._footerRowDefs =
                mergeQueryListAndSet(this._contentFooterRowDefs, this._customFooterRowDefs);
            this._rowDefs =
                mergeQueryListAndSet(this._contentRowDefs, this._customRowDefs);
            // After all row definitions are determined, find the row definition to be considered default.
            var /** @type {?} */ defaultRowDefs = this._rowDefs.filter(function (def) { return !def.when; });
            if (!this.multiTemplateDataRows && defaultRowDefs.length > 1) {
                throw getTableMultipleDefaultRowDefsError();
            }
            this._defaultRowDef = defaultRowDefs[0];
        };
        /**
         * Check if the header, data, or footer rows have changed what columns they want to display or
         * whether the sticky states have changed for the header or footer. If there is a diff, then
         * re-render that section.
         * @return {?}
         */
        CdkTable.prototype._renderUpdatedColumns = /**
         * Check if the header, data, or footer rows have changed what columns they want to display or
         * whether the sticky states have changed for the header or footer. If there is a diff, then
         * re-render that section.
         * @return {?}
         */
        function () {
            var /** @type {?} */ columnsDiffReducer = function (acc, def) { return acc || !!def.getColumnsDiff(); };
            // Force re-render data rows if the list of column definitions have changed.
            if (this._rowDefs.reduce(columnsDiffReducer, false)) {
                this._forceRenderDataRows();
            }
            // Force re-render header/footer rows if the list of column definitions have changed..
            if (this._headerRowDefs.reduce(columnsDiffReducer, false)) {
                this._forceRenderHeaderRows();
            }
            if (this._footerRowDefs.reduce(columnsDiffReducer, false)) {
                this._forceRenderFooterRows();
            }
        };
        /**
         * Switch to the provided data source by resetting the data and unsubscribing from the current
         * render change subscription if one exists. If the data source is null, interpret this by
         * clearing the row outlet. Otherwise start listening for new data.
         * @param {?} dataSource
         * @return {?}
         */
        CdkTable.prototype._switchDataSource = /**
         * Switch to the provided data source by resetting the data and unsubscribing from the current
         * render change subscription if one exists. If the data source is null, interpret this by
         * clearing the row outlet. Otherwise start listening for new data.
         * @param {?} dataSource
         * @return {?}
         */
        function (dataSource) {
            this._data = [];
            if (this.dataSource instanceof DataSource) {
                this.dataSource.disconnect(this);
            }
            // Stop listening for data from the previous data source.
            if (this._renderChangeSubscription) {
                this._renderChangeSubscription.unsubscribe();
                this._renderChangeSubscription = null;
            }
            if (!dataSource) {
                if (this._dataDiffer) {
                    this._dataDiffer.diff([]);
                }
                this._rowOutlet.viewContainer.clear();
            }
            this._dataSource = dataSource;
        };
        /**
         * Set up a subscription for the data provided by the data source.
         * @return {?}
         */
        CdkTable.prototype._observeRenderChanges = /**
         * Set up a subscription for the data provided by the data source.
         * @return {?}
         */
        function () {
            var _this = this;
            // If no data source has been set, there is nothing to observe for changes.
            if (!this.dataSource) {
                return;
            }
            var /** @type {?} */ dataStream;
            // Check if the datasource is a DataSource object by observing if it has a connect function.
            // Cannot check this.dataSource['connect'] due to potential property renaming, nor can it
            // checked as an instanceof DataSource<T> since the table should allow for data sources
            // that did not explicitly extend DataSource<T>.
            if ((/** @type {?} */ (this.dataSource)).connect instanceof Function) {
                dataStream = (/** @type {?} */ (this.dataSource)).connect(this);
            }
            else if (this.dataSource instanceof rxjs.Observable) {
                dataStream = this.dataSource;
            }
            else if (Array.isArray(this.dataSource)) {
                dataStream = rxjs.of(this.dataSource);
            }
            if (dataStream === undefined) {
                throw getTableUnknownDataSourceError();
            }
            this._renderChangeSubscription = dataStream
                .pipe(takeUntil(this._onDestroy))
                .subscribe(function (data) {
                _this._data = data || [];
                _this.renderRows();
            });
        };
        /**
         * Clears any existing content in the header row outlet and creates a new embedded view
         * in the outlet using the header row definition.
         * @return {?}
         */
        CdkTable.prototype._forceRenderHeaderRows = /**
         * Clears any existing content in the header row outlet and creates a new embedded view
         * in the outlet using the header row definition.
         * @return {?}
         */
        function () {
            var _this = this;
            // Clear the header row outlet if any content exists.
            if (this._headerRowOutlet.viewContainer.length > 0) {
                this._headerRowOutlet.viewContainer.clear();
            }
            this._headerRowDefs.forEach(function (def, i) { return _this._renderRow(_this._headerRowOutlet, def, i); });
            this.updateStickyHeaderRowStyles();
            this.updateStickyColumnStyles();
        };
        /**
         * Clears any existing content in the footer row outlet and creates a new embedded view
         * in the outlet using the footer row definition.
         * @return {?}
         */
        CdkTable.prototype._forceRenderFooterRows = /**
         * Clears any existing content in the footer row outlet and creates a new embedded view
         * in the outlet using the footer row definition.
         * @return {?}
         */
        function () {
            var _this = this;
            // Clear the footer row outlet if any content exists.
            if (this._footerRowOutlet.viewContainer.length > 0) {
                this._footerRowOutlet.viewContainer.clear();
            }
            this._footerRowDefs.forEach(function (def, i) { return _this._renderRow(_this._footerRowOutlet, def, i); });
            this.updateStickyFooterRowStyles();
            this.updateStickyColumnStyles();
        };
        /**
         * Adds the sticky column styles for the rows according to the columns' stick states.
         * @param {?} rows
         * @param {?} rowDef
         * @return {?}
         */
        CdkTable.prototype._addStickyColumnStyles = /**
         * Adds the sticky column styles for the rows according to the columns' stick states.
         * @param {?} rows
         * @param {?} rowDef
         * @return {?}
         */
        function (rows, rowDef) {
            var _this = this;
            var /** @type {?} */ columnDefs = Array.from(rowDef.columns || []).map(function (c) { return ((_this._columnDefsByName.get(c))); });
            var /** @type {?} */ stickyStartStates = columnDefs.map(function (columnDef) { return columnDef.sticky; });
            var /** @type {?} */ stickyEndStates = columnDefs.map(function (columnDef) { return columnDef.stickyEnd; });
            this._stickyStyler.updateStickyColumns(rows, stickyStartStates, stickyEndStates);
        };
        /** Gets the list of rows that have been rendered in the row outlet. */
        /**
         * Gets the list of rows that have been rendered in the row outlet.
         * @param {?} rowOutlet
         * @return {?}
         */
        CdkTable.prototype._getRenderedRows = /**
         * Gets the list of rows that have been rendered in the row outlet.
         * @param {?} rowOutlet
         * @return {?}
         */
        function (rowOutlet) {
            var /** @type {?} */ renderedRows = [];
            for (var /** @type {?} */ i = 0; i < rowOutlet.viewContainer.length; i++) {
                var /** @type {?} */ viewRef = (/** @type {?} */ (((rowOutlet.viewContainer.get(i)))));
                renderedRows.push(viewRef.rootNodes[0]);
            }
            return renderedRows;
        };
        /**
         * Get the matching row definitions that should be used for this row data. If there is only
         * one row definition, it is returned. Otherwise, find the row definitions that has a when
         * predicate that returns true with the data. If none return true, return the default row
         * definition.
         */
        /**
         * Get the matching row definitions that should be used for this row data. If there is only
         * one row definition, it is returned. Otherwise, find the row definitions that has a when
         * predicate that returns true with the data. If none return true, return the default row
         * definition.
         * @param {?} data
         * @param {?} dataIndex
         * @return {?}
         */
        CdkTable.prototype._getRowDefs = /**
         * Get the matching row definitions that should be used for this row data. If there is only
         * one row definition, it is returned. Otherwise, find the row definitions that has a when
         * predicate that returns true with the data. If none return true, return the default row
         * definition.
         * @param {?} data
         * @param {?} dataIndex
         * @return {?}
         */
        function (data, dataIndex) {
            if (this._rowDefs.length == 1) {
                return [this._rowDefs[0]];
            }
            var /** @type {?} */ rowDefs = [];
            if (this.multiTemplateDataRows) {
                rowDefs = this._rowDefs.filter(function (def) { return !def.when || def.when(dataIndex, data); });
            }
            else {
                var /** @type {?} */ rowDef = this._rowDefs.find(function (def) { return def.when && def.when(dataIndex, data); }) || this._defaultRowDef;
                if (rowDef) {
                    rowDefs.push(rowDef);
                }
            }
            if (!rowDefs.length) {
                throw getTableMissingMatchingRowDefError(data);
            }
            return rowDefs;
        };
        /**
         * Create the embedded view for the data row template and place it in the correct index location
         * within the data row view container.
         * @param {?} renderRow
         * @param {?} renderIndex
         * @return {?}
         */
        CdkTable.prototype._insertRow = /**
         * Create the embedded view for the data row template and place it in the correct index location
         * within the data row view container.
         * @param {?} renderRow
         * @param {?} renderIndex
         * @return {?}
         */
        function (renderRow, renderIndex) {
            var /** @type {?} */ rowDef = renderRow.rowDef;
            var /** @type {?} */ context = { $implicit: renderRow.data };
            this._renderRow(this._rowOutlet, rowDef, renderIndex, context);
        };
        /**
         * Creates a new row template in the outlet and fills it with the set of cell templates.
         * Optionally takes a context to provide to the row and cells, as well as an optional index
         * of where to place the new row template in the outlet.
         * @param {?} outlet
         * @param {?} rowDef
         * @param {?} index
         * @param {?=} context
         * @return {?}
         */
        CdkTable.prototype._renderRow = /**
         * Creates a new row template in the outlet and fills it with the set of cell templates.
         * Optionally takes a context to provide to the row and cells, as well as an optional index
         * of where to place the new row template in the outlet.
         * @param {?} outlet
         * @param {?} rowDef
         * @param {?} index
         * @param {?=} context
         * @return {?}
         */
        function (outlet, rowDef, index, context) {
            if (context === void 0) { context = {}; }
            // TODO(andrewseguin): enforce that one outlet was instantiated from createEmbeddedView
            outlet.viewContainer.createEmbeddedView(rowDef.template, context, index);
            for (var _a = 0, _b = this._getCellTemplates(rowDef); _a < _b.length; _a++) {
                var cellTemplate = _b[_a];
                if (CdkCellOutlet.mostRecentCellOutlet) {
                    CdkCellOutlet.mostRecentCellOutlet._viewContainer.createEmbeddedView(cellTemplate, context);
                }
            }
            this._changeDetectorRef.markForCheck();
        };
        /**
         * Updates the index-related context for each row to reflect any changes in the index of the rows,
         * e.g. first/last/even/odd.
         * @return {?}
         */
        CdkTable.prototype._updateRowIndexContext = /**
         * Updates the index-related context for each row to reflect any changes in the index of the rows,
         * e.g. first/last/even/odd.
         * @return {?}
         */
        function () {
            var /** @type {?} */ viewContainer = this._rowOutlet.viewContainer;
            for (var /** @type {?} */ renderIndex = 0, /** @type {?} */ count$$1 = viewContainer.length; renderIndex < count$$1; renderIndex++) {
                var /** @type {?} */ viewRef = /** @type {?} */ (viewContainer.get(renderIndex));
                var /** @type {?} */ context = /** @type {?} */ (viewRef.context);
                context.count = count$$1;
                context.first = renderIndex === 0;
                context.last = renderIndex === count$$1 - 1;
                context.even = renderIndex % 2 === 0;
                context.odd = !context.even;
                if (this.multiTemplateDataRows) {
                    context.dataIndex = this._renderRows[renderIndex].dataIndex;
                    context.renderIndex = renderIndex;
                }
                else {
                    context.index = this._renderRows[renderIndex].dataIndex;
                }
            }
        };
        /**
         * Gets the column definitions for the provided row def.
         * @param {?} rowDef
         * @return {?}
         */
        CdkTable.prototype._getCellTemplates = /**
         * Gets the column definitions for the provided row def.
         * @param {?} rowDef
         * @return {?}
         */
        function (rowDef) {
            var _this = this;
            if (!rowDef || !rowDef.columns) {
                return [];
            }
            return Array.from(rowDef.columns, function (columnId) {
                var /** @type {?} */ column = _this._columnDefsByName.get(columnId);
                if (!column) {
                    throw getTableUnknownColumnError(columnId);
                }
                return rowDef.extractCellTemplate(column);
            });
        };
        /**
         * Adds native table sections (e.g. tbody) and moves the row outlets into them.
         * @return {?}
         */
        CdkTable.prototype._applyNativeTableSections = /**
         * Adds native table sections (e.g. tbody) and moves the row outlets into them.
         * @return {?}
         */
        function () {
            var /** @type {?} */ sections = [
                { tag: 'thead', outlet: this._headerRowOutlet },
                { tag: 'tbody', outlet: this._rowOutlet },
                { tag: 'tfoot', outlet: this._footerRowOutlet },
            ];
            for (var _a = 0, sections_1 = sections; _a < sections_1.length; _a++) {
                var section = sections_1[_a];
                var /** @type {?} */ element = document.createElement(section.tag);
                element.appendChild(section.outlet.elementRef.nativeElement);
                this._elementRef.nativeElement.appendChild(element);
            }
        };
        /**
         * Forces a re-render of the data rows. Should be called in cases where there has been an input
         * change that affects the evaluation of which rows should be rendered, e.g. toggling
         * `multiTemplateDataRows` or adding/removing row definitions.
         * @return {?}
         */
        CdkTable.prototype._forceRenderDataRows = /**
         * Forces a re-render of the data rows. Should be called in cases where there has been an input
         * change that affects the evaluation of which rows should be rendered, e.g. toggling
         * `multiTemplateDataRows` or adding/removing row definitions.
         * @return {?}
         */
        function () {
            this._dataDiffer.diff([]);
            this._rowOutlet.viewContainer.clear();
            this.renderRows();
            this.updateStickyColumnStyles();
        };
        /**
         * Checks if there has been a change in sticky states since last check and applies the correct
         * sticky styles. Since checking resets the "dirty" state, this should only be performed once
         * during a change detection and after the inputs are settled (after content check).
         * @return {?}
         */
        CdkTable.prototype._checkStickyStates = /**
         * Checks if there has been a change in sticky states since last check and applies the correct
         * sticky styles. Since checking resets the "dirty" state, this should only be performed once
         * during a change detection and after the inputs are settled (after content check).
         * @return {?}
         */
        function () {
            var /** @type {?} */ stickyCheckReducer = function (acc, d) {
                return acc || d.hasStickyChanged();
            };
            // Note that the check needs to occur for every definition since it notifies the definition
            // that it can reset its dirty state. Using another operator like `some` may short-circuit
            // remaining definitions and leave them in an unchecked state.
            if (this._headerRowDefs.reduce(stickyCheckReducer, false)) {
                this.updateStickyHeaderRowStyles();
            }
            if (this._footerRowDefs.reduce(stickyCheckReducer, false)) {
                this.updateStickyFooterRowStyles();
            }
            if (Array.from(this._columnDefsByName.values()).reduce(stickyCheckReducer, false)) {
                this.updateStickyColumnStyles();
            }
        };
        /**
         * Creates the sticky styler that will be used for sticky rows and columns. Listens
         * for directionality changes and provides the latest direction to the styler. Re-applies column
         * stickiness when directionality changes.
         * @return {?}
         */
        CdkTable.prototype._setupStickyStyler = /**
         * Creates the sticky styler that will be used for sticky rows and columns. Listens
         * for directionality changes and provides the latest direction to the styler. Re-applies column
         * stickiness when directionality changes.
         * @return {?}
         */
        function () {
            var _this = this;
            var /** @type {?} */ direction = this._dir ? this._dir.value : 'ltr';
            this._stickyStyler = new StickyStyler(this._isNativeHtmlTable, this.stickyCssClass, direction);
            (this._dir ? this._dir.change : rxjs.of())
                .pipe(takeUntil(this._onDestroy))
                .subscribe(function (value) {
                _this._stickyStyler.direction = value;
                _this.updateStickyColumnStyles();
            });
        };
        CdkTable.decorators = [
            { type: core.Component, args: [{selector: 'cdk-table, table[cdk-table]',
                        exportAs: 'cdkTable',
                        template: CDK_TABLE_TEMPLATE,
                        host: {
                            'class': 'cdk-table',
                        },
                        encapsulation: core.ViewEncapsulation.None,
                        changeDetection: core.ChangeDetectionStrategy.OnPush,
                    },] },
        ];
        /** @nocollapse */
        CdkTable.ctorParameters = function () { return [
            { type: core.IterableDiffers, },
            { type: core.ChangeDetectorRef, },
            { type: core.ElementRef, },
            { type: undefined, decorators: [{ type: core.Attribute, args: ['role',] },] },
            { type: Directionality, decorators: [{ type: core.Optional },] },
        ]; };
        CdkTable.propDecorators = {
            "trackBy": [{ type: core.Input },],
            "dataSource": [{ type: core.Input },],
            "multiTemplateDataRows": [{ type: core.Input },],
            "_rowOutlet": [{ type: core.ViewChild, args: [DataRowOutlet,] },],
            "_headerRowOutlet": [{ type: core.ViewChild, args: [HeaderRowOutlet,] },],
            "_footerRowOutlet": [{ type: core.ViewChild, args: [FooterRowOutlet,] },],
            "_contentColumnDefs": [{ type: core.ContentChildren, args: [CdkColumnDef,] },],
            "_contentRowDefs": [{ type: core.ContentChildren, args: [CdkRowDef,] },],
            "_contentHeaderRowDefs": [{ type: core.ContentChildren, args: [CdkHeaderRowDef,] },],
            "_contentFooterRowDefs": [{ type: core.ContentChildren, args: [CdkFooterRowDef,] },],
        };
        return CdkTable;
    }());
    /**
     * Utility function that gets a merged list of the entries in a QueryList and values of a Set.
     * @template T
     * @param {?} queryList
     * @param {?} set
     * @return {?}
     */
    function mergeQueryListAndSet(queryList, set) {
        return queryList.toArray().concat(Array.from(set));
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes} checked by tsc
     */
    var /** @type {?} */ EXPORTED_DECLARATIONS = [
        CdkTable,
        CdkRowDef,
        CdkCellDef,
        CdkCellOutlet,
        CdkHeaderCellDef,
        CdkFooterCellDef,
        CdkColumnDef,
        CdkCell,
        CdkRow,
        CdkHeaderCell,
        CdkFooterCell,
        CdkHeaderRow,
        CdkHeaderRowDef,
        CdkFooterRow,
        CdkFooterRowDef,
        DataRowOutlet,
        HeaderRowOutlet,
        FooterRowOutlet,
    ];
    var CdkTableModule = /** @class */ (function () {
        function CdkTableModule() {
        }
        CdkTableModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [common.CommonModule],
                        exports: EXPORTED_DECLARATIONS,
                        declarations: EXPORTED_DECLARATIONS
                    },] },
        ];
        return CdkTableModule;
    }());

    /**
     * @license Angular v6.1.0
     * (c) 2010-2018 Google, Inc. https://angular.io/
     * License: MIT
     */

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Transforms an `HttpRequest` into a stream of `HttpEvent`s, one of which will likely be a
     * `HttpResponse`.
     *
     * `HttpHandler` is injectable. When injected, the handler instance dispatches requests to the
     * first interceptor in the chain, which dispatches to the second, etc, eventually reaching the
     * `HttpBackend`.
     *
     * In an `HttpInterceptor`, the `HttpHandler` parameter is the next interceptor in the chain.
     *
     *
     */
    var HttpHandler = /** @class */ (function () {
        function HttpHandler() {
        }
        return HttpHandler;
    }());
    /**
     * A final `HttpHandler` which will dispatch the request via browser HTTP APIs to a backend.
     *
     * Interceptors sit between the `HttpClient` interface and the `HttpBackend`.
     *
     * When injected, `HttpBackend` dispatches requests directly to the backend, without going
     * through the interceptor chain.
     *
     *
     */
    var HttpBackend = /** @class */ (function () {
        function HttpBackend() {
        }
        return HttpBackend;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Immutable set of Http headers, with lazy parsing.
     *
     */
    var HttpHeaders = /** @class */ (function () {
        function HttpHeaders(headers) {
            var _this = this;
            /**
             * Internal map of lowercased header names to the normalized
             * form of the name (the form seen first).
             */
            this.normalizedNames = new Map();
            /**
             * Queued updates to be materialized the next initialization.
             */
            this.lazyUpdate = null;
            if (!headers) {
                this.headers = new Map();
            }
            else if (typeof headers === 'string') {
                this.lazyInit = function () {
                    _this.headers = new Map();
                    headers.split('\n').forEach(function (line) {
                        var index = line.indexOf(':');
                        if (index > 0) {
                            var name_1 = line.slice(0, index);
                            var key = name_1.toLowerCase();
                            var value = line.slice(index + 1).trim();
                            _this.maybeSetNormalizedName(name_1, key);
                            if (_this.headers.has(key)) {
                                _this.headers.get(key).push(value);
                            }
                            else {
                                _this.headers.set(key, [value]);
                            }
                        }
                    });
                };
            }
            else {
                this.lazyInit = function () {
                    _this.headers = new Map();
                    Object.keys(headers).forEach(function (name) {
                        var values = headers[name];
                        var key = name.toLowerCase();
                        if (typeof values === 'string') {
                            values = [values];
                        }
                        if (values.length > 0) {
                            _this.headers.set(key, values);
                            _this.maybeSetNormalizedName(name, key);
                        }
                    });
                };
            }
        }
        /**
         * Checks for existence of header by given name.
         */
        HttpHeaders.prototype.has = function (name) {
            this.init();
            return this.headers.has(name.toLowerCase());
        };
        /**
         * Returns first header that matches given name.
         */
        HttpHeaders.prototype.get = function (name) {
            this.init();
            var values = this.headers.get(name.toLowerCase());
            return values && values.length > 0 ? values[0] : null;
        };
        /**
         * Returns the names of the headers
         */
        HttpHeaders.prototype.keys = function () {
            this.init();
            return Array.from(this.normalizedNames.values());
        };
        /**
         * Returns list of header values for a given name.
         */
        HttpHeaders.prototype.getAll = function (name) {
            this.init();
            return this.headers.get(name.toLowerCase()) || null;
        };
        HttpHeaders.prototype.append = function (name, value) {
            return this.clone({ name: name, value: value, op: 'a' });
        };
        HttpHeaders.prototype.set = function (name, value) {
            return this.clone({ name: name, value: value, op: 's' });
        };
        HttpHeaders.prototype.delete = function (name, value) {
            return this.clone({ name: name, value: value, op: 'd' });
        };
        HttpHeaders.prototype.maybeSetNormalizedName = function (name, lcName) {
            if (!this.normalizedNames.has(lcName)) {
                this.normalizedNames.set(lcName, name);
            }
        };
        HttpHeaders.prototype.init = function () {
            var _this = this;
            if (!!this.lazyInit) {
                if (this.lazyInit instanceof HttpHeaders) {
                    this.copyFrom(this.lazyInit);
                }
                else {
                    this.lazyInit();
                }
                this.lazyInit = null;
                if (!!this.lazyUpdate) {
                    this.lazyUpdate.forEach(function (update) { return _this.applyUpdate(update); });
                    this.lazyUpdate = null;
                }
            }
        };
        HttpHeaders.prototype.copyFrom = function (other) {
            var _this = this;
            other.init();
            Array.from(other.headers.keys()).forEach(function (key) {
                _this.headers.set(key, other.headers.get(key));
                _this.normalizedNames.set(key, other.normalizedNames.get(key));
            });
        };
        HttpHeaders.prototype.clone = function (update) {
            var clone = new HttpHeaders();
            clone.lazyInit =
                (!!this.lazyInit && this.lazyInit instanceof HttpHeaders) ? this.lazyInit : this;
            clone.lazyUpdate = (this.lazyUpdate || []).concat([update]);
            return clone;
        };
        HttpHeaders.prototype.applyUpdate = function (update) {
            var key = update.name.toLowerCase();
            switch (update.op) {
                case 'a':
                case 's':
                    var value = update.value;
                    if (typeof value === 'string') {
                        value = [value];
                    }
                    if (value.length === 0) {
                        return;
                    }
                    this.maybeSetNormalizedName(update.name, key);
                    var base = (update.op === 'a' ? this.headers.get(key) : undefined) || [];
                    base.push.apply(base, __spread(value));
                    this.headers.set(key, base);
                    break;
                case 'd':
                    var toDelete_1 = update.value;
                    if (!toDelete_1) {
                        this.headers.delete(key);
                        this.normalizedNames.delete(key);
                    }
                    else {
                        var existing = this.headers.get(key);
                        if (!existing) {
                            return;
                        }
                        existing = existing.filter(function (value) { return toDelete_1.indexOf(value) === -1; });
                        if (existing.length === 0) {
                            this.headers.delete(key);
                            this.normalizedNames.delete(key);
                        }
                        else {
                            this.headers.set(key, existing);
                        }
                    }
                    break;
            }
        };
        /**
         * @internal
         */
        HttpHeaders.prototype.forEach = function (fn) {
            var _this = this;
            this.init();
            Array.from(this.normalizedNames.keys())
                .forEach(function (key) { return fn(_this.normalizedNames.get(key), _this.headers.get(key)); });
        };
        return HttpHeaders;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * A `HttpParameterCodec` that uses `encodeURIComponent` and `decodeURIComponent` to
     * serialize and parse URL parameter keys and values.
     *
     *
     */
    var HttpUrlEncodingCodec = /** @class */ (function () {
        function HttpUrlEncodingCodec() {
        }
        HttpUrlEncodingCodec.prototype.encodeKey = function (k) { return standardEncoding(k); };
        HttpUrlEncodingCodec.prototype.encodeValue = function (v) { return standardEncoding(v); };
        HttpUrlEncodingCodec.prototype.decodeKey = function (k) { return decodeURIComponent(k); };
        HttpUrlEncodingCodec.prototype.decodeValue = function (v) { return decodeURIComponent(v); };
        return HttpUrlEncodingCodec;
    }());
    function paramParser(rawParams, codec) {
        var map$$1 = new Map();
        if (rawParams.length > 0) {
            var params = rawParams.split('&');
            params.forEach(function (param) {
                var eqIdx = param.indexOf('=');
                var _a = __read(eqIdx == -1 ?
                    [codec.decodeKey(param), ''] :
                    [codec.decodeKey(param.slice(0, eqIdx)), codec.decodeValue(param.slice(eqIdx + 1))], 2), key = _a[0], val = _a[1];
                var list = map$$1.get(key) || [];
                list.push(val);
                map$$1.set(key, list);
            });
        }
        return map$$1;
    }
    function standardEncoding(v) {
        return encodeURIComponent(v)
            .replace(/%40/gi, '@')
            .replace(/%3A/gi, ':')
            .replace(/%24/gi, '$')
            .replace(/%2C/gi, ',')
            .replace(/%3B/gi, ';')
            .replace(/%2B/gi, '+')
            .replace(/%3D/gi, '=')
            .replace(/%3F/gi, '?')
            .replace(/%2F/gi, '/');
    }
    /**
     * An HTTP request/response body that represents serialized parameters,
     * per the MIME type `application/x-www-form-urlencoded`.
     *
     * This class is immutable - all mutation operations return a new instance.
     *
     *
     */
    var HttpParams = /** @class */ (function () {
        function HttpParams(options) {
            if (options === void 0) { options = {}; }
            var _this = this;
            this.updates = null;
            this.cloneFrom = null;
            this.encoder = options.encoder || new HttpUrlEncodingCodec();
            if (!!options.fromString) {
                if (!!options.fromObject) {
                    throw new Error("Cannot specify both fromString and fromObject.");
                }
                this.map = paramParser(options.fromString, this.encoder);
            }
            else if (!!options.fromObject) {
                this.map = new Map();
                Object.keys(options.fromObject).forEach(function (key) {
                    var value = options.fromObject[key];
                    _this.map.set(key, Array.isArray(value) ? value : [value]);
                });
            }
            else {
                this.map = null;
            }
        }
        /**
         * Check whether the body has one or more values for the given parameter name.
         */
        HttpParams.prototype.has = function (param) {
            this.init();
            return this.map.has(param);
        };
        /**
         * Get the first value for the given parameter name, or `null` if it's not present.
         */
        HttpParams.prototype.get = function (param) {
            this.init();
            var res = this.map.get(param);
            return !!res ? res[0] : null;
        };
        /**
         * Get all values for the given parameter name, or `null` if it's not present.
         */
        HttpParams.prototype.getAll = function (param) {
            this.init();
            return this.map.get(param) || null;
        };
        /**
         * Get all the parameter names for this body.
         */
        HttpParams.prototype.keys = function () {
            this.init();
            return Array.from(this.map.keys());
        };
        /**
         * Construct a new body with an appended value for the given parameter name.
         */
        HttpParams.prototype.append = function (param, value) { return this.clone({ param: param, value: value, op: 'a' }); };
        /**
         * Construct a new body with a new value for the given parameter name.
         */
        HttpParams.prototype.set = function (param, value) { return this.clone({ param: param, value: value, op: 's' }); };
        /**
         * Construct a new body with either the given value for the given parameter
         * removed, if a value is given, or all values for the given parameter removed
         * if not.
         */
        HttpParams.prototype.delete = function (param, value) { return this.clone({ param: param, value: value, op: 'd' }); };
        /**
         * Serialize the body to an encoded string, where key-value pairs (separated by `=`) are
         * separated by `&`s.
         */
        HttpParams.prototype.toString = function () {
            var _this = this;
            this.init();
            return this.keys()
                .map(function (key) {
                var eKey = _this.encoder.encodeKey(key);
                return _this.map.get(key).map(function (value) { return eKey + '=' + _this.encoder.encodeValue(value); })
                    .join('&');
            })
                .join('&');
        };
        HttpParams.prototype.clone = function (update) {
            var clone = new HttpParams({ encoder: this.encoder });
            clone.cloneFrom = this.cloneFrom || this;
            clone.updates = (this.updates || []).concat([update]);
            return clone;
        };
        HttpParams.prototype.init = function () {
            var _this = this;
            if (this.map === null) {
                this.map = new Map();
            }
            if (this.cloneFrom !== null) {
                this.cloneFrom.init();
                this.cloneFrom.keys().forEach(function (key) { return _this.map.set(key, _this.cloneFrom.map.get(key)); });
                this.updates.forEach(function (update) {
                    switch (update.op) {
                        case 'a':
                        case 's':
                            var base = (update.op === 'a' ? _this.map.get(update.param) : undefined) || [];
                            base.push(update.value);
                            _this.map.set(update.param, base);
                            break;
                        case 'd':
                            if (update.value !== undefined) {
                                var base_1 = _this.map.get(update.param) || [];
                                var idx = base_1.indexOf(update.value);
                                if (idx !== -1) {
                                    base_1.splice(idx, 1);
                                }
                                if (base_1.length > 0) {
                                    _this.map.set(update.param, base_1);
                                }
                                else {
                                    _this.map.delete(update.param);
                                }
                            }
                            else {
                                _this.map.delete(update.param);
                                break;
                            }
                    }
                });
                this.cloneFrom = null;
            }
        };
        return HttpParams;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Determine whether the given HTTP method may include a body.
     */
    function mightHaveBody(method) {
        switch (method) {
            case 'DELETE':
            case 'GET':
            case 'HEAD':
            case 'OPTIONS':
            case 'JSONP':
                return false;
            default:
                return true;
        }
    }
    /**
     * Safely assert whether the given value is an ArrayBuffer.
     *
     * In some execution environments ArrayBuffer is not defined.
     */
    function isArrayBuffer(value) {
        return typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer;
    }
    /**
     * Safely assert whether the given value is a Blob.
     *
     * In some execution environments Blob is not defined.
     */
    function isBlob(value) {
        return typeof Blob !== 'undefined' && value instanceof Blob;
    }
    /**
     * Safely assert whether the given value is a FormData instance.
     *
     * In some execution environments FormData is not defined.
     */
    function isFormData(value) {
        return typeof FormData !== 'undefined' && value instanceof FormData;
    }
    /**
     * An outgoing HTTP request with an optional typed body.
     *
     * `HttpRequest` represents an outgoing request, including URL, method,
     * headers, body, and other request configuration options. Instances should be
     * assumed to be immutable. To modify a `HttpRequest`, the `clone`
     * method should be used.
     *
     *
     */
    var HttpRequest = /** @class */ (function () {
        function HttpRequest(method, url, third, fourth) {
            this.url = url;
            /**
             * The request body, or `null` if one isn't set.
             *
             * Bodies are not enforced to be immutable, as they can include a reference to any
             * user-defined data type. However, interceptors should take care to preserve
             * idempotence by treating them as such.
             */
            this.body = null;
            /**
             * Whether this request should be made in a way that exposes progress events.
             *
             * Progress events are expensive (change detection runs on each event) and so
             * they should only be requested if the consumer intends to monitor them.
             */
            this.reportProgress = false;
            /**
             * Whether this request should be sent with outgoing credentials (cookies).
             */
            this.withCredentials = false;
            /**
             * The expected response type of the server.
             *
             * This is used to parse the response appropriately before returning it to
             * the requestee.
             */
            this.responseType = 'json';
            this.method = method.toUpperCase();
            // Next, need to figure out which argument holds the HttpRequestInit
            // options, if any.
            var options;
            // Check whether a body argument is expected. The only valid way to omit
            // the body argument is to use a known no-body method like GET.
            if (mightHaveBody(this.method) || !!fourth) {
                // Body is the third argument, options are the fourth.
                this.body = (third !== undefined) ? third : null;
                options = fourth;
            }
            else {
                // No body required, options are the third argument. The body stays null.
                options = third;
            }
            // If options have been passed, interpret them.
            if (options) {
                // Normalize reportProgress and withCredentials.
                this.reportProgress = !!options.reportProgress;
                this.withCredentials = !!options.withCredentials;
                // Override default response type of 'json' if one is provided.
                if (!!options.responseType) {
                    this.responseType = options.responseType;
                }
                // Override headers if they're provided.
                if (!!options.headers) {
                    this.headers = options.headers;
                }
                if (!!options.params) {
                    this.params = options.params;
                }
            }
            // If no headers have been passed in, construct a new HttpHeaders instance.
            if (!this.headers) {
                this.headers = new HttpHeaders();
            }
            // If no parameters have been passed in, construct a new HttpUrlEncodedParams instance.
            if (!this.params) {
                this.params = new HttpParams();
                this.urlWithParams = url;
            }
            else {
                // Encode the parameters to a string in preparation for inclusion in the URL.
                var params = this.params.toString();
                if (params.length === 0) {
                    // No parameters, the visible URL is just the URL given at creation time.
                    this.urlWithParams = url;
                }
                else {
                    // Does the URL already have query parameters? Look for '?'.
                    var qIdx = url.indexOf('?');
                    // There are 3 cases to handle:
                    // 1) No existing parameters -> append '?' followed by params.
                    // 2) '?' exists and is followed by existing query string ->
                    //    append '&' followed by params.
                    // 3) '?' exists at the end of the url -> append params directly.
                    // This basically amounts to determining the character, if any, with
                    // which to join the URL and parameters.
                    var sep = qIdx === -1 ? '?' : (qIdx < url.length - 1 ? '&' : '');
                    this.urlWithParams = url + sep + params;
                }
            }
        }
        /**
         * Transform the free-form body into a serialized format suitable for
         * transmission to the server.
         */
        HttpRequest.prototype.serializeBody = function () {
            // If no body is present, no need to serialize it.
            if (this.body === null) {
                return null;
            }
            // Check whether the body is already in a serialized form. If so,
            // it can just be returned directly.
            if (isArrayBuffer(this.body) || isBlob(this.body) || isFormData(this.body) ||
                typeof this.body === 'string') {
                return this.body;
            }
            // Check whether the body is an instance of HttpUrlEncodedParams.
            if (this.body instanceof HttpParams) {
                return this.body.toString();
            }
            // Check whether the body is an object or array, and serialize with JSON if so.
            if (typeof this.body === 'object' || typeof this.body === 'boolean' ||
                Array.isArray(this.body)) {
                return JSON.stringify(this.body);
            }
            // Fall back on toString() for everything else.
            return this.body.toString();
        };
        /**
         * Examine the body and attempt to infer an appropriate MIME type
         * for it.
         *
         * If no such type can be inferred, this method will return `null`.
         */
        HttpRequest.prototype.detectContentTypeHeader = function () {
            // An empty body has no content type.
            if (this.body === null) {
                return null;
            }
            // FormData bodies rely on the browser's content type assignment.
            if (isFormData(this.body)) {
                return null;
            }
            // Blobs usually have their own content type. If it doesn't, then
            // no type can be inferred.
            if (isBlob(this.body)) {
                return this.body.type || null;
            }
            // Array buffers have unknown contents and thus no type can be inferred.
            if (isArrayBuffer(this.body)) {
                return null;
            }
            // Technically, strings could be a form of JSON data, but it's safe enough
            // to assume they're plain strings.
            if (typeof this.body === 'string') {
                return 'text/plain';
            }
            // `HttpUrlEncodedParams` has its own content-type.
            if (this.body instanceof HttpParams) {
                return 'application/x-www-form-urlencoded;charset=UTF-8';
            }
            // Arrays, objects, and numbers will be encoded as JSON.
            if (typeof this.body === 'object' || typeof this.body === 'number' ||
                Array.isArray(this.body)) {
                return 'application/json';
            }
            // No type could be inferred.
            return null;
        };
        HttpRequest.prototype.clone = function (update) {
            if (update === void 0) { update = {}; }
            // For method, url, and responseType, take the current value unless
            // it is overridden in the update hash.
            var method = update.method || this.method;
            var url = update.url || this.url;
            var responseType = update.responseType || this.responseType;
            // The body is somewhat special - a `null` value in update.body means
            // whatever current body is present is being overridden with an empty
            // body, whereas an `undefined` value in update.body implies no
            // override.
            var body = (update.body !== undefined) ? update.body : this.body;
            // Carefully handle the boolean options to differentiate between
            // `false` and `undefined` in the update args.
            var withCredentials = (update.withCredentials !== undefined) ? update.withCredentials : this.withCredentials;
            var reportProgress = (update.reportProgress !== undefined) ? update.reportProgress : this.reportProgress;
            // Headers and params may be appended to if `setHeaders` or
            // `setParams` are used.
            var headers = update.headers || this.headers;
            var params = update.params || this.params;
            // Check whether the caller has asked to add headers.
            if (update.setHeaders !== undefined) {
                // Set every requested header.
                headers =
                    Object.keys(update.setHeaders)
                        .reduce(function (headers, name) { return headers.set(name, update.setHeaders[name]); }, headers);
            }
            // Check whether the caller has asked to set params.
            if (update.setParams) {
                // Set every requested param.
                params = Object.keys(update.setParams)
                    .reduce(function (params, param) { return params.set(param, update.setParams[param]); }, params);
            }
            // Finally, construct the new HttpRequest using the pieces from above.
            return new HttpRequest(method, url, body, {
                params: params, headers: headers, reportProgress: reportProgress, responseType: responseType, withCredentials: withCredentials,
            });
        };
        return HttpRequest;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Type enumeration for the different kinds of `HttpEvent`.
     *
     *
     */
    var HttpEventType;
    (function (HttpEventType) {
        /**
         * The request was sent out over the wire.
         */
        HttpEventType[HttpEventType["Sent"] = 0] = "Sent";
        /**
         * An upload progress event was received.
         */
        HttpEventType[HttpEventType["UploadProgress"] = 1] = "UploadProgress";
        /**
         * The response status code and headers were received.
         */
        HttpEventType[HttpEventType["ResponseHeader"] = 2] = "ResponseHeader";
        /**
         * A download progress event was received.
         */
        HttpEventType[HttpEventType["DownloadProgress"] = 3] = "DownloadProgress";
        /**
         * The full response including the body was received.
         */
        HttpEventType[HttpEventType["Response"] = 4] = "Response";
        /**
         * A custom event from an interceptor or a backend.
         */
        HttpEventType[HttpEventType["User"] = 5] = "User";
    })(HttpEventType || (HttpEventType = {}));
    /**
     * Base class for both `HttpResponse` and `HttpHeaderResponse`.
     *
     *
     */
    var HttpResponseBase = /** @class */ (function () {
        /**
         * Super-constructor for all responses.
         *
         * The single parameter accepted is an initialization hash. Any properties
         * of the response passed there will override the default values.
         */
        function HttpResponseBase(init, defaultStatus, defaultStatusText) {
            if (defaultStatus === void 0) { defaultStatus = 200; }
            if (defaultStatusText === void 0) { defaultStatusText = 'OK'; }
            // If the hash has values passed, use them to initialize the response.
            // Otherwise use the default values.
            this.headers = init.headers || new HttpHeaders();
            this.status = init.status !== undefined ? init.status : defaultStatus;
            this.statusText = init.statusText || defaultStatusText;
            this.url = init.url || null;
            // Cache the ok value to avoid defining a getter.
            this.ok = this.status >= 200 && this.status < 300;
        }
        return HttpResponseBase;
    }());
    /**
     * A partial HTTP response which only includes the status and header data,
     * but no response body.
     *
     * `HttpHeaderResponse` is a `HttpEvent` available on the response
     * event stream, only when progress events are requested.
     *
     *
     */
    var HttpHeaderResponse = /** @class */ (function (_super) {
        __extends(HttpHeaderResponse, _super);
        /**
         * Create a new `HttpHeaderResponse` with the given parameters.
         */
        function HttpHeaderResponse(init) {
            if (init === void 0) { init = {}; }
            var _this = _super.call(this, init) || this;
            _this.type = HttpEventType.ResponseHeader;
            return _this;
        }
        /**
         * Copy this `HttpHeaderResponse`, overriding its contents with the
         * given parameter hash.
         */
        HttpHeaderResponse.prototype.clone = function (update) {
            if (update === void 0) { update = {}; }
            // Perform a straightforward initialization of the new HttpHeaderResponse,
            // overriding the current parameters with new ones if given.
            return new HttpHeaderResponse({
                headers: update.headers || this.headers,
                status: update.status !== undefined ? update.status : this.status,
                statusText: update.statusText || this.statusText,
                url: update.url || this.url || undefined,
            });
        };
        return HttpHeaderResponse;
    }(HttpResponseBase));
    /**
     * A full HTTP response, including a typed response body (which may be `null`
     * if one was not returned).
     *
     * `HttpResponse` is a `HttpEvent` available on the response event
     * stream.
     *
     *
     */
    var HttpResponse = /** @class */ (function (_super) {
        __extends(HttpResponse, _super);
        /**
         * Construct a new `HttpResponse`.
         */
        function HttpResponse(init) {
            if (init === void 0) { init = {}; }
            var _this = _super.call(this, init) || this;
            _this.type = HttpEventType.Response;
            _this.body = init.body !== undefined ? init.body : null;
            return _this;
        }
        HttpResponse.prototype.clone = function (update) {
            if (update === void 0) { update = {}; }
            return new HttpResponse({
                body: (update.body !== undefined) ? update.body : this.body,
                headers: update.headers || this.headers,
                status: (update.status !== undefined) ? update.status : this.status,
                statusText: update.statusText || this.statusText,
                url: update.url || this.url || undefined,
            });
        };
        return HttpResponse;
    }(HttpResponseBase));
    /**
     * A response that represents an error or failure, either from a
     * non-successful HTTP status, an error while executing the request,
     * or some other failure which occurred during the parsing of the response.
     *
     * Any error returned on the `Observable` response stream will be
     * wrapped in an `HttpErrorResponse` to provide additional context about
     * the state of the HTTP layer when the error occurred. The error property
     * will contain either a wrapped Error object or the error response returned
     * from the server.
     *
     *
     */
    var HttpErrorResponse = /** @class */ (function (_super) {
        __extends(HttpErrorResponse, _super);
        function HttpErrorResponse(init) {
            var _this = 
            // Initialize with a default status of 0 / Unknown Error.
            _super.call(this, init, 0, 'Unknown Error') || this;
            _this.name = 'HttpErrorResponse';
            /**
             * Errors are never okay, even when the status code is in the 2xx success range.
             */
            _this.ok = false;
            // If the response was successful, then this was a parse error. Otherwise, it was
            // a protocol-level failure of some sort. Either the request failed in transit
            // or the server returned an unsuccessful status code.
            if (_this.status >= 200 && _this.status < 300) {
                _this.message = "Http failure during parsing for " + (init.url || '(unknown url)');
            }
            else {
                _this.message =
                    "Http failure response for " + (init.url || '(unknown url)') + ": " + init.status + " " + init.statusText;
            }
            _this.error = init.error || null;
            return _this;
        }
        return HttpErrorResponse;
    }(HttpResponseBase));

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * Construct an instance of `HttpRequestOptions<T>` from a source `HttpMethodOptions` and
     * the given `body`. Basically, this clones the object and adds the body.
     */
    function addBody(options, body) {
        return {
            body: body,
            headers: options.headers,
            observe: options.observe,
            params: options.params,
            reportProgress: options.reportProgress,
            responseType: options.responseType,
            withCredentials: options.withCredentials,
        };
    }
    /**
     * Perform HTTP requests.
     *
     * `HttpClient` is available as an injectable class, with methods to perform HTTP requests.
     * Each request method has multiple signatures, and the return type varies according to which
     * signature is called (mainly the values of `observe` and `responseType`).
     *
     *
     */
    var HttpClient = /** @class */ (function () {
        function HttpClient(handler) {
            this.handler = handler;
        }
        /**
         * Constructs an `Observable` for a particular HTTP request that, when subscribed,
         * fires the request through the chain of registered interceptors and on to the
         * server.
         *
         * This method can be called in one of two ways. Either an `HttpRequest`
         * instance can be passed directly as the only parameter, or a method can be
         * passed as the first parameter, a string URL as the second, and an
         * options hash as the third.
         *
         * If a `HttpRequest` object is passed directly, an `Observable` of the
         * raw `HttpEvent` stream will be returned.
         *
         * If a request is instead built by providing a URL, the options object
         * determines the return type of `request()`. In addition to configuring
         * request parameters such as the outgoing headers and/or the body, the options
         * hash specifies two key pieces of information about the request: the
         * `responseType` and what to `observe`.
         *
         * The `responseType` value determines how a successful response body will be
         * parsed. If `responseType` is the default `json`, a type interface for the
         * resulting object may be passed as a type parameter to `request()`.
         *
         * The `observe` value determines the return type of `request()`, based on what
         * the consumer is interested in observing. A value of `events` will return an
         * `Observable<HttpEvent>` representing the raw `HttpEvent` stream,
         * including progress events by default. A value of `response` will return an
         * `Observable<HttpResponse<T>>` where the `T` parameter of `HttpResponse`
         * depends on the `responseType` and any optionally provided type parameter.
         * A value of `body` will return an `Observable<T>` with the same `T` body type.
         */
        HttpClient.prototype.request = function (first$$1, url, options) {
            var _this = this;
            if (options === void 0) { options = {}; }
            var req;
            // Firstly, check whether the primary argument is an instance of `HttpRequest`.
            if (first$$1 instanceof HttpRequest) {
                // It is. The other arguments must be undefined (per the signatures) and can be
                // ignored.
                req = first$$1;
            }
            else {
                // It's a string, so it represents a URL. Construct a request based on it,
                // and incorporate the remaining arguments (assuming GET unless a method is
                // provided.
                // Figure out the headers.
                var headers = undefined;
                if (options.headers instanceof HttpHeaders) {
                    headers = options.headers;
                }
                else {
                    headers = new HttpHeaders(options.headers);
                }
                // Sort out parameters.
                var params = undefined;
                if (!!options.params) {
                    if (options.params instanceof HttpParams) {
                        params = options.params;
                    }
                    else {
                        params = new HttpParams({ fromObject: options.params });
                    }
                }
                // Construct the request.
                req = new HttpRequest(first$$1, url, (options.body !== undefined ? options.body : null), {
                    headers: headers,
                    params: params,
                    reportProgress: options.reportProgress,
                    // By default, JSON is assumed to be returned for all calls.
                    responseType: options.responseType || 'json',
                    withCredentials: options.withCredentials,
                });
            }
            // Start with an Observable.of() the initial request, and run the handler (which
            // includes all interceptors) inside a concatMap(). This way, the handler runs
            // inside an Observable chain, which causes interceptors to be re-run on every
            // subscription (this also makes retries re-run the handler, including interceptors).
            var events$ = rxjs.of(req).pipe(concatMap(function (req) { return _this.handler.handle(req); }));
            // If coming via the API signature which accepts a previously constructed HttpRequest,
            // the only option is to get the event stream. Otherwise, return the event stream if
            // that is what was requested.
            if (first$$1 instanceof HttpRequest || options.observe === 'events') {
                return events$;
            }
            // The requested stream contains either the full response or the body. In either
            // case, the first step is to filter the event stream to extract a stream of
            // responses(s).
            var res$ = events$.pipe(filter(function (event) { return event instanceof HttpResponse; }));
            // Decide which stream to return.
            switch (options.observe || 'body') {
                case 'body':
                    // The requested stream is the body. Map the response stream to the response
                    // body. This could be done more simply, but a misbehaving interceptor might
                    // transform the response body into a different format and ignore the requested
                    // responseType. Guard against this by validating that the response is of the
                    // requested type.
                    switch (req.responseType) {
                        case 'arraybuffer':
                            return res$.pipe(map(function (res) {
                                // Validate that the body is an ArrayBuffer.
                                if (res.body !== null && !(res.body instanceof ArrayBuffer)) {
                                    throw new Error('Response is not an ArrayBuffer.');
                                }
                                return res.body;
                            }));
                        case 'blob':
                            return res$.pipe(map(function (res) {
                                // Validate that the body is a Blob.
                                if (res.body !== null && !(res.body instanceof Blob)) {
                                    throw new Error('Response is not a Blob.');
                                }
                                return res.body;
                            }));
                        case 'text':
                            return res$.pipe(map(function (res) {
                                // Validate that the body is a string.
                                if (res.body !== null && typeof res.body !== 'string') {
                                    throw new Error('Response is not a string.');
                                }
                                return res.body;
                            }));
                        case 'json':
                        default:
                            // No validation needed for JSON responses, as they can be of any type.
                            return res$.pipe(map(function (res) { return res.body; }));
                    }
                case 'response':
                    // The response stream was requested directly, so return it.
                    return res$;
                default:
                    // Guard against new future observe types being added.
                    throw new Error("Unreachable: unhandled observe type " + options.observe + "}");
            }
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * DELETE request to be executed on the server. See the individual overloads for
         * details of `delete()`'s return type based on the provided options.
         */
        HttpClient.prototype.delete = function (url, options) {
            if (options === void 0) { options = {}; }
            return this.request('DELETE', url, options);
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * GET request to be executed on the server. See the individual overloads for
         * details of `get()`'s return type based on the provided options.
         */
        HttpClient.prototype.get = function (url, options) {
            if (options === void 0) { options = {}; }
            return this.request('GET', url, options);
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * HEAD request to be executed on the server. See the individual overloads for
         * details of `head()`'s return type based on the provided options.
         */
        HttpClient.prototype.head = function (url, options) {
            if (options === void 0) { options = {}; }
            return this.request('HEAD', url, options);
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause a request
         * with the special method `JSONP` to be dispatched via the interceptor pipeline.
         *
         * A suitable interceptor must be installed (e.g. via the `HttpClientJsonpModule`).
         * If no such interceptor is reached, then the `JSONP` request will likely be
         * rejected by the configured backend.
         */
        HttpClient.prototype.jsonp = function (url, callbackParam) {
            return this.request('JSONP', url, {
                params: new HttpParams().append(callbackParam, 'JSONP_CALLBACK'),
                observe: 'body',
                responseType: 'json',
            });
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * OPTIONS request to be executed on the server. See the individual overloads for
         * details of `options()`'s return type based on the provided options.
         */
        HttpClient.prototype.options = function (url, options) {
            if (options === void 0) { options = {}; }
            return this.request('OPTIONS', url, options);
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * PATCH request to be executed on the server. See the individual overloads for
         * details of `patch()`'s return type based on the provided options.
         */
        HttpClient.prototype.patch = function (url, body, options) {
            if (options === void 0) { options = {}; }
            return this.request('PATCH', url, addBody(options, body));
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * POST request to be executed on the server. See the individual overloads for
         * details of `post()`'s return type based on the provided options.
         */
        HttpClient.prototype.post = function (url, body, options) {
            if (options === void 0) { options = {}; }
            return this.request('POST', url, addBody(options, body));
        };
        /**
         * Constructs an `Observable` which, when subscribed, will cause the configured
         * PUT request to be executed on the server. See the individual overloads for
         * details of `put()`'s return type based on the provided options.
         */
        HttpClient.prototype.put = function (url, body, options) {
            if (options === void 0) { options = {}; }
            return this.request('PUT', url, addBody(options, body));
        };
        HttpClient.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        HttpClient.ctorParameters = function () { return [
            { type: HttpHandler }
        ]; };
        return HttpClient;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * `HttpHandler` which applies an `HttpInterceptor` to an `HttpRequest`.
     *
     *
     */
    var HttpInterceptorHandler = /** @class */ (function () {
        function HttpInterceptorHandler(next, interceptor) {
            this.next = next;
            this.interceptor = interceptor;
        }
        HttpInterceptorHandler.prototype.handle = function (req) {
            return this.interceptor.intercept(req, this.next);
        };
        return HttpInterceptorHandler;
    }());
    /**
     * A multi-provider token which represents the array of `HttpInterceptor`s that
     * are registered.
     *
     *
     */
    var HTTP_INTERCEPTORS = new core.InjectionToken('HTTP_INTERCEPTORS');
    var NoopInterceptor = /** @class */ (function () {
        function NoopInterceptor() {
        }
        NoopInterceptor.prototype.intercept = function (req, next) {
            return next.handle(req);
        };
        NoopInterceptor.decorators = [
            { type: core.Injectable }
        ];
        return NoopInterceptor;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    // Every request made through JSONP needs a callback name that's unique across the
    // whole page. Each request is assigned an id and the callback name is constructed
    // from that. The next id to be assigned is tracked in a global variable here that
    // is shared among all applications on the page.
    var nextRequestId = 0;
    // Error text given when a JSONP script is injected, but doesn't invoke the callback
    // passed in its URL.
    var JSONP_ERR_NO_CALLBACK = 'JSONP injected script did not invoke callback.';
    // Error text given when a request is passed to the JsonpClientBackend that doesn't
    // have a request method JSONP.
    var JSONP_ERR_WRONG_METHOD = 'JSONP requests must use JSONP request method.';
    var JSONP_ERR_WRONG_RESPONSE_TYPE = 'JSONP requests must use Json response type.';
    /**
     * DI token/abstract type representing a map of JSONP callbacks.
     *
     * In the browser, this should always be the `window` object.
     *
     *
     */
    var JsonpCallbackContext = /** @class */ (function () {
        function JsonpCallbackContext() {
        }
        return JsonpCallbackContext;
    }());
    /**
     * `HttpBackend` that only processes `HttpRequest` with the JSONP method,
     * by performing JSONP style requests.
     *
     *
     */
    var JsonpClientBackend = /** @class */ (function () {
        function JsonpClientBackend(callbackMap, document) {
            this.callbackMap = callbackMap;
            this.document = document;
        }
        /**
         * Get the name of the next callback method, by incrementing the global `nextRequestId`.
         */
        JsonpClientBackend.prototype.nextCallback = function () { return "ng_jsonp_callback_" + nextRequestId++; };
        /**
         * Process a JSONP request and return an event stream of the results.
         */
        JsonpClientBackend.prototype.handle = function (req) {
            var _this = this;
            // Firstly, check both the method and response type. If either doesn't match
            // then the request was improperly routed here and cannot be handled.
            if (req.method !== 'JSONP') {
                throw new Error(JSONP_ERR_WRONG_METHOD);
            }
            else if (req.responseType !== 'json') {
                throw new Error(JSONP_ERR_WRONG_RESPONSE_TYPE);
            }
            // Everything else happens inside the Observable boundary.
            return new rxjs.Observable(function (observer) {
                // The first step to make a request is to generate the callback name, and replace the
                // callback placeholder in the URL with the name. Care has to be taken here to ensure
                // a trailing &, if matched, gets inserted back into the URL in the correct place.
                var callback = _this.nextCallback();
                var url = req.urlWithParams.replace(/=JSONP_CALLBACK(&|$)/, "=" + callback + "$1");
                // Construct the <script> tag and point it at the URL.
                var node = _this.document.createElement('script');
                node.src = url;
                // A JSONP request requires waiting for multiple callbacks. These variables
                // are closed over and track state across those callbacks.
                // The response object, if one has been received, or null otherwise.
                var body = null;
                // Whether the response callback has been called.
                var finished = false;
                // Whether the request has been cancelled (and thus any other callbacks)
                // should be ignored.
                var cancelled = false;
                // Set the response callback in this.callbackMap (which will be the window
                // object in the browser. The script being loaded via the <script> tag will
                // eventually call this callback.
                _this.callbackMap[callback] = function (data) {
                    // Data has been received from the JSONP script. Firstly, delete this callback.
                    delete _this.callbackMap[callback];
                    // Next, make sure the request wasn't cancelled in the meantime.
                    if (cancelled) {
                        return;
                    }
                    // Set state to indicate data was received.
                    body = data;
                    finished = true;
                };
                // cleanup() is a utility closure that removes the <script> from the page and
                // the response callback from the window. This logic is used in both the
                // success, error, and cancellation paths, so it's extracted out for convenience.
                var cleanup = function () {
                    // Remove the <script> tag if it's still on the page.
                    if (node.parentNode) {
                        node.parentNode.removeChild(node);
                    }
                    // Remove the response callback from the callbackMap (window object in the
                    // browser).
                    delete _this.callbackMap[callback];
                };
                // onLoad() is the success callback which runs after the response callback
                // if the JSONP script loads successfully. The event itself is unimportant.
                // If something went wrong, onLoad() may run without the response callback
                // having been invoked.
                var onLoad = function (event) {
                    // Do nothing if the request has been cancelled.
                    if (cancelled) {
                        return;
                    }
                    // Cleanup the page.
                    cleanup();
                    // Check whether the response callback has run.
                    if (!finished) {
                        // It hasn't, something went wrong with the request. Return an error via
                        // the Observable error path. All JSONP errors have status 0.
                        observer.error(new HttpErrorResponse({
                            url: url,
                            status: 0,
                            statusText: 'JSONP Error',
                            error: new Error(JSONP_ERR_NO_CALLBACK),
                        }));
                        return;
                    }
                    // Success. body either contains the response body or null if none was
                    // returned.
                    observer.next(new HttpResponse({
                        body: body,
                        status: 200,
                        statusText: 'OK', url: url,
                    }));
                    // Complete the stream, the response is over.
                    observer.complete();
                };
                // onError() is the error callback, which runs if the script returned generates
                // a Javascript error. It emits the error via the Observable error channel as
                // a HttpErrorResponse.
                var onError = function (error) {
                    // If the request was already cancelled, no need to emit anything.
                    if (cancelled) {
                        return;
                    }
                    cleanup();
                    // Wrap the error in a HttpErrorResponse.
                    observer.error(new HttpErrorResponse({
                        error: error,
                        status: 0,
                        statusText: 'JSONP Error', url: url,
                    }));
                };
                // Subscribe to both the success (load) and error events on the <script> tag,
                // and add it to the page.
                node.addEventListener('load', onLoad);
                node.addEventListener('error', onError);
                _this.document.body.appendChild(node);
                // The request has now been successfully sent.
                observer.next({ type: HttpEventType.Sent });
                // Cancellation handler.
                return function () {
                    // Track the cancellation so event listeners won't do anything even if already scheduled.
                    cancelled = true;
                    // Remove the event listeners so they won't run if the events later fire.
                    node.removeEventListener('load', onLoad);
                    node.removeEventListener('error', onError);
                    // And finally, clean up the page.
                    cleanup();
                };
            });
        };
        JsonpClientBackend.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        JsonpClientBackend.ctorParameters = function () { return [
            { type: JsonpCallbackContext },
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] }
        ]; };
        return JsonpClientBackend;
    }());
    /**
     * An `HttpInterceptor` which identifies requests with the method JSONP and
     * shifts them to the `JsonpClientBackend`.
     *
     *
     */
    var JsonpInterceptor = /** @class */ (function () {
        function JsonpInterceptor(jsonp) {
            this.jsonp = jsonp;
        }
        JsonpInterceptor.prototype.intercept = function (req, next) {
            if (req.method === 'JSONP') {
                return this.jsonp.handle(req);
            }
            // Fall through for normal HTTP requests.
            return next.handle(req);
        };
        JsonpInterceptor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        JsonpInterceptor.ctorParameters = function () { return [
            { type: JsonpClientBackend }
        ]; };
        return JsonpInterceptor;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var XSSI_PREFIX = /^\)\]\}',?\n/;
    /**
     * Determine an appropriate URL for the response, by checking either
     * XMLHttpRequest.responseURL or the X-Request-URL header.
     */
    function getResponseUrl(xhr) {
        if ('responseURL' in xhr && xhr.responseURL) {
            return xhr.responseURL;
        }
        if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
            return xhr.getResponseHeader('X-Request-URL');
        }
        return null;
    }
    /**
     * A wrapper around the `XMLHttpRequest` constructor.
     *
     *
     */
    var XhrFactory = /** @class */ (function () {
        function XhrFactory() {
        }
        return XhrFactory;
    }());
    /**
     * A factory for @{link HttpXhrBackend} that uses the `XMLHttpRequest` browser API.
     *
     *
     */
    var BrowserXhr = /** @class */ (function () {
        function BrowserXhr() {
        }
        BrowserXhr.prototype.build = function () { return (new XMLHttpRequest()); };
        BrowserXhr.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        BrowserXhr.ctorParameters = function () { return []; };
        return BrowserXhr;
    }());
    /**
     * An `HttpBackend` which uses the XMLHttpRequest API to send
     * requests to a backend server.
     *
     *
     */
    var HttpXhrBackend = /** @class */ (function () {
        function HttpXhrBackend(xhrFactory) {
            this.xhrFactory = xhrFactory;
        }
        /**
         * Process a request and return a stream of response events.
         */
        HttpXhrBackend.prototype.handle = function (req) {
            var _this = this;
            // Quick check to give a better error message when a user attempts to use
            // HttpClient.jsonp() without installing the JsonpClientModule
            if (req.method === 'JSONP') {
                throw new Error("Attempted to construct Jsonp request without JsonpClientModule installed.");
            }
            // Everything happens on Observable subscription.
            return new rxjs.Observable(function (observer) {
                // Start by setting up the XHR object with request method, URL, and withCredentials flag.
                var xhr = _this.xhrFactory.build();
                xhr.open(req.method, req.urlWithParams);
                if (!!req.withCredentials) {
                    xhr.withCredentials = true;
                }
                // Add all the requested headers.
                req.headers.forEach(function (name, values) { return xhr.setRequestHeader(name, values.join(',')); });
                // Add an Accept header if one isn't present already.
                if (!req.headers.has('Accept')) {
                    xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
                }
                // Auto-detect the Content-Type header if one isn't present already.
                if (!req.headers.has('Content-Type')) {
                    var detectedType = req.detectContentTypeHeader();
                    // Sometimes Content-Type detection fails.
                    if (detectedType !== null) {
                        xhr.setRequestHeader('Content-Type', detectedType);
                    }
                }
                // Set the responseType if one was requested.
                if (req.responseType) {
                    var responseType = req.responseType.toLowerCase();
                    // JSON responses need to be processed as text. This is because if the server
                    // returns an XSSI-prefixed JSON response, the browser will fail to parse it,
                    // xhr.response will be null, and xhr.responseText cannot be accessed to
                    // retrieve the prefixed JSON data in order to strip the prefix. Thus, all JSON
                    // is parsed by first requesting text and then applying JSON.parse.
                    xhr.responseType = ((responseType !== 'json') ? responseType : 'text');
                }
                // Serialize the request body if one is present. If not, this will be set to null.
                var reqBody = req.serializeBody();
                // If progress events are enabled, response headers will be delivered
                // in two events - the HttpHeaderResponse event and the full HttpResponse
                // event. However, since response headers don't change in between these
                // two events, it doesn't make sense to parse them twice. So headerResponse
                // caches the data extracted from the response whenever it's first parsed,
                // to ensure parsing isn't duplicated.
                var headerResponse = null;
                // partialFromXhr extracts the HttpHeaderResponse from the current XMLHttpRequest
                // state, and memoizes it into headerResponse.
                var partialFromXhr = function () {
                    if (headerResponse !== null) {
                        return headerResponse;
                    }
                    // Read status and normalize an IE9 bug (http://bugs.jquery.com/ticket/1450).
                    var status = xhr.status === 1223 ? 204 : xhr.status;
                    var statusText = xhr.statusText || 'OK';
                    // Parse headers from XMLHttpRequest - this step is lazy.
                    var headers = new HttpHeaders(xhr.getAllResponseHeaders());
                    // Read the response URL from the XMLHttpResponse instance and fall back on the
                    // request URL.
                    var url = getResponseUrl(xhr) || req.url;
                    // Construct the HttpHeaderResponse and memoize it.
                    headerResponse = new HttpHeaderResponse({ headers: headers, status: status, statusText: statusText, url: url });
                    return headerResponse;
                };
                // Next, a few closures are defined for the various events which XMLHttpRequest can
                // emit. This allows them to be unregistered as event listeners later.
                // First up is the load event, which represents a response being fully available.
                var onLoad = function () {
                    // Read response state from the memoized partial data.
                    var _a = partialFromXhr(), headers = _a.headers, status = _a.status, statusText = _a.statusText, url = _a.url;
                    // The body will be read out if present.
                    var body = null;
                    if (status !== 204) {
                        // Use XMLHttpRequest.response if set, responseText otherwise.
                        body = (typeof xhr.response === 'undefined') ? xhr.responseText : xhr.response;
                    }
                    // Normalize another potential bug (this one comes from CORS).
                    if (status === 0) {
                        status = !!body ? 200 : 0;
                    }
                    // ok determines whether the response will be transmitted on the event or
                    // error channel. Unsuccessful status codes (not 2xx) will always be errors,
                    // but a successful status code can still result in an error if the user
                    // asked for JSON data and the body cannot be parsed as such.
                    var ok = status >= 200 && status < 300;
                    // Check whether the body needs to be parsed as JSON (in many cases the browser
                    // will have done that already).
                    if (req.responseType === 'json' && typeof body === 'string') {
                        // Save the original body, before attempting XSSI prefix stripping.
                        var originalBody = body;
                        body = body.replace(XSSI_PREFIX, '');
                        try {
                            // Attempt the parse. If it fails, a parse error should be delivered to the user.
                            body = body !== '' ? JSON.parse(body) : null;
                        }
                        catch (error) {
                            // Since the JSON.parse failed, it's reasonable to assume this might not have been a
                            // JSON response. Restore the original body (including any XSSI prefix) to deliver
                            // a better error response.
                            body = originalBody;
                            // If this was an error request to begin with, leave it as a string, it probably
                            // just isn't JSON. Otherwise, deliver the parsing error to the user.
                            if (ok) {
                                // Even though the response status was 2xx, this is still an error.
                                ok = false;
                                // The parse error contains the text of the body that failed to parse.
                                body = { error: error, text: body };
                            }
                        }
                    }
                    if (ok) {
                        // A successful response is delivered on the event stream.
                        observer.next(new HttpResponse({
                            body: body,
                            headers: headers,
                            status: status,
                            statusText: statusText,
                            url: url || undefined,
                        }));
                        // The full body has been received and delivered, no further events
                        // are possible. This request is complete.
                        observer.complete();
                    }
                    else {
                        // An unsuccessful request is delivered on the error channel.
                        observer.error(new HttpErrorResponse({
                            // The error in this case is the response body (error from the server).
                            error: body,
                            headers: headers,
                            status: status,
                            statusText: statusText,
                            url: url || undefined,
                        }));
                    }
                };
                // The onError callback is called when something goes wrong at the network level.
                // Connection timeout, DNS error, offline, etc. These are actual errors, and are
                // transmitted on the error channel.
                var onError = function (error) {
                    var res = new HttpErrorResponse({
                        error: error,
                        status: xhr.status || 0,
                        statusText: xhr.statusText || 'Unknown Error',
                    });
                    observer.error(res);
                };
                // The sentHeaders flag tracks whether the HttpResponseHeaders event
                // has been sent on the stream. This is necessary to track if progress
                // is enabled since the event will be sent on only the first download
                // progerss event.
                var sentHeaders = false;
                // The download progress event handler, which is only registered if
                // progress events are enabled.
                var onDownProgress = function (event) {
                    // Send the HttpResponseHeaders event if it hasn't been sent already.
                    if (!sentHeaders) {
                        observer.next(partialFromXhr());
                        sentHeaders = true;
                    }
                    // Start building the download progress event to deliver on the response
                    // event stream.
                    var progressEvent = {
                        type: HttpEventType.DownloadProgress,
                        loaded: event.loaded,
                    };
                    // Set the total number of bytes in the event if it's available.
                    if (event.lengthComputable) {
                        progressEvent.total = event.total;
                    }
                    // If the request was for text content and a partial response is
                    // available on XMLHttpRequest, include it in the progress event
                    // to allow for streaming reads.
                    if (req.responseType === 'text' && !!xhr.responseText) {
                        progressEvent.partialText = xhr.responseText;
                    }
                    // Finally, fire the event.
                    observer.next(progressEvent);
                };
                // The upload progress event handler, which is only registered if
                // progress events are enabled.
                var onUpProgress = function (event) {
                    // Upload progress events are simpler. Begin building the progress
                    // event.
                    var progress = {
                        type: HttpEventType.UploadProgress,
                        loaded: event.loaded,
                    };
                    // If the total number of bytes being uploaded is available, include
                    // it.
                    if (event.lengthComputable) {
                        progress.total = event.total;
                    }
                    // Send the event.
                    observer.next(progress);
                };
                // By default, register for load and error events.
                xhr.addEventListener('load', onLoad);
                xhr.addEventListener('error', onError);
                // Progress events are only enabled if requested.
                if (req.reportProgress) {
                    // Download progress is always enabled if requested.
                    xhr.addEventListener('progress', onDownProgress);
                    // Upload progress depends on whether there is a body to upload.
                    if (reqBody !== null && xhr.upload) {
                        xhr.upload.addEventListener('progress', onUpProgress);
                    }
                }
                // Fire the request, and notify the event stream that it was fired.
                xhr.send(reqBody);
                observer.next({ type: HttpEventType.Sent });
                // This is the return from the Observable function, which is the
                // request cancellation handler.
                return function () {
                    // On a cancellation, remove all registered event listeners.
                    xhr.removeEventListener('error', onError);
                    xhr.removeEventListener('load', onLoad);
                    if (req.reportProgress) {
                        xhr.removeEventListener('progress', onDownProgress);
                        if (reqBody !== null && xhr.upload) {
                            xhr.upload.removeEventListener('progress', onUpProgress);
                        }
                    }
                    // Finally, abort the in-flight request.
                    xhr.abort();
                };
            });
        };
        HttpXhrBackend.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        HttpXhrBackend.ctorParameters = function () { return [
            { type: XhrFactory }
        ]; };
        return HttpXhrBackend;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var XSRF_COOKIE_NAME = new core.InjectionToken('XSRF_COOKIE_NAME');
    var XSRF_HEADER_NAME = new core.InjectionToken('XSRF_HEADER_NAME');
    /**
     * Retrieves the current XSRF token to use with the next outgoing request.
     *
     *
     */
    var HttpXsrfTokenExtractor = /** @class */ (function () {
        function HttpXsrfTokenExtractor() {
        }
        return HttpXsrfTokenExtractor;
    }());
    /**
     * `HttpXsrfTokenExtractor` which retrieves the token from a cookie.
     */
    var HttpXsrfCookieExtractor = /** @class */ (function () {
        function HttpXsrfCookieExtractor(doc, platform, cookieName) {
            this.doc = doc;
            this.platform = platform;
            this.cookieName = cookieName;
            this.lastCookieString = '';
            this.lastToken = null;
            /**
             * @internal for testing
             */
            this.parseCount = 0;
        }
        HttpXsrfCookieExtractor.prototype.getToken = function () {
            if (this.platform === 'server') {
                return null;
            }
            var cookieString = this.doc.cookie || '';
            if (cookieString !== this.lastCookieString) {
                this.parseCount++;
                this.lastToken = common.ɵparseCookieValue(cookieString, this.cookieName);
                this.lastCookieString = cookieString;
            }
            return this.lastToken;
        };
        HttpXsrfCookieExtractor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        HttpXsrfCookieExtractor.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: String, decorators: [{ type: core.Inject, args: [core.PLATFORM_ID,] }] },
            { type: String, decorators: [{ type: core.Inject, args: [XSRF_COOKIE_NAME,] }] }
        ]; };
        return HttpXsrfCookieExtractor;
    }());
    /**
     * `HttpInterceptor` which adds an XSRF token to eligible outgoing requests.
     */
    var HttpXsrfInterceptor = /** @class */ (function () {
        function HttpXsrfInterceptor(tokenService, headerName) {
            this.tokenService = tokenService;
            this.headerName = headerName;
        }
        HttpXsrfInterceptor.prototype.intercept = function (req, next) {
            var lcUrl = req.url.toLowerCase();
            // Skip both non-mutating requests and absolute URLs.
            // Non-mutating requests don't require a token, and absolute URLs require special handling
            // anyway as the cookie set
            // on our origin is not the same as the token expected by another origin.
            if (req.method === 'GET' || req.method === 'HEAD' || lcUrl.startsWith('http://') ||
                lcUrl.startsWith('https://')) {
                return next.handle(req);
            }
            var token = this.tokenService.getToken();
            // Be careful not to overwrite an existing header of the same name.
            if (token !== null && !req.headers.has(this.headerName)) {
                req = req.clone({ headers: req.headers.set(this.headerName, token) });
            }
            return next.handle(req);
        };
        HttpXsrfInterceptor.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        HttpXsrfInterceptor.ctorParameters = function () { return [
            { type: HttpXsrfTokenExtractor },
            { type: String, decorators: [{ type: core.Inject, args: [XSRF_HEADER_NAME,] }] }
        ]; };
        return HttpXsrfInterceptor;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * An injectable `HttpHandler` that applies multiple interceptors
     * to a request before passing it to the given `HttpBackend`.
     *
     * The interceptors are loaded lazily from the injector, to allow
     * interceptors to themselves inject classes depending indirectly
     * on `HttpInterceptingHandler` itself.
     * @see `HttpInterceptor`
     */
    var HttpInterceptingHandler = /** @class */ (function () {
        function HttpInterceptingHandler(backend, injector) {
            this.backend = backend;
            this.injector = injector;
            this.chain = null;
        }
        HttpInterceptingHandler.prototype.handle = function (req) {
            if (this.chain === null) {
                var interceptors = this.injector.get(HTTP_INTERCEPTORS, []);
                this.chain = interceptors.reduceRight(function (next, interceptor) { return new HttpInterceptorHandler(next, interceptor); }, this.backend);
            }
            return this.chain.handle(req);
        };
        HttpInterceptingHandler.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        HttpInterceptingHandler.ctorParameters = function () { return [
            { type: HttpBackend },
            { type: core.Injector }
        ]; };
        return HttpInterceptingHandler;
    }());
    /**
     * An NgModule that adds XSRF protection support to outgoing requests.
     *
     * For a server that supports a cookie-based XSRF protection system,
     * use directly to configure XSRF protection with the correct
     * cookie and header names.
     *
     * If no names are supplied, the default cookie name is `XSRF-TOKEN`
     * and the default header name is `X-XSRF-TOKEN`.
     *
     *
     */
    var HttpClientXsrfModule = /** @class */ (function () {
        function HttpClientXsrfModule() {
        }
        /**
         * Disable the default XSRF protection.
         */
        HttpClientXsrfModule.disable = function () {
            return {
                ngModule: HttpClientXsrfModule,
                providers: [
                    { provide: HttpXsrfInterceptor, useClass: NoopInterceptor },
                ],
            };
        };
        /**
         * Configure XSRF protection.
         * @param options An object that can specify either or both
         * cookie name or header name.
         * - Cookie name default is `XSRF-TOKEN`.
         * - Header name default is `X-XSRF-TOKEN`.
         *
         */
        HttpClientXsrfModule.withOptions = function (options) {
            if (options === void 0) { options = {}; }
            return {
                ngModule: HttpClientXsrfModule,
                providers: [
                    options.cookieName ? { provide: XSRF_COOKIE_NAME, useValue: options.cookieName } : [],
                    options.headerName ? { provide: XSRF_HEADER_NAME, useValue: options.headerName } : [],
                ],
            };
        };
        HttpClientXsrfModule.decorators = [
            { type: core.NgModule, args: [{
                        providers: [
                            HttpXsrfInterceptor,
                            { provide: HTTP_INTERCEPTORS, useExisting: HttpXsrfInterceptor, multi: true },
                            { provide: HttpXsrfTokenExtractor, useClass: HttpXsrfCookieExtractor },
                            { provide: XSRF_COOKIE_NAME, useValue: 'XSRF-TOKEN' },
                            { provide: XSRF_HEADER_NAME, useValue: 'X-XSRF-TOKEN' },
                        ],
                    },] }
        ];
        return HttpClientXsrfModule;
    }());
    /**
     * An NgModule that provides the `HttpClient` and associated services.
     *
     * Interceptors can be added to the chain behind `HttpClient` by binding them
     * to the multiprovider for `HTTP_INTERCEPTORS`.
     *
     *
     */
    var HttpClientModule = /** @class */ (function () {
        function HttpClientModule() {
        }
        HttpClientModule.decorators = [
            { type: core.NgModule, args: [{
                        /**
                         * Optional configuration for XSRF protection.
                         */
                        imports: [
                            HttpClientXsrfModule.withOptions({
                                cookieName: 'XSRF-TOKEN',
                                headerName: 'X-XSRF-TOKEN',
                            }),
                        ],
                        /**
                         * The module provides `HttpClient` itself, and supporting services.
                         */
                        providers: [
                            HttpClient,
                            { provide: HttpHandler, useClass: HttpInterceptingHandler },
                            HttpXhrBackend,
                            { provide: HttpBackend, useExisting: HttpXhrBackend },
                            BrowserXhr,
                            { provide: XhrFactory, useExisting: BrowserXhr },
                        ],
                    },] }
        ];
        return HttpClientModule;
    }());

    /**
     * @license Angular v6.1.0
     * (c) 2010-2018 Google, Inc. https://angular.io/
     * License: MIT
     */

    function isBrowser() {
        return (typeof window !== 'undefined' && typeof window.document !== 'undefined');
    }
    function isNode() {
        return (typeof process !== 'undefined');
    }
    function optimizeGroupPlayer(players) {
        switch (players.length) {
            case 0:
                return new animations.NoopAnimationPlayer();
            case 1:
                return players[0];
            default:
                return new animations.ɵAnimationGroupPlayer(players);
        }
    }
    function normalizeKeyframes(driver, normalizer, element, keyframes, preStyles, postStyles) {
        if (preStyles === void 0) { preStyles = {}; }
        if (postStyles === void 0) { postStyles = {}; }
        var errors = [];
        var normalizedKeyframes = [];
        var previousOffset = -1;
        var previousKeyframe = null;
        keyframes.forEach(function (kf) {
            var offset = kf['offset'];
            var isSameOffset = offset == previousOffset;
            var normalizedKeyframe = (isSameOffset && previousKeyframe) || {};
            Object.keys(kf).forEach(function (prop) {
                var normalizedProp = prop;
                var normalizedValue = kf[prop];
                if (prop !== 'offset') {
                    normalizedProp = normalizer.normalizePropertyName(normalizedProp, errors);
                    switch (normalizedValue) {
                        case animations.ɵPRE_STYLE:
                            normalizedValue = preStyles[prop];
                            break;
                        case animations.AUTO_STYLE:
                            normalizedValue = postStyles[prop];
                            break;
                        default:
                            normalizedValue =
                                normalizer.normalizeStyleValue(prop, normalizedProp, normalizedValue, errors);
                            break;
                    }
                }
                normalizedKeyframe[normalizedProp] = normalizedValue;
            });
            if (!isSameOffset) {
                normalizedKeyframes.push(normalizedKeyframe);
            }
            previousKeyframe = normalizedKeyframe;
            previousOffset = offset;
        });
        if (errors.length) {
            var LINE_START = '\n - ';
            throw new Error("Unable to animate due to the following errors:" + LINE_START + errors.join(LINE_START));
        }
        return normalizedKeyframes;
    }
    function listenOnPlayer(player, eventName, event, callback) {
        switch (eventName) {
            case 'start':
                player.onStart(function () { return callback(event && copyAnimationEvent(event, 'start', player)); });
                break;
            case 'done':
                player.onDone(function () { return callback(event && copyAnimationEvent(event, 'done', player)); });
                break;
            case 'destroy':
                player.onDestroy(function () { return callback(event && copyAnimationEvent(event, 'destroy', player)); });
                break;
        }
    }
    function copyAnimationEvent(e, phaseName, player) {
        var totalTime = player.totalTime;
        var disabled = player.disabled ? true : false;
        var event = makeAnimationEvent(e.element, e.triggerName, e.fromState, e.toState, phaseName || e.phaseName, totalTime == undefined ? e.totalTime : totalTime, disabled);
        var data = e['_data'];
        if (data != null) {
            event['_data'] = data;
        }
        return event;
    }
    function makeAnimationEvent(element, triggerName, fromState, toState, phaseName, totalTime, disabled) {
        if (phaseName === void 0) { phaseName = ''; }
        if (totalTime === void 0) { totalTime = 0; }
        return { element: element, triggerName: triggerName, fromState: fromState, toState: toState, phaseName: phaseName, totalTime: totalTime, disabled: !!disabled };
    }
    function getOrSetAsInMap(map, key, defaultValue) {
        var value;
        if (map instanceof Map) {
            value = map.get(key);
            if (!value) {
                map.set(key, value = defaultValue);
            }
        }
        else {
            value = map[key];
            if (!value) {
                value = map[key] = defaultValue;
            }
        }
        return value;
    }
    function parseTimelineCommand(command) {
        var separatorPos = command.indexOf(':');
        var id = command.substring(1, separatorPos);
        var action = command.substr(separatorPos + 1);
        return [id, action];
    }
    var _contains = function (elm1, elm2) { return false; };
    var _matches = function (element, selector) {
        return false;
    };
    var _query = function (element, selector, multi) {
        return [];
    };
    // Define utility methods for browsers and platform-server(domino) where Element
    // and utility methods exist.
    var _isNode = isNode();
    if (_isNode || typeof Element !== 'undefined') {
        // this is well supported in all browsers
        _contains = function (elm1, elm2) { return elm1.contains(elm2); };
        if (_isNode || Element.prototype.matches) {
            _matches = function (element, selector) { return element.matches(selector); };
        }
        else {
            var proto = Element.prototype;
            var fn_1 = proto.matchesSelector || proto.mozMatchesSelector || proto.msMatchesSelector ||
                proto.oMatchesSelector || proto.webkitMatchesSelector;
            if (fn_1) {
                _matches = function (element, selector) { return fn_1.apply(element, [selector]); };
            }
        }
        _query = function (element, selector, multi) {
            var results = [];
            if (multi) {
                results.push.apply(results, __spread(element.querySelectorAll(selector)));
            }
            else {
                var elm = element.querySelector(selector);
                if (elm) {
                    results.push(elm);
                }
            }
            return results;
        };
    }
    function containsVendorPrefix(prop) {
        // Webkit is the only real popular vendor prefix nowadays
        // cc: http://shouldiprefix.com/
        return prop.substring(1, 6) == 'ebkit'; // webkit or Webkit
    }
    var _CACHED_BODY = null;
    var _IS_WEBKIT = false;
    function validateStyleProperty(prop) {
        if (!_CACHED_BODY) {
            _CACHED_BODY = getBodyNode() || {};
            _IS_WEBKIT = _CACHED_BODY.style ? ('WebkitAppearance' in _CACHED_BODY.style) : false;
        }
        var result = true;
        if (_CACHED_BODY.style && !containsVendorPrefix(prop)) {
            result = prop in _CACHED_BODY.style;
            if (!result && _IS_WEBKIT) {
                var camelProp = 'Webkit' + prop.charAt(0).toUpperCase() + prop.substr(1);
                result = camelProp in _CACHED_BODY.style;
            }
        }
        return result;
    }
    function getBodyNode() {
        if (typeof document != 'undefined') {
            return document.body;
        }
        return null;
    }
    var matchesElement = _matches;
    var containsElement = _contains;
    var invokeQuery = _query;
    function hypenatePropsObject(object) {
        var newObj = {};
        Object.keys(object).forEach(function (prop) {
            var newProp = prop.replace(/([a-z])([A-Z])/g, '$1-$2');
            newObj[newProp] = object[prop];
        });
        return newObj;
    }

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * @experimental
     */
    var NoopAnimationDriver = /** @class */ (function () {
        function NoopAnimationDriver() {
        }
        NoopAnimationDriver.prototype.validateStyleProperty = function (prop) { return validateStyleProperty(prop); };
        NoopAnimationDriver.prototype.matchesElement = function (element, selector) {
            return matchesElement(element, selector);
        };
        NoopAnimationDriver.prototype.containsElement = function (elm1, elm2) { return containsElement(elm1, elm2); };
        NoopAnimationDriver.prototype.query = function (element, selector, multi) {
            return invokeQuery(element, selector, multi);
        };
        NoopAnimationDriver.prototype.computeStyle = function (element, prop, defaultValue) {
            return defaultValue || '';
        };
        NoopAnimationDriver.prototype.animate = function (element, keyframes, duration, delay, easing, previousPlayers, scrubberAccessRequested) {
            if (previousPlayers === void 0) { previousPlayers = []; }
            return new animations.NoopAnimationPlayer(duration, delay);
        };
        NoopAnimationDriver.decorators = [
            { type: core.Injectable }
        ];
        return NoopAnimationDriver;
    }());
    /**
     * @experimental
     */
    var AnimationDriver = /** @class */ (function () {
        function AnimationDriver() {
        }
        AnimationDriver.NOOP = new NoopAnimationDriver();
        return AnimationDriver;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var ONE_SECOND = 1000;
    var SUBSTITUTION_EXPR_START = '{{';
    var SUBSTITUTION_EXPR_END = '}}';
    var ENTER_CLASSNAME = 'ng-enter';
    var LEAVE_CLASSNAME = 'ng-leave';
    var NG_TRIGGER_CLASSNAME = 'ng-trigger';
    var NG_TRIGGER_SELECTOR = '.ng-trigger';
    var NG_ANIMATING_CLASSNAME = 'ng-animating';
    var NG_ANIMATING_SELECTOR = '.ng-animating';
    function resolveTimingValue(value) {
        if (typeof value == 'number')
            return value;
        var matches = value.match(/^(-?[\.\d]+)(m?s)/);
        if (!matches || matches.length < 2)
            return 0;
        return _convertTimeValueToMS(parseFloat(matches[1]), matches[2]);
    }
    function _convertTimeValueToMS(value, unit) {
        switch (unit) {
            case 's':
                return value * ONE_SECOND;
            default: // ms or something else
                return value;
        }
    }
    function resolveTiming(timings, errors, allowNegativeValues) {
        return timings.hasOwnProperty('duration') ?
            timings :
            parseTimeExpression(timings, errors, allowNegativeValues);
    }
    function parseTimeExpression(exp, errors, allowNegativeValues) {
        var regex = /^(-?[\.\d]+)(m?s)(?:\s+(-?[\.\d]+)(m?s))?(?:\s+([-a-z]+(?:\(.+?\))?))?$/i;
        var duration;
        var delay = 0;
        var easing = '';
        if (typeof exp === 'string') {
            var matches = exp.match(regex);
            if (matches === null) {
                errors.push("The provided timing value \"" + exp + "\" is invalid.");
                return { duration: 0, delay: 0, easing: '' };
            }
            duration = _convertTimeValueToMS(parseFloat(matches[1]), matches[2]);
            var delayMatch = matches[3];
            if (delayMatch != null) {
                delay = _convertTimeValueToMS(Math.floor(parseFloat(delayMatch)), matches[4]);
            }
            var easingVal = matches[5];
            if (easingVal) {
                easing = easingVal;
            }
        }
        else {
            duration = exp;
        }
        if (!allowNegativeValues) {
            var containsErrors = false;
            var startIndex = errors.length;
            if (duration < 0) {
                errors.push("Duration values below 0 are not allowed for this animation step.");
                containsErrors = true;
            }
            if (delay < 0) {
                errors.push("Delay values below 0 are not allowed for this animation step.");
                containsErrors = true;
            }
            if (containsErrors) {
                errors.splice(startIndex, 0, "The provided timing value \"" + exp + "\" is invalid.");
            }
        }
        return { duration: duration, delay: delay, easing: easing };
    }
    function copyObj(obj, destination) {
        if (destination === void 0) { destination = {}; }
        Object.keys(obj).forEach(function (prop) { destination[prop] = obj[prop]; });
        return destination;
    }
    function copyStyles(styles, readPrototype, destination) {
        if (destination === void 0) { destination = {}; }
        if (readPrototype) {
            // we make use of a for-in loop so that the
            // prototypically inherited properties are
            // revealed from the backFill map
            for (var prop in styles) {
                destination[prop] = styles[prop];
            }
        }
        else {
            copyObj(styles, destination);
        }
        return destination;
    }
    function getStyleAttributeString(element, key, value) {
        // Return the key-value pair string to be added to the style attribute for the
        // given CSS style key.
        if (value) {
            return key + ':' + value + ';';
        }
        else {
            return '';
        }
    }
    function writeStyleAttribute(element) {
        // Read the style property of the element and manually reflect it to the
        // style attribute. This is needed because Domino on platform-server doesn't
        // understand the full set of allowed CSS properties and doesn't reflect some
        // of them automatically.
        var styleAttrValue = '';
        for (var i = 0; i < element.style.length; i++) {
            var key = element.style.item(i);
            styleAttrValue += getStyleAttributeString(element, key, element.style.getPropertyValue(key));
        }
        for (var key in element.style) {
            // Skip internal Domino properties that don't need to be reflected.
            if (!element.style.hasOwnProperty(key) || key.startsWith('_')) {
                continue;
            }
            var dashKey = camelCaseToDashCase(key);
            styleAttrValue += getStyleAttributeString(element, dashKey, element.style[key]);
        }
        element.setAttribute('style', styleAttrValue);
    }
    function setStyles(element, styles) {
        if (element['style']) {
            Object.keys(styles).forEach(function (prop) {
                var camelProp = dashCaseToCamelCase(prop);
                element.style[camelProp] = styles[prop];
            });
            // On the server set the 'style' attribute since it's not automatically reflected.
            if (isNode()) {
                writeStyleAttribute(element);
            }
        }
    }
    function eraseStyles(element, styles) {
        if (element['style']) {
            Object.keys(styles).forEach(function (prop) {
                var camelProp = dashCaseToCamelCase(prop);
                element.style[camelProp] = '';
            });
            // On the server set the 'style' attribute since it's not automatically reflected.
            if (isNode()) {
                writeStyleAttribute(element);
            }
        }
    }
    function normalizeAnimationEntry(steps) {
        if (Array.isArray(steps)) {
            if (steps.length == 1)
                return steps[0];
            return animations.sequence(steps);
        }
        return steps;
    }
    function validateStyleParams(value, options, errors) {
        var params = options.params || {};
        var matches = extractStyleParams(value);
        if (matches.length) {
            matches.forEach(function (varName) {
                if (!params.hasOwnProperty(varName)) {
                    errors.push("Unable to resolve the local animation param " + varName + " in the given list of values");
                }
            });
        }
    }
    var PARAM_REGEX = new RegExp(SUBSTITUTION_EXPR_START + "\\s*(.+?)\\s*" + SUBSTITUTION_EXPR_END, 'g');
    function extractStyleParams(value) {
        var params = [];
        if (typeof value === 'string') {
            var val = value.toString();
            var match = void 0;
            while (match = PARAM_REGEX.exec(val)) {
                params.push(match[1]);
            }
            PARAM_REGEX.lastIndex = 0;
        }
        return params;
    }
    function interpolateParams(value, params, errors) {
        var original = value.toString();
        var str = original.replace(PARAM_REGEX, function (_, varName) {
            var localVal = params[varName];
            // this means that the value was never overridden by the data passed in by the user
            if (!params.hasOwnProperty(varName)) {
                errors.push("Please provide a value for the animation param " + varName);
                localVal = '';
            }
            return localVal.toString();
        });
        // we do this to assert that numeric values stay as they are
        return str == original ? value : str;
    }
    function iteratorToArray(iterator) {
        var arr = [];
        var item = iterator.next();
        while (!item.done) {
            arr.push(item.value);
            item = iterator.next();
        }
        return arr;
    }
    var DASH_CASE_REGEXP = /-+([a-z0-9])/g;
    function dashCaseToCamelCase(input) {
        return input.replace(DASH_CASE_REGEXP, function () {
            var m = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                m[_i] = arguments[_i];
            }
            return m[1].toUpperCase();
        });
    }
    function camelCaseToDashCase(input) {
        return input.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    }
    function allowPreviousPlayerStylesMerge(duration, delay) {
        return duration === 0 || delay === 0;
    }
    function balancePreviousStylesIntoKeyframes(element, keyframes, previousStyles) {
        var previousStyleProps = Object.keys(previousStyles);
        if (previousStyleProps.length && keyframes.length) {
            var startingKeyframe_1 = keyframes[0];
            var missingStyleProps_1 = [];
            previousStyleProps.forEach(function (prop) {
                if (!startingKeyframe_1.hasOwnProperty(prop)) {
                    missingStyleProps_1.push(prop);
                }
                startingKeyframe_1[prop] = previousStyles[prop];
            });
            if (missingStyleProps_1.length) {
                var _loop_1 = function () {
                    var kf = keyframes[i];
                    missingStyleProps_1.forEach(function (prop) { kf[prop] = computeStyle(element, prop); });
                };
                // tslint:disable-next-line
                for (var i = 1; i < keyframes.length; i++) {
                    _loop_1();
                }
            }
        }
        return keyframes;
    }
    function visitDslNode(visitor, node, context) {
        switch (node.type) {
            case 7 /* Trigger */:
                return visitor.visitTrigger(node, context);
            case 0 /* State */:
                return visitor.visitState(node, context);
            case 1 /* Transition */:
                return visitor.visitTransition(node, context);
            case 2 /* Sequence */:
                return visitor.visitSequence(node, context);
            case 3 /* Group */:
                return visitor.visitGroup(node, context);
            case 4 /* Animate */:
                return visitor.visitAnimate(node, context);
            case 5 /* Keyframes */:
                return visitor.visitKeyframes(node, context);
            case 6 /* Style */:
                return visitor.visitStyle(node, context);
            case 8 /* Reference */:
                return visitor.visitReference(node, context);
            case 9 /* AnimateChild */:
                return visitor.visitAnimateChild(node, context);
            case 10 /* AnimateRef */:
                return visitor.visitAnimateRef(node, context);
            case 11 /* Query */:
                return visitor.visitQuery(node, context);
            case 12 /* Stagger */:
                return visitor.visitStagger(node, context);
            default:
                throw new Error("Unable to resolve animation metadata node #" + node.type);
        }
    }
    function computeStyle(element, prop) {
        return window.getComputedStyle(element)[prop];
    }

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var ANY_STATE = '*';
    function parseTransitionExpr(transitionValue, errors) {
        var expressions = [];
        if (typeof transitionValue == 'string') {
            transitionValue
                .split(/\s*,\s*/)
                .forEach(function (str) { return parseInnerTransitionStr(str, expressions, errors); });
        }
        else {
            expressions.push(transitionValue);
        }
        return expressions;
    }
    function parseInnerTransitionStr(eventStr, expressions, errors) {
        if (eventStr[0] == ':') {
            var result = parseAnimationAlias(eventStr, errors);
            if (typeof result == 'function') {
                expressions.push(result);
                return;
            }
            eventStr = result;
        }
        var match = eventStr.match(/^(\*|[-\w]+)\s*(<?[=-]>)\s*(\*|[-\w]+)$/);
        if (match == null || match.length < 4) {
            errors.push("The provided transition expression \"" + eventStr + "\" is not supported");
            return expressions;
        }
        var fromState = match[1];
        var separator = match[2];
        var toState = match[3];
        expressions.push(makeLambdaFromStates(fromState, toState));
        var isFullAnyStateExpr = fromState == ANY_STATE && toState == ANY_STATE;
        if (separator[0] == '<' && !isFullAnyStateExpr) {
            expressions.push(makeLambdaFromStates(toState, fromState));
        }
    }
    function parseAnimationAlias(alias, errors) {
        switch (alias) {
            case ':enter':
                return 'void => *';
            case ':leave':
                return '* => void';
            case ':increment':
                return function (fromState, toState) { return parseFloat(toState) > parseFloat(fromState); };
            case ':decrement':
                return function (fromState, toState) { return parseFloat(toState) < parseFloat(fromState); };
            default:
                errors.push("The transition alias value \"" + alias + "\" is not supported");
                return '* => *';
        }
    }
    // DO NOT REFACTOR ... keep the follow set instantiations
    // with the values intact (closure compiler for some reason
    // removes follow-up lines that add the values outside of
    // the constructor...
    var TRUE_BOOLEAN_VALUES = new Set(['true', '1']);
    var FALSE_BOOLEAN_VALUES = new Set(['false', '0']);
    function makeLambdaFromStates(lhs, rhs) {
        var LHS_MATCH_BOOLEAN = TRUE_BOOLEAN_VALUES.has(lhs) || FALSE_BOOLEAN_VALUES.has(lhs);
        var RHS_MATCH_BOOLEAN = TRUE_BOOLEAN_VALUES.has(rhs) || FALSE_BOOLEAN_VALUES.has(rhs);
        return function (fromState, toState) {
            var lhsMatch = lhs == ANY_STATE || lhs == fromState;
            var rhsMatch = rhs == ANY_STATE || rhs == toState;
            if (!lhsMatch && LHS_MATCH_BOOLEAN && typeof fromState === 'boolean') {
                lhsMatch = fromState ? TRUE_BOOLEAN_VALUES.has(lhs) : FALSE_BOOLEAN_VALUES.has(lhs);
            }
            if (!rhsMatch && RHS_MATCH_BOOLEAN && typeof toState === 'boolean') {
                rhsMatch = toState ? TRUE_BOOLEAN_VALUES.has(rhs) : FALSE_BOOLEAN_VALUES.has(rhs);
            }
            return lhsMatch && rhsMatch;
        };
    }

    var SELF_TOKEN = ':self';
    var SELF_TOKEN_REGEX = new RegExp("s*" + SELF_TOKEN + "s*,?", 'g');
    /*
     * [Validation]
     * The visitor code below will traverse the animation AST generated by the animation verb functions
     * (the output is a tree of objects) and attempt to perform a series of validations on the data. The
     * following corner-cases will be validated:
     *
     * 1. Overlap of animations
     * Given that a CSS property cannot be animated in more than one place at the same time, it's
     * important that this behaviour is detected and validated. The way in which this occurs is that
     * each time a style property is examined, a string-map containing the property will be updated with
     * the start and end times for when the property is used within an animation step.
     *
     * If there are two or more parallel animations that are currently running (these are invoked by the
     * group()) on the same element then the validator will throw an error. Since the start/end timing
     * values are collected for each property then if the current animation step is animating the same
     * property and its timing values fall anywhere into the window of time that the property is
     * currently being animated within then this is what causes an error.
     *
     * 2. Timing values
     * The validator will validate to see if a timing value of `duration delay easing` or
     * `durationNumber` is valid or not.
     *
     * (note that upon validation the code below will replace the timing data with an object containing
     * {duration,delay,easing}.
     *
     * 3. Offset Validation
     * Each of the style() calls are allowed to have an offset value when placed inside of keyframes().
     * Offsets within keyframes() are considered valid when:
     *
     *   - No offsets are used at all
     *   - Each style() entry contains an offset value
     *   - Each offset is between 0 and 1
     *   - Each offset is greater to or equal than the previous one
     *
     * Otherwise an error will be thrown.
     */
    function buildAnimationAst(driver, metadata, errors) {
        return new AnimationAstBuilderVisitor(driver).build(metadata, errors);
    }
    var ROOT_SELECTOR = '';
    var AnimationAstBuilderVisitor = /** @class */ (function () {
        function AnimationAstBuilderVisitor(_driver) {
            this._driver = _driver;
        }
        AnimationAstBuilderVisitor.prototype.build = function (metadata, errors) {
            var context = new AnimationAstBuilderContext(errors);
            this._resetContextStyleTimingState(context);
            return visitDslNode(this, normalizeAnimationEntry(metadata), context);
        };
        AnimationAstBuilderVisitor.prototype._resetContextStyleTimingState = function (context) {
            context.currentQuerySelector = ROOT_SELECTOR;
            context.collectedStyles = {};
            context.collectedStyles[ROOT_SELECTOR] = {};
            context.currentTime = 0;
        };
        AnimationAstBuilderVisitor.prototype.visitTrigger = function (metadata, context) {
            var _this = this;
            var queryCount = context.queryCount = 0;
            var depCount = context.depCount = 0;
            var states = [];
            var transitions = [];
            if (metadata.name.charAt(0) == '@') {
                context.errors.push('animation triggers cannot be prefixed with an `@` sign (e.g. trigger(\'@foo\', [...]))');
            }
            metadata.definitions.forEach(function (def) {
                _this._resetContextStyleTimingState(context);
                if (def.type == 0 /* State */) {
                    var stateDef_1 = def;
                    var name_1 = stateDef_1.name;
                    name_1.toString().split(/\s*,\s*/).forEach(function (n) {
                        stateDef_1.name = n;
                        states.push(_this.visitState(stateDef_1, context));
                    });
                    stateDef_1.name = name_1;
                }
                else if (def.type == 1 /* Transition */) {
                    var transition = _this.visitTransition(def, context);
                    queryCount += transition.queryCount;
                    depCount += transition.depCount;
                    transitions.push(transition);
                }
                else {
                    context.errors.push('only state() and transition() definitions can sit inside of a trigger()');
                }
            });
            return {
                type: 7 /* Trigger */,
                name: metadata.name, states: states, transitions: transitions, queryCount: queryCount, depCount: depCount,
                options: null
            };
        };
        AnimationAstBuilderVisitor.prototype.visitState = function (metadata, context) {
            var styleAst = this.visitStyle(metadata.styles, context);
            var astParams = (metadata.options && metadata.options.params) || null;
            if (styleAst.containsDynamicStyles) {
                var missingSubs_1 = new Set();
                var params_1 = astParams || {};
                styleAst.styles.forEach(function (value) {
                    if (isObject$1(value)) {
                        var stylesObj_1 = value;
                        Object.keys(stylesObj_1).forEach(function (prop) {
                            extractStyleParams(stylesObj_1[prop]).forEach(function (sub) {
                                if (!params_1.hasOwnProperty(sub)) {
                                    missingSubs_1.add(sub);
                                }
                            });
                        });
                    }
                });
                if (missingSubs_1.size) {
                    var missingSubsArr = iteratorToArray(missingSubs_1.values());
                    context.errors.push("state(\"" + metadata.name + "\", ...) must define default values for all the following style substitutions: " + missingSubsArr.join(', '));
                }
            }
            return {
                type: 0 /* State */,
                name: metadata.name,
                style: styleAst,
                options: astParams ? { params: astParams } : null
            };
        };
        AnimationAstBuilderVisitor.prototype.visitTransition = function (metadata, context) {
            context.queryCount = 0;
            context.depCount = 0;
            var animation = visitDslNode(this, normalizeAnimationEntry(metadata.animation), context);
            var matchers = parseTransitionExpr(metadata.expr, context.errors);
            return {
                type: 1 /* Transition */,
                matchers: matchers,
                animation: animation,
                queryCount: context.queryCount,
                depCount: context.depCount,
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitSequence = function (metadata, context) {
            var _this = this;
            return {
                type: 2 /* Sequence */,
                steps: metadata.steps.map(function (s) { return visitDslNode(_this, s, context); }),
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitGroup = function (metadata, context) {
            var _this = this;
            var currentTime = context.currentTime;
            var furthestTime = 0;
            var steps = metadata.steps.map(function (step) {
                context.currentTime = currentTime;
                var innerAst = visitDslNode(_this, step, context);
                furthestTime = Math.max(furthestTime, context.currentTime);
                return innerAst;
            });
            context.currentTime = furthestTime;
            return {
                type: 3 /* Group */,
                steps: steps,
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitAnimate = function (metadata, context) {
            var timingAst = constructTimingAst(metadata.timings, context.errors);
            context.currentAnimateTimings = timingAst;
            var styleAst;
            var styleMetadata = metadata.styles ? metadata.styles : animations.style({});
            if (styleMetadata.type == 5 /* Keyframes */) {
                styleAst = this.visitKeyframes(styleMetadata, context);
            }
            else {
                var styleMetadata_1 = metadata.styles;
                var isEmpty = false;
                if (!styleMetadata_1) {
                    isEmpty = true;
                    var newStyleData = {};
                    if (timingAst.easing) {
                        newStyleData['easing'] = timingAst.easing;
                    }
                    styleMetadata_1 = animations.style(newStyleData);
                }
                context.currentTime += timingAst.duration + timingAst.delay;
                var _styleAst = this.visitStyle(styleMetadata_1, context);
                _styleAst.isEmptyStep = isEmpty;
                styleAst = _styleAst;
            }
            context.currentAnimateTimings = null;
            return {
                type: 4 /* Animate */,
                timings: timingAst,
                style: styleAst,
                options: null
            };
        };
        AnimationAstBuilderVisitor.prototype.visitStyle = function (metadata, context) {
            var ast = this._makeStyleAst(metadata, context);
            this._validateStyleAst(ast, context);
            return ast;
        };
        AnimationAstBuilderVisitor.prototype._makeStyleAst = function (metadata, context) {
            var styles = [];
            if (Array.isArray(metadata.styles)) {
                metadata.styles.forEach(function (styleTuple) {
                    if (typeof styleTuple == 'string') {
                        if (styleTuple == animations.AUTO_STYLE) {
                            styles.push(styleTuple);
                        }
                        else {
                            context.errors.push("The provided style string value " + styleTuple + " is not allowed.");
                        }
                    }
                    else {
                        styles.push(styleTuple);
                    }
                });
            }
            else {
                styles.push(metadata.styles);
            }
            var containsDynamicStyles = false;
            var collectedEasing = null;
            styles.forEach(function (styleData) {
                if (isObject$1(styleData)) {
                    var styleMap = styleData;
                    var easing = styleMap['easing'];
                    if (easing) {
                        collectedEasing = easing;
                        delete styleMap['easing'];
                    }
                    if (!containsDynamicStyles) {
                        for (var prop in styleMap) {
                            var value = styleMap[prop];
                            if (value.toString().indexOf(SUBSTITUTION_EXPR_START) >= 0) {
                                containsDynamicStyles = true;
                                break;
                            }
                        }
                    }
                }
            });
            return {
                type: 6 /* Style */,
                styles: styles,
                easing: collectedEasing,
                offset: metadata.offset, containsDynamicStyles: containsDynamicStyles,
                options: null
            };
        };
        AnimationAstBuilderVisitor.prototype._validateStyleAst = function (ast, context) {
            var _this = this;
            var timings = context.currentAnimateTimings;
            var endTime = context.currentTime;
            var startTime = context.currentTime;
            if (timings && startTime > 0) {
                startTime -= timings.duration + timings.delay;
            }
            ast.styles.forEach(function (tuple) {
                if (typeof tuple == 'string')
                    return;
                Object.keys(tuple).forEach(function (prop) {
                    if (!_this._driver.validateStyleProperty(prop)) {
                        context.errors.push("The provided animation property \"" + prop + "\" is not a supported CSS property for animations");
                        return;
                    }
                    var collectedStyles = context.collectedStyles[context.currentQuerySelector];
                    var collectedEntry = collectedStyles[prop];
                    var updateCollectedStyle = true;
                    if (collectedEntry) {
                        if (startTime != endTime && startTime >= collectedEntry.startTime &&
                            endTime <= collectedEntry.endTime) {
                            context.errors.push("The CSS property \"" + prop + "\" that exists between the times of \"" + collectedEntry.startTime + "ms\" and \"" + collectedEntry.endTime + "ms\" is also being animated in a parallel animation between the times of \"" + startTime + "ms\" and \"" + endTime + "ms\"");
                            updateCollectedStyle = false;
                        }
                        // we always choose the smaller start time value since we
                        // want to have a record of the entire animation window where
                        // the style property is being animated in between
                        startTime = collectedEntry.startTime;
                    }
                    if (updateCollectedStyle) {
                        collectedStyles[prop] = { startTime: startTime, endTime: endTime };
                    }
                    if (context.options) {
                        validateStyleParams(tuple[prop], context.options, context.errors);
                    }
                });
            });
        };
        AnimationAstBuilderVisitor.prototype.visitKeyframes = function (metadata, context) {
            var _this = this;
            var ast = { type: 5 /* Keyframes */, styles: [], options: null };
            if (!context.currentAnimateTimings) {
                context.errors.push("keyframes() must be placed inside of a call to animate()");
                return ast;
            }
            var MAX_KEYFRAME_OFFSET = 1;
            var totalKeyframesWithOffsets = 0;
            var offsets = [];
            var offsetsOutOfOrder = false;
            var keyframesOutOfRange = false;
            var previousOffset = 0;
            var keyframes = metadata.steps.map(function (styles) {
                var style$$1 = _this._makeStyleAst(styles, context);
                var offsetVal = style$$1.offset != null ? style$$1.offset : consumeOffset(style$$1.styles);
                var offset = 0;
                if (offsetVal != null) {
                    totalKeyframesWithOffsets++;
                    offset = style$$1.offset = offsetVal;
                }
                keyframesOutOfRange = keyframesOutOfRange || offset < 0 || offset > 1;
                offsetsOutOfOrder = offsetsOutOfOrder || offset < previousOffset;
                previousOffset = offset;
                offsets.push(offset);
                return style$$1;
            });
            if (keyframesOutOfRange) {
                context.errors.push("Please ensure that all keyframe offsets are between 0 and 1");
            }
            if (offsetsOutOfOrder) {
                context.errors.push("Please ensure that all keyframe offsets are in order");
            }
            var length = metadata.steps.length;
            var generatedOffset = 0;
            if (totalKeyframesWithOffsets > 0 && totalKeyframesWithOffsets < length) {
                context.errors.push("Not all style() steps within the declared keyframes() contain offsets");
            }
            else if (totalKeyframesWithOffsets == 0) {
                generatedOffset = MAX_KEYFRAME_OFFSET / (length - 1);
            }
            var limit = length - 1;
            var currentTime = context.currentTime;
            var currentAnimateTimings = context.currentAnimateTimings;
            var animateDuration = currentAnimateTimings.duration;
            keyframes.forEach(function (kf, i) {
                var offset = generatedOffset > 0 ? (i == limit ? 1 : (generatedOffset * i)) : offsets[i];
                var durationUpToThisFrame = offset * animateDuration;
                context.currentTime = currentTime + currentAnimateTimings.delay + durationUpToThisFrame;
                currentAnimateTimings.duration = durationUpToThisFrame;
                _this._validateStyleAst(kf, context);
                kf.offset = offset;
                ast.styles.push(kf);
            });
            return ast;
        };
        AnimationAstBuilderVisitor.prototype.visitReference = function (metadata, context) {
            return {
                type: 8 /* Reference */,
                animation: visitDslNode(this, normalizeAnimationEntry(metadata.animation), context),
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitAnimateChild = function (metadata, context) {
            context.depCount++;
            return {
                type: 9 /* AnimateChild */,
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitAnimateRef = function (metadata, context) {
            return {
                type: 10 /* AnimateRef */,
                animation: this.visitReference(metadata.animation, context),
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitQuery = function (metadata, context) {
            var parentSelector = context.currentQuerySelector;
            var options = (metadata.options || {});
            context.queryCount++;
            context.currentQuery = metadata;
            var _a = __read(normalizeSelector(metadata.selector), 2), selector = _a[0], includeSelf = _a[1];
            context.currentQuerySelector =
                parentSelector.length ? (parentSelector + ' ' + selector) : selector;
            getOrSetAsInMap(context.collectedStyles, context.currentQuerySelector, {});
            var animation = visitDslNode(this, normalizeAnimationEntry(metadata.animation), context);
            context.currentQuery = null;
            context.currentQuerySelector = parentSelector;
            return {
                type: 11 /* Query */,
                selector: selector,
                limit: options.limit || 0,
                optional: !!options.optional, includeSelf: includeSelf, animation: animation,
                originalSelector: metadata.selector,
                options: normalizeAnimationOptions(metadata.options)
            };
        };
        AnimationAstBuilderVisitor.prototype.visitStagger = function (metadata, context) {
            if (!context.currentQuery) {
                context.errors.push("stagger() can only be used inside of query()");
            }
            var timings = metadata.timings === 'full' ?
                { duration: 0, delay: 0, easing: 'full' } :
                resolveTiming(metadata.timings, context.errors, true);
            return {
                type: 12 /* Stagger */,
                animation: visitDslNode(this, normalizeAnimationEntry(metadata.animation), context), timings: timings,
                options: null
            };
        };
        return AnimationAstBuilderVisitor;
    }());
    function normalizeSelector(selector) {
        var hasAmpersand = selector.split(/\s*,\s*/).find(function (token) { return token == SELF_TOKEN; }) ? true : false;
        if (hasAmpersand) {
            selector = selector.replace(SELF_TOKEN_REGEX, '');
        }
        // the :enter and :leave selectors are filled in at runtime during timeline building
        selector = selector.replace(/@\*/g, NG_TRIGGER_SELECTOR)
            .replace(/@\w+/g, function (match) { return NG_TRIGGER_SELECTOR + '-' + match.substr(1); })
            .replace(/:animating/g, NG_ANIMATING_SELECTOR);
        return [selector, hasAmpersand];
    }
    function normalizeParams(obj) {
        return obj ? copyObj(obj) : null;
    }
    var AnimationAstBuilderContext = /** @class */ (function () {
        function AnimationAstBuilderContext(errors) {
            this.errors = errors;
            this.queryCount = 0;
            this.depCount = 0;
            this.currentTransition = null;
            this.currentQuery = null;
            this.currentQuerySelector = null;
            this.currentAnimateTimings = null;
            this.currentTime = 0;
            this.collectedStyles = {};
            this.options = null;
        }
        return AnimationAstBuilderContext;
    }());
    function consumeOffset(styles) {
        if (typeof styles == 'string')
            return null;
        var offset = null;
        if (Array.isArray(styles)) {
            styles.forEach(function (styleTuple) {
                if (isObject$1(styleTuple) && styleTuple.hasOwnProperty('offset')) {
                    var obj = styleTuple;
                    offset = parseFloat(obj['offset']);
                    delete obj['offset'];
                }
            });
        }
        else if (isObject$1(styles) && styles.hasOwnProperty('offset')) {
            var obj = styles;
            offset = parseFloat(obj['offset']);
            delete obj['offset'];
        }
        return offset;
    }
    function isObject$1(value) {
        return !Array.isArray(value) && typeof value == 'object';
    }
    function constructTimingAst(value, errors) {
        var timings = null;
        if (value.hasOwnProperty('duration')) {
            timings = value;
        }
        else if (typeof value == 'number') {
            var duration = resolveTiming(value, errors).duration;
            return makeTimingAst(duration, 0, '');
        }
        var strValue = value;
        var isDynamic = strValue.split(/\s+/).some(function (v) { return v.charAt(0) == '{' && v.charAt(1) == '{'; });
        if (isDynamic) {
            var ast = makeTimingAst(0, 0, '');
            ast.dynamic = true;
            ast.strValue = strValue;
            return ast;
        }
        timings = timings || resolveTiming(strValue, errors);
        return makeTimingAst(timings.duration, timings.delay, timings.easing);
    }
    function normalizeAnimationOptions(options) {
        if (options) {
            options = copyObj(options);
            if (options['params']) {
                options['params'] = normalizeParams(options['params']);
            }
        }
        else {
            options = {};
        }
        return options;
    }
    function makeTimingAst(duration, delay, easing) {
        return { duration: duration, delay: delay, easing: easing };
    }

    function createTimelineInstruction(element, keyframes, preStyleProps, postStyleProps, duration, delay, easing, subTimeline) {
        if (easing === void 0) { easing = null; }
        if (subTimeline === void 0) { subTimeline = false; }
        return {
            type: 1 /* TimelineAnimation */,
            element: element,
            keyframes: keyframes,
            preStyleProps: preStyleProps,
            postStyleProps: postStyleProps,
            duration: duration,
            delay: delay,
            totalTime: duration + delay, easing: easing, subTimeline: subTimeline
        };
    }

    var ElementInstructionMap = /** @class */ (function () {
        function ElementInstructionMap() {
            this._map = new Map();
        }
        ElementInstructionMap.prototype.consume = function (element) {
            var instructions = this._map.get(element);
            if (instructions) {
                this._map.delete(element);
            }
            else {
                instructions = [];
            }
            return instructions;
        };
        ElementInstructionMap.prototype.append = function (element, instructions) {
            var existingInstructions = this._map.get(element);
            if (!existingInstructions) {
                this._map.set(element, existingInstructions = []);
            }
            existingInstructions.push.apply(existingInstructions, __spread(instructions));
        };
        ElementInstructionMap.prototype.has = function (element) { return this._map.has(element); };
        ElementInstructionMap.prototype.clear = function () { this._map.clear(); };
        return ElementInstructionMap;
    }());

    var ONE_FRAME_IN_MILLISECONDS = 1;
    var ENTER_TOKEN = ':enter';
    var ENTER_TOKEN_REGEX = new RegExp(ENTER_TOKEN, 'g');
    var LEAVE_TOKEN = ':leave';
    var LEAVE_TOKEN_REGEX = new RegExp(LEAVE_TOKEN, 'g');
    /*
     * The code within this file aims to generate web-animations-compatible keyframes from Angular's
     * animation DSL code.
     *
     * The code below will be converted from:
     *
     * ```
     * sequence([
     *   style({ opacity: 0 }),
     *   animate(1000, style({ opacity: 0 }))
     * ])
     * ```
     *
     * To:
     * ```
     * keyframes = [{ opacity: 0, offset: 0 }, { opacity: 1, offset: 1 }]
     * duration = 1000
     * delay = 0
     * easing = ''
     * ```
     *
     * For this operation to cover the combination of animation verbs (style, animate, group, etc...) a
     * combination of prototypical inheritance, AST traversal and merge-sort-like algorithms are used.
     *
     * [AST Traversal]
     * Each of the animation verbs, when executed, will return an string-map object representing what
     * type of action it is (style, animate, group, etc...) and the data associated with it. This means
     * that when functional composition mix of these functions is evaluated (like in the example above)
     * then it will end up producing a tree of objects representing the animation itself.
     *
     * When this animation object tree is processed by the visitor code below it will visit each of the
     * verb statements within the visitor. And during each visit it will build the context of the
     * animation keyframes by interacting with the `TimelineBuilder`.
     *
     * [TimelineBuilder]
     * This class is responsible for tracking the styles and building a series of keyframe objects for a
     * timeline between a start and end time. The builder starts off with an initial timeline and each
     * time the AST comes across a `group()`, `keyframes()` or a combination of the two wihtin a
     * `sequence()` then it will generate a sub timeline for each step as well as a new one after
     * they are complete.
     *
     * As the AST is traversed, the timing state on each of the timelines will be incremented. If a sub
     * timeline was created (based on one of the cases above) then the parent timeline will attempt to
     * merge the styles used within the sub timelines into itself (only with group() this will happen).
     * This happens with a merge operation (much like how the merge works in mergesort) and it will only
     * copy the most recently used styles from the sub timelines into the parent timeline. This ensures
     * that if the styles are used later on in another phase of the animation then they will be the most
     * up-to-date values.
     *
     * [How Missing Styles Are Updated]
     * Each timeline has a `backFill` property which is responsible for filling in new styles into
     * already processed keyframes if a new style shows up later within the animation sequence.
     *
     * ```
     * sequence([
     *   style({ width: 0 }),
     *   animate(1000, style({ width: 100 })),
     *   animate(1000, style({ width: 200 })),
     *   animate(1000, style({ width: 300 }))
     *   animate(1000, style({ width: 400, height: 400 })) // notice how `height` doesn't exist anywhere
     * else
     * ])
     * ```
     *
     * What is happening here is that the `height` value is added later in the sequence, but is missing
     * from all previous animation steps. Therefore when a keyframe is created it would also be missing
     * from all previous keyframes up until where it is first used. For the timeline keyframe generation
     * to properly fill in the style it will place the previous value (the value from the parent
     * timeline) or a default value of `*` into the backFill object. Given that each of the keyframe
     * styles are objects that prototypically inhert from the backFill object, this means that if a
     * value is added into the backFill then it will automatically propagate any missing values to all
     * keyframes. Therefore the missing `height` value will be properly filled into the already
     * processed keyframes.
     *
     * When a sub-timeline is created it will have its own backFill property. This is done so that
     * styles present within the sub-timeline do not accidentally seep into the previous/future timeline
     * keyframes
     *
     * (For prototypically-inherited contents to be detected a `for(i in obj)` loop must be used.)
     *
     * [Validation]
     * The code in this file is not responsible for validation. That functionality happens with within
     * the `AnimationValidatorVisitor` code.
     */
    function buildAnimationTimelines(driver, rootElement, ast, enterClassName, leaveClassName, startingStyles, finalStyles, options, subInstructions, errors) {
        if (startingStyles === void 0) { startingStyles = {}; }
        if (finalStyles === void 0) { finalStyles = {}; }
        if (errors === void 0) { errors = []; }
        return new AnimationTimelineBuilderVisitor().buildKeyframes(driver, rootElement, ast, enterClassName, leaveClassName, startingStyles, finalStyles, options, subInstructions, errors);
    }
    var AnimationTimelineBuilderVisitor = /** @class */ (function () {
        function AnimationTimelineBuilderVisitor() {
        }
        AnimationTimelineBuilderVisitor.prototype.buildKeyframes = function (driver, rootElement, ast, enterClassName, leaveClassName, startingStyles, finalStyles, options, subInstructions, errors) {
            if (errors === void 0) { errors = []; }
            subInstructions = subInstructions || new ElementInstructionMap();
            var context = new AnimationTimelineContext(driver, rootElement, subInstructions, enterClassName, leaveClassName, errors, []);
            context.options = options;
            context.currentTimeline.setStyles([startingStyles], null, context.errors, options);
            visitDslNode(this, ast, context);
            // this checks to see if an actual animation happened
            var timelines = context.timelines.filter(function (timeline) { return timeline.containsAnimation(); });
            if (timelines.length && Object.keys(finalStyles).length) {
                var tl = timelines[timelines.length - 1];
                if (!tl.allowOnlyTimelineStyles()) {
                    tl.setStyles([finalStyles], null, context.errors, options);
                }
            }
            return timelines.length ? timelines.map(function (timeline) { return timeline.buildKeyframes(); }) :
                [createTimelineInstruction(rootElement, [], [], [], 0, 0, '', false)];
        };
        AnimationTimelineBuilderVisitor.prototype.visitTrigger = function (ast, context) {
            // these values are not visited in this AST
        };
        AnimationTimelineBuilderVisitor.prototype.visitState = function (ast, context) {
            // these values are not visited in this AST
        };
        AnimationTimelineBuilderVisitor.prototype.visitTransition = function (ast, context) {
            // these values are not visited in this AST
        };
        AnimationTimelineBuilderVisitor.prototype.visitAnimateChild = function (ast, context) {
            var elementInstructions = context.subInstructions.consume(context.element);
            if (elementInstructions) {
                var innerContext = context.createSubContext(ast.options);
                var startTime = context.currentTimeline.currentTime;
                var endTime = this._visitSubInstructions(elementInstructions, innerContext, innerContext.options);
                if (startTime != endTime) {
                    // we do this on the upper context because we created a sub context for
                    // the sub child animations
                    context.transformIntoNewTimeline(endTime);
                }
            }
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitAnimateRef = function (ast, context) {
            var innerContext = context.createSubContext(ast.options);
            innerContext.transformIntoNewTimeline();
            this.visitReference(ast.animation, innerContext);
            context.transformIntoNewTimeline(innerContext.currentTimeline.currentTime);
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype._visitSubInstructions = function (instructions, context, options) {
            var startTime = context.currentTimeline.currentTime;
            var furthestTime = startTime;
            // this is a special-case for when a user wants to skip a sub
            // animation from being fired entirely.
            var duration = options.duration != null ? resolveTimingValue(options.duration) : null;
            var delay = options.delay != null ? resolveTimingValue(options.delay) : null;
            if (duration !== 0) {
                instructions.forEach(function (instruction) {
                    var instructionTimings = context.appendInstructionToTimeline(instruction, duration, delay);
                    furthestTime =
                        Math.max(furthestTime, instructionTimings.duration + instructionTimings.delay);
                });
            }
            return furthestTime;
        };
        AnimationTimelineBuilderVisitor.prototype.visitReference = function (ast, context) {
            context.updateOptions(ast.options, true);
            visitDslNode(this, ast.animation, context);
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitSequence = function (ast, context) {
            var _this = this;
            var subContextCount = context.subContextCount;
            var ctx = context;
            var options = ast.options;
            if (options && (options.params || options.delay)) {
                ctx = context.createSubContext(options);
                ctx.transformIntoNewTimeline();
                if (options.delay != null) {
                    if (ctx.previousNode.type == 6 /* Style */) {
                        ctx.currentTimeline.snapshotCurrentStyles();
                        ctx.previousNode = DEFAULT_NOOP_PREVIOUS_NODE;
                    }
                    var delay = resolveTimingValue(options.delay);
                    ctx.delayNextStep(delay);
                }
            }
            if (ast.steps.length) {
                ast.steps.forEach(function (s) { return visitDslNode(_this, s, ctx); });
                // this is here just incase the inner steps only contain or end with a style() call
                ctx.currentTimeline.applyStylesToKeyframe();
                // this means that some animation function within the sequence
                // ended up creating a sub timeline (which means the current
                // timeline cannot overlap with the contents of the sequence)
                if (ctx.subContextCount > subContextCount) {
                    ctx.transformIntoNewTimeline();
                }
            }
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitGroup = function (ast, context) {
            var _this = this;
            var innerTimelines = [];
            var furthestTime = context.currentTimeline.currentTime;
            var delay = ast.options && ast.options.delay ? resolveTimingValue(ast.options.delay) : 0;
            ast.steps.forEach(function (s) {
                var innerContext = context.createSubContext(ast.options);
                if (delay) {
                    innerContext.delayNextStep(delay);
                }
                visitDslNode(_this, s, innerContext);
                furthestTime = Math.max(furthestTime, innerContext.currentTimeline.currentTime);
                innerTimelines.push(innerContext.currentTimeline);
            });
            // this operation is run after the AST loop because otherwise
            // if the parent timeline's collected styles were updated then
            // it would pass in invalid data into the new-to-be forked items
            innerTimelines.forEach(function (timeline) { return context.currentTimeline.mergeTimelineCollectedStyles(timeline); });
            context.transformIntoNewTimeline(furthestTime);
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype._visitTiming = function (ast, context) {
            if (ast.dynamic) {
                var strValue = ast.strValue;
                var timingValue = context.params ? interpolateParams(strValue, context.params, context.errors) : strValue;
                return resolveTiming(timingValue, context.errors);
            }
            else {
                return { duration: ast.duration, delay: ast.delay, easing: ast.easing };
            }
        };
        AnimationTimelineBuilderVisitor.prototype.visitAnimate = function (ast, context) {
            var timings = context.currentAnimateTimings = this._visitTiming(ast.timings, context);
            var timeline = context.currentTimeline;
            if (timings.delay) {
                context.incrementTime(timings.delay);
                timeline.snapshotCurrentStyles();
            }
            var style$$1 = ast.style;
            if (style$$1.type == 5 /* Keyframes */) {
                this.visitKeyframes(style$$1, context);
            }
            else {
                context.incrementTime(timings.duration);
                this.visitStyle(style$$1, context);
                timeline.applyStylesToKeyframe();
            }
            context.currentAnimateTimings = null;
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitStyle = function (ast, context) {
            var timeline = context.currentTimeline;
            var timings = context.currentAnimateTimings;
            // this is a special case for when a style() call
            // directly follows  an animate() call (but not inside of an animate() call)
            if (!timings && timeline.getCurrentStyleProperties().length) {
                timeline.forwardFrame();
            }
            var easing = (timings && timings.easing) || ast.easing;
            if (ast.isEmptyStep) {
                timeline.applyEmptyStep(easing);
            }
            else {
                timeline.setStyles(ast.styles, easing, context.errors, context.options);
            }
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitKeyframes = function (ast, context) {
            var currentAnimateTimings = context.currentAnimateTimings;
            var startTime = (context.currentTimeline).duration;
            var duration = currentAnimateTimings.duration;
            var innerContext = context.createSubContext();
            var innerTimeline = innerContext.currentTimeline;
            innerTimeline.easing = currentAnimateTimings.easing;
            ast.styles.forEach(function (step) {
                var offset = step.offset || 0;
                innerTimeline.forwardTime(offset * duration);
                innerTimeline.setStyles(step.styles, step.easing, context.errors, context.options);
                innerTimeline.applyStylesToKeyframe();
            });
            // this will ensure that the parent timeline gets all the styles from
            // the child even if the new timeline below is not used
            context.currentTimeline.mergeTimelineCollectedStyles(innerTimeline);
            // we do this because the window between this timeline and the sub timeline
            // should ensure that the styles within are exactly the same as they were before
            context.transformIntoNewTimeline(startTime + duration);
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitQuery = function (ast, context) {
            var _this = this;
            // in the event that the first step before this is a style step we need
            // to ensure the styles are applied before the children are animated
            var startTime = context.currentTimeline.currentTime;
            var options = (ast.options || {});
            var delay = options.delay ? resolveTimingValue(options.delay) : 0;
            if (delay && (context.previousNode.type === 6 /* Style */ ||
                (startTime == 0 && context.currentTimeline.getCurrentStyleProperties().length))) {
                context.currentTimeline.snapshotCurrentStyles();
                context.previousNode = DEFAULT_NOOP_PREVIOUS_NODE;
            }
            var furthestTime = startTime;
            var elms = context.invokeQuery(ast.selector, ast.originalSelector, ast.limit, ast.includeSelf, options.optional ? true : false, context.errors);
            context.currentQueryTotal = elms.length;
            var sameElementTimeline = null;
            elms.forEach(function (element, i) {
                context.currentQueryIndex = i;
                var innerContext = context.createSubContext(ast.options, element);
                if (delay) {
                    innerContext.delayNextStep(delay);
                }
                if (element === context.element) {
                    sameElementTimeline = innerContext.currentTimeline;
                }
                visitDslNode(_this, ast.animation, innerContext);
                // this is here just incase the inner steps only contain or end
                // with a style() call (which is here to signal that this is a preparatory
                // call to style an element before it is animated again)
                innerContext.currentTimeline.applyStylesToKeyframe();
                var endTime = innerContext.currentTimeline.currentTime;
                furthestTime = Math.max(furthestTime, endTime);
            });
            context.currentQueryIndex = 0;
            context.currentQueryTotal = 0;
            context.transformIntoNewTimeline(furthestTime);
            if (sameElementTimeline) {
                context.currentTimeline.mergeTimelineCollectedStyles(sameElementTimeline);
                context.currentTimeline.snapshotCurrentStyles();
            }
            context.previousNode = ast;
        };
        AnimationTimelineBuilderVisitor.prototype.visitStagger = function (ast, context) {
            var parentContext = context.parentContext;
            var tl = context.currentTimeline;
            var timings = ast.timings;
            var duration = Math.abs(timings.duration);
            var maxTime = duration * (context.currentQueryTotal - 1);
            var delay = duration * context.currentQueryIndex;
            var staggerTransformer = timings.duration < 0 ? 'reverse' : timings.easing;
            switch (staggerTransformer) {
                case 'reverse':
                    delay = maxTime - delay;
                    break;
                case 'full':
                    delay = parentContext.currentStaggerTime;
                    break;
            }
            var timeline = context.currentTimeline;
            if (delay) {
                timeline.delayNextStep(delay);
            }
            var startingTime = timeline.currentTime;
            visitDslNode(this, ast.animation, context);
            context.previousNode = ast;
            // time = duration + delay
            // the reason why this computation is so complex is because
            // the inner timeline may either have a delay value or a stretched
            // keyframe depending on if a subtimeline is not used or is used.
            parentContext.currentStaggerTime =
                (tl.currentTime - startingTime) + (tl.startTime - parentContext.currentTimeline.startTime);
        };
        return AnimationTimelineBuilderVisitor;
    }());
    var DEFAULT_NOOP_PREVIOUS_NODE = {};
    var AnimationTimelineContext = /** @class */ (function () {
        function AnimationTimelineContext(_driver, element, subInstructions, _enterClassName, _leaveClassName, errors, timelines, initialTimeline) {
            this._driver = _driver;
            this.element = element;
            this.subInstructions = subInstructions;
            this._enterClassName = _enterClassName;
            this._leaveClassName = _leaveClassName;
            this.errors = errors;
            this.timelines = timelines;
            this.parentContext = null;
            this.currentAnimateTimings = null;
            this.previousNode = DEFAULT_NOOP_PREVIOUS_NODE;
            this.subContextCount = 0;
            this.options = {};
            this.currentQueryIndex = 0;
            this.currentQueryTotal = 0;
            this.currentStaggerTime = 0;
            this.currentTimeline = initialTimeline || new TimelineBuilder(this._driver, element, 0);
            timelines.push(this.currentTimeline);
        }
        Object.defineProperty(AnimationTimelineContext.prototype, "params", {
            get: function () { return this.options.params; },
            enumerable: true,
            configurable: true
        });
        AnimationTimelineContext.prototype.updateOptions = function (options, skipIfExists) {
            var _this = this;
            if (!options)
                return;
            var newOptions = options;
            var optionsToUpdate = this.options;
            // NOTE: this will get patched up when other animation methods support duration overrides
            if (newOptions.duration != null) {
                optionsToUpdate.duration = resolveTimingValue(newOptions.duration);
            }
            if (newOptions.delay != null) {
                optionsToUpdate.delay = resolveTimingValue(newOptions.delay);
            }
            var newParams = newOptions.params;
            if (newParams) {
                var paramsToUpdate_1 = optionsToUpdate.params;
                if (!paramsToUpdate_1) {
                    paramsToUpdate_1 = this.options.params = {};
                }
                Object.keys(newParams).forEach(function (name) {
                    if (!skipIfExists || !paramsToUpdate_1.hasOwnProperty(name)) {
                        paramsToUpdate_1[name] = interpolateParams(newParams[name], paramsToUpdate_1, _this.errors);
                    }
                });
            }
        };
        AnimationTimelineContext.prototype._copyOptions = function () {
            var options = {};
            if (this.options) {
                var oldParams_1 = this.options.params;
                if (oldParams_1) {
                    var params_1 = options['params'] = {};
                    Object.keys(oldParams_1).forEach(function (name) { params_1[name] = oldParams_1[name]; });
                }
            }
            return options;
        };
        AnimationTimelineContext.prototype.createSubContext = function (options, element, newTime) {
            if (options === void 0) { options = null; }
            var target = element || this.element;
            var context = new AnimationTimelineContext(this._driver, target, this.subInstructions, this._enterClassName, this._leaveClassName, this.errors, this.timelines, this.currentTimeline.fork(target, newTime || 0));
            context.previousNode = this.previousNode;
            context.currentAnimateTimings = this.currentAnimateTimings;
            context.options = this._copyOptions();
            context.updateOptions(options);
            context.currentQueryIndex = this.currentQueryIndex;
            context.currentQueryTotal = this.currentQueryTotal;
            context.parentContext = this;
            this.subContextCount++;
            return context;
        };
        AnimationTimelineContext.prototype.transformIntoNewTimeline = function (newTime) {
            this.previousNode = DEFAULT_NOOP_PREVIOUS_NODE;
            this.currentTimeline = this.currentTimeline.fork(this.element, newTime);
            this.timelines.push(this.currentTimeline);
            return this.currentTimeline;
        };
        AnimationTimelineContext.prototype.appendInstructionToTimeline = function (instruction, duration, delay) {
            var updatedTimings = {
                duration: duration != null ? duration : instruction.duration,
                delay: this.currentTimeline.currentTime + (delay != null ? delay : 0) + instruction.delay,
                easing: ''
            };
            var builder = new SubTimelineBuilder(this._driver, instruction.element, instruction.keyframes, instruction.preStyleProps, instruction.postStyleProps, updatedTimings, instruction.stretchStartingKeyframe);
            this.timelines.push(builder);
            return updatedTimings;
        };
        AnimationTimelineContext.prototype.incrementTime = function (time) {
            this.currentTimeline.forwardTime(this.currentTimeline.duration + time);
        };
        AnimationTimelineContext.prototype.delayNextStep = function (delay) {
            // negative delays are not yet supported
            if (delay > 0) {
                this.currentTimeline.delayNextStep(delay);
            }
        };
        AnimationTimelineContext.prototype.invokeQuery = function (selector, originalSelector, limit, includeSelf, optional, errors) {
            var results = [];
            if (includeSelf) {
                results.push(this.element);
            }
            if (selector.length > 0) { // if :self is only used then the selector is empty
                selector = selector.replace(ENTER_TOKEN_REGEX, '.' + this._enterClassName);
                selector = selector.replace(LEAVE_TOKEN_REGEX, '.' + this._leaveClassName);
                var multi = limit != 1;
                var elements = this._driver.query(this.element, selector, multi);
                if (limit !== 0) {
                    elements = limit < 0 ? elements.slice(elements.length + limit, elements.length) :
                        elements.slice(0, limit);
                }
                results.push.apply(results, __spread(elements));
            }
            if (!optional && results.length == 0) {
                errors.push("`query(\"" + originalSelector + "\")` returned zero elements. (Use `query(\"" + originalSelector + "\", { optional: true })` if you wish to allow this.)");
            }
            return results;
        };
        return AnimationTimelineContext;
    }());
    var TimelineBuilder = /** @class */ (function () {
        function TimelineBuilder(_driver, element, startTime, _elementTimelineStylesLookup) {
            this._driver = _driver;
            this.element = element;
            this.startTime = startTime;
            this._elementTimelineStylesLookup = _elementTimelineStylesLookup;
            this.duration = 0;
            this._previousKeyframe = {};
            this._currentKeyframe = {};
            this._keyframes = new Map();
            this._styleSummary = {};
            this._pendingStyles = {};
            this._backFill = {};
            this._currentEmptyStepKeyframe = null;
            if (!this._elementTimelineStylesLookup) {
                this._elementTimelineStylesLookup = new Map();
            }
            this._localTimelineStyles = Object.create(this._backFill, {});
            this._globalTimelineStyles = this._elementTimelineStylesLookup.get(element);
            if (!this._globalTimelineStyles) {
                this._globalTimelineStyles = this._localTimelineStyles;
                this._elementTimelineStylesLookup.set(element, this._localTimelineStyles);
            }
            this._loadKeyframe();
        }
        TimelineBuilder.prototype.containsAnimation = function () {
            switch (this._keyframes.size) {
                case 0:
                    return false;
                case 1:
                    return this.getCurrentStyleProperties().length > 0;
                default:
                    return true;
            }
        };
        TimelineBuilder.prototype.getCurrentStyleProperties = function () { return Object.keys(this._currentKeyframe); };
        Object.defineProperty(TimelineBuilder.prototype, "currentTime", {
            get: function () { return this.startTime + this.duration; },
            enumerable: true,
            configurable: true
        });
        TimelineBuilder.prototype.delayNextStep = function (delay) {
            // in the event that a style() step is placed right before a stagger()
            // and that style() step is the very first style() value in the animation
            // then we need to make a copy of the keyframe [0, copy, 1] so that the delay
            // properly applies the style() values to work with the stagger...
            var hasPreStyleStep = this._keyframes.size == 1 && Object.keys(this._pendingStyles).length;
            if (this.duration || hasPreStyleStep) {
                this.forwardTime(this.currentTime + delay);
                if (hasPreStyleStep) {
                    this.snapshotCurrentStyles();
                }
            }
            else {
                this.startTime += delay;
            }
        };
        TimelineBuilder.prototype.fork = function (element, currentTime) {
            this.applyStylesToKeyframe();
            return new TimelineBuilder(this._driver, element, currentTime || this.currentTime, this._elementTimelineStylesLookup);
        };
        TimelineBuilder.prototype._loadKeyframe = function () {
            if (this._currentKeyframe) {
                this._previousKeyframe = this._currentKeyframe;
            }
            this._currentKeyframe = this._keyframes.get(this.duration);
            if (!this._currentKeyframe) {
                this._currentKeyframe = Object.create(this._backFill, {});
                this._keyframes.set(this.duration, this._currentKeyframe);
            }
        };
        TimelineBuilder.prototype.forwardFrame = function () {
            this.duration += ONE_FRAME_IN_MILLISECONDS;
            this._loadKeyframe();
        };
        TimelineBuilder.prototype.forwardTime = function (time) {
            this.applyStylesToKeyframe();
            this.duration = time;
            this._loadKeyframe();
        };
        TimelineBuilder.prototype._updateStyle = function (prop, value) {
            this._localTimelineStyles[prop] = value;
            this._globalTimelineStyles[prop] = value;
            this._styleSummary[prop] = { time: this.currentTime, value: value };
        };
        TimelineBuilder.prototype.allowOnlyTimelineStyles = function () { return this._currentEmptyStepKeyframe !== this._currentKeyframe; };
        TimelineBuilder.prototype.applyEmptyStep = function (easing) {
            var _this = this;
            if (easing) {
                this._previousKeyframe['easing'] = easing;
            }
            // special case for animate(duration):
            // all missing styles are filled with a `*` value then
            // if any destination styles are filled in later on the same
            // keyframe then they will override the overridden styles
            // We use `_globalTimelineStyles` here because there may be
            // styles in previous keyframes that are not present in this timeline
            Object.keys(this._globalTimelineStyles).forEach(function (prop) {
                _this._backFill[prop] = _this._globalTimelineStyles[prop] || animations.AUTO_STYLE;
                _this._currentKeyframe[prop] = animations.AUTO_STYLE;
            });
            this._currentEmptyStepKeyframe = this._currentKeyframe;
        };
        TimelineBuilder.prototype.setStyles = function (input, easing, errors, options) {
            var _this = this;
            if (easing) {
                this._previousKeyframe['easing'] = easing;
            }
            var params = (options && options.params) || {};
            var styles = flattenStyles(input, this._globalTimelineStyles);
            Object.keys(styles).forEach(function (prop) {
                var val = interpolateParams(styles[prop], params, errors);
                _this._pendingStyles[prop] = val;
                if (!_this._localTimelineStyles.hasOwnProperty(prop)) {
                    _this._backFill[prop] = _this._globalTimelineStyles.hasOwnProperty(prop) ?
                        _this._globalTimelineStyles[prop] :
                        animations.AUTO_STYLE;
                }
                _this._updateStyle(prop, val);
            });
        };
        TimelineBuilder.prototype.applyStylesToKeyframe = function () {
            var _this = this;
            var styles = this._pendingStyles;
            var props = Object.keys(styles);
            if (props.length == 0)
                return;
            this._pendingStyles = {};
            props.forEach(function (prop) {
                var val = styles[prop];
                _this._currentKeyframe[prop] = val;
            });
            Object.keys(this._localTimelineStyles).forEach(function (prop) {
                if (!_this._currentKeyframe.hasOwnProperty(prop)) {
                    _this._currentKeyframe[prop] = _this._localTimelineStyles[prop];
                }
            });
        };
        TimelineBuilder.prototype.snapshotCurrentStyles = function () {
            var _this = this;
            Object.keys(this._localTimelineStyles).forEach(function (prop) {
                var val = _this._localTimelineStyles[prop];
                _this._pendingStyles[prop] = val;
                _this._updateStyle(prop, val);
            });
        };
        TimelineBuilder.prototype.getFinalKeyframe = function () { return this._keyframes.get(this.duration); };
        Object.defineProperty(TimelineBuilder.prototype, "properties", {
            get: function () {
                var properties = [];
                for (var prop in this._currentKeyframe) {
                    properties.push(prop);
                }
                return properties;
            },
            enumerable: true,
            configurable: true
        });
        TimelineBuilder.prototype.mergeTimelineCollectedStyles = function (timeline) {
            var _this = this;
            Object.keys(timeline._styleSummary).forEach(function (prop) {
                var details0 = _this._styleSummary[prop];
                var details1 = timeline._styleSummary[prop];
                if (!details0 || details1.time > details0.time) {
                    _this._updateStyle(prop, details1.value);
                }
            });
        };
        TimelineBuilder.prototype.buildKeyframes = function () {
            var _this = this;
            this.applyStylesToKeyframe();
            var preStyleProps = new Set();
            var postStyleProps = new Set();
            var isEmpty = this._keyframes.size === 1 && this.duration === 0;
            var finalKeyframes = [];
            this._keyframes.forEach(function (keyframe, time) {
                var finalKeyframe = copyStyles(keyframe, true);
                Object.keys(finalKeyframe).forEach(function (prop) {
                    var value = finalKeyframe[prop];
                    if (value == animations.ɵPRE_STYLE) {
                        preStyleProps.add(prop);
                    }
                    else if (value == animations.AUTO_STYLE) {
                        postStyleProps.add(prop);
                    }
                });
                if (!isEmpty) {
                    finalKeyframe['offset'] = time / _this.duration;
                }
                finalKeyframes.push(finalKeyframe);
            });
            var preProps = preStyleProps.size ? iteratorToArray(preStyleProps.values()) : [];
            var postProps = postStyleProps.size ? iteratorToArray(postStyleProps.values()) : [];
            // special case for a 0-second animation (which is designed just to place styles onscreen)
            if (isEmpty) {
                var kf0 = finalKeyframes[0];
                var kf1 = copyObj(kf0);
                kf0['offset'] = 0;
                kf1['offset'] = 1;
                finalKeyframes = [kf0, kf1];
            }
            return createTimelineInstruction(this.element, finalKeyframes, preProps, postProps, this.duration, this.startTime, this.easing, false);
        };
        return TimelineBuilder;
    }());
    var SubTimelineBuilder = /** @class */ (function (_super) {
        __extends(SubTimelineBuilder, _super);
        function SubTimelineBuilder(driver, element, keyframes, preStyleProps, postStyleProps, timings, _stretchStartingKeyframe) {
            if (_stretchStartingKeyframe === void 0) { _stretchStartingKeyframe = false; }
            var _this = _super.call(this, driver, element, timings.delay) || this;
            _this.element = element;
            _this.keyframes = keyframes;
            _this.preStyleProps = preStyleProps;
            _this.postStyleProps = postStyleProps;
            _this._stretchStartingKeyframe = _stretchStartingKeyframe;
            _this.timings = { duration: timings.duration, delay: timings.delay, easing: timings.easing };
            return _this;
        }
        SubTimelineBuilder.prototype.containsAnimation = function () { return this.keyframes.length > 1; };
        SubTimelineBuilder.prototype.buildKeyframes = function () {
            var keyframes = this.keyframes;
            var _a = this.timings, delay = _a.delay, duration = _a.duration, easing = _a.easing;
            if (this._stretchStartingKeyframe && delay) {
                var newKeyframes = [];
                var totalTime = duration + delay;
                var startingGap = delay / totalTime;
                // the original starting keyframe now starts once the delay is done
                var newFirstKeyframe = copyStyles(keyframes[0], false);
                newFirstKeyframe['offset'] = 0;
                newKeyframes.push(newFirstKeyframe);
                var oldFirstKeyframe = copyStyles(keyframes[0], false);
                oldFirstKeyframe['offset'] = roundOffset(startingGap);
                newKeyframes.push(oldFirstKeyframe);
                /*
                  When the keyframe is stretched then it means that the delay before the animation
                  starts is gone. Instead the first keyframe is placed at the start of the animation
                  and it is then copied to where it starts when the original delay is over. This basically
                  means nothing animates during that delay, but the styles are still renderered. For this
                  to work the original offset values that exist in the original keyframes must be "warped"
                  so that they can take the new keyframe + delay into account.
          
                  delay=1000, duration=1000, keyframes = 0 .5 1
          
                  turns into
          
                  delay=0, duration=2000, keyframes = 0 .33 .66 1
                 */
                // offsets between 1 ... n -1 are all warped by the keyframe stretch
                var limit = keyframes.length - 1;
                for (var i = 1; i <= limit; i++) {
                    var kf = copyStyles(keyframes[i], false);
                    var oldOffset = kf['offset'];
                    var timeAtKeyframe = delay + oldOffset * duration;
                    kf['offset'] = roundOffset(timeAtKeyframe / totalTime);
                    newKeyframes.push(kf);
                }
                // the new starting keyframe should be added at the start
                duration = totalTime;
                delay = 0;
                easing = '';
                keyframes = newKeyframes;
            }
            return createTimelineInstruction(this.element, keyframes, this.preStyleProps, this.postStyleProps, duration, delay, easing, true);
        };
        return SubTimelineBuilder;
    }(TimelineBuilder));
    function roundOffset(offset, decimalPoints) {
        if (decimalPoints === void 0) { decimalPoints = 3; }
        var mult = Math.pow(10, decimalPoints - 1);
        return Math.round(offset * mult) / mult;
    }
    function flattenStyles(input, allStyles) {
        var styles = {};
        var allProperties;
        input.forEach(function (token) {
            if (token === '*') {
                allProperties = allProperties || Object.keys(allStyles);
                allProperties.forEach(function (prop) { styles[prop] = animations.AUTO_STYLE; });
            }
            else {
                copyStyles(token, false, styles);
            }
        });
        return styles;
    }

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * @experimental Animation support is experimental.
     */
    var AnimationStyleNormalizer = /** @class */ (function () {
        function AnimationStyleNormalizer() {
        }
        return AnimationStyleNormalizer;
    }());

    var WebAnimationsStyleNormalizer = /** @class */ (function (_super) {
        __extends(WebAnimationsStyleNormalizer, _super);
        function WebAnimationsStyleNormalizer() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        WebAnimationsStyleNormalizer.prototype.normalizePropertyName = function (propertyName, errors) {
            return dashCaseToCamelCase(propertyName);
        };
        WebAnimationsStyleNormalizer.prototype.normalizeStyleValue = function (userProvidedProperty, normalizedProperty, value, errors) {
            var unit = '';
            var strVal = value.toString().trim();
            if (DIMENSIONAL_PROP_MAP[normalizedProperty] && value !== 0 && value !== '0') {
                if (typeof value === 'number') {
                    unit = 'px';
                }
                else {
                    var valAndSuffixMatch = value.match(/^[+-]?[\d\.]+([a-z]*)$/);
                    if (valAndSuffixMatch && valAndSuffixMatch[1].length == 0) {
                        errors.push("Please provide a CSS unit value for " + userProvidedProperty + ":" + value);
                    }
                }
            }
            return strVal + unit;
        };
        return WebAnimationsStyleNormalizer;
    }(AnimationStyleNormalizer));
    var DIMENSIONAL_PROP_MAP = makeBooleanMap('width,height,minWidth,minHeight,maxWidth,maxHeight,left,top,bottom,right,fontSize,outlineWidth,outlineOffset,paddingTop,paddingLeft,paddingBottom,paddingRight,marginTop,marginLeft,marginBottom,marginRight,borderRadius,borderWidth,borderTopWidth,borderLeftWidth,borderRightWidth,borderBottomWidth,textIndent,perspective'
        .split(','));
    function makeBooleanMap(keys) {
        var map = {};
        keys.forEach(function (key) { return map[key] = true; });
        return map;
    }

    function createTransitionInstruction(element, triggerName, fromState, toState, isRemovalTransition, fromStyles, toStyles, timelines, queriedElements, preStyleProps, postStyleProps, totalTime, errors) {
        return {
            type: 0 /* TransitionAnimation */,
            element: element,
            triggerName: triggerName,
            isRemovalTransition: isRemovalTransition,
            fromState: fromState,
            fromStyles: fromStyles,
            toState: toState,
            toStyles: toStyles,
            timelines: timelines,
            queriedElements: queriedElements,
            preStyleProps: preStyleProps,
            postStyleProps: postStyleProps,
            totalTime: totalTime,
            errors: errors
        };
    }

    var EMPTY_OBJECT = {};
    var AnimationTransitionFactory = /** @class */ (function () {
        function AnimationTransitionFactory(_triggerName, ast, _stateStyles) {
            this._triggerName = _triggerName;
            this.ast = ast;
            this._stateStyles = _stateStyles;
        }
        AnimationTransitionFactory.prototype.match = function (currentState, nextState, element, params) {
            return oneOrMoreTransitionsMatch(this.ast.matchers, currentState, nextState, element, params);
        };
        AnimationTransitionFactory.prototype.buildStyles = function (stateName, params, errors) {
            var backupStateStyler = this._stateStyles['*'];
            var stateStyler = this._stateStyles[stateName];
            var backupStyles = backupStateStyler ? backupStateStyler.buildStyles(params, errors) : {};
            return stateStyler ? stateStyler.buildStyles(params, errors) : backupStyles;
        };
        AnimationTransitionFactory.prototype.build = function (driver, element, currentState, nextState, enterClassName, leaveClassName, currentOptions, nextOptions, subInstructions, skipAstBuild) {
            var errors = [];
            var transitionAnimationParams = this.ast.options && this.ast.options.params || EMPTY_OBJECT;
            var currentAnimationParams = currentOptions && currentOptions.params || EMPTY_OBJECT;
            var currentStateStyles = this.buildStyles(currentState, currentAnimationParams, errors);
            var nextAnimationParams = nextOptions && nextOptions.params || EMPTY_OBJECT;
            var nextStateStyles = this.buildStyles(nextState, nextAnimationParams, errors);
            var queriedElements = new Set();
            var preStyleMap = new Map();
            var postStyleMap = new Map();
            var isRemoval = nextState === 'void';
            var animationOptions = { params: __assign({}, transitionAnimationParams, nextAnimationParams) };
            var timelines = skipAstBuild ? [] : buildAnimationTimelines(driver, element, this.ast.animation, enterClassName, leaveClassName, currentStateStyles, nextStateStyles, animationOptions, subInstructions, errors);
            var totalTime = 0;
            timelines.forEach(function (tl) { totalTime = Math.max(tl.duration + tl.delay, totalTime); });
            if (errors.length) {
                return createTransitionInstruction(element, this._triggerName, currentState, nextState, isRemoval, currentStateStyles, nextStateStyles, [], [], preStyleMap, postStyleMap, totalTime, errors);
            }
            timelines.forEach(function (tl) {
                var elm = tl.element;
                var preProps = getOrSetAsInMap(preStyleMap, elm, {});
                tl.preStyleProps.forEach(function (prop) { return preProps[prop] = true; });
                var postProps = getOrSetAsInMap(postStyleMap, elm, {});
                tl.postStyleProps.forEach(function (prop) { return postProps[prop] = true; });
                if (elm !== element) {
                    queriedElements.add(elm);
                }
            });
            var queriedElementsList = iteratorToArray(queriedElements.values());
            return createTransitionInstruction(element, this._triggerName, currentState, nextState, isRemoval, currentStateStyles, nextStateStyles, timelines, queriedElementsList, preStyleMap, postStyleMap, totalTime);
        };
        return AnimationTransitionFactory;
    }());
    function oneOrMoreTransitionsMatch(matchFns, currentState, nextState, element, params) {
        return matchFns.some(function (fn) { return fn(currentState, nextState, element, params); });
    }
    var AnimationStateStyles = /** @class */ (function () {
        function AnimationStateStyles(styles, defaultParams) {
            this.styles = styles;
            this.defaultParams = defaultParams;
        }
        AnimationStateStyles.prototype.buildStyles = function (params, errors) {
            var finalStyles = {};
            var combinedParams = copyObj(this.defaultParams);
            Object.keys(params).forEach(function (key) {
                var value = params[key];
                if (value != null) {
                    combinedParams[key] = value;
                }
            });
            this.styles.styles.forEach(function (value) {
                if (typeof value !== 'string') {
                    var styleObj_1 = value;
                    Object.keys(styleObj_1).forEach(function (prop) {
                        var val = styleObj_1[prop];
                        if (val.length > 1) {
                            val = interpolateParams(val, combinedParams, errors);
                        }
                        finalStyles[prop] = val;
                    });
                }
            });
            return finalStyles;
        };
        return AnimationStateStyles;
    }());

    /**
     * @experimental Animation support is experimental.
     */
    function buildTrigger(name, ast) {
        return new AnimationTrigger(name, ast);
    }
    /**
    * @experimental Animation support is experimental.
    */
    var AnimationTrigger = /** @class */ (function () {
        function AnimationTrigger(name, ast) {
            var _this = this;
            this.name = name;
            this.ast = ast;
            this.transitionFactories = [];
            this.states = {};
            ast.states.forEach(function (ast) {
                var defaultParams = (ast.options && ast.options.params) || {};
                _this.states[ast.name] = new AnimationStateStyles(ast.style, defaultParams);
            });
            balanceProperties(this.states, 'true', '1');
            balanceProperties(this.states, 'false', '0');
            ast.transitions.forEach(function (ast) {
                _this.transitionFactories.push(new AnimationTransitionFactory(name, ast, _this.states));
            });
            this.fallbackTransition = createFallbackTransition(name, this.states);
        }
        Object.defineProperty(AnimationTrigger.prototype, "containsQueries", {
            get: function () { return this.ast.queryCount > 0; },
            enumerable: true,
            configurable: true
        });
        AnimationTrigger.prototype.matchTransition = function (currentState, nextState, element, params) {
            var entry = this.transitionFactories.find(function (f) { return f.match(currentState, nextState, element, params); });
            return entry || null;
        };
        AnimationTrigger.prototype.matchStyles = function (currentState, params, errors) {
            return this.fallbackTransition.buildStyles(currentState, params, errors);
        };
        return AnimationTrigger;
    }());
    function createFallbackTransition(triggerName, states) {
        var matchers = [function (fromState, toState) { return true; }];
        var animation = { type: 2 /* Sequence */, steps: [], options: null };
        var transition = {
            type: 1 /* Transition */,
            animation: animation,
            matchers: matchers,
            options: null,
            queryCount: 0,
            depCount: 0
        };
        return new AnimationTransitionFactory(triggerName, transition, states);
    }
    function balanceProperties(obj, key1, key2) {
        if (obj.hasOwnProperty(key1)) {
            if (!obj.hasOwnProperty(key2)) {
                obj[key2] = obj[key1];
            }
        }
        else if (obj.hasOwnProperty(key2)) {
            obj[key1] = obj[key2];
        }
    }

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var EMPTY_INSTRUCTION_MAP = new ElementInstructionMap();
    var TimelineAnimationEngine = /** @class */ (function () {
        function TimelineAnimationEngine(bodyNode, _driver, _normalizer) {
            this.bodyNode = bodyNode;
            this._driver = _driver;
            this._normalizer = _normalizer;
            this._animations = {};
            this._playersById = {};
            this.players = [];
        }
        TimelineAnimationEngine.prototype.register = function (id, metadata) {
            var errors = [];
            var ast = buildAnimationAst(this._driver, metadata, errors);
            if (errors.length) {
                throw new Error("Unable to build the animation due to the following errors: " + errors.join("\n"));
            }
            else {
                this._animations[id] = ast;
            }
        };
        TimelineAnimationEngine.prototype._buildPlayer = function (i, preStyles, postStyles) {
            var element = i.element;
            var keyframes = normalizeKeyframes(this._driver, this._normalizer, element, i.keyframes, preStyles, postStyles);
            return this._driver.animate(element, keyframes, i.duration, i.delay, i.easing, [], true);
        };
        TimelineAnimationEngine.prototype.create = function (id, element, options) {
            var _this = this;
            if (options === void 0) { options = {}; }
            var errors = [];
            var ast = this._animations[id];
            var instructions;
            var autoStylesMap = new Map();
            if (ast) {
                instructions = buildAnimationTimelines(this._driver, element, ast, ENTER_CLASSNAME, LEAVE_CLASSNAME, {}, {}, options, EMPTY_INSTRUCTION_MAP, errors);
                instructions.forEach(function (inst) {
                    var styles = getOrSetAsInMap(autoStylesMap, inst.element, {});
                    inst.postStyleProps.forEach(function (prop) { return styles[prop] = null; });
                });
            }
            else {
                errors.push('The requested animation doesn\'t exist or has already been destroyed');
                instructions = [];
            }
            if (errors.length) {
                throw new Error("Unable to create the animation due to the following errors: " + errors.join("\n"));
            }
            autoStylesMap.forEach(function (styles, element) {
                Object.keys(styles).forEach(function (prop) { styles[prop] = _this._driver.computeStyle(element, prop, animations.AUTO_STYLE); });
            });
            var players = instructions.map(function (i) {
                var styles = autoStylesMap.get(i.element);
                return _this._buildPlayer(i, {}, styles);
            });
            var player = optimizeGroupPlayer(players);
            this._playersById[id] = player;
            player.onDestroy(function () { return _this.destroy(id); });
            this.players.push(player);
            return player;
        };
        TimelineAnimationEngine.prototype.destroy = function (id) {
            var player = this._getPlayer(id);
            player.destroy();
            delete this._playersById[id];
            var index = this.players.indexOf(player);
            if (index >= 0) {
                this.players.splice(index, 1);
            }
        };
        TimelineAnimationEngine.prototype._getPlayer = function (id) {
            var player = this._playersById[id];
            if (!player) {
                throw new Error("Unable to find the timeline player referenced by " + id);
            }
            return player;
        };
        TimelineAnimationEngine.prototype.listen = function (id, element, eventName, callback) {
            // triggerName, fromState, toState are all ignored for timeline animations
            var baseEvent = makeAnimationEvent(element, '', '', '');
            listenOnPlayer(this._getPlayer(id), eventName, baseEvent, callback);
            return function () { };
        };
        TimelineAnimationEngine.prototype.command = function (id, element, command, args) {
            if (command == 'register') {
                this.register(id, args[0]);
                return;
            }
            if (command == 'create') {
                var options = (args[0] || {});
                this.create(id, element, options);
                return;
            }
            var player = this._getPlayer(id);
            switch (command) {
                case 'play':
                    player.play();
                    break;
                case 'pause':
                    player.pause();
                    break;
                case 'reset':
                    player.reset();
                    break;
                case 'restart':
                    player.restart();
                    break;
                case 'finish':
                    player.finish();
                    break;
                case 'init':
                    player.init();
                    break;
                case 'setPosition':
                    player.setPosition(parseFloat(args[0]));
                    break;
                case 'destroy':
                    this.destroy(id);
                    break;
            }
        };
        return TimelineAnimationEngine;
    }());

    var QUEUED_CLASSNAME = 'ng-animate-queued';
    var QUEUED_SELECTOR = '.ng-animate-queued';
    var DISABLED_CLASSNAME = 'ng-animate-disabled';
    var DISABLED_SELECTOR = '.ng-animate-disabled';
    var STAR_CLASSNAME = 'ng-star-inserted';
    var STAR_SELECTOR = '.ng-star-inserted';
    var EMPTY_PLAYER_ARRAY = [];
    var NULL_REMOVAL_STATE = {
        namespaceId: '',
        setForRemoval: false,
        setForMove: false,
        hasAnimation: false,
        removedBeforeQueried: false
    };
    var NULL_REMOVED_QUERIED_STATE = {
        namespaceId: '',
        setForMove: false,
        setForRemoval: false,
        hasAnimation: false,
        removedBeforeQueried: true
    };
    var REMOVAL_FLAG = '__ng_removed';
    var StateValue = /** @class */ (function () {
        function StateValue(input, namespaceId) {
            if (namespaceId === void 0) { namespaceId = ''; }
            this.namespaceId = namespaceId;
            var isObj = input && input.hasOwnProperty('value');
            var value = isObj ? input['value'] : input;
            this.value = normalizeTriggerValue(value);
            if (isObj) {
                var options = copyObj(input);
                delete options['value'];
                this.options = options;
            }
            else {
                this.options = {};
            }
            if (!this.options.params) {
                this.options.params = {};
            }
        }
        Object.defineProperty(StateValue.prototype, "params", {
            get: function () { return this.options.params; },
            enumerable: true,
            configurable: true
        });
        StateValue.prototype.absorbOptions = function (options) {
            var newParams = options.params;
            if (newParams) {
                var oldParams_1 = this.options.params;
                Object.keys(newParams).forEach(function (prop) {
                    if (oldParams_1[prop] == null) {
                        oldParams_1[prop] = newParams[prop];
                    }
                });
            }
        };
        return StateValue;
    }());
    var VOID_VALUE = 'void';
    var DEFAULT_STATE_VALUE = new StateValue(VOID_VALUE);
    var AnimationTransitionNamespace = /** @class */ (function () {
        function AnimationTransitionNamespace(id, hostElement, _engine) {
            this.id = id;
            this.hostElement = hostElement;
            this._engine = _engine;
            this.players = [];
            this._triggers = {};
            this._queue = [];
            this._elementListeners = new Map();
            this._hostClassName = 'ng-tns-' + id;
            addClass(hostElement, this._hostClassName);
        }
        AnimationTransitionNamespace.prototype.listen = function (element, name, phase, callback) {
            var _this = this;
            if (!this._triggers.hasOwnProperty(name)) {
                throw new Error("Unable to listen on the animation trigger event \"" + phase + "\" because the animation trigger \"" + name + "\" doesn't exist!");
            }
            if (phase == null || phase.length == 0) {
                throw new Error("Unable to listen on the animation trigger \"" + name + "\" because the provided event is undefined!");
            }
            if (!isTriggerEventValid(phase)) {
                throw new Error("The provided animation trigger event \"" + phase + "\" for the animation trigger \"" + name + "\" is not supported!");
            }
            var listeners = getOrSetAsInMap(this._elementListeners, element, []);
            var data = { name: name, phase: phase, callback: callback };
            listeners.push(data);
            var triggersWithStates = getOrSetAsInMap(this._engine.statesByElement, element, {});
            if (!triggersWithStates.hasOwnProperty(name)) {
                addClass(element, NG_TRIGGER_CLASSNAME);
                addClass(element, NG_TRIGGER_CLASSNAME + '-' + name);
                triggersWithStates[name] = DEFAULT_STATE_VALUE;
            }
            return function () {
                // the event listener is removed AFTER the flush has occurred such
                // that leave animations callbacks can fire (otherwise if the node
                // is removed in between then the listeners would be deregistered)
                _this._engine.afterFlush(function () {
                    var index = listeners.indexOf(data);
                    if (index >= 0) {
                        listeners.splice(index, 1);
                    }
                    if (!_this._triggers[name]) {
                        delete triggersWithStates[name];
                    }
                });
            };
        };
        AnimationTransitionNamespace.prototype.register = function (name, ast) {
            if (this._triggers[name]) {
                // throw
                return false;
            }
            else {
                this._triggers[name] = ast;
                return true;
            }
        };
        AnimationTransitionNamespace.prototype._getTrigger = function (name) {
            var trigger = this._triggers[name];
            if (!trigger) {
                throw new Error("The provided animation trigger \"" + name + "\" has not been registered!");
            }
            return trigger;
        };
        AnimationTransitionNamespace.prototype.trigger = function (element, triggerName, value, defaultToFallback) {
            var _this = this;
            if (defaultToFallback === void 0) { defaultToFallback = true; }
            var trigger = this._getTrigger(triggerName);
            var player = new TransitionAnimationPlayer(this.id, triggerName, element);
            var triggersWithStates = this._engine.statesByElement.get(element);
            if (!triggersWithStates) {
                addClass(element, NG_TRIGGER_CLASSNAME);
                addClass(element, NG_TRIGGER_CLASSNAME + '-' + triggerName);
                this._engine.statesByElement.set(element, triggersWithStates = {});
            }
            var fromState = triggersWithStates[triggerName];
            var toState = new StateValue(value, this.id);
            var isObj = value && value.hasOwnProperty('value');
            if (!isObj && fromState) {
                toState.absorbOptions(fromState.options);
            }
            triggersWithStates[triggerName] = toState;
            if (!fromState) {
                fromState = DEFAULT_STATE_VALUE;
            }
            var isRemoval = toState.value === VOID_VALUE;
            // normally this isn't reached by here, however, if an object expression
            // is passed in then it may be a new object each time. Comparing the value
            // is important since that will stay the same despite there being a new object.
            // The removal arc here is special cased because the same element is triggered
            // twice in the event that it contains animations on the outer/inner portions
            // of the host container
            if (!isRemoval && fromState.value === toState.value) {
                // this means that despite the value not changing, some inner params
                // have changed which means that the animation final styles need to be applied
                if (!objEquals(fromState.params, toState.params)) {
                    var errors = [];
                    var fromStyles_1 = trigger.matchStyles(fromState.value, fromState.params, errors);
                    var toStyles_1 = trigger.matchStyles(toState.value, toState.params, errors);
                    if (errors.length) {
                        this._engine.reportError(errors);
                    }
                    else {
                        this._engine.afterFlush(function () {
                            eraseStyles(element, fromStyles_1);
                            setStyles(element, toStyles_1);
                        });
                    }
                }
                return;
            }
            var playersOnElement = getOrSetAsInMap(this._engine.playersByElement, element, []);
            playersOnElement.forEach(function (player) {
                // only remove the player if it is queued on the EXACT same trigger/namespace
                // we only also deal with queued players here because if the animation has
                // started then we want to keep the player alive until the flush happens
                // (which is where the previousPlayers are passed into the new palyer)
                if (player.namespaceId == _this.id && player.triggerName == triggerName && player.queued) {
                    player.destroy();
                }
            });
            var transition = trigger.matchTransition(fromState.value, toState.value, element, toState.params);
            var isFallbackTransition = false;
            if (!transition) {
                if (!defaultToFallback)
                    return;
                transition = trigger.fallbackTransition;
                isFallbackTransition = true;
            }
            this._engine.totalQueuedPlayers++;
            this._queue.push({ element: element, triggerName: triggerName, transition: transition, fromState: fromState, toState: toState, player: player, isFallbackTransition: isFallbackTransition });
            if (!isFallbackTransition) {
                addClass(element, QUEUED_CLASSNAME);
                player.onStart(function () { removeClass(element, QUEUED_CLASSNAME); });
            }
            player.onDone(function () {
                var index = _this.players.indexOf(player);
                if (index >= 0) {
                    _this.players.splice(index, 1);
                }
                var players = _this._engine.playersByElement.get(element);
                if (players) {
                    var index_1 = players.indexOf(player);
                    if (index_1 >= 0) {
                        players.splice(index_1, 1);
                    }
                }
            });
            this.players.push(player);
            playersOnElement.push(player);
            return player;
        };
        AnimationTransitionNamespace.prototype.deregister = function (name) {
            var _this = this;
            delete this._triggers[name];
            this._engine.statesByElement.forEach(function (stateMap, element) { delete stateMap[name]; });
            this._elementListeners.forEach(function (listeners, element) {
                _this._elementListeners.set(element, listeners.filter(function (entry) { return entry.name != name; }));
            });
        };
        AnimationTransitionNamespace.prototype.clearElementCache = function (element) {
            this._engine.statesByElement.delete(element);
            this._elementListeners.delete(element);
            var elementPlayers = this._engine.playersByElement.get(element);
            if (elementPlayers) {
                elementPlayers.forEach(function (player) { return player.destroy(); });
                this._engine.playersByElement.delete(element);
            }
        };
        AnimationTransitionNamespace.prototype._signalRemovalForInnerTriggers = function (rootElement, context, animate) {
            var _this = this;
            if (animate === void 0) { animate = false; }
            // emulate a leave animation for all inner nodes within this node.
            // If there are no animations found for any of the nodes then clear the cache
            // for the element.
            this._engine.driver.query(rootElement, NG_TRIGGER_SELECTOR, true).forEach(function (elm) {
                // this means that an inner remove() operation has already kicked off
                // the animation on this element...
                if (elm[REMOVAL_FLAG])
                    return;
                var namespaces = _this._engine.fetchNamespacesByElement(elm);
                if (namespaces.size) {
                    namespaces.forEach(function (ns) { return ns.triggerLeaveAnimation(elm, context, false, true); });
                }
                else {
                    _this.clearElementCache(elm);
                }
            });
        };
        AnimationTransitionNamespace.prototype.triggerLeaveAnimation = function (element, context, destroyAfterComplete, defaultToFallback) {
            var _this = this;
            var triggerStates = this._engine.statesByElement.get(element);
            if (triggerStates) {
                var players_1 = [];
                Object.keys(triggerStates).forEach(function (triggerName) {
                    // this check is here in the event that an element is removed
                    // twice (both on the host level and the component level)
                    if (_this._triggers[triggerName]) {
                        var player = _this.trigger(element, triggerName, VOID_VALUE, defaultToFallback);
                        if (player) {
                            players_1.push(player);
                        }
                    }
                });
                if (players_1.length) {
                    this._engine.markElementAsRemoved(this.id, element, true, context);
                    if (destroyAfterComplete) {
                        optimizeGroupPlayer(players_1).onDone(function () { return _this._engine.processLeaveNode(element); });
                    }
                    return true;
                }
            }
            return false;
        };
        AnimationTransitionNamespace.prototype.prepareLeaveAnimationListeners = function (element) {
            var _this = this;
            var listeners = this._elementListeners.get(element);
            if (listeners) {
                var visitedTriggers_1 = new Set();
                listeners.forEach(function (listener) {
                    var triggerName = listener.name;
                    if (visitedTriggers_1.has(triggerName))
                        return;
                    visitedTriggers_1.add(triggerName);
                    var trigger = _this._triggers[triggerName];
                    var transition = trigger.fallbackTransition;
                    var elementStates = _this._engine.statesByElement.get(element);
                    var fromState = elementStates[triggerName] || DEFAULT_STATE_VALUE;
                    var toState = new StateValue(VOID_VALUE);
                    var player = new TransitionAnimationPlayer(_this.id, triggerName, element);
                    _this._engine.totalQueuedPlayers++;
                    _this._queue.push({
                        element: element,
                        triggerName: triggerName,
                        transition: transition,
                        fromState: fromState,
                        toState: toState,
                        player: player,
                        isFallbackTransition: true
                    });
                });
            }
        };
        AnimationTransitionNamespace.prototype.removeNode = function (element, context) {
            var _this = this;
            var engine = this._engine;
            if (element.childElementCount) {
                this._signalRemovalForInnerTriggers(element, context, true);
            }
            // this means that a * => VOID animation was detected and kicked off
            if (this.triggerLeaveAnimation(element, context, true))
                return;
            // find the player that is animating and make sure that the
            // removal is delayed until that player has completed
            var containsPotentialParentTransition = false;
            if (engine.totalAnimations) {
                var currentPlayers = engine.players.length ? engine.playersByQueriedElement.get(element) : [];
                // when this `if statement` does not continue forward it means that
                // a previous animation query has selected the current element and
                // is animating it. In this situation want to continue forwards and
                // allow the element to be queued up for animation later.
                if (currentPlayers && currentPlayers.length) {
                    containsPotentialParentTransition = true;
                }
                else {
                    var parent_1 = element;
                    while (parent_1 = parent_1.parentNode) {
                        var triggers = engine.statesByElement.get(parent_1);
                        if (triggers) {
                            containsPotentialParentTransition = true;
                            break;
                        }
                    }
                }
            }
            // at this stage we know that the element will either get removed
            // during flush or will be picked up by a parent query. Either way
            // we need to fire the listeners for this element when it DOES get
            // removed (once the query parent animation is done or after flush)
            this.prepareLeaveAnimationListeners(element);
            // whether or not a parent has an animation we need to delay the deferral of the leave
            // operation until we have more information (which we do after flush() has been called)
            if (containsPotentialParentTransition) {
                engine.markElementAsRemoved(this.id, element, false, context);
            }
            else {
                // we do this after the flush has occurred such
                // that the callbacks can be fired
                engine.afterFlush(function () { return _this.clearElementCache(element); });
                engine.destroyInnerAnimations(element);
                engine._onRemovalComplete(element, context);
            }
        };
        AnimationTransitionNamespace.prototype.insertNode = function (element, parent) { addClass(element, this._hostClassName); };
        AnimationTransitionNamespace.prototype.drainQueuedTransitions = function (microtaskId) {
            var _this = this;
            var instructions = [];
            this._queue.forEach(function (entry) {
                var player = entry.player;
                if (player.destroyed)
                    return;
                var element = entry.element;
                var listeners = _this._elementListeners.get(element);
                if (listeners) {
                    listeners.forEach(function (listener) {
                        if (listener.name == entry.triggerName) {
                            var baseEvent = makeAnimationEvent(element, entry.triggerName, entry.fromState.value, entry.toState.value);
                            baseEvent['_data'] = microtaskId;
                            listenOnPlayer(entry.player, listener.phase, baseEvent, listener.callback);
                        }
                    });
                }
                if (player.markedForDestroy) {
                    _this._engine.afterFlush(function () {
                        // now we can destroy the element properly since the event listeners have
                        // been bound to the player
                        player.destroy();
                    });
                }
                else {
                    instructions.push(entry);
                }
            });
            this._queue = [];
            return instructions.sort(function (a, b) {
                // if depCount == 0 them move to front
                // otherwise if a contains b then move back
                var d0 = a.transition.ast.depCount;
                var d1 = b.transition.ast.depCount;
                if (d0 == 0 || d1 == 0) {
                    return d0 - d1;
                }
                return _this._engine.driver.containsElement(a.element, b.element) ? 1 : -1;
            });
        };
        AnimationTransitionNamespace.prototype.destroy = function (context) {
            this.players.forEach(function (p) { return p.destroy(); });
            this._signalRemovalForInnerTriggers(this.hostElement, context);
        };
        AnimationTransitionNamespace.prototype.elementContainsData = function (element) {
            var containsData = false;
            if (this._elementListeners.has(element))
                containsData = true;
            containsData =
                (this._queue.find(function (entry) { return entry.element === element; }) ? true : false) || containsData;
            return containsData;
        };
        return AnimationTransitionNamespace;
    }());
    var TransitionAnimationEngine = /** @class */ (function () {
        function TransitionAnimationEngine(bodyNode, driver, _normalizer) {
            this.bodyNode = bodyNode;
            this.driver = driver;
            this._normalizer = _normalizer;
            this.players = [];
            this.newHostElements = new Map();
            this.playersByElement = new Map();
            this.playersByQueriedElement = new Map();
            this.statesByElement = new Map();
            this.disabledNodes = new Set();
            this.totalAnimations = 0;
            this.totalQueuedPlayers = 0;
            this._namespaceLookup = {};
            this._namespaceList = [];
            this._flushFns = [];
            this._whenQuietFns = [];
            this.namespacesByHostElement = new Map();
            this.collectedEnterElements = [];
            this.collectedLeaveElements = [];
            // this method is designed to be overridden by the code that uses this engine
            this.onRemovalComplete = function (element, context) { };
        }
        /** @internal */
        TransitionAnimationEngine.prototype._onRemovalComplete = function (element, context) { this.onRemovalComplete(element, context); };
        Object.defineProperty(TransitionAnimationEngine.prototype, "queuedPlayers", {
            get: function () {
                var players = [];
                this._namespaceList.forEach(function (ns) {
                    ns.players.forEach(function (player) {
                        if (player.queued) {
                            players.push(player);
                        }
                    });
                });
                return players;
            },
            enumerable: true,
            configurable: true
        });
        TransitionAnimationEngine.prototype.createNamespace = function (namespaceId, hostElement) {
            var ns = new AnimationTransitionNamespace(namespaceId, hostElement, this);
            if (hostElement.parentNode) {
                this._balanceNamespaceList(ns, hostElement);
            }
            else {
                // defer this later until flush during when the host element has
                // been inserted so that we know exactly where to place it in
                // the namespace list
                this.newHostElements.set(hostElement, ns);
                // given that this host element is apart of the animation code, it
                // may or may not be inserted by a parent node that is an of an
                // animation renderer type. If this happens then we can still have
                // access to this item when we query for :enter nodes. If the parent
                // is a renderer then the set data-structure will normalize the entry
                this.collectEnterElement(hostElement);
            }
            return this._namespaceLookup[namespaceId] = ns;
        };
        TransitionAnimationEngine.prototype._balanceNamespaceList = function (ns, hostElement) {
            var limit = this._namespaceList.length - 1;
            if (limit >= 0) {
                var found = false;
                for (var i = limit; i >= 0; i--) {
                    var nextNamespace = this._namespaceList[i];
                    if (this.driver.containsElement(nextNamespace.hostElement, hostElement)) {
                        this._namespaceList.splice(i + 1, 0, ns);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    this._namespaceList.splice(0, 0, ns);
                }
            }
            else {
                this._namespaceList.push(ns);
            }
            this.namespacesByHostElement.set(hostElement, ns);
            return ns;
        };
        TransitionAnimationEngine.prototype.register = function (namespaceId, hostElement) {
            var ns = this._namespaceLookup[namespaceId];
            if (!ns) {
                ns = this.createNamespace(namespaceId, hostElement);
            }
            return ns;
        };
        TransitionAnimationEngine.prototype.registerTrigger = function (namespaceId, name, trigger) {
            var ns = this._namespaceLookup[namespaceId];
            if (ns && ns.register(name, trigger)) {
                this.totalAnimations++;
            }
        };
        TransitionAnimationEngine.prototype.destroy = function (namespaceId, context) {
            var _this = this;
            if (!namespaceId)
                return;
            var ns = this._fetchNamespace(namespaceId);
            this.afterFlush(function () {
                _this.namespacesByHostElement.delete(ns.hostElement);
                delete _this._namespaceLookup[namespaceId];
                var index = _this._namespaceList.indexOf(ns);
                if (index >= 0) {
                    _this._namespaceList.splice(index, 1);
                }
            });
            this.afterFlushAnimationsDone(function () { return ns.destroy(context); });
        };
        TransitionAnimationEngine.prototype._fetchNamespace = function (id) { return this._namespaceLookup[id]; };
        TransitionAnimationEngine.prototype.fetchNamespacesByElement = function (element) {
            // normally there should only be one namespace per element, however
            // if @triggers are placed on both the component element and then
            // its host element (within the component code) then there will be
            // two namespaces returned. We use a set here to simply the dedupe
            // of namespaces incase there are multiple triggers both the elm and host
            var namespaces = new Set();
            var elementStates = this.statesByElement.get(element);
            if (elementStates) {
                var keys = Object.keys(elementStates);
                for (var i = 0; i < keys.length; i++) {
                    var nsId = elementStates[keys[i]].namespaceId;
                    if (nsId) {
                        var ns = this._fetchNamespace(nsId);
                        if (ns) {
                            namespaces.add(ns);
                        }
                    }
                }
            }
            return namespaces;
        };
        TransitionAnimationEngine.prototype.trigger = function (namespaceId, element, name, value) {
            if (isElementNode(element)) {
                var ns = this._fetchNamespace(namespaceId);
                if (ns) {
                    ns.trigger(element, name, value);
                    return true;
                }
            }
            return false;
        };
        TransitionAnimationEngine.prototype.insertNode = function (namespaceId, element, parent, insertBefore) {
            if (!isElementNode(element))
                return;
            // special case for when an element is removed and reinserted (move operation)
            // when this occurs we do not want to use the element for deletion later
            var details = element[REMOVAL_FLAG];
            if (details && details.setForRemoval) {
                details.setForRemoval = false;
                details.setForMove = true;
                var index = this.collectedLeaveElements.indexOf(element);
                if (index >= 0) {
                    this.collectedLeaveElements.splice(index, 1);
                }
            }
            // in the event that the namespaceId is blank then the caller
            // code does not contain any animation code in it, but it is
            // just being called so that the node is marked as being inserted
            if (namespaceId) {
                var ns = this._fetchNamespace(namespaceId);
                // This if-statement is a workaround for router issue #21947.
                // The router sometimes hits a race condition where while a route
                // is being instantiated a new navigation arrives, triggering leave
                // animation of DOM that has not been fully initialized, until this
                // is resolved, we need to handle the scenario when DOM is not in a
                // consistent state during the animation.
                if (ns) {
                    ns.insertNode(element, parent);
                }
            }
            // only *directives and host elements are inserted before
            if (insertBefore) {
                this.collectEnterElement(element);
            }
        };
        TransitionAnimationEngine.prototype.collectEnterElement = function (element) { this.collectedEnterElements.push(element); };
        TransitionAnimationEngine.prototype.markElementAsDisabled = function (element, value) {
            if (value) {
                if (!this.disabledNodes.has(element)) {
                    this.disabledNodes.add(element);
                    addClass(element, DISABLED_CLASSNAME);
                }
            }
            else if (this.disabledNodes.has(element)) {
                this.disabledNodes.delete(element);
                removeClass(element, DISABLED_CLASSNAME);
            }
        };
        TransitionAnimationEngine.prototype.removeNode = function (namespaceId, element, context) {
            if (!isElementNode(element)) {
                this._onRemovalComplete(element, context);
                return;
            }
            var ns = namespaceId ? this._fetchNamespace(namespaceId) : null;
            if (ns) {
                ns.removeNode(element, context);
            }
            else {
                this.markElementAsRemoved(namespaceId, element, false, context);
            }
        };
        TransitionAnimationEngine.prototype.markElementAsRemoved = function (namespaceId, element, hasAnimation, context) {
            this.collectedLeaveElements.push(element);
            element[REMOVAL_FLAG] = {
                namespaceId: namespaceId,
                setForRemoval: context, hasAnimation: hasAnimation,
                removedBeforeQueried: false
            };
        };
        TransitionAnimationEngine.prototype.listen = function (namespaceId, element, name, phase, callback) {
            if (isElementNode(element)) {
                return this._fetchNamespace(namespaceId).listen(element, name, phase, callback);
            }
            return function () { };
        };
        TransitionAnimationEngine.prototype._buildInstruction = function (entry, subTimelines, enterClassName, leaveClassName, skipBuildAst) {
            return entry.transition.build(this.driver, entry.element, entry.fromState.value, entry.toState.value, enterClassName, leaveClassName, entry.fromState.options, entry.toState.options, subTimelines, skipBuildAst);
        };
        TransitionAnimationEngine.prototype.destroyInnerAnimations = function (containerElement) {
            var _this = this;
            var elements = this.driver.query(containerElement, NG_TRIGGER_SELECTOR, true);
            elements.forEach(function (element) { return _this.destroyActiveAnimationsForElement(element); });
            if (this.playersByQueriedElement.size == 0)
                return;
            elements = this.driver.query(containerElement, NG_ANIMATING_SELECTOR, true);
            elements.forEach(function (element) { return _this.finishActiveQueriedAnimationOnElement(element); });
        };
        TransitionAnimationEngine.prototype.destroyActiveAnimationsForElement = function (element) {
            var players = this.playersByElement.get(element);
            if (players) {
                players.forEach(function (player) {
                    // special case for when an element is set for destruction, but hasn't started.
                    // in this situation we want to delay the destruction until the flush occurs
                    // so that any event listeners attached to the player are triggered.
                    if (player.queued) {
                        player.markedForDestroy = true;
                    }
                    else {
                        player.destroy();
                    }
                });
            }
        };
        TransitionAnimationEngine.prototype.finishActiveQueriedAnimationOnElement = function (element) {
            var players = this.playersByQueriedElement.get(element);
            if (players) {
                players.forEach(function (player) { return player.finish(); });
            }
        };
        TransitionAnimationEngine.prototype.whenRenderingDone = function () {
            var _this = this;
            return new Promise(function (resolve) {
                if (_this.players.length) {
                    return optimizeGroupPlayer(_this.players).onDone(function () { return resolve(); });
                }
                else {
                    resolve();
                }
            });
        };
        TransitionAnimationEngine.prototype.processLeaveNode = function (element) {
            var _this = this;
            var details = element[REMOVAL_FLAG];
            if (details && details.setForRemoval) {
                // this will prevent it from removing it twice
                element[REMOVAL_FLAG] = NULL_REMOVAL_STATE;
                if (details.namespaceId) {
                    this.destroyInnerAnimations(element);
                    var ns = this._fetchNamespace(details.namespaceId);
                    if (ns) {
                        ns.clearElementCache(element);
                    }
                }
                this._onRemovalComplete(element, details.setForRemoval);
            }
            if (this.driver.matchesElement(element, DISABLED_SELECTOR)) {
                this.markElementAsDisabled(element, false);
            }
            this.driver.query(element, DISABLED_SELECTOR, true).forEach(function (node) {
                _this.markElementAsDisabled(element, false);
            });
        };
        TransitionAnimationEngine.prototype.flush = function (microtaskId) {
            var _this = this;
            if (microtaskId === void 0) { microtaskId = -1; }
            var players = [];
            if (this.newHostElements.size) {
                this.newHostElements.forEach(function (ns, element) { return _this._balanceNamespaceList(ns, element); });
                this.newHostElements.clear();
            }
            if (this.totalAnimations && this.collectedEnterElements.length) {
                for (var i = 0; i < this.collectedEnterElements.length; i++) {
                    var elm = this.collectedEnterElements[i];
                    addClass(elm, STAR_CLASSNAME);
                }
            }
            if (this._namespaceList.length &&
                (this.totalQueuedPlayers || this.collectedLeaveElements.length)) {
                var cleanupFns = [];
                try {
                    players = this._flushAnimations(cleanupFns, microtaskId);
                }
                finally {
                    for (var i = 0; i < cleanupFns.length; i++) {
                        cleanupFns[i]();
                    }
                }
            }
            else {
                for (var i = 0; i < this.collectedLeaveElements.length; i++) {
                    var element = this.collectedLeaveElements[i];
                    this.processLeaveNode(element);
                }
            }
            this.totalQueuedPlayers = 0;
            this.collectedEnterElements.length = 0;
            this.collectedLeaveElements.length = 0;
            this._flushFns.forEach(function (fn) { return fn(); });
            this._flushFns = [];
            if (this._whenQuietFns.length) {
                // we move these over to a variable so that
                // if any new callbacks are registered in another
                // flush they do not populate the existing set
                var quietFns_1 = this._whenQuietFns;
                this._whenQuietFns = [];
                if (players.length) {
                    optimizeGroupPlayer(players).onDone(function () { quietFns_1.forEach(function (fn) { return fn(); }); });
                }
                else {
                    quietFns_1.forEach(function (fn) { return fn(); });
                }
            }
        };
        TransitionAnimationEngine.prototype.reportError = function (errors) {
            throw new Error("Unable to process animations due to the following failed trigger transitions\n " + errors.join('\n'));
        };
        TransitionAnimationEngine.prototype._flushAnimations = function (cleanupFns, microtaskId) {
            var _this = this;
            var subTimelines = new ElementInstructionMap();
            var skippedPlayers = [];
            var skippedPlayersMap = new Map();
            var queuedInstructions = [];
            var queriedElements = new Map();
            var allPreStyleElements = new Map();
            var allPostStyleElements = new Map();
            var disabledElementsSet = new Set();
            this.disabledNodes.forEach(function (node) {
                disabledElementsSet.add(node);
                var nodesThatAreDisabled = _this.driver.query(node, QUEUED_SELECTOR, true);
                for (var i_1 = 0; i_1 < nodesThatAreDisabled.length; i_1++) {
                    disabledElementsSet.add(nodesThatAreDisabled[i_1]);
                }
            });
            var bodyNode = this.bodyNode;
            var allTriggerElements = Array.from(this.statesByElement.keys());
            var enterNodeMap = buildRootMap(allTriggerElements, this.collectedEnterElements);
            // this must occur before the instructions are built below such that
            // the :enter queries match the elements (since the timeline queries
            // are fired during instruction building).
            var enterNodeMapIds = new Map();
            var i = 0;
            enterNodeMap.forEach(function (nodes, root) {
                var className = ENTER_CLASSNAME + i++;
                enterNodeMapIds.set(root, className);
                nodes.forEach(function (node) { return addClass(node, className); });
            });
            var allLeaveNodes = [];
            var mergedLeaveNodes = new Set();
            var leaveNodesWithoutAnimations = new Set();
            for (var i_2 = 0; i_2 < this.collectedLeaveElements.length; i_2++) {
                var element = this.collectedLeaveElements[i_2];
                var details = element[REMOVAL_FLAG];
                if (details && details.setForRemoval) {
                    allLeaveNodes.push(element);
                    mergedLeaveNodes.add(element);
                    if (details.hasAnimation) {
                        this.driver.query(element, STAR_SELECTOR, true).forEach(function (elm) { return mergedLeaveNodes.add(elm); });
                    }
                    else {
                        leaveNodesWithoutAnimations.add(element);
                    }
                }
            }
            var leaveNodeMapIds = new Map();
            var leaveNodeMap = buildRootMap(allTriggerElements, Array.from(mergedLeaveNodes));
            leaveNodeMap.forEach(function (nodes, root) {
                var className = LEAVE_CLASSNAME + i++;
                leaveNodeMapIds.set(root, className);
                nodes.forEach(function (node) { return addClass(node, className); });
            });
            cleanupFns.push(function () {
                enterNodeMap.forEach(function (nodes, root) {
                    var className = enterNodeMapIds.get(root);
                    nodes.forEach(function (node) { return removeClass(node, className); });
                });
                leaveNodeMap.forEach(function (nodes, root) {
                    var className = leaveNodeMapIds.get(root);
                    nodes.forEach(function (node) { return removeClass(node, className); });
                });
                allLeaveNodes.forEach(function (element) { _this.processLeaveNode(element); });
            });
            var allPlayers = [];
            var erroneousTransitions = [];
            for (var i_3 = this._namespaceList.length - 1; i_3 >= 0; i_3--) {
                var ns = this._namespaceList[i_3];
                ns.drainQueuedTransitions(microtaskId).forEach(function (entry) {
                    var player = entry.player;
                    var element = entry.element;
                    allPlayers.push(player);
                    if (_this.collectedEnterElements.length) {
                        var details = element[REMOVAL_FLAG];
                        // move animations are currently not supported...
                        if (details && details.setForMove) {
                            player.destroy();
                            return;
                        }
                    }
                    var nodeIsOrphaned = !bodyNode || !_this.driver.containsElement(bodyNode, element);
                    var leaveClassName = leaveNodeMapIds.get(element);
                    var enterClassName = enterNodeMapIds.get(element);
                    var instruction = _this._buildInstruction(entry, subTimelines, enterClassName, leaveClassName, nodeIsOrphaned);
                    if (instruction.errors && instruction.errors.length) {
                        erroneousTransitions.push(instruction);
                        return;
                    }
                    // even though the element may not be apart of the DOM, it may
                    // still be added at a later point (due to the mechanics of content
                    // projection and/or dynamic component insertion) therefore it's
                    // important we still style the element.
                    if (nodeIsOrphaned) {
                        player.onStart(function () { return eraseStyles(element, instruction.fromStyles); });
                        player.onDestroy(function () { return setStyles(element, instruction.toStyles); });
                        skippedPlayers.push(player);
                        return;
                    }
                    // if a unmatched transition is queued to go then it SHOULD NOT render
                    // an animation and cancel the previously running animations.
                    if (entry.isFallbackTransition) {
                        player.onStart(function () { return eraseStyles(element, instruction.fromStyles); });
                        player.onDestroy(function () { return setStyles(element, instruction.toStyles); });
                        skippedPlayers.push(player);
                        return;
                    }
                    // this means that if a parent animation uses this animation as a sub trigger
                    // then it will instruct the timeline builder to not add a player delay, but
                    // instead stretch the first keyframe gap up until the animation starts. The
                    // reason this is important is to prevent extra initialization styles from being
                    // required by the user in the animation.
                    instruction.timelines.forEach(function (tl) { return tl.stretchStartingKeyframe = true; });
                    subTimelines.append(element, instruction.timelines);
                    var tuple = { instruction: instruction, player: player, element: element };
                    queuedInstructions.push(tuple);
                    instruction.queriedElements.forEach(function (element) { return getOrSetAsInMap(queriedElements, element, []).push(player); });
                    instruction.preStyleProps.forEach(function (stringMap, element) {
                        var props = Object.keys(stringMap);
                        if (props.length) {
                            var setVal_1 = allPreStyleElements.get(element);
                            if (!setVal_1) {
                                allPreStyleElements.set(element, setVal_1 = new Set());
                            }
                            props.forEach(function (prop) { return setVal_1.add(prop); });
                        }
                    });
                    instruction.postStyleProps.forEach(function (stringMap, element) {
                        var props = Object.keys(stringMap);
                        var setVal = allPostStyleElements.get(element);
                        if (!setVal) {
                            allPostStyleElements.set(element, setVal = new Set());
                        }
                        props.forEach(function (prop) { return setVal.add(prop); });
                    });
                });
            }
            if (erroneousTransitions.length) {
                var errors_1 = [];
                erroneousTransitions.forEach(function (instruction) {
                    errors_1.push("@" + instruction.triggerName + " has failed due to:\n");
                    instruction.errors.forEach(function (error) { return errors_1.push("- " + error + "\n"); });
                });
                allPlayers.forEach(function (player) { return player.destroy(); });
                this.reportError(errors_1);
            }
            var allPreviousPlayersMap = new Map();
            // this map works to tell which element in the DOM tree is contained by
            // which animation. Further down below this map will get populated once
            // the players are built and in doing so it can efficiently figure out
            // if a sub player is skipped due to a parent player having priority.
            var animationElementMap = new Map();
            queuedInstructions.forEach(function (entry) {
                var element = entry.element;
                if (subTimelines.has(element)) {
                    animationElementMap.set(element, element);
                    _this._beforeAnimationBuild(entry.player.namespaceId, entry.instruction, allPreviousPlayersMap);
                }
            });
            skippedPlayers.forEach(function (player) {
                var element = player.element;
                var previousPlayers = _this._getPreviousPlayers(element, false, player.namespaceId, player.triggerName, null);
                previousPlayers.forEach(function (prevPlayer) {
                    getOrSetAsInMap(allPreviousPlayersMap, element, []).push(prevPlayer);
                    prevPlayer.destroy();
                });
            });
            // this is a special case for nodes that will be removed (either by)
            // having their own leave animations or by being queried in a container
            // that will be removed once a parent animation is complete. The idea
            // here is that * styles must be identical to ! styles because of
            // backwards compatibility (* is also filled in by default in many places).
            // Otherwise * styles will return an empty value or auto since the element
            // that is being getComputedStyle'd will not be visible (since * = destination)
            var replaceNodes = allLeaveNodes.filter(function (node) {
                return replacePostStylesAsPre(node, allPreStyleElements, allPostStyleElements);
            });
            // POST STAGE: fill the * styles
            var postStylesMap = new Map();
            var allLeaveQueriedNodes = cloakAndComputeStyles(postStylesMap, this.driver, leaveNodesWithoutAnimations, allPostStyleElements, animations.AUTO_STYLE);
            allLeaveQueriedNodes.forEach(function (node) {
                if (replacePostStylesAsPre(node, allPreStyleElements, allPostStyleElements)) {
                    replaceNodes.push(node);
                }
            });
            // PRE STAGE: fill the ! styles
            var preStylesMap = new Map();
            enterNodeMap.forEach(function (nodes, root) {
                cloakAndComputeStyles(preStylesMap, _this.driver, new Set(nodes), allPreStyleElements, animations.ɵPRE_STYLE);
            });
            replaceNodes.forEach(function (node) {
                var post = postStylesMap.get(node);
                var pre = preStylesMap.get(node);
                postStylesMap.set(node, __assign({}, post, pre));
            });
            var rootPlayers = [];
            var subPlayers = [];
            var NO_PARENT_ANIMATION_ELEMENT_DETECTED = {};
            queuedInstructions.forEach(function (entry) {
                var element = entry.element, player = entry.player, instruction = entry.instruction;
                // this means that it was never consumed by a parent animation which
                // means that it is independent and therefore should be set for animation
                if (subTimelines.has(element)) {
                    if (disabledElementsSet.has(element)) {
                        player.onDestroy(function () { return setStyles(element, instruction.toStyles); });
                        player.disabled = true;
                        player.overrideTotalTime(instruction.totalTime);
                        skippedPlayers.push(player);
                        return;
                    }
                    // this will flow up the DOM and query the map to figure out
                    // if a parent animation has priority over it. In the situation
                    // that a parent is detected then it will cancel the loop. If
                    // nothing is detected, or it takes a few hops to find a parent,
                    // then it will fill in the missing nodes and signal them as having
                    // a detected parent (or a NO_PARENT value via a special constant).
                    var parentWithAnimation_1 = NO_PARENT_ANIMATION_ELEMENT_DETECTED;
                    if (animationElementMap.size > 1) {
                        var elm = element;
                        var parentsToAdd = [];
                        while (elm = elm.parentNode) {
                            var detectedParent = animationElementMap.get(elm);
                            if (detectedParent) {
                                parentWithAnimation_1 = detectedParent;
                                break;
                            }
                            parentsToAdd.push(elm);
                        }
                        parentsToAdd.forEach(function (parent) { return animationElementMap.set(parent, parentWithAnimation_1); });
                    }
                    var innerPlayer = _this._buildAnimation(player.namespaceId, instruction, allPreviousPlayersMap, skippedPlayersMap, preStylesMap, postStylesMap);
                    player.setRealPlayer(innerPlayer);
                    if (parentWithAnimation_1 === NO_PARENT_ANIMATION_ELEMENT_DETECTED) {
                        rootPlayers.push(player);
                    }
                    else {
                        var parentPlayers = _this.playersByElement.get(parentWithAnimation_1);
                        if (parentPlayers && parentPlayers.length) {
                            player.parentPlayer = optimizeGroupPlayer(parentPlayers);
                        }
                        skippedPlayers.push(player);
                    }
                }
                else {
                    eraseStyles(element, instruction.fromStyles);
                    player.onDestroy(function () { return setStyles(element, instruction.toStyles); });
                    // there still might be a ancestor player animating this
                    // element therefore we will still add it as a sub player
                    // even if its animation may be disabled
                    subPlayers.push(player);
                    if (disabledElementsSet.has(element)) {
                        skippedPlayers.push(player);
                    }
                }
            });
            // find all of the sub players' corresponding inner animation player
            subPlayers.forEach(function (player) {
                // even if any players are not found for a sub animation then it
                // will still complete itself after the next tick since it's Noop
                var playersForElement = skippedPlayersMap.get(player.element);
                if (playersForElement && playersForElement.length) {
                    var innerPlayer = optimizeGroupPlayer(playersForElement);
                    player.setRealPlayer(innerPlayer);
                }
            });
            // the reason why we don't actually play the animation is
            // because all that a skipped player is designed to do is to
            // fire the start/done transition callback events
            skippedPlayers.forEach(function (player) {
                if (player.parentPlayer) {
                    player.syncPlayerEvents(player.parentPlayer);
                }
                else {
                    player.destroy();
                }
            });
            // run through all of the queued removals and see if they
            // were picked up by a query. If not then perform the removal
            // operation right away unless a parent animation is ongoing.
            for (var i_4 = 0; i_4 < allLeaveNodes.length; i_4++) {
                var element = allLeaveNodes[i_4];
                var details = element[REMOVAL_FLAG];
                removeClass(element, LEAVE_CLASSNAME);
                // this means the element has a removal animation that is being
                // taken care of and therefore the inner elements will hang around
                // until that animation is over (or the parent queried animation)
                if (details && details.hasAnimation)
                    continue;
                var players = [];
                // if this element is queried or if it contains queried children
                // then we want for the element not to be removed from the page
                // until the queried animations have finished
                if (queriedElements.size) {
                    var queriedPlayerResults = queriedElements.get(element);
                    if (queriedPlayerResults && queriedPlayerResults.length) {
                        players.push.apply(players, __spread(queriedPlayerResults));
                    }
                    var queriedInnerElements = this.driver.query(element, NG_ANIMATING_SELECTOR, true);
                    for (var j = 0; j < queriedInnerElements.length; j++) {
                        var queriedPlayers = queriedElements.get(queriedInnerElements[j]);
                        if (queriedPlayers && queriedPlayers.length) {
                            players.push.apply(players, __spread(queriedPlayers));
                        }
                    }
                }
                var activePlayers = players.filter(function (p) { return !p.destroyed; });
                if (activePlayers.length) {
                    removeNodesAfterAnimationDone(this, element, activePlayers);
                }
                else {
                    this.processLeaveNode(element);
                }
            }
            // this is required so the cleanup method doesn't remove them
            allLeaveNodes.length = 0;
            rootPlayers.forEach(function (player) {
                _this.players.push(player);
                player.onDone(function () {
                    player.destroy();
                    var index = _this.players.indexOf(player);
                    _this.players.splice(index, 1);
                });
                player.play();
            });
            return rootPlayers;
        };
        TransitionAnimationEngine.prototype.elementContainsData = function (namespaceId, element) {
            var containsData = false;
            var details = element[REMOVAL_FLAG];
            if (details && details.setForRemoval)
                containsData = true;
            if (this.playersByElement.has(element))
                containsData = true;
            if (this.playersByQueriedElement.has(element))
                containsData = true;
            if (this.statesByElement.has(element))
                containsData = true;
            return this._fetchNamespace(namespaceId).elementContainsData(element) || containsData;
        };
        TransitionAnimationEngine.prototype.afterFlush = function (callback) { this._flushFns.push(callback); };
        TransitionAnimationEngine.prototype.afterFlushAnimationsDone = function (callback) { this._whenQuietFns.push(callback); };
        TransitionAnimationEngine.prototype._getPreviousPlayers = function (element, isQueriedElement, namespaceId, triggerName, toStateValue) {
            var players = [];
            if (isQueriedElement) {
                var queriedElementPlayers = this.playersByQueriedElement.get(element);
                if (queriedElementPlayers) {
                    players = queriedElementPlayers;
                }
            }
            else {
                var elementPlayers = this.playersByElement.get(element);
                if (elementPlayers) {
                    var isRemovalAnimation_1 = !toStateValue || toStateValue == VOID_VALUE;
                    elementPlayers.forEach(function (player) {
                        if (player.queued)
                            return;
                        if (!isRemovalAnimation_1 && player.triggerName != triggerName)
                            return;
                        players.push(player);
                    });
                }
            }
            if (namespaceId || triggerName) {
                players = players.filter(function (player) {
                    if (namespaceId && namespaceId != player.namespaceId)
                        return false;
                    if (triggerName && triggerName != player.triggerName)
                        return false;
                    return true;
                });
            }
            return players;
        };
        TransitionAnimationEngine.prototype._beforeAnimationBuild = function (namespaceId, instruction, allPreviousPlayersMap) {
            var e_1, _a;
            var triggerName = instruction.triggerName;
            var rootElement = instruction.element;
            // when a removal animation occurs, ALL previous players are collected
            // and destroyed (even if they are outside of the current namespace)
            var targetNameSpaceId = instruction.isRemovalTransition ? undefined : namespaceId;
            var targetTriggerName = instruction.isRemovalTransition ? undefined : triggerName;
            var _loop_1 = function (timelineInstruction) {
                var element = timelineInstruction.element;
                var isQueriedElement = element !== rootElement;
                var players = getOrSetAsInMap(allPreviousPlayersMap, element, []);
                var previousPlayers = this_1._getPreviousPlayers(element, isQueriedElement, targetNameSpaceId, targetTriggerName, instruction.toState);
                previousPlayers.forEach(function (player) {
                    var realPlayer = player.getRealPlayer();
                    if (realPlayer.beforeDestroy) {
                        realPlayer.beforeDestroy();
                    }
                    player.destroy();
                    players.push(player);
                });
            };
            var this_1 = this;
            try {
                for (var _b = __values(instruction.timelines), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var timelineInstruction = _c.value;
                    _loop_1(timelineInstruction);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // this needs to be done so that the PRE/POST styles can be
            // computed properly without interfering with the previous animation
            eraseStyles(rootElement, instruction.fromStyles);
        };
        TransitionAnimationEngine.prototype._buildAnimation = function (namespaceId, instruction, allPreviousPlayersMap, skippedPlayersMap, preStylesMap, postStylesMap) {
            var _this = this;
            var triggerName = instruction.triggerName;
            var rootElement = instruction.element;
            // we first run this so that the previous animation player
            // data can be passed into the successive animation players
            var allQueriedPlayers = [];
            var allConsumedElements = new Set();
            var allSubElements = new Set();
            var allNewPlayers = instruction.timelines.map(function (timelineInstruction) {
                var element = timelineInstruction.element;
                allConsumedElements.add(element);
                // FIXME (matsko): make sure to-be-removed animations are removed properly
                var details = element[REMOVAL_FLAG];
                if (details && details.removedBeforeQueried)
                    return new animations.NoopAnimationPlayer(timelineInstruction.duration, timelineInstruction.delay);
                var isQueriedElement = element !== rootElement;
                var previousPlayers = flattenGroupPlayers((allPreviousPlayersMap.get(element) || EMPTY_PLAYER_ARRAY)
                    .map(function (p) { return p.getRealPlayer(); }))
                    .filter(function (p) {
                    // the `element` is not apart of the AnimationPlayer definition, but
                    // Mock/WebAnimations
                    // use the element within their implementation. This will be added in Angular5 to
                    // AnimationPlayer
                    var pp = p;
                    return pp.element ? pp.element === element : false;
                });
                var preStyles = preStylesMap.get(element);
                var postStyles = postStylesMap.get(element);
                var keyframes = normalizeKeyframes(_this.driver, _this._normalizer, element, timelineInstruction.keyframes, preStyles, postStyles);
                var player = _this._buildPlayer(timelineInstruction, keyframes, previousPlayers);
                // this means that this particular player belongs to a sub trigger. It is
                // important that we match this player up with the corresponding (@trigger.listener)
                if (timelineInstruction.subTimeline && skippedPlayersMap) {
                    allSubElements.add(element);
                }
                if (isQueriedElement) {
                    var wrappedPlayer = new TransitionAnimationPlayer(namespaceId, triggerName, element);
                    wrappedPlayer.setRealPlayer(player);
                    allQueriedPlayers.push(wrappedPlayer);
                }
                return player;
            });
            allQueriedPlayers.forEach(function (player) {
                getOrSetAsInMap(_this.playersByQueriedElement, player.element, []).push(player);
                player.onDone(function () { return deleteOrUnsetInMap(_this.playersByQueriedElement, player.element, player); });
            });
            allConsumedElements.forEach(function (element) { return addClass(element, NG_ANIMATING_CLASSNAME); });
            var player = optimizeGroupPlayer(allNewPlayers);
            player.onDestroy(function () {
                allConsumedElements.forEach(function (element) { return removeClass(element, NG_ANIMATING_CLASSNAME); });
                setStyles(rootElement, instruction.toStyles);
            });
            // this basically makes all of the callbacks for sub element animations
            // be dependent on the upper players for when they finish
            allSubElements.forEach(function (element) { getOrSetAsInMap(skippedPlayersMap, element, []).push(player); });
            return player;
        };
        TransitionAnimationEngine.prototype._buildPlayer = function (instruction, keyframes, previousPlayers) {
            if (keyframes.length > 0) {
                return this.driver.animate(instruction.element, keyframes, instruction.duration, instruction.delay, instruction.easing, previousPlayers);
            }
            // special case for when an empty transition|definition is provided
            // ... there is no point in rendering an empty animation
            return new animations.NoopAnimationPlayer(instruction.duration, instruction.delay);
        };
        return TransitionAnimationEngine;
    }());
    var TransitionAnimationPlayer = /** @class */ (function () {
        function TransitionAnimationPlayer(namespaceId, triggerName, element) {
            this.namespaceId = namespaceId;
            this.triggerName = triggerName;
            this.element = element;
            this._player = new animations.NoopAnimationPlayer();
            this._containsRealPlayer = false;
            this._queuedCallbacks = {};
            this.destroyed = false;
            this.markedForDestroy = false;
            this.disabled = false;
            this.queued = true;
            this.totalTime = 0;
        }
        TransitionAnimationPlayer.prototype.setRealPlayer = function (player) {
            var _this = this;
            if (this._containsRealPlayer)
                return;
            this._player = player;
            Object.keys(this._queuedCallbacks).forEach(function (phase) {
                _this._queuedCallbacks[phase].forEach(function (callback) { return listenOnPlayer(player, phase, undefined, callback); });
            });
            this._queuedCallbacks = {};
            this._containsRealPlayer = true;
            this.overrideTotalTime(player.totalTime);
            this.queued = false;
        };
        TransitionAnimationPlayer.prototype.getRealPlayer = function () { return this._player; };
        TransitionAnimationPlayer.prototype.overrideTotalTime = function (totalTime) { this.totalTime = totalTime; };
        TransitionAnimationPlayer.prototype.syncPlayerEvents = function (player) {
            var _this = this;
            var p = this._player;
            if (p.triggerCallback) {
                player.onStart(function () { return p.triggerCallback('start'); });
            }
            player.onDone(function () { return _this.finish(); });
            player.onDestroy(function () { return _this.destroy(); });
        };
        TransitionAnimationPlayer.prototype._queueEvent = function (name, callback) {
            getOrSetAsInMap(this._queuedCallbacks, name, []).push(callback);
        };
        TransitionAnimationPlayer.prototype.onDone = function (fn) {
            if (this.queued) {
                this._queueEvent('done', fn);
            }
            this._player.onDone(fn);
        };
        TransitionAnimationPlayer.prototype.onStart = function (fn) {
            if (this.queued) {
                this._queueEvent('start', fn);
            }
            this._player.onStart(fn);
        };
        TransitionAnimationPlayer.prototype.onDestroy = function (fn) {
            if (this.queued) {
                this._queueEvent('destroy', fn);
            }
            this._player.onDestroy(fn);
        };
        TransitionAnimationPlayer.prototype.init = function () { this._player.init(); };
        TransitionAnimationPlayer.prototype.hasStarted = function () { return this.queued ? false : this._player.hasStarted(); };
        TransitionAnimationPlayer.prototype.play = function () { !this.queued && this._player.play(); };
        TransitionAnimationPlayer.prototype.pause = function () { !this.queued && this._player.pause(); };
        TransitionAnimationPlayer.prototype.restart = function () { !this.queued && this._player.restart(); };
        TransitionAnimationPlayer.prototype.finish = function () { this._player.finish(); };
        TransitionAnimationPlayer.prototype.destroy = function () {
            this.destroyed = true;
            this._player.destroy();
        };
        TransitionAnimationPlayer.prototype.reset = function () { !this.queued && this._player.reset(); };
        TransitionAnimationPlayer.prototype.setPosition = function (p) {
            if (!this.queued) {
                this._player.setPosition(p);
            }
        };
        TransitionAnimationPlayer.prototype.getPosition = function () { return this.queued ? 0 : this._player.getPosition(); };
        /** @internal */
        TransitionAnimationPlayer.prototype.triggerCallback = function (phaseName) {
            var p = this._player;
            if (p.triggerCallback) {
                p.triggerCallback(phaseName);
            }
        };
        return TransitionAnimationPlayer;
    }());
    function deleteOrUnsetInMap(map, key, value) {
        var currentValues;
        if (map instanceof Map) {
            currentValues = map.get(key);
            if (currentValues) {
                if (currentValues.length) {
                    var index = currentValues.indexOf(value);
                    currentValues.splice(index, 1);
                }
                if (currentValues.length == 0) {
                    map.delete(key);
                }
            }
        }
        else {
            currentValues = map[key];
            if (currentValues) {
                if (currentValues.length) {
                    var index = currentValues.indexOf(value);
                    currentValues.splice(index, 1);
                }
                if (currentValues.length == 0) {
                    delete map[key];
                }
            }
        }
        return currentValues;
    }
    function normalizeTriggerValue(value) {
        // we use `!= null` here because it's the most simple
        // way to test against a "falsy" value without mixing
        // in empty strings or a zero value. DO NOT OPTIMIZE.
        return value != null ? value : null;
    }
    function isElementNode(node) {
        return node && node['nodeType'] === 1;
    }
    function isTriggerEventValid(eventName) {
        return eventName == 'start' || eventName == 'done';
    }
    function cloakElement(element, value) {
        var oldValue = element.style.display;
        element.style.display = value != null ? value : 'none';
        return oldValue;
    }
    function cloakAndComputeStyles(valuesMap, driver, elements, elementPropsMap, defaultStyle) {
        var cloakVals = [];
        elements.forEach(function (element) { return cloakVals.push(cloakElement(element)); });
        var failedElements = [];
        elementPropsMap.forEach(function (props, element) {
            var styles = {};
            props.forEach(function (prop) {
                var value = styles[prop] = driver.computeStyle(element, prop, defaultStyle);
                // there is no easy way to detect this because a sub element could be removed
                // by a parent animation element being detached.
                if (!value || value.length == 0) {
                    element[REMOVAL_FLAG] = NULL_REMOVED_QUERIED_STATE;
                    failedElements.push(element);
                }
            });
            valuesMap.set(element, styles);
        });
        // we use a index variable here since Set.forEach(a, i) does not return
        // an index value for the closure (but instead just the value)
        var i = 0;
        elements.forEach(function (element) { return cloakElement(element, cloakVals[i++]); });
        return failedElements;
    }
    /*
    Since the Angular renderer code will return a collection of inserted
    nodes in all areas of a DOM tree, it's up to this algorithm to figure
    out which nodes are roots for each animation @trigger.

    By placing each inserted node into a Set and traversing upwards, it
    is possible to find the @trigger elements and well any direct *star
    insertion nodes, if a @trigger root is found then the enter element
    is placed into the Map[@trigger] spot.
     */
    function buildRootMap(roots, nodes) {
        var rootMap = new Map();
        roots.forEach(function (root) { return rootMap.set(root, []); });
        if (nodes.length == 0)
            return rootMap;
        var NULL_NODE = 1;
        var nodeSet = new Set(nodes);
        var localRootMap = new Map();
        function getRoot(node) {
            if (!node)
                return NULL_NODE;
            var root = localRootMap.get(node);
            if (root)
                return root;
            var parent = node.parentNode;
            if (rootMap.has(parent)) { // ngIf inside @trigger
                root = parent;
            }
            else if (nodeSet.has(parent)) { // ngIf inside ngIf
                root = NULL_NODE;
            }
            else { // recurse upwards
                root = getRoot(parent);
            }
            localRootMap.set(node, root);
            return root;
        }
        nodes.forEach(function (node) {
            var root = getRoot(node);
            if (root !== NULL_NODE) {
                rootMap.get(root).push(node);
            }
        });
        return rootMap;
    }
    var CLASSES_CACHE_KEY = '$$classes';
    function addClass(element, className) {
        if (element.classList) {
            element.classList.add(className);
        }
        else {
            var classes = element[CLASSES_CACHE_KEY];
            if (!classes) {
                classes = element[CLASSES_CACHE_KEY] = {};
            }
            classes[className] = true;
        }
    }
    function removeClass(element, className) {
        if (element.classList) {
            element.classList.remove(className);
        }
        else {
            var classes = element[CLASSES_CACHE_KEY];
            if (classes) {
                delete classes[className];
            }
        }
    }
    function removeNodesAfterAnimationDone(engine, element, players) {
        optimizeGroupPlayer(players).onDone(function () { return engine.processLeaveNode(element); });
    }
    function flattenGroupPlayers(players) {
        var finalPlayers = [];
        _flattenGroupPlayersRecur(players, finalPlayers);
        return finalPlayers;
    }
    function _flattenGroupPlayersRecur(players, finalPlayers) {
        for (var i = 0; i < players.length; i++) {
            var player = players[i];
            if (player instanceof animations.ɵAnimationGroupPlayer) {
                _flattenGroupPlayersRecur(player.players, finalPlayers);
            }
            else {
                finalPlayers.push(player);
            }
        }
    }
    function objEquals(a, b) {
        var k1 = Object.keys(a);
        var k2 = Object.keys(b);
        if (k1.length != k2.length)
            return false;
        for (var i = 0; i < k1.length; i++) {
            var prop = k1[i];
            if (!b.hasOwnProperty(prop) || a[prop] !== b[prop])
                return false;
        }
        return true;
    }
    function replacePostStylesAsPre(element, allPreStyleElements, allPostStyleElements) {
        var postEntry = allPostStyleElements.get(element);
        if (!postEntry)
            return false;
        var preEntry = allPreStyleElements.get(element);
        if (preEntry) {
            postEntry.forEach(function (data) { return preEntry.add(data); });
        }
        else {
            allPreStyleElements.set(element, postEntry);
        }
        allPostStyleElements.delete(element);
        return true;
    }

    var AnimationEngine = /** @class */ (function () {
        function AnimationEngine(bodyNode, _driver, normalizer) {
            var _this = this;
            this.bodyNode = bodyNode;
            this._driver = _driver;
            this._triggerCache = {};
            // this method is designed to be overridden by the code that uses this engine
            this.onRemovalComplete = function (element, context) { };
            this._transitionEngine = new TransitionAnimationEngine(bodyNode, _driver, normalizer);
            this._timelineEngine = new TimelineAnimationEngine(bodyNode, _driver, normalizer);
            this._transitionEngine.onRemovalComplete = function (element, context) {
                return _this.onRemovalComplete(element, context);
            };
        }
        AnimationEngine.prototype.registerTrigger = function (componentId, namespaceId, hostElement, name, metadata) {
            var cacheKey = componentId + '-' + name;
            var trigger = this._triggerCache[cacheKey];
            if (!trigger) {
                var errors = [];
                var ast = buildAnimationAst(this._driver, metadata, errors);
                if (errors.length) {
                    throw new Error("The animation trigger \"" + name + "\" has failed to build due to the following errors:\n - " + errors.join("\n - "));
                }
                trigger = buildTrigger(name, ast);
                this._triggerCache[cacheKey] = trigger;
            }
            this._transitionEngine.registerTrigger(namespaceId, name, trigger);
        };
        AnimationEngine.prototype.register = function (namespaceId, hostElement) {
            this._transitionEngine.register(namespaceId, hostElement);
        };
        AnimationEngine.prototype.destroy = function (namespaceId, context) {
            this._transitionEngine.destroy(namespaceId, context);
        };
        AnimationEngine.prototype.onInsert = function (namespaceId, element, parent, insertBefore) {
            this._transitionEngine.insertNode(namespaceId, element, parent, insertBefore);
        };
        AnimationEngine.prototype.onRemove = function (namespaceId, element, context) {
            this._transitionEngine.removeNode(namespaceId, element, context);
        };
        AnimationEngine.prototype.disableAnimations = function (element, disable) {
            this._transitionEngine.markElementAsDisabled(element, disable);
        };
        AnimationEngine.prototype.process = function (namespaceId, element, property, value) {
            if (property.charAt(0) == '@') {
                var _a = __read(parseTimelineCommand(property), 2), id = _a[0], action = _a[1];
                var args = value;
                this._timelineEngine.command(id, element, action, args);
            }
            else {
                this._transitionEngine.trigger(namespaceId, element, property, value);
            }
        };
        AnimationEngine.prototype.listen = function (namespaceId, element, eventName, eventPhase, callback) {
            // @@listen
            if (eventName.charAt(0) == '@') {
                var _a = __read(parseTimelineCommand(eventName), 2), id = _a[0], action = _a[1];
                return this._timelineEngine.listen(id, element, action, callback);
            }
            return this._transitionEngine.listen(namespaceId, element, eventName, eventPhase, callback);
        };
        AnimationEngine.prototype.flush = function (microtaskId) {
            if (microtaskId === void 0) { microtaskId = -1; }
            this._transitionEngine.flush(microtaskId);
        };
        Object.defineProperty(AnimationEngine.prototype, "players", {
            get: function () {
                return this._transitionEngine.players
                    .concat(this._timelineEngine.players);
            },
            enumerable: true,
            configurable: true
        });
        AnimationEngine.prototype.whenRenderingDone = function () { return this._transitionEngine.whenRenderingDone(); };
        return AnimationEngine;
    }());

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var ELAPSED_TIME_MAX_DECIMAL_PLACES = 3;
    var ANIMATION_PROP = 'animation';
    var ANIMATIONEND_EVENT = 'animationend';
    var ONE_SECOND$1 = 1000;
    var ElementAnimationStyleHandler = /** @class */ (function () {
        function ElementAnimationStyleHandler(_element, _name, _duration, _delay, _easing, _fillMode, _onDoneFn) {
            var _this = this;
            this._element = _element;
            this._name = _name;
            this._duration = _duration;
            this._delay = _delay;
            this._easing = _easing;
            this._fillMode = _fillMode;
            this._onDoneFn = _onDoneFn;
            this._finished = false;
            this._destroyed = false;
            this._startTime = 0;
            this._position = 0;
            this._eventFn = function (e) { return _this._handleCallback(e); };
        }
        ElementAnimationStyleHandler.prototype.apply = function () {
            applyKeyframeAnimation(this._element, this._duration + "ms " + this._easing + " " + this._delay + "ms 1 normal " + this._fillMode + " " + this._name);
            addRemoveAnimationEvent(this._element, this._eventFn, false);
            this._startTime = Date.now();
        };
        ElementAnimationStyleHandler.prototype.pause = function () { playPauseAnimation(this._element, this._name, 'paused'); };
        ElementAnimationStyleHandler.prototype.resume = function () { playPauseAnimation(this._element, this._name, 'running'); };
        ElementAnimationStyleHandler.prototype.setPosition = function (position) {
            var index = findIndexForAnimation(this._element, this._name);
            this._position = position * this._duration;
            setAnimationStyle(this._element, 'Delay', "-" + this._position + "ms", index);
        };
        ElementAnimationStyleHandler.prototype.getPosition = function () { return this._position; };
        ElementAnimationStyleHandler.prototype._handleCallback = function (event) {
            var timestamp = event._ngTestManualTimestamp || Date.now();
            var elapsedTime = parseFloat(event.elapsedTime.toFixed(ELAPSED_TIME_MAX_DECIMAL_PLACES)) * ONE_SECOND$1;
            if (event.animationName == this._name &&
                Math.max(timestamp - this._startTime, 0) >= this._delay && elapsedTime >= this._duration) {
                this.finish();
            }
        };
        ElementAnimationStyleHandler.prototype.finish = function () {
            if (this._finished)
                return;
            this._finished = true;
            this._onDoneFn();
            addRemoveAnimationEvent(this._element, this._eventFn, true);
        };
        ElementAnimationStyleHandler.prototype.destroy = function () {
            if (this._destroyed)
                return;
            this._destroyed = true;
            this.finish();
            removeKeyframeAnimation(this._element, this._name);
        };
        return ElementAnimationStyleHandler;
    }());
    function playPauseAnimation(element, name, status) {
        var index = findIndexForAnimation(element, name);
        setAnimationStyle(element, 'PlayState', status, index);
    }
    function applyKeyframeAnimation(element, value) {
        var anim = getAnimationStyle(element, '').trim();
        var index = 0;
        if (anim.length) {
            index = countChars(anim, ',') + 1;
            value = anim + ", " + value;
        }
        setAnimationStyle(element, '', value);
        return index;
    }
    function removeKeyframeAnimation(element, name) {
        var anim = getAnimationStyle(element, '');
        var tokens = anim.split(',');
        var index = findMatchingTokenIndex(tokens, name);
        if (index >= 0) {
            tokens.splice(index, 1);
            var newValue = tokens.join(',');
            setAnimationStyle(element, '', newValue);
        }
    }
    function findIndexForAnimation(element, value) {
        var anim = getAnimationStyle(element, '');
        if (anim.indexOf(',') > 0) {
            var tokens = anim.split(',');
            return findMatchingTokenIndex(tokens, value);
        }
        return findMatchingTokenIndex([anim], value);
    }
    function findMatchingTokenIndex(tokens, searchToken) {
        for (var i = 0; i < tokens.length; i++) {
            if (tokens[i].indexOf(searchToken) >= 0) {
                return i;
            }
        }
        return -1;
    }
    function addRemoveAnimationEvent(element, fn, doRemove) {
        doRemove ? element.removeEventListener(ANIMATIONEND_EVENT, fn) :
            element.addEventListener(ANIMATIONEND_EVENT, fn);
    }
    function setAnimationStyle(element, name, value, index) {
        var prop = ANIMATION_PROP + name;
        if (index != null) {
            var oldValue = element.style[prop];
            if (oldValue.length) {
                var tokens = oldValue.split(',');
                tokens[index] = value;
                value = tokens.join(',');
            }
        }
        element.style[prop] = value;
    }
    function getAnimationStyle(element, name) {
        return element.style[ANIMATION_PROP + name];
    }
    function countChars(value, char) {
        var count = 0;
        for (var i = 0; i < value.length; i++) {
            var c = value.charAt(i);
            if (c === char)
                count++;
        }
        return count;
    }

    var DEFAULT_FILL_MODE = 'forwards';
    var DEFAULT_EASING = 'linear';
    var CssKeyframesPlayer = /** @class */ (function () {
        function CssKeyframesPlayer(element, keyframes, animationName, _duration, _delay, easing, _finalStyles) {
            this.element = element;
            this.keyframes = keyframes;
            this.animationName = animationName;
            this._duration = _duration;
            this._delay = _delay;
            this._finalStyles = _finalStyles;
            this._onDoneFns = [];
            this._onStartFns = [];
            this._onDestroyFns = [];
            this._started = false;
            this.currentSnapshot = {};
            this._state = 0;
            this.easing = easing || DEFAULT_EASING;
            this.totalTime = _duration + _delay;
            this._buildStyler();
        }
        CssKeyframesPlayer.prototype.onStart = function (fn) { this._onStartFns.push(fn); };
        CssKeyframesPlayer.prototype.onDone = function (fn) { this._onDoneFns.push(fn); };
        CssKeyframesPlayer.prototype.onDestroy = function (fn) { this._onDestroyFns.push(fn); };
        CssKeyframesPlayer.prototype.destroy = function () {
            this.init();
            if (this._state >= 4 /* DESTROYED */)
                return;
            this._state = 4 /* DESTROYED */;
            this._styler.destroy();
            this._flushStartFns();
            this._flushDoneFns();
            this._onDestroyFns.forEach(function (fn) { return fn(); });
            this._onDestroyFns = [];
        };
        CssKeyframesPlayer.prototype._flushDoneFns = function () {
            this._onDoneFns.forEach(function (fn) { return fn(); });
            this._onDoneFns = [];
        };
        CssKeyframesPlayer.prototype._flushStartFns = function () {
            this._onStartFns.forEach(function (fn) { return fn(); });
            this._onStartFns = [];
        };
        CssKeyframesPlayer.prototype.finish = function () {
            this.init();
            if (this._state >= 3 /* FINISHED */)
                return;
            this._state = 3 /* FINISHED */;
            this._styler.finish();
            this._flushStartFns();
            this._flushDoneFns();
        };
        CssKeyframesPlayer.prototype.setPosition = function (value) { this._styler.setPosition(value); };
        CssKeyframesPlayer.prototype.getPosition = function () { return this._styler.getPosition(); };
        CssKeyframesPlayer.prototype.hasStarted = function () { return this._state >= 2 /* STARTED */; };
        CssKeyframesPlayer.prototype.init = function () {
            if (this._state >= 1 /* INITIALIZED */)
                return;
            this._state = 1 /* INITIALIZED */;
            var elm = this.element;
            this._styler.apply();
            if (this._delay) {
                this._styler.pause();
            }
        };
        CssKeyframesPlayer.prototype.play = function () {
            this.init();
            if (!this.hasStarted()) {
                this._flushStartFns();
                this._state = 2 /* STARTED */;
            }
            this._styler.resume();
        };
        CssKeyframesPlayer.prototype.pause = function () {
            this.init();
            this._styler.pause();
        };
        CssKeyframesPlayer.prototype.restart = function () {
            this.reset();
            this.play();
        };
        CssKeyframesPlayer.prototype.reset = function () {
            this._styler.destroy();
            this._buildStyler();
            this._styler.apply();
        };
        CssKeyframesPlayer.prototype._buildStyler = function () {
            var _this = this;
            this._styler = new ElementAnimationStyleHandler(this.element, this.animationName, this._duration, this._delay, this.easing, DEFAULT_FILL_MODE, function () { return _this.finish(); });
        };
        /** @internal */
        CssKeyframesPlayer.prototype.triggerCallback = function (phaseName) {
            var methods = phaseName == 'start' ? this._onStartFns : this._onDoneFns;
            methods.forEach(function (fn) { return fn(); });
            methods.length = 0;
        };
        CssKeyframesPlayer.prototype.beforeDestroy = function () {
            var _this = this;
            this.init();
            var styles = {};
            if (this.hasStarted()) {
                var finished_1 = this._state >= 3 /* FINISHED */;
                Object.keys(this._finalStyles).forEach(function (prop) {
                    if (prop != 'offset') {
                        styles[prop] = finished_1 ? _this._finalStyles[prop] : computeStyle(_this.element, prop);
                    }
                });
            }
            this.currentSnapshot = styles;
        };
        return CssKeyframesPlayer;
    }());

    var DirectStylePlayer = /** @class */ (function (_super) {
        __extends(DirectStylePlayer, _super);
        function DirectStylePlayer(element, styles) {
            var _this = _super.call(this) || this;
            _this.element = element;
            _this._startingStyles = {};
            _this.__initialized = false;
            _this._styles = hypenatePropsObject(styles);
            return _this;
        }
        DirectStylePlayer.prototype.init = function () {
            var _this = this;
            if (this.__initialized || !this._startingStyles)
                return;
            this.__initialized = true;
            Object.keys(this._styles).forEach(function (prop) {
                _this._startingStyles[prop] = _this.element.style[prop];
            });
            _super.prototype.init.call(this);
        };
        DirectStylePlayer.prototype.play = function () {
            var _this = this;
            if (!this._startingStyles)
                return;
            this.init();
            Object.keys(this._styles)
                .forEach(function (prop) { return _this.element.style.setProperty(prop, _this._styles[prop]); });
            _super.prototype.play.call(this);
        };
        DirectStylePlayer.prototype.destroy = function () {
            var _this = this;
            if (!this._startingStyles)
                return;
            Object.keys(this._startingStyles).forEach(function (prop) {
                var value = _this._startingStyles[prop];
                if (value) {
                    _this.element.style.setProperty(prop, value);
                }
                else {
                    _this.element.style.removeProperty(prop);
                }
            });
            this._startingStyles = null;
            _super.prototype.destroy.call(this);
        };
        return DirectStylePlayer;
    }(animations.NoopAnimationPlayer));

    var KEYFRAMES_NAME_PREFIX = 'gen_css_kf_';
    var TAB_SPACE = ' ';
    var CssKeyframesDriver = /** @class */ (function () {
        function CssKeyframesDriver() {
            this._count = 0;
            this._head = document.querySelector('head');
            this._warningIssued = false;
        }
        CssKeyframesDriver.prototype.validateStyleProperty = function (prop) { return validateStyleProperty(prop); };
        CssKeyframesDriver.prototype.matchesElement = function (element, selector) {
            return matchesElement(element, selector);
        };
        CssKeyframesDriver.prototype.containsElement = function (elm1, elm2) { return containsElement(elm1, elm2); };
        CssKeyframesDriver.prototype.query = function (element, selector, multi) {
            return invokeQuery(element, selector, multi);
        };
        CssKeyframesDriver.prototype.computeStyle = function (element, prop, defaultValue) {
            return window.getComputedStyle(element)[prop];
        };
        CssKeyframesDriver.prototype.buildKeyframeElement = function (element, name, keyframes) {
            keyframes = keyframes.map(function (kf) { return hypenatePropsObject(kf); });
            var keyframeStr = "@keyframes " + name + " {\n";
            var tab = '';
            keyframes.forEach(function (kf) {
                tab = TAB_SPACE;
                var offset = parseFloat(kf.offset);
                keyframeStr += "" + tab + offset * 100 + "% {\n";
                tab += TAB_SPACE;
                Object.keys(kf).forEach(function (prop) {
                    var value = kf[prop];
                    switch (prop) {
                        case 'offset':
                            return;
                        case 'easing':
                            if (value) {
                                keyframeStr += tab + "animation-timing-function: " + value + ";\n";
                            }
                            return;
                        default:
                            keyframeStr += "" + tab + prop + ": " + value + ";\n";
                            return;
                    }
                });
                keyframeStr += tab + "}\n";
            });
            keyframeStr += "}\n";
            var kfElm = document.createElement('style');
            kfElm.innerHTML = keyframeStr;
            return kfElm;
        };
        CssKeyframesDriver.prototype.animate = function (element, keyframes, duration, delay, easing, previousPlayers, scrubberAccessRequested) {
            if (previousPlayers === void 0) { previousPlayers = []; }
            if (scrubberAccessRequested) {
                this._notifyFaultyScrubber();
            }
            var previousCssKeyframePlayers = previousPlayers.filter(function (player) { return player instanceof CssKeyframesPlayer; });
            var previousStyles = {};
            if (allowPreviousPlayerStylesMerge(duration, delay)) {
                previousCssKeyframePlayers.forEach(function (player) {
                    var styles = player.currentSnapshot;
                    Object.keys(styles).forEach(function (prop) { return previousStyles[prop] = styles[prop]; });
                });
            }
            keyframes = balancePreviousStylesIntoKeyframes(element, keyframes, previousStyles);
            var finalStyles = flattenKeyframesIntoStyles(keyframes);
            // if there is no animation then there is no point in applying
            // styles and waiting for an event to get fired. This causes lag.
            // It's better to just directly apply the styles to the element
            // via the direct styling animation player.
            if (duration == 0) {
                return new DirectStylePlayer(element, finalStyles);
            }
            var animationName = "" + KEYFRAMES_NAME_PREFIX + this._count++;
            var kfElm = this.buildKeyframeElement(element, animationName, keyframes);
            document.querySelector('head').appendChild(kfElm);
            var player = new CssKeyframesPlayer(element, keyframes, animationName, duration, delay, easing, finalStyles);
            player.onDestroy(function () { return removeElement(kfElm); });
            return player;
        };
        CssKeyframesDriver.prototype._notifyFaultyScrubber = function () {
            if (!this._warningIssued) {
                console.warn('@angular/animations: please load the web-animations.js polyfill to allow programmatic access...\n', '  visit http://bit.ly/IWukam to learn more about using the web-animation-js polyfill.');
                this._warningIssued = true;
            }
        };
        return CssKeyframesDriver;
    }());
    function flattenKeyframesIntoStyles(keyframes) {
        var flatKeyframes = {};
        if (keyframes) {
            var kfs = Array.isArray(keyframes) ? keyframes : [keyframes];
            kfs.forEach(function (kf) {
                Object.keys(kf).forEach(function (prop) {
                    if (prop == 'offset' || prop == 'easing')
                        return;
                    flatKeyframes[prop] = kf[prop];
                });
            });
        }
        return flatKeyframes;
    }
    function removeElement(node) {
        node.parentNode.removeChild(node);
    }

    var WebAnimationsPlayer = /** @class */ (function () {
        function WebAnimationsPlayer(element, keyframes, options) {
            this.element = element;
            this.keyframes = keyframes;
            this.options = options;
            this._onDoneFns = [];
            this._onStartFns = [];
            this._onDestroyFns = [];
            this._initialized = false;
            this._finished = false;
            this._started = false;
            this._destroyed = false;
            this.time = 0;
            this.parentPlayer = null;
            this.currentSnapshot = {};
            this._duration = options['duration'];
            this._delay = options['delay'] || 0;
            this.time = this._duration + this._delay;
        }
        WebAnimationsPlayer.prototype._onFinish = function () {
            if (!this._finished) {
                this._finished = true;
                this._onDoneFns.forEach(function (fn) { return fn(); });
                this._onDoneFns = [];
            }
        };
        WebAnimationsPlayer.prototype.init = function () {
            this._buildPlayer();
            this._preparePlayerBeforeStart();
        };
        WebAnimationsPlayer.prototype._buildPlayer = function () {
            var _this = this;
            if (this._initialized)
                return;
            this._initialized = true;
            var keyframes = this.keyframes;
            this.domPlayer =
                this._triggerWebAnimation(this.element, keyframes, this.options);
            this._finalKeyframe = keyframes.length ? keyframes[keyframes.length - 1] : {};
            this.domPlayer.addEventListener('finish', function () { return _this._onFinish(); });
        };
        WebAnimationsPlayer.prototype._preparePlayerBeforeStart = function () {
            // this is required so that the player doesn't start to animate right away
            if (this._delay) {
                this._resetDomPlayerState();
            }
            else {
                this.domPlayer.pause();
            }
        };
        /** @internal */
        WebAnimationsPlayer.prototype._triggerWebAnimation = function (element, keyframes, options) {
            // jscompiler doesn't seem to know animate is a native property because it's not fully
            // supported yet across common browsers (we polyfill it for Edge/Safari) [CL #143630929]
            return element['animate'](keyframes, options);
        };
        WebAnimationsPlayer.prototype.onStart = function (fn) { this._onStartFns.push(fn); };
        WebAnimationsPlayer.prototype.onDone = function (fn) { this._onDoneFns.push(fn); };
        WebAnimationsPlayer.prototype.onDestroy = function (fn) { this._onDestroyFns.push(fn); };
        WebAnimationsPlayer.prototype.play = function () {
            this._buildPlayer();
            if (!this.hasStarted()) {
                this._onStartFns.forEach(function (fn) { return fn(); });
                this._onStartFns = [];
                this._started = true;
            }
            this.domPlayer.play();
        };
        WebAnimationsPlayer.prototype.pause = function () {
            this.init();
            this.domPlayer.pause();
        };
        WebAnimationsPlayer.prototype.finish = function () {
            this.init();
            this._onFinish();
            this.domPlayer.finish();
        };
        WebAnimationsPlayer.prototype.reset = function () {
            this._resetDomPlayerState();
            this._destroyed = false;
            this._finished = false;
            this._started = false;
        };
        WebAnimationsPlayer.prototype._resetDomPlayerState = function () {
            if (this.domPlayer) {
                this.domPlayer.cancel();
            }
        };
        WebAnimationsPlayer.prototype.restart = function () {
            this.reset();
            this.play();
        };
        WebAnimationsPlayer.prototype.hasStarted = function () { return this._started; };
        WebAnimationsPlayer.prototype.destroy = function () {
            if (!this._destroyed) {
                this._destroyed = true;
                this._resetDomPlayerState();
                this._onFinish();
                this._onDestroyFns.forEach(function (fn) { return fn(); });
                this._onDestroyFns = [];
            }
        };
        WebAnimationsPlayer.prototype.setPosition = function (p) { this.domPlayer.currentTime = p * this.time; };
        WebAnimationsPlayer.prototype.getPosition = function () { return this.domPlayer.currentTime / this.time; };
        Object.defineProperty(WebAnimationsPlayer.prototype, "totalTime", {
            get: function () { return this._delay + this._duration; },
            enumerable: true,
            configurable: true
        });
        WebAnimationsPlayer.prototype.beforeDestroy = function () {
            var _this = this;
            var styles = {};
            if (this.hasStarted()) {
                Object.keys(this._finalKeyframe).forEach(function (prop) {
                    if (prop != 'offset') {
                        styles[prop] =
                            _this._finished ? _this._finalKeyframe[prop] : computeStyle(_this.element, prop);
                    }
                });
            }
            this.currentSnapshot = styles;
        };
        /** @internal */
        WebAnimationsPlayer.prototype.triggerCallback = function (phaseName) {
            var methods = phaseName == 'start' ? this._onStartFns : this._onDoneFns;
            methods.forEach(function (fn) { return fn(); });
            methods.length = 0;
        };
        return WebAnimationsPlayer;
    }());

    var WebAnimationsDriver = /** @class */ (function () {
        function WebAnimationsDriver() {
            this._isNativeImpl = /\{\s*\[native\s+code\]\s*\}/.test(getElementAnimateFn().toString());
            this._cssKeyframesDriver = new CssKeyframesDriver();
        }
        WebAnimationsDriver.prototype.validateStyleProperty = function (prop) { return validateStyleProperty(prop); };
        WebAnimationsDriver.prototype.matchesElement = function (element, selector) {
            return matchesElement(element, selector);
        };
        WebAnimationsDriver.prototype.containsElement = function (elm1, elm2) { return containsElement(elm1, elm2); };
        WebAnimationsDriver.prototype.query = function (element, selector, multi) {
            return invokeQuery(element, selector, multi);
        };
        WebAnimationsDriver.prototype.computeStyle = function (element, prop, defaultValue) {
            return window.getComputedStyle(element)[prop];
        };
        WebAnimationsDriver.prototype.overrideWebAnimationsSupport = function (supported) { this._isNativeImpl = supported; };
        WebAnimationsDriver.prototype.animate = function (element, keyframes, duration, delay, easing, previousPlayers, scrubberAccessRequested) {
            if (previousPlayers === void 0) { previousPlayers = []; }
            var useKeyframes = !scrubberAccessRequested && !this._isNativeImpl;
            if (useKeyframes) {
                return this._cssKeyframesDriver.animate(element, keyframes, duration, delay, easing, previousPlayers);
            }
            var fill = delay == 0 ? 'both' : 'forwards';
            var playerOptions = { duration: duration, delay: delay, fill: fill };
            // we check for this to avoid having a null|undefined value be present
            // for the easing (which results in an error for certain browsers #9752)
            if (easing) {
                playerOptions['easing'] = easing;
            }
            var previousStyles = {};
            var previousWebAnimationPlayers = previousPlayers.filter(function (player) { return player instanceof WebAnimationsPlayer; });
            if (allowPreviousPlayerStylesMerge(duration, delay)) {
                previousWebAnimationPlayers.forEach(function (player) {
                    var styles = player.currentSnapshot;
                    Object.keys(styles).forEach(function (prop) { return previousStyles[prop] = styles[prop]; });
                });
            }
            keyframes = keyframes.map(function (styles) { return copyStyles(styles, false); });
            keyframes = balancePreviousStylesIntoKeyframes(element, keyframes, previousStyles);
            return new WebAnimationsPlayer(element, keyframes, playerOptions);
        };
        return WebAnimationsDriver;
    }());
    function supportsWebAnimations() {
        return typeof getElementAnimateFn() === 'function';
    }
    function getElementAnimateFn() {
        return (isBrowser() && Element.prototype['animate']) || {};
    }

    /**
     * @license Angular v6.1.0
     * (c) 2010-2018 Google, Inc. https://angular.io/
     * License: MIT
     */

    var BrowserAnimationBuilder = /** @class */ (function (_super) {
        __extends(BrowserAnimationBuilder, _super);
        function BrowserAnimationBuilder(rootRenderer, doc) {
            var _this = _super.call(this) || this;
            _this._nextAnimationId = 0;
            var typeData = {
                id: '0',
                encapsulation: core.ViewEncapsulation.None,
                styles: [],
                data: { animation: [] }
            };
            _this._renderer = rootRenderer.createRenderer(doc.body, typeData);
            return _this;
        }
        BrowserAnimationBuilder.prototype.build = function (animation) {
            var id = this._nextAnimationId.toString();
            this._nextAnimationId++;
            var entry = Array.isArray(animation) ? animations.sequence(animation) : animation;
            issueAnimationCommand(this._renderer, null, id, 'register', [entry]);
            return new BrowserAnimationFactory(id, this._renderer);
        };
        BrowserAnimationBuilder.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        BrowserAnimationBuilder.ctorParameters = function () { return [
            { type: core.RendererFactory2 },
            { type: undefined, decorators: [{ type: core.Inject, args: [platformBrowser.DOCUMENT,] }] }
        ]; };
        return BrowserAnimationBuilder;
    }(animations.AnimationBuilder));
    var BrowserAnimationFactory = /** @class */ (function (_super) {
        __extends(BrowserAnimationFactory, _super);
        function BrowserAnimationFactory(_id, _renderer) {
            var _this = _super.call(this) || this;
            _this._id = _id;
            _this._renderer = _renderer;
            return _this;
        }
        BrowserAnimationFactory.prototype.create = function (element, options) {
            return new RendererAnimationPlayer(this._id, element, options || {}, this._renderer);
        };
        return BrowserAnimationFactory;
    }(animations.AnimationFactory));
    var RendererAnimationPlayer = /** @class */ (function () {
        function RendererAnimationPlayer(id, element, options, _renderer) {
            this.id = id;
            this.element = element;
            this._renderer = _renderer;
            this.parentPlayer = null;
            this._started = false;
            this.totalTime = 0;
            this._command('create', options);
        }
        RendererAnimationPlayer.prototype._listen = function (eventName, callback) {
            return this._renderer.listen(this.element, "@@" + this.id + ":" + eventName, callback);
        };
        RendererAnimationPlayer.prototype._command = function (command) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            return issueAnimationCommand(this._renderer, this.element, this.id, command, args);
        };
        RendererAnimationPlayer.prototype.onDone = function (fn) { this._listen('done', fn); };
        RendererAnimationPlayer.prototype.onStart = function (fn) { this._listen('start', fn); };
        RendererAnimationPlayer.prototype.onDestroy = function (fn) { this._listen('destroy', fn); };
        RendererAnimationPlayer.prototype.init = function () { this._command('init'); };
        RendererAnimationPlayer.prototype.hasStarted = function () { return this._started; };
        RendererAnimationPlayer.prototype.play = function () {
            this._command('play');
            this._started = true;
        };
        RendererAnimationPlayer.prototype.pause = function () { this._command('pause'); };
        RendererAnimationPlayer.prototype.restart = function () { this._command('restart'); };
        RendererAnimationPlayer.prototype.finish = function () { this._command('finish'); };
        RendererAnimationPlayer.prototype.destroy = function () { this._command('destroy'); };
        RendererAnimationPlayer.prototype.reset = function () { this._command('reset'); };
        RendererAnimationPlayer.prototype.setPosition = function (p) { this._command('setPosition', p); };
        RendererAnimationPlayer.prototype.getPosition = function () { return 0; };
        return RendererAnimationPlayer;
    }());
    function issueAnimationCommand(renderer, element, id, command, args) {
        return renderer.setProperty(element, "@@" + id + ":" + command, args);
    }

    var ANIMATION_PREFIX = '@';
    var DISABLE_ANIMATIONS_FLAG = '@.disabled';
    var AnimationRendererFactory = /** @class */ (function () {
        function AnimationRendererFactory(delegate, engine, _zone) {
            this.delegate = delegate;
            this.engine = engine;
            this._zone = _zone;
            this._currentId = 0;
            this._microtaskId = 1;
            this._animationCallbacksBuffer = [];
            this._rendererCache = new Map();
            this._cdRecurDepth = 0;
            this.promise = Promise.resolve(0);
            engine.onRemovalComplete = function (element, delegate) {
                // Note: if an component element has a leave animation, and the component
                // a host leave animation, the view engine will call `removeChild` for the parent
                // component renderer as well as for the child component renderer.
                // Therefore, we need to check if we already removed the element.
                if (delegate && delegate.parentNode(element)) {
                    delegate.removeChild(element.parentNode, element);
                }
            };
        }
        AnimationRendererFactory.prototype.createRenderer = function (hostElement, type) {
            var _this = this;
            var EMPTY_NAMESPACE_ID = '';
            // cache the delegates to find out which cached delegate can
            // be used by which cached renderer
            var delegate = this.delegate.createRenderer(hostElement, type);
            if (!hostElement || !type || !type.data || !type.data['animation']) {
                var renderer = this._rendererCache.get(delegate);
                if (!renderer) {
                    renderer = new BaseAnimationRenderer(EMPTY_NAMESPACE_ID, delegate, this.engine);
                    // only cache this result when the base renderer is used
                    this._rendererCache.set(delegate, renderer);
                }
                return renderer;
            }
            var componentId = type.id;
            var namespaceId = type.id + '-' + this._currentId;
            this._currentId++;
            this.engine.register(namespaceId, hostElement);
            var animationTriggers = type.data['animation'];
            animationTriggers.forEach(function (trigger) { return _this.engine.registerTrigger(componentId, namespaceId, hostElement, trigger.name, trigger); });
            return new AnimationRenderer(this, namespaceId, delegate, this.engine);
        };
        AnimationRendererFactory.prototype.begin = function () {
            this._cdRecurDepth++;
            if (this.delegate.begin) {
                this.delegate.begin();
            }
        };
        AnimationRendererFactory.prototype._scheduleCountTask = function () {
            var _this = this;
            // always use promise to schedule microtask instead of use Zone
            this.promise.then(function () { _this._microtaskId++; });
        };
        /** @internal */
        AnimationRendererFactory.prototype.scheduleListenerCallback = function (count, fn, data) {
            var _this = this;
            if (count >= 0 && count < this._microtaskId) {
                this._zone.run(function () { return fn(data); });
                return;
            }
            if (this._animationCallbacksBuffer.length == 0) {
                Promise.resolve(null).then(function () {
                    _this._zone.run(function () {
                        _this._animationCallbacksBuffer.forEach(function (tuple) {
                            var _a = __read(tuple, 2), fn = _a[0], data = _a[1];
                            fn(data);
                        });
                        _this._animationCallbacksBuffer = [];
                    });
                });
            }
            this._animationCallbacksBuffer.push([fn, data]);
        };
        AnimationRendererFactory.prototype.end = function () {
            var _this = this;
            this._cdRecurDepth--;
            // this is to prevent animations from running twice when an inner
            // component does CD when a parent component insted has inserted it
            if (this._cdRecurDepth == 0) {
                this._zone.runOutsideAngular(function () {
                    _this._scheduleCountTask();
                    _this.engine.flush(_this._microtaskId);
                });
            }
            if (this.delegate.end) {
                this.delegate.end();
            }
        };
        AnimationRendererFactory.prototype.whenRenderingDone = function () { return this.engine.whenRenderingDone(); };
        AnimationRendererFactory.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        AnimationRendererFactory.ctorParameters = function () { return [
            { type: core.RendererFactory2 },
            { type: AnimationEngine },
            { type: core.NgZone }
        ]; };
        return AnimationRendererFactory;
    }());
    var BaseAnimationRenderer = /** @class */ (function () {
        function BaseAnimationRenderer(namespaceId, delegate, engine) {
            this.namespaceId = namespaceId;
            this.delegate = delegate;
            this.engine = engine;
            this.destroyNode = this.delegate.destroyNode ? function (n) { return delegate.destroyNode(n); } : null;
        }
        Object.defineProperty(BaseAnimationRenderer.prototype, "data", {
            get: function () { return this.delegate.data; },
            enumerable: true,
            configurable: true
        });
        BaseAnimationRenderer.prototype.destroy = function () {
            this.engine.destroy(this.namespaceId, this.delegate);
            this.delegate.destroy();
        };
        BaseAnimationRenderer.prototype.createElement = function (name, namespace) {
            return this.delegate.createElement(name, namespace);
        };
        BaseAnimationRenderer.prototype.createComment = function (value) { return this.delegate.createComment(value); };
        BaseAnimationRenderer.prototype.createText = function (value) { return this.delegate.createText(value); };
        BaseAnimationRenderer.prototype.appendChild = function (parent, newChild) {
            this.delegate.appendChild(parent, newChild);
            this.engine.onInsert(this.namespaceId, newChild, parent, false);
        };
        BaseAnimationRenderer.prototype.insertBefore = function (parent, newChild, refChild) {
            this.delegate.insertBefore(parent, newChild, refChild);
            this.engine.onInsert(this.namespaceId, newChild, parent, true);
        };
        BaseAnimationRenderer.prototype.removeChild = function (parent, oldChild) {
            this.engine.onRemove(this.namespaceId, oldChild, this.delegate);
        };
        BaseAnimationRenderer.prototype.selectRootElement = function (selectorOrNode) { return this.delegate.selectRootElement(selectorOrNode); };
        BaseAnimationRenderer.prototype.parentNode = function (node) { return this.delegate.parentNode(node); };
        BaseAnimationRenderer.prototype.nextSibling = function (node) { return this.delegate.nextSibling(node); };
        BaseAnimationRenderer.prototype.setAttribute = function (el, name, value, namespace) {
            this.delegate.setAttribute(el, name, value, namespace);
        };
        BaseAnimationRenderer.prototype.removeAttribute = function (el, name, namespace) {
            this.delegate.removeAttribute(el, name, namespace);
        };
        BaseAnimationRenderer.prototype.addClass = function (el, name) { this.delegate.addClass(el, name); };
        BaseAnimationRenderer.prototype.removeClass = function (el, name) { this.delegate.removeClass(el, name); };
        BaseAnimationRenderer.prototype.setStyle = function (el, style, value, flags) {
            this.delegate.setStyle(el, style, value, flags);
        };
        BaseAnimationRenderer.prototype.removeStyle = function (el, style, flags) {
            this.delegate.removeStyle(el, style, flags);
        };
        BaseAnimationRenderer.prototype.setProperty = function (el, name, value) {
            if (name.charAt(0) == ANIMATION_PREFIX && name == DISABLE_ANIMATIONS_FLAG) {
                this.disableAnimations(el, !!value);
            }
            else {
                this.delegate.setProperty(el, name, value);
            }
        };
        BaseAnimationRenderer.prototype.setValue = function (node, value) { this.delegate.setValue(node, value); };
        BaseAnimationRenderer.prototype.listen = function (target, eventName, callback) {
            return this.delegate.listen(target, eventName, callback);
        };
        BaseAnimationRenderer.prototype.disableAnimations = function (element, value) {
            this.engine.disableAnimations(element, value);
        };
        return BaseAnimationRenderer;
    }());
    var AnimationRenderer = /** @class */ (function (_super) {
        __extends(AnimationRenderer, _super);
        function AnimationRenderer(factory, namespaceId, delegate, engine) {
            var _this = _super.call(this, namespaceId, delegate, engine) || this;
            _this.factory = factory;
            _this.namespaceId = namespaceId;
            return _this;
        }
        AnimationRenderer.prototype.setProperty = function (el, name, value) {
            if (name.charAt(0) == ANIMATION_PREFIX) {
                if (name.charAt(1) == '.' && name == DISABLE_ANIMATIONS_FLAG) {
                    value = value === undefined ? true : !!value;
                    this.disableAnimations(el, value);
                }
                else {
                    this.engine.process(this.namespaceId, el, name.substr(1), value);
                }
            }
            else {
                this.delegate.setProperty(el, name, value);
            }
        };
        AnimationRenderer.prototype.listen = function (target, eventName, callback) {
            var _this = this;
            var _a;
            if (eventName.charAt(0) == ANIMATION_PREFIX) {
                var element = resolveElementFromTarget(target);
                var name_1 = eventName.substr(1);
                var phase = '';
                // @listener.phase is for trigger animation callbacks
                // @@listener is for animation builder callbacks
                if (name_1.charAt(0) != ANIMATION_PREFIX) {
                    _a = __read(parseTriggerCallbackName(name_1), 2), name_1 = _a[0], phase = _a[1];
                }
                return this.engine.listen(this.namespaceId, element, name_1, phase, function (event) {
                    var countId = event['_data'] || -1;
                    _this.factory.scheduleListenerCallback(countId, callback, event);
                });
            }
            return this.delegate.listen(target, eventName, callback);
        };
        return AnimationRenderer;
    }(BaseAnimationRenderer));
    function resolveElementFromTarget(target) {
        switch (target) {
            case 'body':
                return document.body;
            case 'document':
                return document;
            case 'window':
                return window;
            default:
                return target;
        }
    }
    function parseTriggerCallbackName(triggerName) {
        var dotIndex = triggerName.indexOf('.');
        var trigger = triggerName.substring(0, dotIndex);
        var phase = triggerName.substr(dotIndex + 1);
        return [trigger, phase];
    }

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    var InjectableAnimationEngine = /** @class */ (function (_super) {
        __extends(InjectableAnimationEngine, _super);
        function InjectableAnimationEngine(doc, driver, normalizer) {
            return _super.call(this, doc.body, driver, normalizer) || this;
        }
        InjectableAnimationEngine.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        InjectableAnimationEngine.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
            { type: AnimationDriver },
            { type: AnimationStyleNormalizer }
        ]; };
        return InjectableAnimationEngine;
    }(AnimationEngine));
    function instantiateSupportedAnimationDriver() {
        return supportsWebAnimations() ? new WebAnimationsDriver() : new CssKeyframesDriver();
    }
    function instantiateDefaultStyleNormalizer() {
        return new WebAnimationsStyleNormalizer();
    }
    function instantiateRendererFactory(renderer, engine, zone) {
        return new AnimationRendererFactory(renderer, engine, zone);
    }
    /**
     * @experimental Animation support is experimental.
     */
    var ANIMATION_MODULE_TYPE = new core.InjectionToken('AnimationModuleType');
    var SHARED_ANIMATION_PROVIDERS = [
        { provide: animations.AnimationBuilder, useClass: BrowserAnimationBuilder },
        { provide: AnimationStyleNormalizer, useFactory: instantiateDefaultStyleNormalizer },
        { provide: AnimationEngine, useClass: InjectableAnimationEngine }, {
            provide: core.RendererFactory2,
            useFactory: instantiateRendererFactory,
            deps: [platformBrowser.ɵDomRendererFactory2, AnimationEngine, core.NgZone]
        }
    ];
    /**
     * Separate providers from the actual module so that we can do a local modification in Google3 to
     * include them in the BrowserModule.
     */
    var BROWSER_ANIMATIONS_PROVIDERS = __spread([
        { provide: AnimationDriver, useFactory: instantiateSupportedAnimationDriver },
        { provide: ANIMATION_MODULE_TYPE, useValue: 'BrowserAnimations' }
    ], SHARED_ANIMATION_PROVIDERS);
    /**
     * Separate providers from the actual module so that we can do a local modification in Google3 to
     * include them in the BrowserTestingModule.
     */
    var BROWSER_NOOP_ANIMATIONS_PROVIDERS = __spread([
        { provide: AnimationDriver, useClass: NoopAnimationDriver },
        { provide: ANIMATION_MODULE_TYPE, useValue: 'NoopAnimations' }
    ], SHARED_ANIMATION_PROVIDERS);

    /**
     * @license
     * Copyright Google Inc. All Rights Reserved.
     *
     * Use of this source code is governed by an MIT-style license that can be
     * found in the LICENSE file at https://angular.io/license
     */
    /**
     * @experimental Animation support is experimental.
     */
    var BrowserAnimationsModule = /** @class */ (function () {
        function BrowserAnimationsModule() {
        }
        BrowserAnimationsModule.decorators = [
            { type: core.NgModule, args: [{
                        exports: [platformBrowser.BrowserModule],
                        providers: BROWSER_ANIMATIONS_PROVIDERS,
                    },] }
        ];
        return BrowserAnimationsModule;
    }());

    /**
     * @title Table with editing
     */
    var AppComponentresellerlist = /** @class */function () {
        function AppComponentresellerlist() {
            this.isShowDetail = false;
        }
        AppComponentresellerlist.decorators = [{ type: core.Component, args: [{
                selector: 'my-appresellerlist',
                styles: ["\n\n  "],
                template: "\n    <mat-sidenav-container class=\"animate-show\">\n    \t<mat-sidenav-content>\n    \t<main-pane class=\"pane\"></main-pane>\n      </mat-sidenav-content>\n       <!-- <mat-sidenav #sidenav mode=\"side\" align=\"end\" [(opened)]=\"isShowDetail\">\n        <detail-pane class=\"pane\"></detail-pane>\n      </mat-sidenav>  -->\n    </mat-sidenav-container>\n    <!-- <app-categorytree>loading...sdsdsds</app-categorytree> -->\n  "
            }] }];
        return AppComponentresellerlist;
    }();
    var Globals = /** @class */function () {
        function Globals() {
            this.isEditMode = false;
            this.isFirstEditClicked = false;
            this.project = null;
            this.mode = null;
            this.token = '';
        }
        Globals.decorators = [{ type: core.Injectable }];
        return Globals;
    }();

    var FlowPartnerService = /** @class */function () {
        function FlowPartnerService() {}
        FlowPartnerService.decorators = [{ type: core.Injectable }];
        /** @nocollapse */
        FlowPartnerService.ctorParameters = function () {
            return [];
        };
        return FlowPartnerService;
    }();

    var DialogsService = /** @class */function () {
        function DialogsService() {}
        DialogsService.decorators = [{ type: core.Injectable }];
        return DialogsService;
    }();

    /**
     * @title Table with editing
     */
    var DetailPaneComponent = /** @class */function () {
        function DetailPaneComponent(appcomponent, globals, flowpartnerservice, snackBar, dialogsService) {
            this.appcomponent = appcomponent;
            this.globals = globals;
            this.flowpartnerservice = flowpartnerservice;
            this.snackBar = snackBar;
            this.dialogsService = dialogsService;
            this.displayedColumns = ['name', 'value'];
            this.dataSource = new material.MatTableDataSource();
            this.dataSourceBackUp = new material.MatTableDataSource();
        }
        DetailPaneComponent.prototype.ngOnInit = function () {
            // this.flowpartnerservice.getnewPartnerInfo().subscribe(info => {
            //   this.globals.isFirstEditClicked = false;
            //   this.updatePartnerInfo = info;
            //   this.dataSource = new MatTableDataSource(this.updatePartnerInfo.Parameters);
            // })
        };
        DetailPaneComponent.prototype.applyFilter = function (filterValue) {
            filterValue = filterValue.trim(); // Remove whitespace
            filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
            this.dataSource.filter = filterValue;
        };
        DetailPaneComponent.prototype.turnEditMode = function () {
            if (!this.globals.isFirstEditClicked) {
                this.sourcePartnerInfo = JSON.parse(JSON.stringify(this.updatePartnerInfo));
                this.globals.isFirstEditClicked = true;
            }
            this.globals.isEditMode = true;
        };
        DetailPaneComponent.prototype.saveDetail = function () {
            // this.flowpartnerservice.updatePartner(this.updatePartnerInfo).subscribe(data => {
            //   console.log(data);
            // });
            // this.openSnacBar('Successfully saved');
            // this.globals.isEditMode = false;
        };
        DetailPaneComponent.prototype.resetDetail = function () {
            // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
            // this.globals.isEditMode = false;
            // this.dialogsService
            //   .openDialog("Reset partner's parameters", 'Are you sure want to reset this partner?','Yes','No')
            //   .subscribe(res => {
            //     if (res) {
            //       // bug cannot reset
            //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
            //       // this.globals.isEditMode = false;
            //     }
            //   });
        };
        DetailPaneComponent.prototype.closeDetail = function () {
            // this.globals.isEditMode = false;
            // this.appcomponent.isShowDetail = false;
            // if(this.globals.isFirstEditClicked && this.globals.isEditMode)
            //   this.dialogsService
            //     .openDialog('Discard changes?', 'You have unsaved changes','Save','Discard changes')
            //     .subscribe(res => {
            //       if (res) {
            //         // if save
            //         // bug cannot reset
            //         this.saveDetail();
            //       }
            //       this.globals.isEditMode = false;
            //       this.appcomponent.isShowDetail = false;
            //       // bug cannot reset
            //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
            //     });
            // else {
            //   this.globals.isEditMode = false;
            //   this.appcomponent.isShowDetail = false;
            // }
        };
        DetailPaneComponent.prototype.openSnacBar = function (mess) {
            this.snackBar.open(mess, 'Close', {
                duration: 1000,
                verticalPosition: 'bottom',
                horizontalPosition: 'right'
            });
        };
        DetailPaneComponent.decorators = [{ type: core.Component, args: [{
                selector: 'detail-pane',
                styles: ["\n\n  "],
                template: "\n    \t\t<div class=\"pane-header \">\n    \t\t\t<h2>Company name: {{updatePartnerInfo.CompanyName}}</h2>\n    \t\t\t<p>Detail Values</p>\n\n    \t\t\t<div class=\"header-row\">\n    \t\t\t     <mat-form-field>\n    \t\t\t\t\t<input matInput #filter (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    \t\t\t\t\t<button mat-icon-button matSuffix aria-label=\"clear\" *ngIf=\"filter.value\" (click)=\"filter.value=''; applyFilter('');\">\n    \t\t\t        \t<mat-icon>close</mat-icon>\n    \t\t\t     \t</button>\n    \t\t\t\t</mat-form-field>\n    \t\t\t\t<div class=\"button-row\">\n    \t\t\t        <button mat-button *ngIf=!globals.isEditMode (click)=\"turnEditMode()\"><mat-icon>mode_edit</mat-icon>EDIT</button>\n\n    \t\t\t        <button mat-button *ngIf=globals.isEditMode (click)=\"saveDetail()\"><mat-icon>save</mat-icon>SAVE</button>\n    \t\t\t        <!-- <button mat-button *ngIf=globals.isEditMode (click)=\"resetDetail()\"><mat-icon>restore</mat-icon>RESET</button> -->\n    \t\t\t        <button mat-button (click)=\"closeDetail()\"><mat-icon>close</mat-icon>CLOSE</button>\n    \t\t\t      </div>\n    \t\t\t</div>\n    \t\t</div>\n    \t\t<mat-table #table [dataSource]=\"dataSource\" matSortActive=\"name\" matSortDirection=\"asc\" matSortDisableClear>\n\n    \t\t<!-- Name Column -->\n    \t\t<ng-container matColumnDef=\"name\">\n    \t\t\t<mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\n    \t\t\t<mat-cell *matCellDef=\"let element\"> {{element.Name}} </mat-cell>\n    \t\t</ng-container>\n\n    \t\t<!-- Value Column -->\n    \t\t<ng-container matColumnDef=\"value\">\n    \t\t\t<mat-header-cell *matHeaderCellDef> Value </mat-header-cell>\n    \t\t\t<mat-cell *matCellDef=\"let element\">\n    \t\t\t\t<mat-form-field floatLabel=\"never\">\n    \t\t\t\t\t<input matInput placeholder=\"\" [value]=\"element.Value\" [(ngModel)]=\"element.Value\" [disabled]=\"!globals.isEditMode\">\n    \t\t\t\t</mat-form-field>\n    \t\t\t</mat-cell>\n    \t\t</ng-container>\n\n    \t\t<mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    \t\t<mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n    \t</mat-table>\n  "
            }] }];
        /** @nocollapse */
        DetailPaneComponent.ctorParameters = function () {
            return [{ type: AppComponentresellerlist }, { type: Globals }, { type: FlowPartnerService }, { type: material.MatSnackBar }, { type: DialogsService }];
        };
        return DetailPaneComponent;
    }();
    /**  Copyright 2018 Google Inc. All Rights Reserved.
        Use of this source code is governed by an MIT-style license that
        can be found in the LICENSE file at http://angular.io/license */

    // import { ActivatedRoute } from '@angular/router'
    /**
     * @title Table retrieving data through HTTP
     */
    var MainPaneComponent = /** @class */function () {
        function MainPaneComponent(http$$1, appcomponent, globals, detailpanecomponent, flowpartnerservice, dialogservice) {
            this.http = http$$1;
            this.appcomponent = appcomponent;
            this.globals = globals;
            this.detailpanecomponent = detailpanecomponent;
            this.flowpartnerservice = flowpartnerservice;
            this.dialogservice = dialogservice;
            this.displayedColumns = ['companyname', 'test', 'state', 'country', 'datesubscribed', 'action'];
            this.dataSource = new material.MatTableDataSource();
            this.dataSourceBackUp = new material.MatTableDataSource();
            this.resultsLength = 0;
            this.isLoadingResults = false;
            this.isRateLimitReached = false;
            this.selectedRowIndex = -1;
            this.isTestUser = false;
            this.isViewAll = true;
        }
        MainPaneComponent.prototype.applyFilter = function (filterValue) {
            filterValue = filterValue.trim(); // Remove whitespace
            filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
            this.dataSource.filter = filterValue;
        };
        MainPaneComponent.prototype.selectRow = function (row) {
            // this.paginator._changePageSize(this.paginator.pageSize);
            if (this.globals.isEditMode) {
                this.detailpanecomponent.closeDetail();
            }
            this.appcomponent.isShowDetail = true;
            this.selectedRowIndex = row.Id;
            // this.flowpartnerservice.setnewPartnerInfo({
            //   Id: row.Id,
            //   CompanyName: row.CompanyName,
            //   Addresss: row.Addresss,
            //   City: row.City,
            //   State: row.State,
            //   Zipcode: row.Zipcode,
            //   Country: row.Country,
            //   WebsiteUrl: row.WebsiteUrl,
            //   DeeplinkUrl: row.DeeplinkUrl,
            //   Latitude: row.Latitude,
            //   Longitude: row.Longitude,
            //   IsConsumer: row.IsConsumer,
            //   IsSupplier: row.IsSupplier,
            //   PartnerType: row.PartnerType,
            //   BaseDomain: row.BaseDomain,
            //   Parameters: row.Parameters
            // });
        };
        MainPaneComponent.prototype.editRow = function () {
            // this.appcomponent.isShowDetail = true;
            // this.globals.isEditMode = true;
        };
        MainPaneComponent.prototype.ngAfterViewInit = function () {
            // this.activatedRoute.queryParams.subscribe(params => {
            //   if (typeof(params['token']) == 'undefined' || params['token'] == undefined
            //       || typeof(params['project']) == 'undefined' || params['project'] == undefined
            //       || typeof(params['mode']) == 'undefined' || params['mode'] == undefined) return;
            //   const requestUrl = `https://sso.tiekinetix.net/connect/accesstokenvalidation/?token=${params['token']}`;
            //   this.globals.project = params['project'];
            //   this.globals.mode = params['mode'];
            //   this.globals.token = params['token'];
            //   this.dataSource.paginator = this.paginator;
            //       this.flowDatabase = new FlowPartnerService(this.http, this.globals);
            //       this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
            //       merge(this.sort.sortChange, this.paginator.page).pipe(
            //         // startWith({}),
            //         switchMap(() => {
            //           this.isLoadingResults = true;
            //           return this.flowDatabase!.getRepoPartner();
            //         }), map(data => {
            //           this.isLoadingResults = false;
            //           this.isRateLimitReached = false;
            //           // this.resultsLength = data.total_count;
            //           return data.value;
            //         }), catchError(() => {
            //           this.isLoadingResults = false;
            //           this.isRateLimitReached = true;
            //           return observableOf([]);
            //         })).subscribe(data => {
            //           this.dataSource.data = data;
            //           this.dataSource.data.map(obj => {
            //             obj["Parameters"].sort(function(a,b) {return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0);} );
            //           });
            //       });
            //   this.http.get(requestUrl).subscribe((res: any[]) => {
            //       if (typeof(res["preferred_username"]) == 'undefined' && typeof(res["customer_id"]) == 'undefined') return;
            //   });
            // });
        };
        MainPaneComponent.decorators = [{ type: core.Component, args: [{
                selector: 'main-pane',
                styles: ["\n\n  "],
                template: "\n    <div class=\"pane-header \">\n    \t<h1>Manage Partner</h1>\n    \t<div class=\"header-row\" *ngIf=\"dataSource.data.length > 0\">\n    \t\t<mat-form-field>\n    \t\t\t<input matInput #filter (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    \t\t\t<button mat-icon-button matSuffix aria-label=\"clear\" *ngIf=\"filter.value\" (click)=\"filter.value=''; applyFilter('');\">\n              <mat-icon>close</mat-icon>\n            </button>\n    \t\t</mat-form-field>\n    \t\t<div class=\"button-row\">\n    \t\t\t<button mat-button (click)=\"exportAll(true)\"><mat-icon>launch</mat-icon>EXPORT</button>\n    \t\t\t<button mat-button (click)=\"exportAll(false)\"><mat-icon>launch</mat-icon>EXPORT TEST ACOUNTS</button>\n          <button mat-button (click)=\"viewAll(true)\" *ngIf=\"!isViewAll\"><mat-icon>view_headline</mat-icon>VIEW ALL</button>\n    \t\t\t<button mat-button (click)=\"viewAll(false)\" *ngIf=\"isViewAll\"><mat-icon>view_headline</mat-icon>VIEW PROD ACOUNTS</button>\n    \t\t</div>\n    \t</div>\n    </div>\n\n    <div class=\"circle-loader\" *ngIf=\"isLoadingResults\"></div>\n    <p class=\"error-mess animate-show\" *ngIf=\"dataSource.data.length == 0 && !isLoadingResults\"><mat-icon>error</mat-icon>Please check access token</p>\n\n    <mat-table #table [dataSource]=\"dataSource\" matSort matSortActive=\"created\" matSortDisableClear matSortDirection=\"asc\">\n\n        <ng-container matColumnDef=\"companyname\">\n          <mat-header-cell *matHeaderCellDef\n                           mat-sort-header\n                           disableClear=\"true\">\n          Company Name\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.CompanyName }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"test\">\n          <mat-header-cell *matHeaderCellDef>Test</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\"><mat-icon *ngIf=\"isTestCompany(row)\">check</mat-icon></mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"state\">\n          <mat-header-cell *matHeaderCellDef>State</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.State }}</mat-cell>\n        </ng-container>\n\n         <ng-container matColumnDef=\"country\">\n          <mat-header-cell *matHeaderCellDef>Country</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.Country }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"datesubscribed\">\n          <mat-header-cell *matHeaderCellDef>Date Subscribed</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.datesubscribed }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"action\">\n    \t\t<mat-header-cell *matHeaderCellDef></mat-header-cell>\n    \t\t<mat-cell *matCellDef=\"let row\">\n    \t\t\t<button mat-icon-button (click)=\"editRow()\">\n              <mat-icon>mode_edit</mat-icon>\n            </button>\n    \t\t</mat-cell>\n    \t</ng-container>\n    \t<mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    \t<mat-row *matRowDef=\"let row; columns: displayedColumns;\" [ngClass]=\"{'highlight': selectedRowIndex == row.Id}\" (click)=\"selectRow(row)\" class=\"animate-show\" ></mat-row>\n    </mat-table>\n\n    <mat-paginator [length]=\"resultsLength\" [pageSize]=\"14\">\n    </mat-paginator>\n  "
            }] }];
        /** @nocollapse */
        MainPaneComponent.ctorParameters = function () {
            return [{ type: HttpClient }, { type: AppComponentresellerlist }, { type: Globals }, { type: DetailPaneComponent }, { type: FlowPartnerService }, { type: DialogsService }];
        };
        MainPaneComponent.propDecorators = {
            paginator: [{ type: core.ViewChild, args: [material.MatPaginator] }],
            sort: [{ type: core.ViewChild, args: [material.MatSort] }]
        };
        return MainPaneComponent;
    }();

    var ConfirmDialogComponent = /** @class */function () {
        function ConfirmDialogComponent(dialogRef) {
            this.dialogRef = dialogRef;
        }
        ConfirmDialogComponent.decorators = [{ type: core.Component, args: [{
                selector: 'app-confirm-dialog',
                template: "\n\t\t<h2 mat-dialog-title>{{ title }}</h2>\n\t\t<mat-dialog-content>{{ message }}</mat-dialog-content>\n\t\t<br>\n\t\t<mat-dialog-actions>\n\t\t    <button type=\"button\" mat-button (click)=\"dialogRef.close(true)\">{{ btnTitle1 }}</button>\n\t\t    <button type=\"button\" mat-button mat-dialog-close (click)=\"dialogRef.close(false)\">{{ btnTitle2 }}</button>\n\t\t</mat-dialog-actions>\n  "
            }] }];
        /** @nocollapse */
        ConfirmDialogComponent.ctorParameters = function () {
            return [{ type: material.MatDialogRef }];
        };
        return ConfirmDialogComponent;
    }();

    var DialogsModule = /** @class */function () {
        function DialogsModule() {}
        DialogsModule.decorators = [{ type: core.NgModule, args: [{
                imports: [common.CommonModule, material.MatDialogModule, material.MatButtonModule],
                declarations: [ConfirmDialogComponent],
                exports: [ConfirmDialogComponent],
                entryComponents: [ConfirmDialogComponent],
                providers: [DialogsService]
            }] }];
        return DialogsModule;
    }();

    var AppRoutingModule = /** @class */function () {
        function AppRoutingModule() {}
        AppRoutingModule.decorators = [{ type: core.NgModule, args: [{}] }];
        return AppRoutingModule;
    }();

    // import { CategoryTreeModule } from "@coderrental/csp-categorytree";
    var DemoMaterialModule = /** @class */function () {
        function DemoMaterialModule() {}
        DemoMaterialModule.decorators = [{ type: core.NgModule, args: [{
                exports: [CdkTableModule, material.MatAutocompleteModule, material.MatButtonModule, material.MatButtonToggleModule, material.MatCardModule, material.MatCheckboxModule, material.MatChipsModule, material.MatStepperModule, material.MatDatepickerModule, material.MatDialogModule, material.MatDividerModule, material.MatExpansionModule, material.MatGridListModule, material.MatIconModule, material.MatInputModule, material.MatListModule, material.MatMenuModule, material.MatNativeDateModule, material.MatPaginatorModule, material.MatProgressBarModule, material.MatProgressSpinnerModule, material.MatRadioModule, material.MatRippleModule, material.MatSelectModule, material.MatSidenavModule, material.MatSliderModule, material.MatSlideToggleModule, material.MatSnackBarModule, material.MatSortModule, material.MatTableModule, material.MatTabsModule, material.MatToolbarModule, material.MatTooltipModule]
            }] }];
        return DemoMaterialModule;
    }();
    var AppModuleresellerlist = /** @class */function () {
        function AppModuleresellerlist() {}
        AppModuleresellerlist.decorators = [{ type: core.NgModule, args: [{
                imports: [platformBrowser.BrowserModule, BrowserAnimationsModule, forms.FormsModule, http.HttpModule, HttpClientModule, DemoMaterialModule, material.MatNativeDateModule, forms.ReactiveFormsModule, DialogsModule, AppRoutingModule],
                schemas: [core.CUSTOM_ELEMENTS_SCHEMA],
                entryComponents: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                declarations: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                bootstrap: [AppComponentresellerlist],
                exports: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                providers: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent, FlowPartnerService, Globals, { provide: common.APP_BASE_HREF, useValue: '/resellerlist/' }]
            }] }];
        return AppModuleresellerlist;
    }();

    // export * from './detail-pane/detail-pane.component';
    // export * from './main-pane/main-pane.component';
    // export * from './service/flow-partner.service';

    exports.AppModuleresellerlist = AppModuleresellerlist;
    exports.DemoMaterialModule = DemoMaterialModule;
    exports.AppComponentresellerlist = AppComponentresellerlist;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
