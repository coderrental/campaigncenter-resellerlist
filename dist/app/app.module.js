import { CdkTableModule } from '@angular/cdk/table';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatDividerModule, MatExpansionModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule, MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule, } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponentresellerlist, Globals } from './app.component';
import { MainPaneComponent } from './main-pane/main-pane.component';
import { DetailPaneComponent } from './detail-pane/detail-pane.component';
import { FlowPartnerService } from './service/flow-partner.service';
import { DialogsModule } from './dialogs/dialogs.module';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { APP_BASE_HREF } from '@angular/common';
// import { CategoryTreeModule } from "@coderrental/csp-categorytree";
var DemoMaterialModule = /** @class */ (function () {
    function DemoMaterialModule() {
    }
    DemoMaterialModule.decorators = [
        { type: NgModule, args: [{
                    exports: [
                        CdkTableModule,
                        MatAutocompleteModule,
                        MatButtonModule,
                        MatButtonToggleModule,
                        MatCardModule,
                        MatCheckboxModule,
                        MatChipsModule,
                        MatStepperModule,
                        MatDatepickerModule,
                        MatDialogModule,
                        MatDividerModule,
                        MatExpansionModule,
                        MatGridListModule,
                        MatIconModule,
                        MatInputModule,
                        MatListModule,
                        MatMenuModule,
                        MatNativeDateModule,
                        MatPaginatorModule,
                        MatProgressBarModule,
                        MatProgressSpinnerModule,
                        MatRadioModule,
                        MatRippleModule,
                        MatSelectModule,
                        MatSidenavModule,
                        MatSliderModule,
                        MatSlideToggleModule,
                        MatSnackBarModule,
                        MatSortModule,
                        MatTableModule,
                        MatTabsModule,
                        MatToolbarModule,
                        MatTooltipModule
                    ]
                },] },
    ];
    return DemoMaterialModule;
}());
export { DemoMaterialModule };
var AppModuleresellerlist = /** @class */ (function () {
    function AppModuleresellerlist() {
    }
    AppModuleresellerlist.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        BrowserModule,
                        BrowserAnimationsModule,
                        FormsModule,
                        HttpModule,
                        HttpClientModule,
                        DemoMaterialModule,
                        MatNativeDateModule,
                        ReactiveFormsModule,
                        DialogsModule,
                        AppRoutingModule,
                    ],
                    schemas: [
                        CUSTOM_ELEMENTS_SCHEMA
                    ],
                    entryComponents: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                    declarations: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                    bootstrap: [AppComponentresellerlist],
                    exports: [AppComponentresellerlist, MainPaneComponent, DetailPaneComponent],
                    providers: [
                        AppComponentresellerlist, MainPaneComponent, DetailPaneComponent, FlowPartnerService, Globals,
                        { provide: APP_BASE_HREF, useValue: '/resellerlist/' }
                    ],
                },] },
    ];
    return AppModuleresellerlist;
}());
export { AppModuleresellerlist };
//# sourceMappingURL=app.module.js.map