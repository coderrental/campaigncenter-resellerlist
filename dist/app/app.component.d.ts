/**
 * @title Table with editing
 */
export declare class AppComponentresellerlist {
    isShowDetail: boolean;
}
export declare class Globals {
    isEditMode: boolean;
    isFirstEditClicked: boolean;
    project: string;
    mode: string;
    token: string;
}
