import { AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AppComponentresellerlist, Globals } from '../app.component';
import { DetailPaneComponent } from '../detail-pane/detail-pane.component';
import { FlowPartner } from '../service/flow-partner.model';
import { FlowPartnerService } from '../service/flow-partner.service';
import { DialogsService } from '../dialogs/dialogs.service';
/**
 * @title Table retrieving data through HTTP
 */
export declare class MainPaneComponent implements AfterViewInit {
    private http;
    private appcomponent;
    globals: Globals;
    private detailpanecomponent;
    private flowpartnerservice;
    private dialogservice;
    displayedColumns: string[];
    flowDatabase: FlowPartnerService | null;
    dataSource: MatTableDataSource<{}>;
    dataSourceBackUp: MatTableDataSource<{}>;
    updatePartnerInfo: FlowPartner;
    resultsLength: number;
    isLoadingResults: boolean;
    isRateLimitReached: boolean;
    selectedRowIndex: number;
    isTestUser: boolean;
    isViewAll: boolean;
    applyFilter(filterValue: string): void;
    selectRow(row: FlowPartner): void;
    paginator: MatPaginator;
    sort: MatSort;
    constructor(http: HttpClient, appcomponent: AppComponentresellerlist, globals: Globals, detailpanecomponent: DetailPaneComponent, flowpartnerservice: FlowPartnerService, dialogservice: DialogsService);
    editRow(): void;
    ngAfterViewInit(): void;
}
