import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AppComponentresellerlist, Globals } from '../app.component';
import { DetailPaneComponent } from '../detail-pane/detail-pane.component';
import { FlowPartnerService } from '../service/flow-partner.service';
import { DialogsService } from '../dialogs/dialogs.service';
// import { ActivatedRoute } from '@angular/router'
/**
 * @title Table retrieving data through HTTP
 */
var MainPaneComponent = /** @class */ (function () {
    function MainPaneComponent(http, appcomponent, globals, detailpanecomponent, flowpartnerservice, dialogservice) {
        this.http = http;
        this.appcomponent = appcomponent;
        this.globals = globals;
        this.detailpanecomponent = detailpanecomponent;
        this.flowpartnerservice = flowpartnerservice;
        this.dialogservice = dialogservice;
        this.displayedColumns = ['companyname', 'test', 'state', 'country', 'datesubscribed', 'action'];
        this.dataSource = new MatTableDataSource();
        this.dataSourceBackUp = new MatTableDataSource();
        this.resultsLength = 0;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.selectedRowIndex = -1;
        this.isTestUser = false;
        this.isViewAll = true;
    }
    MainPaneComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    MainPaneComponent.prototype.selectRow = function (row) {
        // this.paginator._changePageSize(this.paginator.pageSize);
        if (this.globals.isEditMode) {
            this.detailpanecomponent.closeDetail();
        }
        this.appcomponent.isShowDetail = true;
        this.selectedRowIndex = row.Id;
        // this.flowpartnerservice.setnewPartnerInfo({
        //   Id: row.Id,
        //   CompanyName: row.CompanyName,
        //   Addresss: row.Addresss,
        //   City: row.City,
        //   State: row.State,
        //   Zipcode: row.Zipcode,
        //   Country: row.Country,
        //   WebsiteUrl: row.WebsiteUrl,
        //   DeeplinkUrl: row.DeeplinkUrl,
        //   Latitude: row.Latitude,
        //   Longitude: row.Longitude,
        //   IsConsumer: row.IsConsumer,
        //   IsSupplier: row.IsSupplier,
        //   PartnerType: row.PartnerType,
        //   BaseDomain: row.BaseDomain,
        //   Parameters: row.Parameters
        // });
    };
    MainPaneComponent.prototype.editRow = function () {
        // this.appcomponent.isShowDetail = true;
        // this.globals.isEditMode = true;
    };
    MainPaneComponent.prototype.ngAfterViewInit = function () {
        // this.activatedRoute.queryParams.subscribe(params => {
        //   if (typeof(params['token']) == 'undefined' || params['token'] == undefined
        //       || typeof(params['project']) == 'undefined' || params['project'] == undefined
        //       || typeof(params['mode']) == 'undefined' || params['mode'] == undefined) return;
        //   const requestUrl = `https://sso.tiekinetix.net/connect/accesstokenvalidation/?token=${params['token']}`;
        //   this.globals.project = params['project'];
        //   this.globals.mode = params['mode'];
        //   this.globals.token = params['token'];
        //   this.dataSource.paginator = this.paginator;
        //       this.flowDatabase = new FlowPartnerService(this.http, this.globals);
        //       this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        //       merge(this.sort.sortChange, this.paginator.page).pipe(
        //         // startWith({}),
        //         switchMap(() => {
        //           this.isLoadingResults = true;
        //           return this.flowDatabase!.getRepoPartner();
        //         }), map(data => {
        //           this.isLoadingResults = false;
        //           this.isRateLimitReached = false;
        //           // this.resultsLength = data.total_count;
        //           return data.value;
        //         }), catchError(() => {
        //           this.isLoadingResults = false;
        //           this.isRateLimitReached = true;
        //           return observableOf([]);
        //         })).subscribe(data => {
        //           this.dataSource.data = data;
        //           this.dataSource.data.map(obj => {
        //             obj["Parameters"].sort(function(a,b) {return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0);} );
        //           });
        //       });
        //   this.http.get(requestUrl).subscribe((res: any[]) => {
        //       if (typeof(res["preferred_username"]) == 'undefined' && typeof(res["customer_id"]) == 'undefined') return;
        //   });
        // });
    };
    MainPaneComponent.decorators = [
        { type: Component, args: [{
                    selector: 'main-pane',
                    styles: ["\n\n  "],
                    template: "\n    <div class=\"pane-header \">\n    \t<h1>Manage Partner</h1>\n    \t<div class=\"header-row\" *ngIf=\"dataSource.data.length > 0\">\n    \t\t<mat-form-field>\n    \t\t\t<input matInput #filter (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    \t\t\t<button mat-icon-button matSuffix aria-label=\"clear\" *ngIf=\"filter.value\" (click)=\"filter.value=''; applyFilter('');\">\n              <mat-icon>close</mat-icon>\n            </button>\n    \t\t</mat-form-field>\n    \t\t<div class=\"button-row\">\n    \t\t\t<button mat-button (click)=\"exportAll(true)\"><mat-icon>launch</mat-icon>EXPORT</button>\n    \t\t\t<button mat-button (click)=\"exportAll(false)\"><mat-icon>launch</mat-icon>EXPORT TEST ACOUNTS</button>\n          <button mat-button (click)=\"viewAll(true)\" *ngIf=\"!isViewAll\"><mat-icon>view_headline</mat-icon>VIEW ALL</button>\n    \t\t\t<button mat-button (click)=\"viewAll(false)\" *ngIf=\"isViewAll\"><mat-icon>view_headline</mat-icon>VIEW PROD ACOUNTS</button>\n    \t\t</div>\n    \t</div>\n    </div>\n\n    <div class=\"circle-loader\" *ngIf=\"isLoadingResults\"></div>\n    <p class=\"error-mess animate-show\" *ngIf=\"dataSource.data.length == 0 && !isLoadingResults\"><mat-icon>error</mat-icon>Please check access token</p>\n\n    <mat-table #table [dataSource]=\"dataSource\" matSort matSortActive=\"created\" matSortDisableClear matSortDirection=\"asc\">\n\n        <ng-container matColumnDef=\"companyname\">\n          <mat-header-cell *matHeaderCellDef\n                           mat-sort-header\n                           disableClear=\"true\">\n          Company Name\n          </mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.CompanyName }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"test\">\n          <mat-header-cell *matHeaderCellDef>Test</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\"><mat-icon *ngIf=\"isTestCompany(row)\">check</mat-icon></mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"state\">\n          <mat-header-cell *matHeaderCellDef>State</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.State }}</mat-cell>\n        </ng-container>\n\n         <ng-container matColumnDef=\"country\">\n          <mat-header-cell *matHeaderCellDef>Country</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.Country }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"datesubscribed\">\n          <mat-header-cell *matHeaderCellDef>Date Subscribed</mat-header-cell>\n          <mat-cell *matCellDef=\"let row\">{{ row.datesubscribed }}</mat-cell>\n        </ng-container>\n\n        <ng-container matColumnDef=\"action\">\n    \t\t<mat-header-cell *matHeaderCellDef></mat-header-cell>\n    \t\t<mat-cell *matCellDef=\"let row\">\n    \t\t\t<button mat-icon-button (click)=\"editRow()\">\n              <mat-icon>mode_edit</mat-icon>\n            </button>\n    \t\t</mat-cell>\n    \t</ng-container>\n    \t<mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    \t<mat-row *matRowDef=\"let row; columns: displayedColumns;\" [ngClass]=\"{'highlight': selectedRowIndex == row.Id}\" (click)=\"selectRow(row)\" class=\"animate-show\" ></mat-row>\n    </mat-table>\n\n    <mat-paginator [length]=\"resultsLength\" [pageSize]=\"14\">\n    </mat-paginator>\n  ",
                },] },
    ];
    /** @nocollapse */
    MainPaneComponent.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AppComponentresellerlist },
        { type: Globals },
        { type: DetailPaneComponent },
        { type: FlowPartnerService },
        { type: DialogsService }
    ]; };
    MainPaneComponent.propDecorators = {
        paginator: [{ type: ViewChild, args: [MatPaginator,] }],
        sort: [{ type: ViewChild, args: [MatSort,] }]
    };
    return MainPaneComponent;
}());
export { MainPaneComponent };
//# sourceMappingURL=main-pane.component.js.map