import { OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AppComponentresellerlist, Globals } from '../app.component';
import { FlowPartner } from '../service/flow-partner.model';
import { FlowPartnerService } from '../service/flow-partner.service';
import { MatSnackBar } from '@angular/material';
import { DialogsService } from '../dialogs/dialogs.service';
/**
 * @title Table with editing
 */
export declare class DetailPaneComponent implements OnInit {
    private appcomponent;
    globals: Globals;
    private flowpartnerservice;
    snackBar: MatSnackBar;
    private dialogsService;
    updatePartnerInfo: FlowPartner;
    sourcePartnerInfo: FlowPartner;
    displayedColumns: string[];
    dataSource: MatTableDataSource<{}>;
    dataSourceBackUp: MatTableDataSource<{}>;
    constructor(appcomponent: AppComponentresellerlist, globals: Globals, flowpartnerservice: FlowPartnerService, snackBar: MatSnackBar, dialogsService: DialogsService);
    ngOnInit(): void;
    applyFilter(filterValue: string): void;
    turnEditMode(): void;
    saveDetail(): void;
    resetDetail(): void;
    closeDetail(): void;
    openSnacBar(mess: any): void;
}
