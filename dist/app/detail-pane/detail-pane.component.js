import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AppComponentresellerlist, Globals } from '../app.component';
import { FlowPartnerService } from '../service/flow-partner.service';
import { MatSnackBar } from '@angular/material';
import { DialogsService } from '../dialogs/dialogs.service';
/**
 * @title Table with editing
 */
var DetailPaneComponent = /** @class */ (function () {
    function DetailPaneComponent(appcomponent, globals, flowpartnerservice, snackBar, dialogsService) {
        this.appcomponent = appcomponent;
        this.globals = globals;
        this.flowpartnerservice = flowpartnerservice;
        this.snackBar = snackBar;
        this.dialogsService = dialogsService;
        this.displayedColumns = ['name', 'value'];
        this.dataSource = new MatTableDataSource();
        this.dataSourceBackUp = new MatTableDataSource();
    }
    DetailPaneComponent.prototype.ngOnInit = function () {
        // this.flowpartnerservice.getnewPartnerInfo().subscribe(info => {
        //   this.globals.isFirstEditClicked = false;
        //   this.updatePartnerInfo = info;
        //   this.dataSource = new MatTableDataSource(this.updatePartnerInfo.Parameters);
        // })
    };
    DetailPaneComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    DetailPaneComponent.prototype.turnEditMode = function () {
        if (!this.globals.isFirstEditClicked) {
            this.sourcePartnerInfo = JSON.parse(JSON.stringify(this.updatePartnerInfo));
            this.globals.isFirstEditClicked = true;
        }
        this.globals.isEditMode = true;
    };
    DetailPaneComponent.prototype.saveDetail = function () {
        // this.flowpartnerservice.updatePartner(this.updatePartnerInfo).subscribe(data => {
        //   console.log(data);
        // });
        // this.openSnacBar('Successfully saved');
        // this.globals.isEditMode = false;
    };
    DetailPaneComponent.prototype.resetDetail = function () {
        // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
        // this.globals.isEditMode = false;
        // this.dialogsService
        //   .openDialog("Reset partner's parameters", 'Are you sure want to reset this partner?','Yes','No')
        //   .subscribe(res => {
        //     if (res) {
        //       // bug cannot reset
        //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
        //       // this.globals.isEditMode = false;
        //     }
        //   });
    };
    DetailPaneComponent.prototype.closeDetail = function () {
        // this.globals.isEditMode = false;
        // this.appcomponent.isShowDetail = false;
        // if(this.globals.isFirstEditClicked && this.globals.isEditMode)
        //   this.dialogsService
        //     .openDialog('Discard changes?', 'You have unsaved changes','Save','Discard changes')
        //     .subscribe(res => {
        //       if (res) {
        //         // if save
        //         // bug cannot reset
        //         this.saveDetail();
        //       }
        //       this.globals.isEditMode = false;
        //       this.appcomponent.isShowDetail = false;
        //       // bug cannot reset
        //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
        //     });
        // else {
        //   this.globals.isEditMode = false;
        //   this.appcomponent.isShowDetail = false;
        // }
    };
    DetailPaneComponent.prototype.openSnacBar = function (mess) {
        this.snackBar.open(mess, 'Close', {
            duration: 1000,
            verticalPosition: 'bottom',
            horizontalPosition: 'right'
        });
    };
    DetailPaneComponent.decorators = [
        { type: Component, args: [{
                    selector: 'detail-pane',
                    styles: ["\n\n  "],
                    template: "\n    \t\t<div class=\"pane-header \">\n    \t\t\t<h2>Company name: {{updatePartnerInfo.CompanyName}}</h2>\n    \t\t\t<p>Detail Values</p>\n\n    \t\t\t<div class=\"header-row\">\n    \t\t\t     <mat-form-field>\n    \t\t\t\t\t<input matInput #filter (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n    \t\t\t\t\t<button mat-icon-button matSuffix aria-label=\"clear\" *ngIf=\"filter.value\" (click)=\"filter.value=''; applyFilter('');\">\n    \t\t\t        \t<mat-icon>close</mat-icon>\n    \t\t\t     \t</button>\n    \t\t\t\t</mat-form-field>\n    \t\t\t\t<div class=\"button-row\">\n    \t\t\t        <button mat-button *ngIf=!globals.isEditMode (click)=\"turnEditMode()\"><mat-icon>mode_edit</mat-icon>EDIT</button>\n\n    \t\t\t        <button mat-button *ngIf=globals.isEditMode (click)=\"saveDetail()\"><mat-icon>save</mat-icon>SAVE</button>\n    \t\t\t        <!-- <button mat-button *ngIf=globals.isEditMode (click)=\"resetDetail()\"><mat-icon>restore</mat-icon>RESET</button> -->\n    \t\t\t        <button mat-button (click)=\"closeDetail()\"><mat-icon>close</mat-icon>CLOSE</button>\n    \t\t\t      </div>\n    \t\t\t</div>\n    \t\t</div>\n    \t\t<mat-table #table [dataSource]=\"dataSource\" matSortActive=\"name\" matSortDirection=\"asc\" matSortDisableClear>\n\n    \t\t<!-- Name Column -->\n    \t\t<ng-container matColumnDef=\"name\">\n    \t\t\t<mat-header-cell *matHeaderCellDef> Name </mat-header-cell>\n    \t\t\t<mat-cell *matCellDef=\"let element\"> {{element.Name}} </mat-cell>\n    \t\t</ng-container>\n\n    \t\t<!-- Value Column -->\n    \t\t<ng-container matColumnDef=\"value\">\n    \t\t\t<mat-header-cell *matHeaderCellDef> Value </mat-header-cell>\n    \t\t\t<mat-cell *matCellDef=\"let element\">\n    \t\t\t\t<mat-form-field floatLabel=\"never\">\n    \t\t\t\t\t<input matInput placeholder=\"\" [value]=\"element.Value\" [(ngModel)]=\"element.Value\" [disabled]=\"!globals.isEditMode\">\n    \t\t\t\t</mat-form-field>\n    \t\t\t</mat-cell>\n    \t\t</ng-container>\n\n    \t\t<mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\n    \t\t<mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\n    \t</mat-table>\n  ",
                },] },
    ];
    /** @nocollapse */
    DetailPaneComponent.ctorParameters = function () { return [
        { type: AppComponentresellerlist },
        { type: Globals },
        { type: FlowPartnerService },
        { type: MatSnackBar },
        { type: DialogsService }
    ]; };
    return DetailPaneComponent;
}());
export { DetailPaneComponent };
/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */ 
//# sourceMappingURL=detail-pane.component.js.map