import { MatDialogRef } from '@angular/material';
export declare class ConfirmDialogComponent {
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    title: string;
    message: string;
    btnTitle1: string;
    btnTitle2: string;
    constructor(dialogRef: MatDialogRef<ConfirmDialogComponent>);
}
