import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog.component';
import { DialogsService } from './dialogs.service';
import { MatButtonModule, MatDialogModule } from '@angular/material';
var DialogsModule = /** @class */ (function () {
    function DialogsModule() {
    }
    DialogsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MatDialogModule,
                        MatButtonModule,
                    ],
                    declarations: [ConfirmDialogComponent],
                    exports: [ConfirmDialogComponent],
                    entryComponents: [ConfirmDialogComponent],
                    providers: [DialogsService]
                },] },
    ];
    return DialogsModule;
}());
export { DialogsModule };
//# sourceMappingURL=dialogs.module.js.map