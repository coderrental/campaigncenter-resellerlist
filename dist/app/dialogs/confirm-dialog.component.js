import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
var ConfirmDialogComponent = /** @class */ (function () {
    function ConfirmDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ConfirmDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-confirm-dialog',
                    template: "\n\t\t<h2 mat-dialog-title>{{ title }}</h2>\n\t\t<mat-dialog-content>{{ message }}</mat-dialog-content>\n\t\t<br>\n\t\t<mat-dialog-actions>\n\t\t    <button type=\"button\" mat-button (click)=\"dialogRef.close(true)\">{{ btnTitle1 }}</button>\n\t\t    <button type=\"button\" mat-button mat-dialog-close (click)=\"dialogRef.close(false)\">{{ btnTitle2 }}</button>\n\t\t</mat-dialog-actions>\n  "
                },] },
    ];
    /** @nocollapse */
    ConfirmDialogComponent.ctorParameters = function () { return [
        { type: MatDialogRef }
    ]; };
    return ConfirmDialogComponent;
}());
export { ConfirmDialogComponent };
//# sourceMappingURL=confirm-dialog.component.js.map