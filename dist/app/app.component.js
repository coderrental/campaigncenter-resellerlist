import { Component, Injectable } from '@angular/core';
/**
 * @title Table with editing
 */
var AppComponentresellerlist = /** @class */ (function () {
    function AppComponentresellerlist() {
        this.isShowDetail = false;
    }
    AppComponentresellerlist.decorators = [
        { type: Component, args: [{
                    selector: 'my-appresellerlist',
                    styles: ["\n\n  "],
                    template: "\n    <mat-sidenav-container class=\"animate-show\">\n    \t<mat-sidenav-content>\n    \t<main-pane class=\"pane\"></main-pane>\n      </mat-sidenav-content>\n       <!-- <mat-sidenav #sidenav mode=\"side\" align=\"end\" [(opened)]=\"isShowDetail\">\n        <detail-pane class=\"pane\"></detail-pane>\n      </mat-sidenav>  -->\n    </mat-sidenav-container>\n    <!-- <app-categorytree>loading...sdsdsds</app-categorytree> -->\n  ",
                },] },
    ];
    return AppComponentresellerlist;
}());
export { AppComponentresellerlist };
var Globals = /** @class */ (function () {
    function Globals() {
        this.isEditMode = false;
        this.isFirstEditClicked = false;
        this.project = null;
        this.mode = null;
        this.token = '';
    }
    Globals.decorators = [
        { type: Injectable },
    ];
    return Globals;
}());
export { Globals };
//# sourceMappingURL=app.component.js.map