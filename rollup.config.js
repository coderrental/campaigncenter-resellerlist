import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import postcss from 'rollup-plugin-postcss';
import postcssModules from 'postcss-modules';
import autoprefixer from 'autoprefixer';
import sass from 'node-sass';
import autoExternal from 'rollup-plugin-auto-external';
const preprocessor = (content, id) => new Promise((resolve, reject) => {
    const result = sass.renderSync({ file: id });
    resolve({ code: result.css.toString() });
});
const rollupGlobals = {
    '@angular/core': 'ng.core',
    '@angular/platform-browser': 'ng.platformBrowser',
    '@angular/common': 'ng.common',
    '@angular/forms': 'ng.forms',
    '@angular/material': 'ng.material',
    '@angular/animations': 'ng.animations',
    '@angular/router': 'ng.router',
    'rxjs': 'ng.rxjs',
    'angular-tree-component': 'ng.angulartreecomponent',
    'rxjs/Observable': 'Rx.Observable',
    'rxjs/Subject': 'Rx.Subject',

    'typescript': 'ts'
}


export default {

    input: 'dist/index.js',
    output: {
        file: 'dist/bundles/resellerlist.umd.js',
        format: 'umd',
        name: 'resellerlist',
        globals: rollupGlobals
    },
    external: [
        // '@angular/core',
        // '@angular/forms',
        // 'rxjs'
    ],
    plugins: [
        autoExternal(),
        // enables us to resolve packages located in node_modules
        resolve(),
        // converts CommonJS modules into a format Rollup understands
        commonjs({}),
     
        // compile CSS Modules
        postcss({
            preprocessor,
            plugins: [
                autoprefixer({ browsers: 'last 2 versions' }),
                postcssModules({
                    getJSON(id, exportTokens) {
                        cssExportMap[id] = exportTokens;
                    }
                })
            ],
            getExport(id) {
                return cssExportMap[id];
            },
            extensions: ['.scss'],
            extract: true
        }),
        babel({
            exclude: 'node_modules/**',
        }),
        // eslint({
        //     exclude: [
        //         'src/styles/**',
        //     ]
        // }),
        // transform ES6 code
        // babel({
        //     exclude: 'node_modules/**',
        //     presets: [
        //         'react',
        //         ['es2015', { modules: false }]
        //     ],
        //     plugins: ['external-helpers']
        // })
    ],
    // dest: 'dist/bundle.js'

};
// import path from 'path'
// import babel from 'rollup-plugin-babel';
// import eslint from 'rollup-plugin-eslint';
// import resolve from 'rollup-plugin-node-resolve';
// import commonjs from 'rollup-plugin-commonjs';
// import replace from 'rollup-plugin-replace';
// import uglify from 'rollup-plugin-uglify';
// import postcss from 'rollup-plugin-postcss';
// // import postcss from 'rollup-plugin-postcss-modules';
// import postcssModules from 'postcss-modules';

// import autoprefixer from 'autoprefixer'

// export default {
//     entry: 'dist/index.js',
//     format: 'umd',
//     // format: 'iife',
//     moduleName: 'resellerlist',
//     dest: 'dist/bundles/resellerlist.umd.js',
//     sourceMap: 'true',
//     plugins: [
//         // eslint({
//         //     exclude: [
//         //         'src/styles/**',
//         //     ]
//         // }),

//         postcss({
//             preprocessor: (content, id) => new Promise((resolve, reject) => {
//                 const result = sass.renderSync({ file: id })
//                 resolve({ code: result.css.toString() })
//             }),

//             extract: true,
//             sourceMap: true,
//             extensions: ['.sass', '.css'],
//             plugins: [
//                 autoprefixer(),
//                 postcssModules({
//                     generateScopedName: '[name]__[local]___[hash:base64:5]',
//                     getJSON(id, exportTokens) {
//                         cssExportMap[id] = exportTokens
//                     }
//                 })
//             ],
//             writeDefinitions: true,
//             // modules: { ... }
//         }),
//         resolve({
//             jsnext: true,
//             main: true,
//             browser: true,
//         }),
//         commonjs(),
//         babel({
//             exclude: 'node_modules/**',
//         }),


//         replace({
//             ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
//         }),
//         (process.env.NODE_ENV === 'production' && uglify()),
//     ],
// }

// export default {
//     entry: 'dist/index.js',
//     // entry: 'src/main.ts',
//     dest: 'dist/bundles/resellerlist.umd.js',
//     sourceMap: false,
//     format: 'umd',
//     // format: 'iife',
//     moduleName: 'resellerlist',
//     globals: {
//         '@angular/core': 'ng.core',
//     },

// }

