export interface FlowApi {
  value: FlowPartner[];
}
export interface FlowPartner {
  Id : number;
  CompanyName : string;
  Addresss : string;
  City : string;
  State : string;
  Zipcode : string;
  Country : string;
  WebsiteUrl : string;
  DeeplinkUrl : string;
  Latitude : string;
  Longitude : string;
  IsConsumer : boolean;
  IsSupplier : boolean;
  PartnerType : string;
  BaseDomain : string;
  Parameters : AccountParameter[];
}
export interface AccountParameter {
  Name: string;
  Value: string;
}
export interface UserInfo {
  Id: string;
  AccountId: string;
  UserName: string;
  FirstName: string;
  LastName: string;
  JobTitle: string;
  PhoneNumber: string;
  SyndicationLanguageId: string;
}
export interface Token {
  client_id: string;
}