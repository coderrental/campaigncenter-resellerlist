import { Component, Injectable, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
/**
 * @title Table with editing
 */
@Component({
  selector: 'my-appresellerlist',
  styleUrls: ['./app.component.css'],
  templateUrl: 'app.component.html',
})
export class AppComponentresellerlist {
 
	isShowDetail: boolean = false;
}
@Injectable()
export class Globals {
  isEditMode: boolean = false;
  isFirstEditClicked: boolean = false;
  project: string = null;
  mode: string = null;
  token: string = '';
}