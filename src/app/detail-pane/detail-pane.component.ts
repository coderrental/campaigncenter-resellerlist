import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AppComponentresellerlist,Globals } from '../app.component';
import { FlowApi,FlowPartner,AccountParameter } from '../service/flow-partner.model';
import { FlowPartnerService } from '../service/flow-partner.service';
import { MatSnackBar } from '@angular/material';
import { DialogsService } from '../dialogs/dialogs.service';
/**
 * @title Table with editing
 */
@Component({
  selector: 'detail-pane',
  styleUrls: [],
  templateUrl: 'detail-pane.component.html',
})
export class DetailPaneComponent implements OnInit {
  // isEditMode: boolean = false;
  // isFirstEditClicked: boolean = false;
  updatePartnerInfo: FlowPartner;
  sourcePartnerInfo: FlowPartner;
  displayedColumns = ['name', 'value'];
  dataSource = new MatTableDataSource();
  dataSourceBackUp = new MatTableDataSource();

  constructor(
    private appcomponent: AppComponentresellerlist,
    public globals: Globals,
    private flowpartnerservice:FlowPartnerService,
    public snackBar: MatSnackBar,
    private dialogsService: DialogsService,
  ) { }
  ngOnInit() {
    // this.flowpartnerservice.getnewPartnerInfo().subscribe(info => {
    //   this.globals.isFirstEditClicked = false;
    //   this.updatePartnerInfo = info;
    //   this.dataSource = new MatTableDataSource(this.updatePartnerInfo.Parameters);
    // })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  turnEditMode() {
    if (!this.globals.isFirstEditClicked) {
      this.sourcePartnerInfo = JSON.parse(JSON.stringify(this.updatePartnerInfo));
      this.globals.isFirstEditClicked = true;
    }
    this.globals.isEditMode = true;
  }
  saveDetail() {
    // this.flowpartnerservice.updatePartner(this.updatePartnerInfo).subscribe(data => {
    //   console.log(data);
    // });
    // this.openSnacBar('Successfully saved');
    // this.globals.isEditMode = false;
  }
  resetDetail() {
    // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
    // this.globals.isEditMode = false;
    // this.dialogsService
    //   .openDialog("Reset partner's parameters", 'Are you sure want to reset this partner?','Yes','No')
    //   .subscribe(res => {
    //     if (res) {
    //       // bug cannot reset
    //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
    //       // this.globals.isEditMode = false;
    //     }
    //   });
  }
  closeDetail() {
    // this.globals.isEditMode = false;
    // this.appcomponent.isShowDetail = false;
    // if(this.globals.isFirstEditClicked && this.globals.isEditMode)
    //   this.dialogsService
    //     .openDialog('Discard changes?', 'You have unsaved changes','Save','Discard changes')
    //     .subscribe(res => {
    //       if (res) {
    //         // if save
    //         // bug cannot reset
    //         this.saveDetail();
    //       }
    //       this.globals.isEditMode = false;
    //       this.appcomponent.isShowDetail = false;
    //       // bug cannot reset
    //       // this.flowpartnerservice.setnewPartnerInfo(this.sourcePartnerInfo);
    //     });
    // else {
    //   this.globals.isEditMode = false;
    //   this.appcomponent.isShowDetail = false;
    // }
  }
  openSnacBar(mess : any){
    this.snackBar.open(mess, 'Close', {
      duration: 1000,
      verticalPosition: 'bottom',
      horizontalPosition: 'right'
    });
  }
}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */