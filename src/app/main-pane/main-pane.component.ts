import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { merge } from 'rxjs/observable/merge';
import { of as observableOf } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators/catchError';
import { map } from 'rxjs/operators/map';
import { startWith } from 'rxjs/operators/startWith';
import { switchMap } from 'rxjs/operators/switchMap';
import { AppComponentresellerlist, Globals } from '../app.component';
import { DetailPaneComponent } from '../detail-pane/detail-pane.component';
import { FlowApi, FlowPartner } from '../service/flow-partner.model';
import { FlowPartnerService } from '../service/flow-partner.service';
import { DialogsService } from '../dialogs/dialogs.service';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
// import { ActivatedRoute } from '@angular/router'

/**
 * @title Table retrieving data through HTTP
 */
@Component({
  selector: 'main-pane',
  styleUrls: ['main-pane.component.css'],
  templateUrl: 'main-pane.component.html',
})

export class MainPaneComponent implements AfterViewInit {
  displayedColumns = ['companyname', 'test', 'state', 'country', 'datesubscribed', 'action'];
  flowDatabase: FlowPartnerService | null;
  dataSource = new MatTableDataSource();
  dataSourceBackUp = new MatTableDataSource();
  updatePartnerInfo: FlowPartner;
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;
  selectedRowIndex: number = -1;
  isTestUser: boolean = false;
  isViewAll: boolean = true;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  selectRow(row: FlowPartner) {
    // this.paginator._changePageSize(this.paginator.pageSize);
    if (this.globals.isEditMode) {
      this.detailpanecomponent.closeDetail();
    }
    this.appcomponent.isShowDetail = true;
    this.selectedRowIndex = row.Id;
    // this.flowpartnerservice.setnewPartnerInfo({
    //   Id: row.Id,
    //   CompanyName: row.CompanyName,
    //   Addresss: row.Addresss,
    //   City: row.City,
    //   State: row.State,
    //   Zipcode: row.Zipcode,
    //   Country: row.Country,
    //   WebsiteUrl: row.WebsiteUrl,
    //   DeeplinkUrl: row.DeeplinkUrl,
    //   Latitude: row.Latitude,
    //   Longitude: row.Longitude,
    //   IsConsumer: row.IsConsumer,
    //   IsSupplier: row.IsSupplier,
    //   PartnerType: row.PartnerType,
    //   BaseDomain: row.BaseDomain,
    //   Parameters: row.Parameters
    // });
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private http: HttpClient,
    private appcomponent: AppComponentresellerlist,
    public globals: Globals,
    private detailpanecomponent: DetailPaneComponent,
    private flowpartnerservice: FlowPartnerService,
    private dialogservice: DialogsService,
    // private activatedRoute: ActivatedRoute
  ) { }
  editRow() {
    // this.appcomponent.isShowDetail = true;
    // this.globals.isEditMode = true;
  }

  ngAfterViewInit() {
    // this.activatedRoute.queryParams.subscribe(params => {
    //   if (typeof(params['token']) == 'undefined' || params['token'] == undefined
    //       || typeof(params['project']) == 'undefined' || params['project'] == undefined
    //       || typeof(params['mode']) == 'undefined' || params['mode'] == undefined) return;
    //   const requestUrl = `https://sso.tiekinetix.net/connect/accesstokenvalidation/?token=${params['token']}`;

    //   this.globals.project = params['project'];
    //   this.globals.mode = params['mode'];
    //   this.globals.token = params['token'];

    //   this.dataSource.paginator = this.paginator;
    //       this.flowDatabase = new FlowPartnerService(this.http, this.globals);
    //       this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    //       merge(this.sort.sortChange, this.paginator.page).pipe(
    //         // startWith({}),
    //         switchMap(() => {
    //           this.isLoadingResults = true;
    //           return this.flowDatabase!.getRepoPartner();
    //         }), map(data => {
    //           this.isLoadingResults = false;
    //           this.isRateLimitReached = false;
    //           // this.resultsLength = data.total_count;
    //           return data.value;
    //         }), catchError(() => {
    //           this.isLoadingResults = false;
    //           this.isRateLimitReached = true;
    //           return observableOf([]);
    //         })).subscribe(data => {
    //           this.dataSource.data = data;
    //           this.dataSource.data.map(obj => {
    //             obj["Parameters"].sort(function(a,b) {return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0);} );
    //           });
    //       });

    //   this.http.get(requestUrl).subscribe((res: any[]) => {
    //       if (typeof(res["preferred_username"]) == 'undefined' && typeof(res["customer_id"]) == 'undefined') return;
          
    //   });
    // });
  }
  // isTestCompany(rowItem: any): boolean {
  //   if (rowItem.CompanyName.indexOf('test_') != -1)
  //     return true;
  //   var consumerTypeParam = rowItem.Parameters.filter(x => x.Name == 'ConsumerType');
  //   if (consumerTypeParam.length > 0) {
  //     if (consumerTypeParam[0].Value.toLowerCase() == 't')
  //       return true;
  //   }
  //   return false;
  // }
  // exportAll(all: boolean) {
  //   // console.log(this.dataSource.data);
  //   var fileContent: Array<Object> = [];
  //   var fileName = "CspContentExportAll";
  //   var fileConfig : any = {
  //     headers: []
  //   };
  //   this.dataSource.data.map((obj, i) => {
  //     var objCsv: any = {};
  //     for (var key in obj) {
  //       if (key != "Parameters" && key != "UserInfo") {
  //         if (i == 0)
  //           fileConfig.headers.push(key);
  //         objCsv[key] = obj[key];
  //       }
  //     }
  //     for (var key in obj["UserInfo"]) {
  //       if (i == 0) {
  //         if (key == "Id")
  //            fileConfig.headers.push("UserInfo.Id");
  //         else
  //         fileConfig.headers.push(key);
  //       }
  //       if (key == "Id")
  //         objCsv["UserInfo.Id"] = obj["UserInfo"][key];
  //       else
  //         objCsv[key] = obj["UserInfo"][key];
  //     }
  //     for (var key in obj["Parameters"]) {
  //       if (i == 0)
  //         fileConfig.headers.push(obj["Parameters"][key].Name);
  //       objCsv[obj["Parameters"][key].Name] = obj["Parameters"][key].Value;
  //     }
  //     fileContent.push(objCsv);
  //   });
  //   // console.log(objCsv);
  //   if (!all) {
  //     // only test accounts
  //     fileName = "CspContentExportTest";
  //     fileContent.map(obj => {
  //       if (obj["CompanyName"].indexOf('test_') == -1 && obj["ConsumerType"] != "T" && obj["ConsumerType"] != "t") {
  //         fileContent = fileContent.filter(o => o != obj);
  //       }
  //     });
  //   }
  //   // console.log(fileContent);
  //   // console.log(fileConfig.headers);
  //   new Angular5Csv(fileContent, fileName, fileConfig);
  // }
  // viewAll(all: boolean) {
  //   this.appcomponent.isShowDetail = false;
  //   this.isViewAll = !this.isViewAll;
  //   if(!all) {
  //     this.dataSourceBackUp.data = this.dataSource.data;
  //     this.dataSource.data.map(row => {
  //       if(this.isTestCompany(row)){
  //         // console.log(row);
  //         this.dataSource.data = this.dataSource.data.filter(r => r != row);
  //       }
  //     });
  //   }
  //   else
  //     this.dataSource.data = this.dataSourceBackUp.data;
  // }
}